<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessor\FieldOutputHandler;

use Civi\DataProcessor\DataSpecification\FieldSpecification;
use Civi\DataProcessor\Exception\DataSourceNotFoundException;
use Civi\DataProcessor\Exception\FieldNotFoundException;
use Civi\DataProcessor\Source\SourceInterface;

use CRM_DataprocessorDuplicatecontacts_ExtensionUtil as E;

class MarkContactsAsNonDedupeLink extends AbstractFieldOutputHandler {

  /**
   * @var FieldSpecification
   */
  protected $outputFieldSpecification;

  /**
   * @var SourceInterface
   */
  protected $contactId1Source;

  /**
   * @var FieldSpecification
   */
  protected $contactId1Field;

  /**
   * @var SourceInterface
   */
  protected $contactId2Source;

  /**
   * @var FieldSpecification
   */
  protected $contactId2Field;

  /**
   * @return \Civi\DataProcessor\DataSpecification\FieldSpecification
   */
  public function getOutputFieldSpecification() {
    return $this->outputFieldSpecification;
  }

  /**
   * Returns the data type of this field
   *
   * @return String
   */
  protected function getType() {
    return 'String';
  }

  /**
   * Initialize the processor
   *
   * @param String $alias
   * @param String $title
   * @param array $configuration
   * @param \Civi\DataProcessor\ProcessorType\AbstractProcessorType $processorType
   */
  public function initialize($alias, $title, $configuration) {
    $this->outputFieldSpecification = new FieldSpecification($alias, 'String', $title, null, $alias);

    $this->contactId1Source = $this->dataProcessor->getDataSourceByName($configuration['contact_id_1_datasource']);
    if (!$this->contactId1Source) {
      throw new DataSourceNotFoundException(E::ts("Field %1 requires data source '%2' which could not be found. Did you rename or deleted the data source?", array(
          1=>$title,
          2=>$configuration['contact_id_1_datasource'])
      ));
    }
    $this->contactId1Field = $this->contactId1Source->getAvailableFields()->getFieldSpecificationByAlias($configuration['contact_id_1_field']);
    if (!$this->contactId1Field) {
      $this->contactId1Field = $this->contactId1Source->getAvailableFields()->getFieldSpecificationByName($configuration['contact_id_1_field']);
    }
    if (!$this->contactId1Field) {
      throw new FieldNotFoundException(E::ts("Field %1 requires a field with the name '%2' in the data source '%3'. Did you change the data source type?", array(
        1 => $title,
        2 => $configuration['contact_id_1_field'],
        3 => $configuration['contact_id_1_datasource']
      )));
    }
    $this->contactId1Source->ensureFieldInSource($this->contactId1Field);

    $this->contactId2Source = $this->dataProcessor->getDataSourceByName($configuration['contact_id_2_datasource']);
    if (!$this->contactId2Source) {
      throw new DataSourceNotFoundException(E::ts("Field %1 requires data source '%2' which could not be found. Did you rename or deleted the data source?", array(
          1=>$title,
          2=>$configuration['contact_id_2_datasource'])
      ));
    }
    $this->contactId2Field = $this->contactId2Source->getAvailableFields()->getFieldSpecificationByAlias($configuration['contact_id_2_field']);
    if (!$this->contactId2Field) {
      $this->contactId2Field = $this->contactId2Source->getAvailableFields()->getFieldSpecificationByName($configuration['contact_id_2_field']);
    }
    if (!$this->contactId2Field) {
      throw new FieldNotFoundException(E::ts("Field %1 requires a field with the name '%2' in the data source '%3'. Did you change the data source type?", array(
        1 => $title,
        2 => $configuration['contact_id_2_field'],
        3 => $configuration['contact_id_2_datasource']
      )));
    }
    $this->contactId2Source->ensureFieldInSource($this->contactId2Field);
  }

  /**
   * Returns the formatted value
   *
   * @param $rawRecord
   * @param $formattedRecord
   *
   * @return \Civi\DataProcessor\FieldOutputHandler\FieldOutput
   */
  public function formatField($rawRecord, $formattedRecord) {
    $contactId1 = $rawRecord[$this->contactId1Field->alias];
    $contactId2 = $rawRecord[$this->contactId2Field->alias];
    $cid1 = $contactId1;
    $cid2= $contactId2;
    if ($contactId2 < $contactId1) {
      $cid1 = $contactId2;
      $cid2 = $contactId1;
    }

    $id = rand().'_'.$cid1.'_'.$cid2;

    $smarty = \CRM_Core_Smarty::singleton();
    $data = array(
      'id' => $id,
      'cid1' => $cid1,
      'cid2' => $cid2,
      'link_text' => E::ts('Not a duplicate'),
      'confirm_text' => E::ts('Are you sure you want to mark this pair of contacts as NOT duplicates?'),
      'marked_text' => E::ts('Not a duplicate'),
    );
    $smarty->pushScope($data);
    $html = $smarty->fetch('CRM/Dataprocessor/Field/MarkContactsAsNonDedupeLink.tpl');
    $smarty->popScope();

    $formattedValue = new HTMLFieldOutput($contactId1);
    $formattedValue->formattedValue = '';
    $formattedValue->setHtmlOutput($html);
    return $formattedValue;
  }

  /**
   * Returns true when this handler has additional configuration.
   *
   * @return bool
   */
  public function hasConfiguration() {
    return true;
  }

  /**
   * When this handler has additional configuration you can add
   * the fields on the form with this function.
   *
   * @param \CRM_Core_Form $form
   * @param array $field
   */
  public function buildConfigurationForm(\CRM_Core_Form $form, $field=array()) {
    $fieldSelect = \CRM_Dataprocessor_Utils_DataSourceFields::getAvailableFieldsInDataSources($field['data_processor_id']);

    $form->add('select', 'contact_id_1_field', E::ts('Contact ID 1 Field'), $fieldSelect, true, array(
      'style' => 'min-width:250px',
      'class' => 'crm-select2 huge',
      'placeholder' => E::ts('- select -'),
    ));
    $form->add('select', 'contact_id_2_field', E::ts('Contact ID 2 Field'), $fieldSelect, true, array(
      'style' => 'min-width:250px',
      'class' => 'crm-select2 huge',
      'placeholder' => E::ts('- select -'),
    ));
    if (isset($field['configuration'])) {
      $configuration = $field['configuration'];
      $defaults = array();
      if (isset($configuration['contact_id_1_field']) && isset($configuration['contact_id_1_datasource'])) {
        $defaults['contact_id_1_field'] = $configuration['contact_id_1_datasource'] . '::' . $configuration['contact_id_1_field'];
      }
      if (isset($configuration['contact_id_2_field']) && isset($configuration['contact_id_2_datasource'])) {
        $defaults['contact_id_2_field'] = $configuration['contact_id_2_datasource'] . '::' . $configuration['contact_id_2_field'];
      }
      $form->setDefaults($defaults);
    }
  }

  /**
   * When this handler has configuration specify the template file name
   * for the configuration form.
   *
   * @return false|string
   */
  public function getConfigurationTemplateFileName() {
    return "CRM/Dataprocessor/Form/Field/Configuration/MarkContactsAsNonDedupeLinkFieldOutputHandler.tpl";
  }


  /**
   * Process the submitted values and create a configuration array
   *
   * @param $submittedValues
   * @return array
   */
  public function processConfiguration($submittedValues) {
    list($contact_id_1_datasource, $contact_id_1_field) = explode('::', $submittedValues['contact_id_1_field'], 2);
    $configuration['contact_id_1_field'] = $contact_id_1_field;
    $configuration['contact_id_1_datasource'] = $contact_id_1_datasource;
    list($contact_id_2_datasource, $contact_id_2_field) = explode('::', $submittedValues['contact_id_2_field'], 2);
    $configuration['contact_id_2_field'] = $contact_id_2_field;
    $configuration['contact_id_2_datasource'] = $contact_id_2_datasource;
    return $configuration;
  }

}
