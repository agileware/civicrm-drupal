<?php

require_once 'dataprocessor_duplicatecontacts.civix.php';
use CRM_DataprocessorDuplicatecontacts_ExtensionUtil as E;

use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Implements hook_civicrm_container()
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_container/
 */
function dataprocessor_duplicatecontacts_civicrm_container(ContainerBuilder $container) {
  $container->addCompilerPass(new Civi\DataProcessor\CompilerPass\DuplicateContacts());
}

/**
 * Implementation of hook_civicrm_pageRun()
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_pageRun/
 */
function dataprocessor_duplicatecontacts_civicrm_pageRun(&$page) {
  if ($page instanceof CRM_Admin_Page_Extensions) {
    $unmet = CRM_DataprocessorDuplicatecontacts_Upgrader::checkExtensionDependencies();
    CRM_DataprocessorDuplicatecontacts_Upgrader::displayDependencyErrors($unmet);
  }
}

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function dataprocessor_duplicatecontacts_civicrm_config(&$config) {
  _dataprocessor_duplicatecontacts_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function dataprocessor_duplicatecontacts_civicrm_install() {
  _dataprocessor_duplicatecontacts_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function dataprocessor_duplicatecontacts_civicrm_enable() {
  _dataprocessor_duplicatecontacts_civix_civicrm_enable();
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 *

 // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 *
function dataprocessor_duplicatecontacts_civicrm_navigationMenu(&$menu) {
  _dataprocessor_duplicatecontacts_civix_insert_navigation_menu($menu, 'Mailings', array(
    'label' => E::ts('New subliminal message'),
    'name' => 'mailing_subliminal_message',
    'url' => 'civicrm/mailing/subliminal',
    'permission' => 'access CiviMail',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _dataprocessor_duplicatecontacts_civix_navigationMenu($menu);
} // */
