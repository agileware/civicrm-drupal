<?php

require_once 'eventredirect.civix.php';
use CRM_Eventredirect_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/ 
 */
function eventredirect_civicrm_config(&$config) {
  _eventredirect_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function eventredirect_civicrm_xmlMenu(&$files) {
  _eventredirect_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function eventredirect_civicrm_install() {
  _eventredirect_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function eventredirect_civicrm_postInstall() {
  _eventredirect_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function eventredirect_civicrm_uninstall() {
  _eventredirect_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function eventredirect_civicrm_enable() {
  _eventredirect_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function eventredirect_civicrm_disable() {
  _eventredirect_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function eventredirect_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _eventredirect_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function eventredirect_civicrm_managed(&$entities) {
  _eventredirect_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function eventredirect_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _eventredirect_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_pageRun().
 *
 * Redirect the Event Info page if the event has a redirect URL.
 */
function eventredirect_civicrm_pageRun(&$page) {
  $pageName = get_class($page);

  if ($pageName == 'CRM_Event_Page_EventInfo') {
    $event_id = $page->getVar('_id');
    CRM_Eventredirect_Utils::redirectIfEnabled($event_id);
  }
}

/**
 * Implements hook_civicrm_buildForm().
 *
 * Redirect the Event Registration page if the event has a redirect URL.
 */
function eventredirect_civicrm_buildForm($formName, &$form) {
  if ($formName == 'CRM_Event_Form_Registration_Register') {
    $event_id = $form->_eventId;
    CRM_Eventredirect_Utils::redirectIfEnabled($event_id);
  }
}
