<?php

use CRM_Eventredirect_ExtensionUtil as E;

/**
 * Collection of upgrade steps.
 */
class CRM_Eventredirect_Upgrader extends CRM_Eventredirect_Upgrader_Base {

  // By convention, functions that look like "function upgrade_NNNN()" are
  // upgrade tasks. They are executed in order (like Drupal's hook_update_N).

  /**
   * Create the custom field for our extension.
   */
  public function install() {
    // Get the highest option value ID for Event Types
    // oddly not working via the API when we have values from 1-15 (returns 9).

    $gid = civicrm_api3('OptionGroup', 'getsingle', [
      'name' => 'event_type',
    ])['id'];

    $value = CRM_Core_DAO::singleValueQuery('SELECT max(cast(value as unsigned int)) + 1 FROM civicrm_option_value WHERE option_group_id = %1', [
      1 => [$gid, 'Positive'],
    ]);

    civicrm_api3('OptionValue', 'create', [
      'option_group_id' => 'event_type',
      'label' => E::ts('External Redirect'),
      'value' => $value,
      'name' => 'external_redirect',
      'filter' => 0,
      'is_default' => 0,
      'weight' => 16,
      'description' => E::ts('Events hosted on other websites. Viewing the info or registration page will redirect to the URL of that event.'),
      'is_optgroup' => 0,
      'is_reserved' => 1,
      'is_active' => 1,
    ]);

    $result = civicrm_api3('CustomGroup', 'create', [
      'name' => 'event_redirect',
      'title' => 'Event Redirect',
      'extends' => 'Event',
      'extends_entity_column_value' => [
        $value,
      ],
      'style' => 'Inline',
      'collapse_display' => 0,
      'weight' => 20,
      'is_active' => 1,
      'table_name' => 'civicrm_value_event_redir',
      'is_multiple' => 0,
      'collapse_adv_display' => 1,
      'is_reserved' => 1,
      'is_public' => 0,
    ]);

    civicrm_api3('CustomField', 'create', [
      'custom_group_id' => $result['id'],
      'name' => 'redirect_url',
      'label' => E::ts('Redirect URL'),
      'data_type' => 'Link',
      'html_type' => 'Link',
      'is_required' => 1,
      'is_search_range' => 0,
      'weight' => 1,
      'help_pre' => '',
      'help_post' => E::ts('Enter a full URL (https://..) to the external event page. Visitors will automatically be redirected to this page.'),
      'is_active' => 1,
      'text_length' => 255,
      'note_columns' => 60,
      'note_rows' => 4,
      'column_name' => 'redirect_url',
      'in_selector' => 0,
    ]);
  }

  /**
   * Delete the custom field.
   */
  public function uninstall() {
    $custom_group_id = civicrm_api3('CustomGroup', 'getsingle', [
      'name' => 'event_redirect',
    ])['id'];

    $custom_field_id = civicrm_api3('CustomField', 'getsingle', [
      'name' => 'redirect_url',
      'custom_group_id' => $custom_group_id,
    ])['id'];

    civicrm_api3('CustomField', 'delete', [
      'id' => $custom_field_id,
    ]);

    civicrm_api3('CustomGroup', 'delete', [
      'id' => $custom_group_id,
    ]);

    $gid = civicrm_api3('OptionGroup', 'getsingle', [
      'name' => 'event_type',
    ])['id'];

    $oid = civicrm_api3('OptionValue', 'get', [
      'option_group_id' => $gid,
      'name' => 'external_redirect',
    ]);

    civicrm_api3('OptionValue', 'delete', [
      'id' => $oid,
    ]);
  }

}
