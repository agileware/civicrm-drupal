<?php

class CRM_Eventredirect_Utils {

  public static function redirectIfEnabled($event_id) {
    $event = \Civi\Api4\Event::get(FALSE)
      ->setSelect(['event_redirect.redirect_url'])
      ->addWhere('id', '=', $event_id)
      ->execute()
      ->first();

    if (!empty($event['event_redirect.redirect_url'])) {
      CRM_Utils_System::redirect($event['event_redirect.redirect_url']);
    }
  }

}
