<?php
namespace Civi\Api4;

use Civi\Api4\Action\CachedMailingStats\GetAction;

/**
 * CachedMailingStats entity.
 *
 * Provided by the Mailing Tracker extension.
 *
 * @package Civi\Api4
 */
class CachedMailingStats extends Generic\DAOEntity {

  /**
   * @param bool $checkPermissions
   * @return GetAction
   */
  public static function get($checkPermissions = TRUE) {
    return (new GetAction(static::class, __FUNCTION__))
      ->setCheckPermissions($checkPermissions);
  }

}
