<?php
namespace Civi\Api4\Action\CachedMailingStats;

use Civi\Api4\Generic\DAOGetAction;
use Civi\Api4\Generic\Result;

/**
 * Retrieve $ENTITIES based on criteria specified in the `where` parameter.
 *
 * Use the `select` param to determine which fields are returned, defaults to `[*]`.
 */
class GetAction extends DAOGetAction {

  /**
   * Should we include mailing data?
   *
   * @param bool $forReport
   */
  protected $forReport = FALSE;

  /**
   * Fetch results from the getter then apply filter/sort/select/limit.
   *
   * @param \Civi\Api4\Generic\Result $result
   */
  public function _run(Result $result) {
    $r = new Result();
    // Do the same as normal...
    parent::_run($r);

    // Now add in mailing names, if the forReport param was set.
    $mailingNames = NULL;
    if ($this->forReport) {
      $mailingIDs = $r->column('mailing_id');
      if ($mailingIDs) {
        $mailingNames = civicrm_api3('Mailing', 'get', [
          'return' => ['id', 'name', 'subject', 'scheduled_date'],
          'id' => ['IN' => $mailingIDs],
          'options' => ['limit' => 0],
        ])['values'] ?? [];
      }
    }

    // This filters out ones without a scheduled date (we should not really have these in the first place...)
    foreach ($r as &$value) {
      if ($mailingNames) {
        $value['mailing'] = $mailingNames[$value['mailing_id']]['name'] ?? NULL;
        $value['subject'] = $mailingNames[$value['mailing_id']]['subject'] ?? NULL;
        $value['scheduled_date'] = $mailingNames[$value['mailing_id']]['scheduled_date'] ?? NULL;
      }
      if ($this->forReport && empty($value['scheduled_date'])) {
        // When reporting, we don't want to include mailings that have not been scheduled yet.
        continue;
      }
      $result[] = $value;
    }

  }

}

