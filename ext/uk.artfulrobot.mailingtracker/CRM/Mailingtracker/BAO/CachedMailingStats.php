<?php
use CRM_Mailingtracker_ExtensionUtil as E;

class CRM_Mailingtracker_BAO_CachedMailingStats extends CRM_Mailingtracker_DAO_CachedMailingStats {

  /**
   * Create a new CachedMailingStats based on array-data
   *
   * @param array $params key-value pairs
   * @return CRM_Mailingtracker_DAO_CachedMailingStats|NULL
   *
  public static function create($params) {
    $className = 'CRM_Mailingtracker_DAO_CachedMailingStats';
    $entityName = 'CachedMailingStats';
    $hook = empty($params['id']) ? 'create' : 'edit';

    CRM_Utils_Hook::pre($hook, $entityName, CRM_Utils_Array::value('id', $params), $params);
    $instance = new $className();
    $instance->copyValues($params);
    $instance->save();
    CRM_Utils_Hook::post($hook, $entityName, $instance->id, $instance);

    return $instance;
  } */

}
