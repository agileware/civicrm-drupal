<?php
/*
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC. All rights reserved.                        |
 |                                                                    |
 | This work is published under the GNU AGPLv3 license with some      |
 | permitted exceptions and without any warranty. For full license    |
 | and copyright information, see https://civicrm.org/licensing       |
 +--------------------------------------------------------------------+
 */

namespace Civi\Formprotection;

use Civi\Api4\ContributionPage;
use Civi\Api4\Event;
use CRM_Formprotection_ExtensionUtil as E;

class Forms {

  /**
   * @param string $protectionType
   * @param array $context
   *
   * @return bool
   */
  public function isFormProtectionActive(string $protectionType, array $context = []): bool {
    $formName = $context['formName'] ?? '';

    switch ($protectionType) {
      case 'floodcontrol':
        // Exclude unsubscribe/optout/resubscribe
        if (in_array($formName, ['CRM_Mailing_Form_Optout', 'CRM_Mailing_Form_Subscribe', 'CRM_Mailing_Form_Unsubscribe'])) {
          return FALSE;
        }
        if (!\Civi::settings()->get('formprotection_enable_floodcontrol')) {
          return FALSE;
        }
        $enabledForms = \Civi::settings()->get('formprotection_floodcontrol_enabled_quickforms');
        if (empty($enabledForms)) {
          // No forms enabled
          return FALSE;
        }
        break;

      case 'honeypot':
        if (!\Civi::settings()->get('formprotection_enable_honeypot')) {
          return FALSE;
        }
        $enabledForms = \Civi::settings()->get('formprotection_honeypot_enabled_quickforms');
        if (empty($enabledForms)) {
          // No forms enabled
          return FALSE;
        }
        break;

      case 'recaptcha':
        if (\Civi::settings()->get('formprotection_enable_recaptcha') === 'none') {
          return FALSE;
        }
        $enabledForms = \Civi::settings()->get('formprotection_recaptcha_enabled_quickforms');
        if (empty($enabledForms)) {
          // No forms enabled
          return FALSE;
        }
        break;

      default:
        return FALSE;
    }

    if (empty($formName)) {
      // We don't have a form!
      // @fixme: this might need work because we just added this for recaptcha?
      return FALSE;
    }

    if (in_array('all', $enabledForms) || in_array($formName, $enabledForms)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * @param \CRM_Core_Form $form
   */
  public function configureSettingsForm(\CRM_Core_Form $form) {
    if (!$form instanceof \CRM_Admin_Form_Generic) {
      return;
    }

    if ($form->getSettingPageFilter() !== 'formprotection') {
      return;
    }

    $helpText = E::ts(
      'Configure settings for Form Protection. Please read the <a href="%1" target="_blank">documentation</a>.',
      [
        1 => 'https://docs.civicrm.org/formprotection'
      ]
    );
    \Civi::resources()
      ->addMarkup('<div class="help">' . $helpText . '</div>', [
        'weight' => -1,
        'region' => 'page-body'
      ]);

    \Civi::resources()->addScriptFile(E::LONG_NAME, 'js/formprotection.settings.js');
  }

  /**
   * Get form context from CRM_Core_Form (Quickform)
   * @param \CRM_Core_Form $form
   *
   * @return array
   */
  public static function getContextFromQuickform(\CRM_Core_Form $form): array {
    $context = [
      'formName' => get_class($form),
      'formID' => $form->getVar('_id')
    ];

    if (method_exists($form, 'getPaymentMode')) {
      $context['test'] = ($form->getPaymentMode() === 'test');
    }

    if (method_exists($form->controller, 'getButtonName')) {
      // We need the button name for some forms.
      // Specifically "Event additional participant forms":
      //   'skip' button is on additional participant forms, we only show reCAPTCHA on the primary form.
      $context['button'] = $form->controller->getButtonName();
    }

    // Get an array of Profile (UFGroup) IDs that are enabled on the form
    if (method_exists($form, 'getUFGroupIDs')) {
      $context['profileIDs'] = $form->getUFGroupIDs();
    }

    // For a Profile, get the "CreateMode" flag
    if (method_exists($form, 'getIsCreateMode')) {
      $context['profileCreateMode'] = $form->getIsCreateMode();
    }

    return $context;
  }

  /**
   * @param $returnDefaults
   *
   * @return string[]
   */
  public static function getSupportedQuickformClasses($returnDefaults = TRUE) {
    if ($returnDefaults) {
      return [
        'all' => E::ts('- all -'),
        'CRM_Contribute_Form_Contribution_Main' => E::ts('Contribution pages'),
        'CRM_Event_Form_Registration_Register' => E::ts('Event Registration pages'),
        'CRM_Mailing_Form_Subscribe' => E::ts('Mailing subscribe forms'),
        'CRM_Event_Cart_Form_Checkout_Payment' => E::ts('Event Cart Payment form'),
        'CRM_PCP_Form_PCPAccount' => E::ts('Personal Campaign Page Account setup'),
        'CRM_Campaign_Form_Petition_Signature' => E::ts('Petition Signature'),
        'CRM_Profile_Form_Edit' => E::ts('Profile edit form'),
        'CRM_Gdpr_Form_UpdatePreference' => E::ts('GDPR update preferences form'),
      ];
    }
    else {
      return \Civi::settings()->get('formprotection_quickform_classes');
    }
  }

  /**
   * Returns an array of contribution pages [id => title]
   * @return array
   * @throws \API_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  public static function getContributionPages() {
    try {
      return ContributionPage::get(FALSE)
        ->addSelect('id', 'title')
        ->addWhere('is_active', '=', TRUE)
        ->execute()
        ->indexBy('id')
        ->column('title');
    }
    catch (\Civi\API\Exception\NotImplementedException $e) {
      return [];
    }
  }

  /**
   * Returns an array of contribution pages [id => title]
   * @return array
   * @throws \API_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  public static function getEvents() {
    try {
      return Event::get(FALSE)
        ->addSelect('id', 'title')
        ->addWhere('is_active', '=', TRUE)
        ->execute()
        ->indexBy('id')
        ->column('title');
    }
    catch (\Civi\API\Exception\NotImplementedException $e) {
      return [];
    }
  }

}
