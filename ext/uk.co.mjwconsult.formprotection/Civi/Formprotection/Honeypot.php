<?php
/*
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC. All rights reserved.                        |
 |                                                                    |
 | This work is published under the GNU AGPLv3 license with some      |
 | permitted exceptions and without any warranty. For full license    |
 | and copyright information, see https://civicrm.org/licensing       |
 +--------------------------------------------------------------------+
 */

namespace Civi\Formprotection;

use Civi\Api4\LocationType;
use CRM_Formprotection_ExtensionUtil as E;
use oasis\names\specification\ubl\schema\xsd\CommonBasicComponents_2\HandlingInstructions;

class Honeypot {

  /**
   * @var Forms
   */
  private $forms;

  /**
   * @var \Civi\Formprotection\Honeypot
   */
  private static $singleton;

  /**
   * @var string
   */
  private $fieldName;

  /**
   * @var string
   */
  private $fieldLabel;

  /**
   * @param bool $fresh
   *   TRUE to force creation of a new system.
   *
   * @return \Civi\Formprotection\Honeypot
   */
  public static function singleton($fresh = FALSE) {
    if (!self::$singleton || $fresh) {
      self::$singleton = new Honeypot();
    }
    return self::$singleton;
  }

  public function __construct() {
    $this->forms = new \Civi\Formprotection\Forms();
  }

  /**
   * For forms with IDs (eg. Contribution pages, Event registration forms) we can filter by ID
   *   and only activate for some IDs. Check if active for ID here.
   *
   * @param array $context
   *
   * @return bool
   */
  private function isActiveForFormID(array $context): bool {
    $formName = $context['formName'] ?? '';
    $formID = $context['formID'] ?? NULL;
    switch ($formName) {
      case 'CRM_Contribute_Form_Contribution_Main':
        $contributionPageIDs = \Civi::settings()->get('formprotection_honeypot_contribution_form_ids');
        if (!empty($contributionPageIDs)) {
          if (!in_array($formID, $contributionPageIDs)) {
            return FALSE;
          }
        }
        break;

      case 'CRM_Event_Form_Registration_Register':
        $eventIDs = \Civi::settings()->get('formprotection_honeypot_event_form_ids');
        if (!empty($eventIDs)) {
          if (!in_array($formID, $eventIDs)) {
            return FALSE;
          }
        }
        break;
    }

    // Now check if we should enable on test forms?
    if (\Civi::settings()->get('formprotection_enable_test')) {
      return TRUE;
    }
    if (!empty($context['test'])) {
      \Civi::log()->debug('formprotection: ignoring form in test mode.');
      return FALSE;
    }

    return TRUE;
  }

  /**
   * @param \CRM_Core_Form $form
   */
  public function buildForm(\CRM_Core_Form &$form) {
    if ($form->isSubmitted()) {
      return;
    }

    $context = $this->forms::getContextFromQuickform($form);

    if (!$this->forms->isFormProtectionActive('honeypot', $context)) {
      return;
    }

    if (!$this->isActiveForFormID($context)) {
      return;
    }

    $quickFormElements = $form->getVar('_elements');
    $textElements = [];
    foreach ($quickFormElements as $element) {
      if (!$element instanceof \HTML_QuickForm_text) {
        continue;
      }
      $textElements[$element->getName()] = $element->getLabel();
    }

    $locationTypeNames = LocationType::get(FALSE)
      ->addSelect('name')
      ->execute()
      ->column('name');

    $fieldnames = explode(',', \Civi::settings()->get('formprotection_honeypot_field_names'));

    if (!empty($fieldnames)) {
      // Generate a semi-random fieldname that does not already exist on the form based on a fieldname from settings (eg. zip_main) + locationType.
      for ($i = 0; $i < (count($fieldnames) + count($locationTypeNames)); $i++) {
        $randfieldname = [mb_strtolower($fieldnames[rand(0, count($fieldnames) - 1)]), mb_strtolower($locationTypeNames[rand(0, count($locationTypeNames) - 1)])];
        $this->fieldName = "{$randfieldname[0]}_{$randfieldname[1]}";
        $this->fieldLabel = ucfirst($randfieldname[0]);
        if (!in_array($this->fieldName, $textElements)) {
          break;
        }
      }
      // Add the new "honeypot" field to the form
      $form->add('text', $this->fieldName, $this->fieldLabel);
      $form->set('honeypotFieldName', $this->fieldName);
    }
  }

  public function alterContent(&$content, &$form) {
    $context = $this->forms::getContextFromQuickform($form);
    if (!$this->forms->isFormProtectionActive('honeypot', $context)) {
      return;
    }

    if (!$this->isActiveForFormID($context)) {
      return;
    }

    // This inserts the honeypot "text" input field before one of the other input elements on the form.
    // It tries to introduce some randomness to make it harder for bots:
    // - The input element that it is inserted before is chosen at random.
    // - The max-length property is random.
    // - The whitespace is messy like the real forms.
    $key = '<input';
    $position = 0;
    $inputElementCount = 0;
    $newPartsBefore = $newPartsAfter = [];

    while ($position !== FALSE) {
      $position = strpos($content, $key, $position + 1);
      $inputElementCount++;
    }
    if ($inputElementCount < 1) {
      \Civi::log()->info('Formprotection Honeypot: There are no input elements on the form!');
      return;
    }

    // Loop through the parts to determine acceptable elements to insert the honey pot field before.
    // Avoid inserting within the price fields.
    // Don't loop if we only have one input element on the form.
    $candidatePartsToInsertBefore = explode($key, $content);
    if ($inputElementCount === 1) {
      $insertBefore = 1;
    }
    else {
      // The "first" element is always the html content up to the first input element.
      // Never insert before the first "element" (0).
      unset($candidatePartsToInsertBefore[0]);
      foreach($candidatePartsToInsertBefore as $partsKey => $partsValue) {
        if (str_contains($partsValue, ' price="[') || str_contains($partsValue, 'name="is_recur_radio"')) {
          unset($candidatePartsToInsertBefore[$partsKey]);
        }
      }
      $insertBefore = array_rand($candidatePartsToInsertBefore);
    }

    $parts = explode($key, $content);

    for ($i = 0; $i < $insertBefore; $i++) {
      $newPartsBefore[] = $parts[$i];
    }

    $maxLength = rand(10, 40);

    $newElement = <<<MARKUP
                            <div class="crm-section $this->fieldName-section" style="display: none">
            <div class="label"><label for="$this->fieldName">$this->fieldLabel</label>
                        </div>
            <div class="content">
                <input size="20" maxlength="$maxLength" autocomplete="off" name="$this->fieldName" type="text" id="$this->fieldName" class="medium crm-form-text" />
                                        </div>
          </div>
MARKUP;

    for ($i = $insertBefore; $i < count($parts); $i++) {
      $newPartsAfter[] = $parts[$i];
    }

    // Now put it all together
    $contentArray['before'] = implode($key, $newPartsBefore);
    $contentArray['new'] = $newElement;
    $contentArray['after'] = implode($key, $newPartsAfter);
    // We used "implode" which didn't add the "<input" ($key) back to the beginning of the first element.
    // So we add it back here. But only if the "after" content does not already have a html element.
    //   (eg. if it was the last element it could be a "<div .." and we don't want to add "<input" before that because it will break the markup.
    if (!empty(trim($contentArray['after']) && substr(trim($contentArray['after']), 0, 1) !== '<')) {
      $contentArray['after'] = $key . $contentArray['after'];
    }
    $content = implode('', $contentArray);
  }

  /**
   * @param \CRM_Core_Form $form
   * @param array $fields
   * @param array $errors
   */
  public function validateForm(\CRM_Core_Form &$form, array $fields, array &$errors) {
    $context = $this->forms::getContextFromQuickform($form);
    if (!$this->forms->isFormProtectionActive('honeypot', $context)) {
      return;
    }

    // Did we setup the form with a honeypot field?
    $this->fieldName = $form->get('honeypotFieldName');
    if (empty($this->fieldName)) {
      return;
    }

    // Check if we have any value in the honeypot field (fail if we do).
    $submittedValues = $form->getSubmitValues();
    if (in_array($this->fieldName, array_keys($submittedValues)) && !empty($submittedValues[$this->fieldName])) {
      \Civi::log()->warning("FormProtection Honeypot: Hidden field {$this->fieldName} contains a value!");
      $errors['_qf_default'] = E::ts('There was a problem with your form submission. Please try again.');

      // If Firewall is enabled, log it there
      if (\Civi::settings()->get('formprotection_enable_firewall_integration') && class_exists('\Civi\Firewall\Event\FormProtectionEvent')) {
        \Civi\Firewall\Event\FormProtectionEvent::trigger(Utils::getIPAddress(), 'honeypot: hidden field completed on ' . $form->getName());
      }

    }
  }

}
