<?php
/*
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC. All rights reserved.                        |
 |                                                                    |
 | This work is published under the GNU AGPLv3 license with some      |
 | permitted exceptions and without any warranty. For full license    |
 | and copyright information, see https://civicrm.org/licensing       |
 +--------------------------------------------------------------------+
 */
namespace Civi\FormProtection\Listener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class StripeAuthorizeRecaptcha implements EventSubscriberInterface {

  /**
   * @return array
   */
  public static function getSubscribedEvents() {
    return [
      'civi.stripe.authorize' => [
        // Positive priority is higher (eg. 200 will run before 100)
        ['onStripeAuthorize', 100],
      ],
    ];
  }

  /**
   * Alters APIv4 permissions to allow users with 'administer search_kit' to create/delete a SavedSearch
   *
   * @param \Civi\Stripe\Event\AuthorizeEvent $event
   *   API authorization event.
   */
  public function onStripeAuthorize(\Civi\Stripe\Event\AuthorizeEvent $event) {
    if ($event->getEntityName() !== 'StripePaymentintent') {
      return;
    }
    // 6.7 has API3 process endpoint, 6.8 has API4 processPublic, processMOTO endpoint.
    if (!in_array($event->getActionName(), ['process', 'processPublic'])) {
      return;
    }

    $reCaptcha = new \Civi\Formprotection\Recaptcha();
    if (!$reCaptcha->isRecaptchaActiveForRequest()) {
      return;
    }

    // Check params
    if (empty($event->getParams()['captcha'])) {
      $event->setAuthorized(FALSE);
      $event->setReasonDescription(__CLASS__, 'Mising captcha token in params');
    }

    $errors = [];
    $captcha = $event->getParams()['captcha'] ?? '';
    if (!\Civi\Formprotection\Recaptcha::validate($captcha, $errors)) {
      \Civi::log('formprotection')->error('StripePaymentintent Recaptcha validate failed: ' . print_r($errors, TRUE));
      $event->setReasonDescription(__CLASS__, print_r($errors, TRUE));
      $event->setAuthorized(FALSE);
    }
  }

}
