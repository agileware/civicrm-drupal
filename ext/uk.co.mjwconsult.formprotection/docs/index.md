# Form Protection

This provides various ways to help protect forms from spammers:

## Honeypot

Based on https://github.com/elisseck/com.elisseck.civihoneypot

## Flood control

Based on https://lab.civicrm.org/extensions/floodcontrol

## ReCAPTCHA

* v2 "I'm not a robot" (replaces the one shipped with CiviCRM core).
* v3.

If you are using the Stripe payment processor ReCAPTCHA will also be used to help mitigate card testing attacks if it is enabled.

## Setup

Configure via *Administer->System Settings->Form Protection Settings*

## Integration with Firewall

When using [Firewall](https://civicrm.org/extensions/firewall) v1.6.0+, this extension will log `FormprotectionEvent`s in
the Firewall, which will block a user's IP address after too many failed attempts. See the Firewall documentation for more
information.

## Support and Maintenance

This extension is supported and maintained by:

[![MJW Consulting](images/mjwconsulting.jpg)](https://www.mjwconsult.co.uk)

We offer paid [support and development](https://mjw.pt/support) as well as a [troubleshooting/investigation service](https://mjw.pt/investigation).
