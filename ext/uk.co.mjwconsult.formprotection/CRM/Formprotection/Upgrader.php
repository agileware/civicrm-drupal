<?php
use CRM_Formprotection_ExtensionUtil as E;

/**
 * Collection of upgrade steps.
 */
class CRM_Formprotection_Upgrader extends CRM_Extension_Upgrader_Base {

  public function postInstall() {
    $this->migrateFromCoreRecaptcha();
  }

  public function onDisable() {
    civicrm_api3('Extension', 'enable', [
      'keys' => 'recaptcha',
    ]);
  }

  public function onEnable() {
    civicrm_api3('Extension', 'disable', [
      'keys' => 'recaptcha',
    ]);
  }

  /**
   * Example: Run a couple simple queries.
   *
   * @return TRUE on success
   * @throws Exception
   */
  public function upgrade_1100(): bool {
    $this->ctx->log->info('Migrating from core ReCAPTCHA extension');
    $this->migrateFromCoreRecaptcha();
    return TRUE;
  }

  private function migrateFromCoreRecaptcha() {
    // Copy settings across
    $settingsMap = [
      'recaptchaPublicKey' => 'formprotection_recaptcha_publickey',
      'recaptchaPrivateKey' => 'formprotection_recaptcha_privatekey',
    ];
    if (!empty(\Civi::settings()->get('recaptchaPublicKey'))) {
      \Civi::settings()->set('formprotection_enable_recaptcha', 'v2checkbox');
    }
    foreach ($settingsMap as $old => $new) {
      $value = \Civi::settings()->get($old);
      if (!empty($value)) {
        \Civi::settings()->set($new, $value);
      }
    }

    // Disable the old core extension
    civicrm_api3('Extension', 'disable', [
      'keys' => 'recaptcha',
    ]);
  }

  public function upgrade_1400(): bool {
    $this->ctx->log->info('Removing deprecated setting for recaptcha options.');
    \CRM_Core_DAO::executeQuery('DELETE FROM civicrm_setting WHERE name = "formprotection_recaptcha_options"');
    return TRUE;
  }

}
