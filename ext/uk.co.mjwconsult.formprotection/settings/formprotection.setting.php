<?php

use CRM_Formprotection_ExtensionUtil as E;
return [
  'formprotection_quickform_classes' => [
    'name' => 'formprotection_quickform_classes',
    'type' => 'Array',
    'default' => \Civi\Formprotection\Forms::getSupportedQuickformClasses(),
    'title' => E::ts('Form Protection supported QuickForm classes'),
    'is_domain' => 0,
    'is_contact' => 0,
    'description' => E::ts('List of quickform classes supported by the formprotection extension'),
    'help_text' => NULL,
  ],

  'formprotection_enable_test' => [
    'name' => 'formprotection_enable_test',
    'type' => 'Boolean',
    'html_type' => 'checkbox',
    'default' => 0,
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Enable form protection for test forms'),
    'description' => E::ts('Enable for test forms (eg. test contribution pages)'),
    'html_attributes' => [],
    'settings_pages' => [
      'formprotection' => [
        'weight' => 5,
      ]
    ],
  ],

  'formprotection_enable_firewall_integration' => [
    'name' => 'formprotection_enable_firewall_integration',
    'type' => 'Boolean',
    'html_type' => 'checkbox',
    'default' => 0,
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Enable Firewall event integration'),
    'description' => E::ts('Log failed attempts as firewall events, and eventually block the client IP.'),
    'html_attributes' => [],
    'settings_pages' => [
      'formprotection' => [
        'weight' => 6,
      ]
    ],
  ],

  'formprotection_enable_floodcontrol' => [
    'name' => 'formprotection_enable_floodcontrol',
    'type' => 'Boolean',
    'html_type' => 'checkbox',
    'default' => 0,
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Enable flood control for forms?'),
    'description' => E::ts('Enable flood control to limit the speed at which forms may be submitted and help reduce the quantity of spam submissions'),
    'html_attributes' => [],
    'settings_pages' => [
      'formprotection' => [
        'weight' => 10,
      ]
    ],
  ],
  'formprotection_floodcontrol_enabled_quickforms' => [
    'name' => 'formprotection_floodcontrol_enabled_quickforms',
    'type' => 'Array',
    'html_type' => 'select',
    'html_attributes' => [
      'class' => 'crm-select2 huge',
      'multiple' => TRUE,
      'placeholder' => E::ts('- select -'),
    ],
    'pseudoconstant' => [
      'callback' => ['\Civi\Formprotection\Forms', 'getSupportedQuickformClasses'],
    ],
    'default' => [],
    'title' => E::ts('Enable floodcontrol on forms'),
    'is_domain' => 1,
    'is_contact' => 0,
    'description' => E::ts('List of forms to enable floodcontrol'),
    'settings_pages' => [
      'formprotection' => [
        'weight' => 15,
      ]
    ],
  ],
  'formprotection_floodcontrol_minimum_seconds_before_post' => [
    'name' => 'formprotection_floodcontrol_minimum_seconds_before_post',
    'type' => 'Integer',
    'default' => 0,
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Minimum seconds before post'),
    'description' => E::ts('Minimum number of seconds required to wait before submitting the form. If the visitor submits too quickly, they will be asked to try again due to vague timeout issues. Set to 0 to disable.'),
    'help_text' => '',
    'html_type' => 'text',
    'settings_pages' => [
      'formprotection' => [
        'weight' => 20,
      ]
    ],
  ],
  'formprotection_floodcontrol_delay_spammers_seconds' => [
    'name' => 'formprotection_floodcontrol_delay_spammers_seconds',
    'type' => 'Integer',
    'default' => 0,
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Spammer delay reload (seconds)'),
    'description' => E::ts('When a visitor was suspected of spamming, this delay slows down page loading by the given number of seconds. Set to 0 to disable.'),
    'help_text' => '',
    'html_type' => 'text',
    'settings_pages' => [
      'formprotection' => [
        'weight' => 30,
      ]
    ],
  ],
  'formprotection_floodcontrol_max_success_count' => [
    'name' => 'formprotection_floodcontrol_max_success_count',
    'type' => 'Integer',
    'default' => 0,
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Maximum valid attempts'),
    'description' => E::ts('Maximum valid attempts in a given period. This means that they managed to get through floodcontrol, but if there are other errors in the form, that would trigger a form reload and add to the number of attempts. It should not be too low. Set to 0 to disable.'),
    'help_text' => '',
    'html_type' => 'text',
    'settings_pages' => [
      'formprotection' => [
        'weight' => 40,
      ]
    ],
  ],
  'formprotection_floodcontrol_max_success_period' => [
    'group_name' => 'domain',
    'group' => 'floodcontrol',
    'name' => 'formprotection_floodcontrol_max_success_period',
    'type' => 'Integer',
    'default' => 0,
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Maximum valid period'),
    'description' => E::ts('Period (in seconds) for which to define the valid attempts. Set to 0 to disable.'),
    'help_text' => '',
    'html_type' => 'text',
    'settings_pages' => [
      'formprotection' => [
        'weight' => 50,
      ]
    ],
  ],
  'formprotection_floodcontrol_max_success_recaptcha' => [
    'group_name' => 'domain',
    'group' => 'floodcontrol',
    'name' => 'formprotection_floodcontrol_max_success_recaptcha',
    'type' => 'Integer',
    'default' => 0,
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Maximum attempts before captcha'),
    'description' => E::ts('Maximum attempts before enabling captcha. For example, more strict methods might kick-in after 5 attempts, but this can display a recaptcha on the second attempt. Set to 0 to disable. Setting this to 1 would annoying ask to answer a captcha after submitting the form for the first time.'),
    'help_text' => '',
    'html_type' => 'text',
    'settings_pages' => [
      'formprotection' => [
        'weight' => 60,
      ]
    ],
  ],
  'formprotection_floodcontrol_recaptcha' => [
    'name' => 'formprotection_floodcontrol_recaptcha',
    'type' => 'Boolean',
    'default' => 0,
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Enable reCaptcha on errors'),
    'description' => E::ts('Display a reCAPTCHA when there are errors.'),
    'help_text' => '',
    'html_type' => 'checkbox',
    'settings_pages' => [
      'formprotection' => [
        'weight' => 70,
      ]
    ],
  ],

  'formprotection_enable_honeypot' => [
    'name' => 'formprotection_enable_honeypot',
    'type' => 'Boolean',
    'html_type' => 'checkbox',
    'default' => 0,
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Enable Honeypot for forms?'),
    'description' => E::ts('Enable Honeypot to reduce submission by bots'),
    'html_attributes' => [],
    'settings_pages' => [
      'formprotection' => [
        'weight' => 100,
      ]
    ],
  ],
  'formprotection_honeypot_enabled_quickforms' => [
    'name' => 'formprotection_honeypot_enabled_quickforms',
    'type' => 'Array',
    'html_type' => 'select',
    'html_attributes' => [
      'class' => 'crm-select2 huge',
      'multiple' => TRUE,
      'placeholder' => E::ts('- select -'),
    ],
    'pseudoconstant' => [
      'callback' => ['\Civi\Formprotection\Forms', 'getSupportedQuickformClasses'],
    ],
    'default' => [],
    'title' => E::ts('Enable Honeypot on forms'),
    'is_domain' => 1,
    'is_contact' => 0,
    'description' => E::ts('List of forms to enable Honeypot'),
    'settings_pages' => [
      'formprotection' => [
        'weight' => 110,
      ]
    ],
  ],
  'formprotection_honeypot_contribution_form_ids' => [
    'name' => 'formprotection_honeypot_contribution_form_ids',
    'type' => 'Array',
    'html_type' => 'select',
    'html_attributes' => [
      'class' => 'crm-select2 huge',
      'multiple' => TRUE,
      'placeholder' => E::ts('- select -'),
    ],
    'pseudoconstant' => [
      'callback' => ['\Civi\Formprotection\Forms', 'getContributionPages'],
    ],
    'default' => [],
    'is_domain' => 1,
    'title' => E::ts('Enable for specific Contribution Pages'),
    'description' => E::ts('Leave blank to enable for all contribution pages'),
    'settings_pages' => [
      'formprotection' => [
        'weight' => 120,
      ]
    ],
  ],
  'formprotection_honeypot_event_form_ids' => [
    'name' => 'formprotection_honeypot_event_form_ids',
    'type' => 'Array',
    'html_type' => 'select',
    'html_attributes' => [
      'class' => 'crm-select2 huge',
      'multiple' => TRUE,
      'placeholder' => E::ts('- select -'),
    ],
    'pseudoconstant' => [
      'callback' => ['\Civi\Formprotection\Forms', 'getEvents'],
    ],
    'default' => [],
    'is_domain' => 1,
    'title' => E::ts('Enable for specific Event registration forms'),
    'description' => E::ts('Leave blank to enable for all event registration forms'),
    'settings_pages' => [
      'formprotection' => [
        'weight' => 120,
      ]
    ],
  ],
  'formprotection_honeypot_field_names' => [
    'name' => 'formprotection_honeypot_field_names',
    'type' => 'String',
    'html_type' => 'text',
    'default' => 'name,email,zip,phone,url',
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Field names'),
    'description' => E::ts('Specify a list of field names which will be randomly selected and combined with locationtypes to generate the name/label for the honeypot field. Default: name,email,zip,phone,url'),
    'html_attributes' => [
      'size' => 40,
    ],
    'settings_pages' => [
      'formprotection' => [
        'weight' => 130,
      ]
    ],
  ],

  'formprotection_enable_recaptcha' => [
    'default' => 'none',
    'html_type' => 'select',
    'name' => 'formprotection_enable_recaptcha',
    'title' => ts('reCAPTCHA version'),
    'type' => CRM_Utils_Type::T_STRING,
    'is_domain' => 1,
    'is_contact' => 0,
    'description' => ts('Select the reCAPTCHA version. Note that you can NOT use the same API keys for v2 and v3.'),
    'options' => [
      'none' => ts('- none - '),
      'v3' => ts('reCAPTCHA v3'),
      'v2checkbox' => ts('reCAPTCHA v2 ("I\'m not a robot" Checkbox)'),
    ],
    'settings_pages' => [
      'formprotection' => [
        'weight' => 200,
      ]
    ],
  ],
  'formprotection_recaptcha_publickey' => [
    'group_name' => 'CiviCRM Preferences',
    'group' => 'core',
    'name' => 'formprotection_recaptcha_publickey',
    'type' => 'String',
    'quick_form_type' => 'Element',
    'html_attributes' => [
      'size' => 64,
      'maxlength' => 64,
    ],
    'html_type' => 'text',
    'default' => NULL,
    'add' => '4.3',
    'title' => E::ts('reCAPTCHA Site Key'),
    'is_domain' => 1,
    'is_contact' => 0,
    'description' => NULL,
    'help_text' => NULL,
    'settings_pages' => [
      'formprotection' => [
        'weight' => 210,
      ],
    ],
  ],
  'formprotection_recaptcha_threshold' => [
    'group_name' => 'CiviCRM Preferences',
    'group' => 'core',
    'name' => 'formprotection_recaptcha_threshold',
    'type' => 'float',
    'quick_form_type' => 'Element',
    'html_attributes' => [
      'size' => 10,
      'maxlength' => 5,
    ],
    'html_type' => 'text',
    'default' => 0.5,
    'add' => '5.68',
    'title' => E::ts('reCAPTCHA Threshold'),
    'is_domain' => 1,
    'is_contact' => 0,
    'description' => 'reCAPTCHA scores submissions from 0 (definitely a bot) to 1.0 (definitely a human).  Lower this number if too many humans are being blocked; raise it if too many bots are being allowed.',
    'validate_callback' => '\Civi\Formprotection\Recaptcha::validateRecaptchaThreshold',
    'help_text' => NULL,
    'settings_pages' => [
      'formprotection' => [
        'weight' => 215,
      ],
    ],
  ],
  'formprotection_recaptcha_privatekey' => [
    'group_name' => 'CiviCRM Preferences',
    'group' => 'core',
    'name' => 'formprotection_recaptcha_privatekey',
    'type' => 'String',
    'quick_form_type' => 'Element',
    'html_attributes' => [
      'size' => 64,
      'maxlength' => 64,
    ],
    'html_type' => 'text',
    'default' => NULL,
    'add' => '4.3',
    'title' => E::ts('reCAPTCHA Secret Key'),
    'is_domain' => 1,
    'is_contact' => 0,
    'description' => NULL,
    'help_text' => NULL,
    'settings_pages' => [
      'formprotection' => [
        'weight' => 220,
      ],
    ],
  ],
  'formprotection_recaptcha_enabled_quickforms' => [
    'name' => 'formprotection_recaptcha_enabled_quickforms',
    'type' => 'Array',
    'html_type' => 'select',
    'html_attributes' => [
      'class' => 'crm-select2 huge',
      'multiple' => TRUE,
      'placeholder' => E::ts('- select -'),
    ],
    'pseudoconstant' => [
      'callback' => ['\Civi\Formprotection\Forms', 'getSupportedQuickformClasses'],
    ],
    'default' => [],
    'title' => E::ts('Enable reCAPTCHA on forms'),
    'is_domain' => 1,
    'is_contact' => 0,
    'description' => E::ts('List of forms to enable reCAPTCHA'),
    'settings_pages' => [
      'formprotection' => [
        'weight' => 250,
      ]
    ],
  ],
  'formprotection_recaptcha_contribution_form_ids' => [
    'name' => 'formprotection_recaptcha_contribution_form_ids',
    'type' => 'Array',
    'html_type' => 'select',
    'html_attributes' => [
      'class' => 'crm-select2 huge',
      'multiple' => TRUE,
      'placeholder' => E::ts('- select -'),
    ],
    'pseudoconstant' => [
      'callback' => ['\Civi\Formprotection\Forms', 'getContributionPages'],
    ],
    'default' => [],
    'is_domain' => 1,
    'title' => E::ts('Enable reCAPTCHA for specific Contribution Pages'),
    'description' => E::ts('Leave blank to enable for all contribution pages'),
    'settings_pages' => [
      'formprotection' => [
        'weight' => 260,
      ]
    ],
  ],
  'formprotection_recaptcha_event_form_ids' => [
    'name' => 'formprotection_recaptcha_event_form_ids',
    'type' => 'Array',
    'html_type' => 'select',
    'html_attributes' => [
      'class' => 'crm-select2 huge',
      'multiple' => TRUE,
      'placeholder' => E::ts('- select -'),
    ],
    'pseudoconstant' => [
      'callback' => ['\Civi\Formprotection\Forms', 'getEvents'],
    ],
    'default' => [],
    'is_domain' => 1,
    'title' => E::ts('Enable reCAPTCHA for specific Event registration forms'),
    'description' => E::ts('Leave blank to enable for all event registration forms'),
    'settings_pages' => [
      'formprotection' => [
        'weight' => 270,
      ]
    ],
  ],

];
