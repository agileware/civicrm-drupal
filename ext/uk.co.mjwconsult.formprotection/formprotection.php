<?php

require_once 'formprotection.civix.php';
$autoload = __DIR__ . '/vendor/autoload.php';
if (file_exists($autoload)) {
  require_once $autoload;
}
// phpcs:disable
use CRM_Formprotection_ExtensionUtil as E;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

// phpcs:enable

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function formprotection_civicrm_config(&$config) {
  _formprotection_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_container().
 */
function formprotection_civicrm_container(ContainerBuilder $container) {
  $container
    ->setDefinition('civi.formprotection.stripeauthorizerecaptcha', new Definition('\Civi\Formprotection\Listener\StripeAuthorizeRecaptcha'))
    ->addTag('kernel.event_subscriber')
    ->setPublic(TRUE);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function formprotection_civicrm_install() {
  _formprotection_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function formprotection_civicrm_enable() {
  _formprotection_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_navigationMenu().
 */
function formprotection_civicrm_navigationMenu(&$menu) {
  _formprotection_civix_insert_navigation_menu($menu, 'Administer/System Settings', [
    'label' => E::ts('Form Protection Settings'),
    'name' => 'formprotection_settings',
    'url' => 'civicrm/admin/setting/formprotection',
    'permission' => 'administer CiviCRM',
    'operator' => 'OR',
    'separator' => 0,
  ]);
  _formprotection_civix_navigationMenu($menu);
}

/**
 * Implements hook_civicrm_buildForm().
 */
function formprotection_civicrm_buildForm($formName, &$form) {
  $forms = new \Civi\Formprotection\Forms();
  $forms->configureSettingsForm($form);

  $floodcontrol = new \Civi\Formprotection\Floodcontrol();
  $floodcontrol->buildForm($form);

  $honeypot = \Civi\Formprotection\Honeypot::singleton();
  $honeypot->buildForm($form);

  $recaptcha = new \Civi\Formprotection\Recaptcha();
  $recaptcha->buildForm($form);
}

/**
 * Implements hook_civicrm_validateForm().
 */
function formprotection_civicrm_validateForm($formName, &$fields, &$files, &$form, &$errors) {
  $floodcontrol = new \Civi\Formprotection\Floodcontrol();
  $floodcontrol->validateForm($form, $errors);

  $honeypot = \Civi\Formprotection\Honeypot::singleton();
  $honeypot->validateForm($form, $fields, $errors);

  $recaptcha = new \Civi\Formprotection\Recaptcha();
  $recaptcha->validateForm($form, $fields, $errors);
}

function formprotection_civicrm_alterContent(&$content, $context, $tplName, &$object) {
  if ($context !== 'form') {
    return;
  }
  $honeypot = \Civi\Formprotection\Honeypot::singleton();
  $honeypot->alterContent($content, $object);
}

/**
 * Implements hook_civicrm_check().
 *
 * @throws \CiviCRM_API3_Exception
 */
function formprotection_civicrm_check(&$messages) {
  $checks = new CRM_Formprotection_Check($messages);
  $messages = $checks->checkRequirements();
}
