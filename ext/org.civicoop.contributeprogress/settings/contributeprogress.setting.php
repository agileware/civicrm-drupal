<?php

use CRM_Contributeprogress_ExtensionUtil as E;

return [
  'contributeprogress_css' => [
    'group_name' => 'domain',
    'group' => 'contributeprogress',
    'name' => 'contributeprogress_css',
    'type' => 'String',
    'html_type' => 'textarea',
    'default' => null,
    'add' => '1.0',
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Custom CSS for Widget Embeds'),
    'description' => E::ts('Allows embedding custom CSS to further customize the widgets when embedded using an iframe.'),
    'settings_pages' => [
      'contributeprogress' => [
        'weight' => 5,
      ],
    ],
  ],
  'contributeprogress_width' => [
    'group_name' => 'domain',
    'group' => 'contributeprogress',
    'name' => 'contributeprogress_width',
    'type' => 'Integer',
    'html_type' => 'text',
    'default' => 350,
    'add' => '1.0',
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Widget Embed Width'),
    'description' => E::ts('Override the default width of the embedded widget. By default, the iframe is 350x200 pixels. You may need to change this if you are using custom CSS.'),
    'settings_pages' => [
      'contributeprogress' => [
        'weight' => 10,
      ],
    ],
  ],
  'contributeprogress_height' => [
    'group_name' => 'domain',
    'group' => 'contributeprogress',
    'name' => 'contributeprogress_height',
    'type' => 'Integer',
    'html_type' => 'text',
    'default' => 200,
    'add' => '1.0',
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Widget Embed Height'),
    'description' => E::ts('Override the default height of the embedded widget. By default, the iframe is 350x200 pixels. You may need to change this if you are using custom CSS.'),
    'settings_pages' => [
      'contributeprogress' => [
        'weight' => 10,
      ],
    ],
  ],
];
