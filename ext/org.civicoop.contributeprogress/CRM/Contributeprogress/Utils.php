<?php

class CRM_Contributeprogress_Utils {

  /**
   * Rewrites CRM_Contribute_BAO_Widget::getContributionPageData()
   * which does a bit too much formatting.
   */
  static public function getContributionPageData($contributionPageID, $widgetID, $includePending = FALSE) {
    $config = CRM_Core_Config::singleton();

    $data = [];
    $data['currencySymbol'] = $config->defaultCurrencySymbol;

    if (empty($contributionPageID)) {
      throw new Exception("Contribution Page ID ($contributionPageID) is not set");
    }

    $widget = new CRM_Contribute_DAO_Widget();
    $widget->contribution_page_id = $contributionPageID;

    if (!$widget->find(TRUE)) {
      throw new Exception("Contribution Page ID ($contributionPageID) not found");
    }

    // Check if pending status needs to be included
    $status = '1';
    if ($includePending) {
      $status = '1,2';
    }

    $query = "
      SELECT count(id) as donors, sum(total_amount) as amount
      FROM civicrm_contribution
      WHERE is_test = 0
        AND contribution_status_id IN ({$status})
        AND contribution_page_id = %1";

    $dao = CRM_Core_DAO::executeQuery($query, [
      1 => [$contributionPageID, 'Integer'],
    ]);

    $data['num_donors'] = 0;
    $data['money_raised'] = 0;
    $data['money_raised_percentage'] = 0;

    if ($dao->fetch()) {
      // Avoid null values
      $data['num_donors'] = $dao->donors ?? 0;
      $data['money_raised'] = $dao->amount ?? 0;
    }

    $query = "SELECT goal_amount, start_date, end_date, is_active
      FROM civicrm_contribution_page
      WHERE id = %1";

    $dao = CRM_Core_DAO::executeQuery($query, [
      1 => [$contributionPageID, 'Integer'],
    ]);

    $data['campaign_start'] = '';
    $startDate = NULL;

    if ($dao->fetch()) {
      $data['money_target'] = (int) $dao->goal_amount;
      $data['start_date'] = $dao->start_date;
      $data['end_date'] = $dao->end_date;

      // FIXME: this will probably be removed, legacy.
      // conditions that needs to be handled
      // 1. Campaign is not active - no text
      // 2. Campaign start date greater than today - show start date
      // 3. Campaign end date is set and greater than today - show end date
      // 4. If no start and end date or no end date and start date greater than today, then it's ongoing
      if ($dao->is_active) {
        $data['campaign_start'] = ts('Campaign is ongoing');

        // check for time being between start and end date
        $now = time();
        if ($dao->start_date) {
          $startDate = CRM_Utils_Date::unixTime($dao->start_date);
          if ($startDate && $startDate >= $now) {
            $data['is_active'] = FALSE;
            $data['campaign_start'] = ts('Campaign starts on %1', [
              1 => CRM_Utils_Date::customFormat($dao->start_date, $config->dateformatFull),
            ]);
          }
        }

        if ($dao->end_date) {
          $endDate = CRM_Utils_Date::unixTime($dao->end_date);
          if ($endDate && $endDate < $now) {
            $data['is_active'] = FALSE;
            $data['campaign_start'] = ts('Campaign ended on %1', [
              1 => CRM_Utils_Date::customFormat($dao->end_date, $config->dateformatFull),
            ]);
          }
          elseif ($startDate >= $now) {
            $data['campaign_start'] = ts('Campaign starts on %1', [
              1 => CRM_Utils_Date::customFormat($dao->start_date, $config->dateformatFull),
            ]);
          }
          else {
            $data['campaign_start'] = ts('Campaign ends on %1', [
              1 => CRM_Utils_Date::customFormat($dao->end_date, $config->dateformatFull),
            ]);
          }
        }
      }
      else {
        $data['is_active'] = FALSE;
      }
    }
    else {
      $data['is_active'] = FALSE;
    }

    if ($data['money_target'] > 0) {
      $percent = $data['money_raised'] / $data['money_target'];
      $data['money_raised_percentage'] = (round($percent, 2)) * 100 . "%";
      $data['money_target_formatted'] = CRM_Utils_Money::format($data['money_target'], NULL, NULL, FALSE, '%!.0n');
    }

    $data['money_raised_formatted'] = CRM_Utils_Money::format($data['money_raised'], NULL, NULL, FALSE, '%!.0n');

    $data['money_low'] = 0;
    $data['num_donors'] = $data['num_donors'];

    $data['colors'] = [];
    $data['colors']["title"] = $widget->color_title;
    $data['colors']["button"] = $widget->color_button;
    $data['colors']["bar"] = $widget->color_bar;
    $data['colors']["main_text"] = $widget->color_main_text;
    $data['colors']["main"] = $widget->color_main;
    $data['colors']["main_bg"] = $widget->color_main_bg;
    $data['colors']["bg"] = $widget->color_bg;
    $data['colors']["about_link"] = $widget->color_about_link;

    self::cleanupWidgetData($data, $widget);

    return $data;
  }

  /**
   * This should be combined in the previous function, but was kept separate
   * to make it easier to diff the function with the one from core.
   */
  static public function cleanupWidgetData(&$data, $widget) {
    // Fixme: copied from the JS logic, because the above function does not return raw values
    // Maybe we should just calculate ourselves.
    $t = explode('%', $data['money_raised_percentage']);
    $data['money_raised_percentage_width'] = ($t[0] > 100 ? 100 : $t[0]);

    // Show a small dent if 0%
    if (round($data['money_raised_percentage_width']) == 0) {
      $data['money_raised_percentage_width'] = 1;
    }

    // Amount left, rounded
    $amount_left = $data['money_target'] - $data['money_raised'];
    $data['amount_left'] = CRM_Utils_Money::format($amount_left, NULL, NULL, FALSE, '%!.0n');

    if ($data['end_date']) {
      $t1 = new DateTime();
      $t2 = new DateTime($data['end_date']);

      $data['days_left'] = $t2->diff($t1)->format('%a');
      $data['show_days_left'] = 1;

      // TODO: If the campaign has ended, we may want something more friendly?
      // "i.e. the campaign has ended", but it needs to be nice visually too.
      if ($data['days_left'] < 0) {
        $data['show_days_left'] = 0;
      }
    }

    // Add the button label
    $data['button_title'] = $widget->button_title ?? ts('Contribute');

    // Donation URL
    $data['contribution_page_url'] = CRM_Utils_System::url('civicrm/contribute/transact', 'reset=1&id=' . $widget->contribution_page_id, TRUE, NULL, TRUE, TRUE);

    // Is active is used to check for permissions by the Widget.php
    $data['is_active'] = $widget->is_active;
  }

}
