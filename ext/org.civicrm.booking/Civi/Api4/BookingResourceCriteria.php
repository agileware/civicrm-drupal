<?php
namespace Civi\Api4;

/**
 * BookingResourceCriteria entity.
 *
 * Provided by the CiviBooking extension.
 *
 * @package Civi\Api4
 */
class BookingResourceCriteria extends Generic\DAOEntity {

}
