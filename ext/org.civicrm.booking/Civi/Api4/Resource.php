<?php
namespace Civi\Api4;

/**
 * Resource entity.
 *
 * Provided by the CiviBooking extension.
 *
 * @package Civi\Api4
 */
class Resource extends Generic\DAOEntity {

}
