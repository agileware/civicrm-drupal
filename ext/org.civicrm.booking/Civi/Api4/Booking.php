<?php
namespace Civi\Api4;

/**
 * Booking entity.
 *
 * Provided by the CiviBooking extension.
 *
 * @package Civi\Api4
 */
class Booking extends Generic\DAOEntity {

}
