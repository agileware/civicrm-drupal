<?php

require_once 'ux.civix.php';
use CRM_UX_ExtensionUtil as E;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function ux_civicrm_config(&$config) {
  // Prevent the following database try/catch from spamming the error log by
  // using our own callback.
  // Note that this is using code that supposed to have been removed in 4.3/4.4
  // when CiviCRM "adopts exception-base error handling" (doesn't seem to have
  // happened fully)
  $errorScope = CRM_Core_TemporaryErrorScope::create('_ux_passErrorAsException', TRUE);

  // CIVIUX-123 set maximum statement time to four minutes
  $statement_limit_args = [
    [ // MariaDB
      'SET SESSION max_statement_time = %1', [ 1 => [ 240, 'Integer' ] ]
    ],
    [ // MySQL 5.7+, CIVIUX-146
      'SET SESSION max_execution_time = %1', [ 1 => [ 240000, 'Integer' ] ]
    ],
  ];

  foreach ($statement_limit_args as $args) {
    try {
      call_user_func_array(['CRM_Core_DAO' , 'executeQuery'], $args);
      break;
    }
    catch (Exception $e) {
      // ...
    }
  }

  unset($errorScope);

  global $civicrm_setting;

  $civicrm_setting['domain']['defaultExternUrl'] = 'router';
  $civicrm_setting['domain']['communityMessagesUrl'] = FALSE;
  $civicrm_setting['domain']['ext_repo_url'] = FALSE;

  /**
   * @var \Civi\Core\SettingsManager $settings_manager
   */
  $settings_manager = Civi::service('settings_manager');

  $settings_manager->useMandatory();

  _ux_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function ux_civicrm_install() {
  _ux_update_settings();
  _ux_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function ux_civicrm_enable() {
  _ux_civix_civicrm_enable();
}

/**
 * Various CiviCRM form changes
 *
 * @param $formName string
 * @param $form CRM_Core_Form
 *
 * @throws \Civi\API\Exception\UnauthorizedException
 */
function ux_civicrm_buildForm($formName, &$form) {
  if (($form instanceof CRM_Import_Form_DataSource) || $formName == "CRM_Contact_Import_Form_DataSource") {
    $importPermission = CRM_Core_Permission::check('CSV Import');
    if (!$importPermission) {
      _ux_throw_access_denied_exception();
    }
  }
  if ($formName == "CRM_Export_Form_Select") {
    $exportPermission = CRM_Core_Permission::check('CSV Export');
    if (!$exportPermission) {
      _ux_throw_access_denied_exception();
    }
  }
  if($formName == "CRM_Admin_Form_Setting_Miscellaneous") {
    $pure = array_merge($form->getTemplateVars('pure_config_settings'), [ 'ux_auto_relationship_date' ]);
    $pure = array_merge($pure, [ 'ux_default_separate_activities_per_contact' ]);
    $form->assign('pure_config_settings', $pure);
  }
  // PROJ-3061 Set default option to create separate Activities for each contact selected
  $separateActivitiesByDefault = (bool) Civi::settings()->get('ux_default_separate_activities_per_contact');
  if($formName == "CRM_Activity_Form_Activity" && $separateActivitiesByDefault) {
    // Check if the 'separation' radio button exists and remove it
    if ($form->elementExists('separation')) {
      // Remove the 'separation' radio button
      $form->removeElement('separation');
      // Add a hidden field with the default value
      $form->add('hidden', 'separation', 'separate');
    }
  }

  $formsList = CRM_Ux_Form_Settings::formsList();

  if (in_array($formName, $formsList) && interface_exists('API_Wrapper')) {
    CRM_Ux_APIWrapper::$ux_opened_form = $formName;
  }

}

/**
 * Implements hook_civicrm_apiWrappers().
 */
function ux_civicrm_apiWrappers(&$wrappers, $apiRequest) {
  if (!interface_exists('API_Wrapper')) return;

  if ($apiRequest['entity'] == 'Contact' && $apiRequest['action'] == 'getlist') {
    $wrappers[] = new CRM_Ux_APIWrapper();
  }

  if ($apiRequest['entity'] == 'Contact' && $apiRequest['action'] == 'getquick') {
    $wrappers[] = new CRM_Ux_APIWrapperGetQuick();
  }
}

/**
 * Manipulates CiviCRM menu.
 *
 * @param $menu
 */
function ux_civicrm_navigationMenu(&$menu) {
  _ux_civix_insert_navigation_menu($menu, 'Support', array(
    'label' => 'Agileware Support',
    'name' => 'Agileware Support',
    'url'   => 'https://support.agileware.com.au',
  ));

  _ux_civix_insert_navigation_menu($menu, 'Administer', array(
    'label' => E::ts('Advanced Search Preferences'),
    'name' => 'AdvancedSearchPreferences',
    'url' => 'civicrm/ux/searchpreferences/settings',
    'permission' => 'administer CiviCRM',
    'operator' => 'OR',
    'separator' => 0,
  ));
  $menuPermission = CRM_Core_Permission::check('Customize Menu');

  foreach ($menu as $menuItemId => $menuItem) {
    if ($menuItem['attributes']['name'] == 'Search') {
      $childItems = $menuItem['child'];
      $childItemNamesToRemove = ['Full-text Search'];
      removeChildItems($childItems, $childItemNamesToRemove);
      $menu[$menuItemId]['child'] = $childItems;
    }
    if ($menuItem['attributes']['name'] == 'Support') {
      $childItems = $menuItem['child'];
      $childItemNamesToRemove = ['Get started', 'Ask a question', 'Get expert help', 'About CiviCRM', 'Register your site', 'Join CiviCRM'];
      removeChildItems($childItems, $childItemNamesToRemove);
      $menu[$menuItemId]['child'] = $childItems;
    }
    if (!$menuPermission && $menuItem['attributes']['name'] == 'Administer') {
      $childItems = $menuItem['child'];
      _ux_remove_navigation_menu_option($childItems);
      $menu[$menuItemId]['child'] = $childItems;
    }
  }
}

/**
 * Removing Navigation Menu if permission not granted for logged in user.
 * @param $menu
 */
function _ux_remove_navigation_menu_option(&$menu) {
  foreach ($menu as $menuItemId => $menuItem) {
    if ($menuItem['attributes']['name'] == 'Customize Data and Screens') {
      $childItems = $menuItem['child'];
      $childItemNamesToRemove = ['Navigation Menu'];
      removeChildItems($childItems, $childItemNamesToRemove);
      $menu[$menuItemId]['child'] = $childItems;
    }
  }
}

function removeChildItems(&$childItems, $childItemNamesToRemove) {
  $childItemIdsToRemove = array();
  foreach ($childItems as $childItemId => $childItem) {
    $childItemName = $childItem['attributes']['name'];
    if (in_array($childItemName, $childItemNamesToRemove)) {
      $childItemIdsToRemove[] = $childItemId;
    }
  }

  foreach ($childItemIdsToRemove as $childItemToRemove) {
    unset($childItems[$childItemToRemove]);
  }
}

/**
 * Restore communityMessagesUrl in settings.
 *
 * @param $page
 */
function ux_civicrm_pageRun(&$page) {
  if ($page instanceof CRM_Admin_Page_Navigation) {
    $menuPermission = CRM_Core_Permission::check('Customize Menu');
    if (!$menuPermission) {
      _ux_throw_access_denied_exception();
    }
  }
  elseif (
    ($page instanceof CRM_Contact_Page_View)
    && class_exists('CRM_Civicase_Page_ContactCaseTab')
    && ((function_exists('_shoreditch_isActive') && !_shoreditch_isActive()) ||
        (function_exists('_theIsland_isActive') && !_theIsland_isActive()))
  ) {
    $themekey = function_exists('_theIsland_isActive') ? 'theisland' : 'org.civicrm.shoreditch';
    // Nabbed from shoreditch. When updating this list, DO NOT include css/custom-civicrm.css
    CRM_Core_Resources::singleton()->addStyleFile($themekey, 'css/bootstrap.css', -50, 'html-header');
    CRM_Core_Resources::singleton()->addScriptFile($themekey, 'base/js/transition.js', 1000, 'html-header');
    CRM_Core_Resources::singleton()->addScriptFile($themekey, 'base/js/scrollspy.js', 1000, 'html-header');
    CRM_Core_Resources::singleton()->addScriptFile($themekey, 'base/js/dropdown.js', 1000, 'html-header');
    CRM_Core_Resources::singleton()->addScriptFile($themekey, 'base/js/collapse.js', 1000, 'html-header');
    CRM_Core_Resources::singleton()->addScriptFile($themekey, 'base/js/modal.js', 1000, 'html-header');
    CRM_Core_Resources::singleton()->addScriptFile($themekey, 'base/js/tab.js', 1000, 'html-header');
    CRM_Core_Resources::singleton()->addScriptFile($themekey, 'base/js/button.js', 1000, 'html-header');
  }

  _ux_remove_status_warning_footer();
}

/**
 * Throws access denied exception.
 * @throws \Civi\API\Exception\UnauthorizedException
 */
function _ux_throw_access_denied_exception() {
  throw new \Civi\API\Exception\UnauthorizedException(ts('%1', [1 => "You don't have permission to access this page."]));
}

/**
 * @param $formName
 * @param $form
 */
function ux_civicrm_preProcess($formName, &$form) {
  _ux_remove_status_warning_footer();
}

function _ux_remove_status_warning_footer() {
  $template = CRM_Core_Smarty::singleton();
  $template->assign('footer_status_severity', '');
  $template->assign('footer_status_message', '');
}

/**
 * @param $content
 * @param $context
 * @param $tplName
 * @param $object
 */
function ux_civicrm_alterContent(&$content, $context, $tplName, &$object) {
  _ux_change_content($content, E::ts('Powered by CiviCRM'), E::ts('Powered by Agileware CiviCRM'));
  _ux_change_content($content, "<a href=\"https://civicrm.org/download\">" . E::ts('Download CiviCRM.') . "</a>", '');
  _ux_change_content($content, "<a href=\"https://lab.civicrm.org/groups/dev/-/issues\">" . E::ts('View issues and report bugs.') . "</a>", '<a href="https://support.agileware.com.au" target="_blank">Contact Agileware support.</a>');
  _ux_change_content($content, 'https://download.civicrm.org/about/', 'https://github.com/agileware/civicrm-core/releases/tag/');
}

/**
 * Find and replace content from renders page/form
 *
 * @param $content
 * @param $find
 * @param $replace
 */
function _ux_change_content(&$content, $find, $replace) {
  $content = str_replace($find, $replace, $content);
}

/**
 * Change the default Display Preferences when extension installed.
 */
function _ux_update_settings() {
  $viewOptions = Civi::settings()->get('contact_view_options');
  $viewOptions = explode(CRM_Core_DAO::VALUE_SEPARATOR, $viewOptions);
  $viewOptions[] = 14;
  $viewOptions = implode(CRM_Core_DAO::VALUE_SEPARATOR, $viewOptions);
  Civi::settings()->set('contact_view_options', $viewOptions);

  $editOptions = Civi::settings()->get('contact_edit_options');
  $editOptions = explode(CRM_Core_DAO::VALUE_SEPARATOR, $editOptions);
  /*
   * Disable the following options:
   * 4: Other Panes, Notes option
   * 6: Other Panes, Tags and Groups option
   * 9: Contact Details, Instant Messenger option
   */
  $disableOptions = array(9, 4, 6);

  foreach ($disableOptions as $disableOption) {
    if ($key = array_search($disableOption, $editOptions)) {
      unset($editOptions[$key]);
    }
  }
  $editOptions = implode(CRM_Core_DAO::VALUE_SEPARATOR, $editOptions);
  Civi::settings()->set('contact_edit_options', $editOptions);
  Civi::settings()->set('activity_assignee_notification',FALSE);
}

/**
 * Implements hook_civicrm_permission().
 *
 */
function ux_civicrm_permission(&$permissions) {
  $permissions['CSV Export'] = [
    'label' => E::ts('CiviCRM: CSV Export'),
    'description' => E::ts('Grants permission to export data from CSV.'),
  ];
  $permissions['CSV Import'] = [
    'label' => E::ts('CiviCRM: CSV Import'),
    'description' => E::ts('Grants permission to import data from CSV.'),
  ];
  $permissions['Customize Menu'] = [
    'label' => E::ts('CiviCRM: Customize Menu'),
    'description' => E::ts('Grants permission to customise the CiviCRM menu.'),
  ];
}

/**
 * Enforce shoreditch theme on CiviCase extension pages.
 * @param &$theme
 * @param $context
 */
function ux_civicrm_activeTheme(&$theme, $context) {
  switch($context['page']) {
    case 'civicrm/case/a':
    case 'civicrm/workflow/a':
      $theme = 'shoreditch';
      break;
    default:
      break;
  }
}

function ux_civicrm_alterBundle($bundle) {
  if($bundle->name == 'coreStyles') {
    $bundle->addStyleFile(E::LONG_NAME, 'css/overrides.css', 10);
  }
}

/* Exception handler to override the CiviCRM temporary error scope with
 * something that doesn't spam the log. */
function _ux_passErrorAsException($pearError) {
  throw new PEAR_Exception($pearError->getMessage(), $pearError);
}

/**
 * Implements hook_civicrm_container()
 *
 * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
 *
 * @return void
 */
function ux_civicrm_container( ContainerBuilder $container) {
  $container->addResource(new \Symfony\Component\Config\Resource\FileResource(__FILE__));
  $container
    ->setDefinition('civi.ux.recurring', new Definition('\Civi\Ux\Recurring'))
    ->addTag('kernel.event_subscriber')
    ->setPublic(TRUE);
  $container
    ->setDefinition('civi.ux.alter', new Definition('\Civi\Ux\Alter'))
    ->addTag('kernel.event_subscriber')
    ->setPublic(TRUE);
}
