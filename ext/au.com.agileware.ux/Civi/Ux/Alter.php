<?php
namespace Civi\Ux;

use Civi\Core\Event\PreEvent;
use Civi\Core\Event\GenericHookEvent;
use Civi\Ux\Validation\Email;
use Civi\Ux\Validation\Mailing;
use Civi\Ux\Validation\MessageTemplates;
use Civi\Ux\Validation\ScheduleReminders;
use Civi\FlexMailer\Event\CheckSendableEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class Alter implements EventSubscriberInterface {
  public static function getSubscribedEvents() {
    $events = [];
    if (\Civi::settings()->get('ux_auto_relationship_date')) {
      $events['hook_civicrm_pre::Relationship'] = 'relationshipPre';
    }

    // Checksum token validation before form save
    $events['hook_civicrm_buildForm'] = 'checksumValidation';
    $events[\Civi\FlexMailer\Validator::EVENT_CHECK_SENDABLE] = 'onCheckSendable';
    $events['hook_civicrm_idsException'] = 'idsException';

    return $events;
  }

  private $entities = [];

  protected function loadEntity(PreEvent $event) {
    if(!$event->id || !$event->entity) {
      return null;
    }

    if(!empty($this->entities[$event->entity][$event->id])) {
      return $this->entities[$event->entity][$event->id];
    }

    try {
      $this->entities[$event->entity][$event->id] = civicrm_api4( $event->entity, 'get', [ 'where' => [[ 'id', '=', $event->id ]] ] )->first();
      return $this->entities[$event->entity][$event->id];
    }
    catch(\Exception $e) {
      return NULL;
    }
  }

  public function relationshipPre(PreEvent $event) {
    switch($event->action) {
      case 'create':
      case 'edit':
        $newActive = $event->params['is_active'];
        if(is_null($newActive)) {
          break;
        }

        $active = $this->loadEntity($event)['is_active'] ?? 0;

        $now = date('YmdHis');

        // On change from disabled to enabled
        if($newActive && !$active) {
          if (($event->params['end_date'] ?? 0) <= $now) {
            // Remove end date if not in the future.
            $event->params['end_date'] = '';
          }
          if (empty($event->params['start_date'])
              && empty($this->loadEntity($event)['start_date'])) {
            // Implicit start date of now if not already set
            $event->params['start_date'] = $now;
          }
        }
        // On change from enabled to disabled
        elseif ($active && !$newActive
                && empty($event->params['end_date'])
                && empty($this->loadEntity($event)['end_date'])) {
          // Implicit end date of now if not already set
          $event->params['end_date'] = $now;
        }
      break;
      default:
        break;
    }
  }

  public function checksumValidation(GenericHookEvent $event) {
    // Check form name for class, then call our corresponding class to add the validation rule
    $formName = $event->formName;
    switch ($formName) {
      case 'CRM_Admin_Form_MessageTemplates':
        MessageTemplates::buildForm($event);
        break;
      case 'CRM_Admin_Form_ScheduleReminders':
        ScheduleReminders::buildForm($event);
        break;
      case 'CRM_Contact_Form_Task_Email': // Activity Send Email
        Email::buildForm($event);
        break;
    }
  }

  // For Mailings, which define CheckSendableEvent and onCheckSendable()
  public function onCheckSendable(CheckSendableEvent $event) {
    Mailing::onCheckSendable($event);
  }

  public function idsException(GenericHookEvent $event) {
    $event->skip[] = 'civicrm/asset/builder';
  }
}
