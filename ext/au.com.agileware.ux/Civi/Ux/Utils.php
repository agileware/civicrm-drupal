<?php

namespace Civi\Ux;

use Civi\FlexMailer\Event\CheckSendableEvent;

class Utils {
  private static $blacklisted = ['cid=', 'cs='];

  /**
  * Implement validation for incorrect checksum usage.
  * Prevent the form from saving if checksum and contact ID are being used, instead of their respective CiviCRM tokens.
  */
  public static function validateChecksumTokens($html, $field) {
    $errors = [];

    // Extract URLs from html
    $urlPattern = '/\bhttps?:\/\/[^\s<>"\']+|www\.[^\s<>"\']+/i';
    preg_match_all($urlPattern, $html, $matches);
    $bodyUrls = $matches[0];

    foreach ($bodyUrls as $url) {
      $error = strpos($url, self::$blacklisted[0]) !== false && strpos($url, self::$blacklisted[1]) !== false;

      if ($error && !isset($errors[$url])) {
        $errors[$url] = 1;
      }
      else if ($error && isset($errors[$url])) {
        $errors[$url] = $errors[$url] + 1;
      }
    }

    return $errors;
  }
}
