<?php

namespace Civi\Ux\Validation;

use Civi\FlexMailer\Event\CheckSendableEvent;
use CRM_Flexmailer_ExtensionUtil as E;
use Civi\Ux\Utils;

class Mailing {

  public static function onCheckSendable(CheckSendableEvent $event) {
    $errors = [];

    foreach (['body_html', 'body_text'] as $field) {
      $problems = [];

      $str = $event->getFullBody($field);
      if (empty($str)) {
        continue;
      }
      $problems = Utils::validateChecksumTokens($str, $field);

      if (!empty($problems)) {
        $event->setError("$field", E::ts('%1',
          [1 => $problems[$field]]
        ));

        $errors = array_merge($errors, $problems);
      }
    }

    if (!empty($errors)) {
      $message = '<ul>';
      foreach ($errors as $error => $count) {
        $message .= "<li>{$error}" . ($count > 1 ? " <strong>x{$count}</strong>" : "") . "</li>";
      }
      $message .= '</ul>';

      throw new \CRM_Core_Exception(ts(
        '<p>Mailing cannot be sent. The following URLs in your content contain errors:</p>
          %1
        <p>Please change URLs to use proper CiviCRM checksum tokens: <strong>https://yourdomain.com/path-to-your-page/?{contact.checksum}&cid={contact.contact_id}</strong></p>',
        [1 => $message]), 'cannot-send', $errors);
    }
  }

}
