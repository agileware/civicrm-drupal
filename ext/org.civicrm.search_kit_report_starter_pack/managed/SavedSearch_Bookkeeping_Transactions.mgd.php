<?php
use CRM_SearchKitReports_ExtensionUtil as E;

// Check CiviContribute is enabled
$entity = \Civi\Api4\Entity::get(FALSE)
  ->addWhere('name', '=', 'Contribution')->execute();
if (!$entity->count()) {
  return [];
}

return [
  [
    'name' => 'SavedSearch_Bookkeeping_Transactions',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Bookkeeping_Transactions',
        'label' => E::ts('Bookkeeping Transactions'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Contribution',
        'api_params' => [
          'version' => 4,
          'select' => [
            'Contribution_Contact_contact_id_01.display_name',
            'Contribution_FinancialType_financial_type_id_01_FinancialType_EntityFinancialAccount_FinancialAccount_01.accounting_code',
            'financial_type_id:label',
            'receive_date',
            'contribution_status_id:label',
            'id',
            'Contribution_EntityFinancialTrxn_FinancialTrxn_01.check_number',
            'Contribution_EntityFinancialTrxn_FinancialTrxn_01.payment_instrument_id:label',
            'Contribution_EntityFinancialTrxn_FinancialTrxn_01.trxn_date',
            'Contribution_EntityFinancialTrxn_FinancialTrxn_01.trxn_id',
            'Contribution_EntityFinancialTrxn_FinancialTrxn_01.amount',
          ],
          'orderBy' => [],
          'where' => [
            [
              'contribution_status_id:name',
              '=',
              'Completed',
            ],
            [
              'Contribution_EntityFinancialTrxn_FinancialTrxn_01.status_id:name',
              '=',
              'Completed',
            ],
          ],
          'groupBy' => [],
          'join' => [
            [
              'Contact AS Contribution_Contact_contact_id_01',
              'LEFT',
              [
                'contact_id',
                '=',
                'Contribution_Contact_contact_id_01.id',
              ],
            ],
            [
              'FinancialTrxn AS Contribution_EntityFinancialTrxn_FinancialTrxn_01',
              'LEFT',
              'EntityFinancialTrxn',
              [
                'id',
                '=',
                'Contribution_EntityFinancialTrxn_FinancialTrxn_01.entity_id',
              ],
              [
                'Contribution_EntityFinancialTrxn_FinancialTrxn_01.entity_table',
                '=',
                "'civicrm_contribution'",
              ],
            ],
            [
              'FinancialType AS Contribution_FinancialType_financial_type_id_01',
              'LEFT',
              [
                'financial_type_id',
                '=',
                'Contribution_FinancialType_financial_type_id_01.id',
              ],
            ],
            [
              'FinancialAccount AS Contribution_FinancialType_financial_type_id_01_FinancialType_EntityFinancialAccount_FinancialAccount_01',
              'LEFT',
              'EntityFinancialAccount',
              [
                'Contribution_FinancialType_financial_type_id_01.id',
                '=',
                'Contribution_FinancialType_financial_type_id_01_FinancialType_EntityFinancialAccount_FinancialAccount_01.entity_id',
              ],
              [
                'Contribution_FinancialType_financial_type_id_01_FinancialType_EntityFinancialAccount_FinancialAccount_01.entity_table',
                '=',
                "'civicrm_financial_type'",
              ],
              [
                'Contribution_FinancialType_financial_type_id_01_FinancialType_EntityFinancialAccount_FinancialAccount_01.account_relationship:name',
                '=',
                '"Income Account is"',
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
