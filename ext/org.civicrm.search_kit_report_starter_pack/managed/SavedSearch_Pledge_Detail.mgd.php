<?php
use CRM_SearchKitReports_ExtensionUtil as E;

// Check CiviContribute and CiviPledge are enabled
$entity = \Civi\Api4\Entity::get(FALSE)
  ->addWhere('name', 'IN', ['Contribution', 'Pledge'])->execute();
if ($entity->count() < 2) {
  return [];
}

return [
  [
    'name' => 'SavedSearch_Pledge_Detail',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Pledge_Detail',
        'label' => E::ts('Pledge Detail'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Pledge',
        'api_params' => [
          'version' => 4,
          'select' => [
            'contact_id.display_name',
            'amount',
            'GROUP_CONCAT(DISTINCT status_id:label) AS GROUP_CONCAT_status_id_label',
            'SUM(Pledge_PledgePayment_pledge_id_01_PledgePayment_Contribution_contribution_id_01.total_amount) AS SUM_Pledge_PledgePayment_pledge_id_01_PledgePayment_Contribution_contribution_id_01_total_amount',
          ],
          'orderBy' => [],
          'where' => [],
          'groupBy' => [
            'contact_id.display_name',
          ],
          'join' => [
            [
              'Contact AS Pledge_Contact_contact_id_01',
              'LEFT',
              [
                'contact_id',
                '=',
                'Pledge_Contact_contact_id_01.id',
              ],
            ],
            [
              'PledgePayment AS Pledge_PledgePayment_pledge_id_01',
              'LEFT',
              [
                'id',
                '=',
                'Pledge_PledgePayment_pledge_id_01.pledge_id',
              ],
            ],
            [
              'Contribution AS Pledge_PledgePayment_pledge_id_01_PledgePayment_Contribution_contribution_id_01',
              'LEFT',
              [
                'Pledge_PledgePayment_pledge_id_01.contribution_id',
                '=',
                'Pledge_PledgePayment_pledge_id_01_PledgePayment_Contribution_contribution_id_01.id',
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
