<?php
use CRM_SearchKitReports_ExtensionUtil as E;

return [
  [
    'name' => 'SavedSearch_Activity_Summary',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Activity_Summary',
        'label' => E::ts('Activity Summary'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Activity',
        'api_params' => [
          'version' => 4,
          'select' => [
            'activity_type_id:label',
            'GROUP_CONCAT(DISTINCT status_id:label) AS GROUP_CONCAT_status_id_label',
            'SUM(duration) AS SUM_duration',
            'COUNT(id) AS COUNT_id',
          ],
          'orderBy' => [],
          'where' => [],
          'groupBy' => [
            'activity_type_id',
          ],
          'join' => [],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
