<?php
use CRM_SearchKitReports_ExtensionUtil as E;

// Check CiviContribute is enabled
$entity = \Civi\Api4\Entity::get(FALSE)
  ->addWhere('name', '=', 'Contribution')->execute();
if (!$entity->count()) {
  return [];
}

return [
  [
    'name' => 'SavedSearch_Contributions_by_Household',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Contributions_by_Household',
        'label' => E::ts('Contributions by Household'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Contribution',
        'api_params' => [
          'version' => 4,
          'select' => [
            'Contribution_Contact_contact_id_01_Contact_RelationshipCache_Contact_01.display_name',
            'Contribution_Contact_contact_id_01.display_name',
            'total_amount',
            'contribution_status_id:label',
            'receive_date',
          ],
          'orderBy' => [],
          'where' => [
            [
              'contribution_status_id:name',
              '=',
              'Completed',
            ],
          ],
          'groupBy' => [],
          'join' => [
            [
              'Contact AS Contribution_Contact_contact_id_01',
              'LEFT',
              [
                'contact_id',
                '=',
                'Contribution_Contact_contact_id_01.id',
              ],
            ],
            [
              'Contact AS Contribution_Contact_contact_id_01_Contact_RelationshipCache_Contact_01',
              'INNER',
              'RelationshipCache',
              [
                'Contribution_Contact_contact_id_01.id',
                '=',
                'Contribution_Contact_contact_id_01_Contact_RelationshipCache_Contact_01.far_contact_id',
              ],
              [
                'Contribution_Contact_contact_id_01_Contact_RelationshipCache_Contact_01.near_relation:name',
                '=',
                '"Head of Household is"',
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
