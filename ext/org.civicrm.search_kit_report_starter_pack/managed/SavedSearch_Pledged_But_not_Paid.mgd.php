<?php
use CRM_SearchKitReports_ExtensionUtil as E;

// Check CiviPledge is enabled
$entity = \Civi\Api4\Entity::get(FALSE)
  ->addWhere('name', '=', 'Pledge')->execute();
if (!$entity->count()) {
  return [];
}

return [
  [
    'name' => 'SavedSearch_Pledged_But_not_Paid',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Pledged_But_not_Paid',
        'label' => E::ts('Pledged But not Paid'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'PledgePayment',
        'api_params' => [
          'version' => 4,
          'select' => [
            'PledgePayment_Pledge_pledge_id_01_Pledge_Contact_contact_id_01.display_name',
            'PledgePayment_Pledge_pledge_id_01.amount',
            'scheduled_date',
            'scheduled_amount',
            'status_id:label',
            'PledgePayment_Pledge_pledge_id_01.financial_type_id:label',
          ],
          'orderBy' => [],
          'where' => [
            [
              'scheduled_date',
              '<=',
              'now',
            ],
            [
              'status_id:name',
              'IN',
              [
                'Pending',
                'Overdue',
              ],
            ],
          ],
          'groupBy' => [],
          'join' => [
            [
              'Pledge AS PledgePayment_Pledge_pledge_id_01',
              'LEFT',
              [
                'pledge_id',
                '=',
                'PledgePayment_Pledge_pledge_id_01.id',
              ],
            ],
            [
              'Contact AS PledgePayment_Pledge_pledge_id_01_Pledge_Contact_contact_id_01',
              'LEFT',
              [
                'PledgePayment_Pledge_pledge_id_01.contact_id',
                '=',
                'PledgePayment_Pledge_pledge_id_01_Pledge_Contact_contact_id_01.id',
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
