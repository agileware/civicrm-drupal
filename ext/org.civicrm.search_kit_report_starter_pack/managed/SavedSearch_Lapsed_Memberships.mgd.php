<?php
use CRM_SearchKitReports_ExtensionUtil as E;

// Check CiviMember is enabled
$entity = \Civi\Api4\Entity::get(FALSE)
  ->addWhere('name', '=', 'Membership')->execute();
if (!$entity->count()) {
  return [];
}

return [
  [
    'name' => 'SavedSearch_Lapsed_Memberships',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Lapsed_Memberships',
        'label' => E::ts('Lapsed Memberships'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Membership',
        'api_params' => [
          'version' => 4,
          'select' => [
            'Membership_Contact_contact_id_01.display_name',
            'membership_type_id:label',
            'end_date',
            'status_id:label',
            'Membership_Contact_contact_id_01.address_primary.country_id:label',
          ],
          'orderBy' => [],
          'where' => [
            [
              'status_id:name',
              'IN',
              [
                'Expired',
                'Cancelled',
                'Deceased',
              ],
            ],
          ],
          'groupBy' => [],
          'join' => [
            [
              'Contact AS Membership_Contact_contact_id_01',
              'LEFT',
              [
                'contact_id',
                '=',
                'Membership_Contact_contact_id_01.id',
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
