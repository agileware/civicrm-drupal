<?php
use CRM_SearchKitReports_ExtensionUtil as E;

// Check CiviContribute is enabled
$entity = \Civi\Api4\Entity::get(FALSE)
  ->addWhere('name', '=', 'Contribution')->execute();
if (!$entity->count()) {
  return [];
}

return [
  [
    'name' => 'SavedSearch_Contribution_Details',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Contribution_Details',
        'label' => E::ts('Contribution Details'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Contribution',
        'api_params' => [
          'version' => 4,
          'select' => [
            'Contribution_Contact_contact_id_01.display_name',
            'Contribution_Contact_contact_id_01.email_primary.email',
            'Contribution_Contact_contact_id_01.phone_primary.phone',
            'financial_type_id:label',
            'receive_date',
            'total_amount',
          ],
          'orderBy' => [],
          'where' => [
            [
              'contribution_status_id:name',
              '=',
              'Completed',
            ],
            [
              'Contribution_Contact_contact_id_01.is_deleted',
              '=',
              FALSE,
            ],
          ],
          'groupBy' => [],
          'join' => [
            [
              'Contact AS Contribution_Contact_contact_id_01',
              'INNER',
              [
                'contact_id',
                '=',
                'Contribution_Contact_contact_id_01.id',
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
