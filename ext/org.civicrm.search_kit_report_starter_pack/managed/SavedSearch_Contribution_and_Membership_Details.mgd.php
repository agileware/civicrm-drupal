<?php
use CRM_SearchKitReports_ExtensionUtil as E;

// Check CiviContribute and CiviMember are enabled
$entity = \Civi\Api4\Entity::get(FALSE)
  ->addWhere('name', 'IN', ['Contribution', 'Membership'])->execute();
if ($entity->count() < 2) {
  return [];
}

return [
  [
    'name' => 'SavedSearch_Contribution_and_Membership_Details',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Contribution_and_Membership_Details',
        'label' => E::ts('Contribution and Membership Details'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Membership',
        'api_params' => [
          'version' => 4,
          'select' => [
            'Membership_Contact_contact_id_01.display_name',
            'Membership_Contact_contact_id_01.email_primary.email',
            'Membership_Contact_contact_id_01.phone_primary.phone',
            'Membership_Contact_contact_id_01_Contact_Contribution_contact_id_01.financial_type_id:label',
            'Membership_Contact_contact_id_01_Contact_Contribution_contact_id_01.receive_date',
            'Membership_Contact_contact_id_01_Contact_Contribution_contact_id_01.total_amount',
            'membership_type_id:label',
            'start_date',
            'end_date',
            'status_id:label',
            'Membership_Contact_contact_id_01.address_primary.country_id:label',
          ],
          'orderBy' => [],
          'where' => [],
          'groupBy' => [],
          'join' => [
            [
              'Contact AS Membership_Contact_contact_id_01',
              'LEFT',
              [
                'contact_id',
                '=',
                'Membership_Contact_contact_id_01.id',
              ],
            ],
            [
              'Contribution AS Membership_Contact_contact_id_01_Contact_Contribution_contact_id_01',
              'LEFT',
              [
                'Membership_Contact_contact_id_01.id',
                '=',
                'Membership_Contact_contact_id_01_Contact_Contribution_contact_id_01.contact_id',
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
