<?php

require_once 'search_kit_report_starter_pack.civix.php';

use CRM_SearchKitReportStarterPack_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function search_kit_report_starter_pack_civicrm_config(&$config): void {
  _search_kit_report_starter_pack_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function search_kit_report_starter_pack_civicrm_install(): void {
  _search_kit_report_starter_pack_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function search_kit_report_starter_pack_civicrm_enable(): void {
  _search_kit_report_starter_pack_civix_civicrm_enable();
}
