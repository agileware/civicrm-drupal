# Inferred Tags

Allows selected tagsets' tags to be inherited via relationships.

![Diagram](/docs/diagram.svg)

Please see documentation under [/doc/index.md](https://lab.civicrm.org/extensions/inferredtags/-/tree/master/docs)

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.1+
* CiviCRM 5.28+

## Installation (Web UI)

This extension has not yet been published for installation via the web UI.

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl inferredtags@https://github.com/FIXME/inferredtags/archive/master.zip
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://github.com/FIXME/inferredtags.git
cv en inferredtags
```

