<?php
use CRM_Inferredtags_ExtensionUtil as E;

/**
 * Job.Updateinferredtags API specification
 *
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 *
 * @see https://docs.civicrm.org/dev/en/latest/framework/api-architecture/
 */
function _civicrm_api3_job_Updateinferredtags_spec(&$spec) {
  $spec['tagset_name']['description'] = 'Which tagset to update. If not specified, update all configured tagsets.';
}

/**
 * Job.Updateinferredtags API
 *
 * @param array $params
 *
 * @return array
 *   API result descriptor
 *
 * @see civicrm_api3_create_success
 *
 * @throws API_Exception
 */
function civicrm_api3_job_Updateinferredtags($params) {


  $inferredTags = new CRM_InferredTags();

  // xxx temp. Configure our thing.
  //$inferredTags->saveConfig([
  //  'tagsets' => [
  //    'Region' => [
  //    'aSide' => [ 5, 6, 9, 10, 119, 120, 121, 122, 123, 125, 126, 128, 129, 130, 131, 132, 133, 134, 135, 140, 141, 142, 143, 144, 145, 149, 150, 151, 168, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 182, 183, 184, 185, 186, 187 ],
  //    'bSide' => [ ],
  //  ]]
  //]);


  if (empty($params['tagset_name'])) {
    $config = $inferredTags->getConfig();
    $tagsets = array_keys($config['tagsets']);
  }
  else {
    $tagsets = (array) $params['tagset_name'];
  }

  try {
    $results = [];
    foreach ($tagsets as $tagset_name) {
      $results[$tagset_name] = $inferredTags->processTagSet($tagset_name);
    }
  }
  catch (InvalidArgumentException $e) {
    throw new API_Exception($e->getMessage());
  }
  catch (RuntimeException $e) {
    throw new API_Exception($e->getMessage());
  }

  return civicrm_api3_create_success($results, $params, 'Job', 'Inferregions');
}
