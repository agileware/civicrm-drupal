# Inferred tags

This extension allows you to configure **tagsets** whose tags can be
inherited via relationships.

An example should make this clearer.

![diagram showing how it works](./diagram.svg)


<a href="https//peopleandplanet.org">People & Planet</a>, the awesome UK
grassroots student campaigning network, has the following contact types:

- student activists
- local campaign groups
- universities and colleges

Universities and colleges are organised by tags into geographical regions,
like *Wales*, or *Northern England*. They have these tags in a specific
**tagset** called *Region*, which enables easy searching and clear
identification.

Contacts are linked by relationships like so:

- Wilma *is contact for* Action for Undoing Borders.
- Action for Undoing Borders *has contact* Wilma
- Action for Undoing Borders *is part of* Sheffield University
- Sheffield University *includes* Action for Undoing Borders

With this extension properly configured and with the region set for
Sheffield University to "Northern England"

- Action for Undoing Borders now has the "Northern England" tag inferred
  upon it from the university.
- Wilma now has the "Northern England" tag inferred, from the campaign
  group.

If Sheffield changes region, those changes will be propagated when the
update job is run. The changes are made recursively.

## Six degrees of separation

It's easy to find that all your contacts are related, once you start
following the web of relationships! e.g. say Sheffield Uni has another
student, Betty, who was a student there but is now a lecturer at Newman
University in London. If you don't limit how it follows relationships,
then it will go *Wilma » Campaign Group » Sheffield » Betty » Newman* and
so Wilma inherits the London region, too.

So for each tagset you choose which relationships should allow
inheritance, and *which way*. For instance in this situation we might want
to inherit along:

- *is contact for* (but not *has contact*)
- *is part of* (but not *includes*)
- and others like *is student at* (but not *has student*)

With those restrictions in place, Wilma's inferred tags will be more
helpful.

## Inactive and expired relationships

By default, these relationships are not followed when looking for tags,
but you can choose to include either, should you need to: it's a setting
for each tagset.

An "expired" relationship is one where there are start/end dates set that
the current date does not fall within. If a start/end date is not set,
it's assumed OK, e.g. if the start date is today or in the past, and there
is no end date set, it's assumed that relationship is NOT expired.
However, relationships with an end date set that is before today, would be
treated as expired, and so would any relationships that have a future
start date.

An "inactive" relationship is one that has had it's "is active" checkbox
un-checked.

!!! note
    CiviCRM's Contact screens include expired relationships as "inactive", yet
    they may still have "✔ is active" set.

## What about manual override?

The algorithm is there to serve you, not the other way around. If you want
to set a contact's tags yourself, just do that and remove the special
*Inferred* tag it and it will stop inferring tags.

## Auto-infer: How to treat contacts without any tags

If a contact does not have any tags in a tagset configured to use this
extension, then you have a choice: you can treat the contact as needing
inferred tags, or not.

By default it will assume that such contacts *should* have inferred tags
added, but you can turn that off with a setting for that tagset. If you do
turn it off then contacts without any tags won't get any inferred tags
added.

## When does it update?

There's a scheduled job that runs daily by default. As with all scheduled
jobs, you can trigger it yourself from **Administer » System Settings
» Scheduled Jobs** should you feel the need.

## Enhancements

Currently there's a hard-coded 10-hop limit on recursion. So tags from
a contact related by 11 hops won't be inferred. Typically it only takes 3 or
4 iterations anyway so this limit is thought of as an emergency/common sense
back-stop. That could be exposed as a setting.

It would be nice if the inferred tags could update whenever something
pertinent changes. However we have to consider performance.

In my testing, on a database of about 150k contacts, with no limits on
relationships, it took about 15s to apply around 70k inferred tags. With
real-world filters on relationship types (but still including expired and
inactive relationships) it took about 8s to apply 45k tags. Once I excluded
expired and inactive, as you would normally want to do, it reduced the
processing to 1.8s.

That's not slow in terms of a useful background cron job, but it is slow
in terms of a user waiting for the UI to respond after they saved
a relationship or added/removed a tag. Likewise, if the update were
triggered by such a change then bulk changes of N records could result in
it being triggered N times! So it seems sensible to accept the trade off
of lag vs. performance. You could always change your scheduled job to run
every time cron does. Technical people might care to know that edits are
done in a transaction, which is great because it means searches on the
data during update will still work, but is potentially less great in terms
of locking delays - e.g. writing a tag change during an update.

Ideas welcome, please use the [Issues list](https://lab.civicrm.org/extensions/inferredtags/-/issues).

## Permissions, ACLs, hooks

- The configuration screen requires "**administer Tagsets**" permission.

- ACLs are **ignored** by this extension.

- Tag updates are done at table level by SQL, which is a performance
  requirement and which means no **hooks are fired**. This is required for
  efficiency. A bit like smart groups
  - you don't get hooks fired everytime those members are updated by running
  the criteria.
