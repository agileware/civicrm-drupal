<?php

class CRM_InferredTags {

  /** @var CRM_InferredTags */
  public static $singleton;

  /** @var Array */
  public static $config;

  /**
   * Get singleton.
   */
  public static function instance($reset = FALSE) {
    if ($reset || !isset(static::$singleton)) {
      static::$singleton = new static();
    }
    return static::$singleton;
  }

  /**
   *
   */
  public function __construct() {
    if (!isset(static::$config)) {
      $this->loadConfig();
    }
  }

  /**
   * Load config from settings.
   *
   * Example config:
   * {
   *   "tagsets": {
   *     "Region": {
   *       "aSide": [ 2, 34, 23, 98 ],
   *       "bSide": [ 56 ],
   *       "autoInfer": false, // optional, default true
   *       "includeExpired": false, // optional
   *       "includeInactive": false, // optional
   *     },
   *     ...
   *   }
   * }
   */
  public function loadConfig() {
    $config = json_decode(Civi::settings()->get('inferredtags'), TRUE) ?? [];
    if (!$config) {
      $config = [
        'tagsets' => ['tagsets' => []],
      ];
    }
    static::$config = $config;
  }

  /**
   * Save config to settings.
   *
   * Example config (see loadConfig)
   */
  public function saveConfig($config) {
    Civi::settings()->set('inferredtags', json_encode($config));
    static::$config = $config;
  }

  /**
   * (re-)create the inferred tags.
   *
   * @param string $tagSetName
   */
  public function processTagSet($tagSetName) {

    if (empty(static::$config['tagsets'][$tagSetName])) {
      throw new InvalidArgumentException("The tagset $tagSetName is not configured.");
    }
    $tagsetConfig = static::$config['tagsets'][$tagSetName];

    // Identify the tags.
    $regionTagsetTagID = (int) array_search($tagSetName, CRM_Core_BAO_Tag::getTagSet('civicrm_contact'));
    if (!$regionTagsetTagID) {
      throw new InvalidArgumentException("Failed to find the $tagSetName tagset! This should not happen.");
    }

    $regionTags = [];
    CRM_Core_BAO_Tag::getTags('civicrm_contact', $regionTags, $regionTagsetTagID);

    // Find the 'Inferrerd' tag, and remove it from the region tags.
    $inferredRegionTagID = (int) array_search('Inferrerd', $regionTags);
    if (!($inferredRegionTagID > 0)) {
      throw new InvalidArgumentException("Failed to find the Inferrerd tag in the $tagSetName tagset! This should not happen.");
    }
    unset($regionTags[$inferredRegionTagID]);
    $regionTagIDs = implode(',', array_keys($regionTags));
    if (!preg_match('/^[0-9,]+$/', $regionTagIDs)) {
      throw new RuntimeException("Failed asserting that tag IDs are SQL safe. Very suspect. This can also happen if there are no tags defined within the tagset.");
    }


    $timings = [];
    CRM_Core_Transaction::create()->run(function($tx) use ($tagsetConfig, $inferredRegionTagID, $regionTagIDs, &$timings) {
      $startOfTime = microtime(TRUE);
      $tagsAdded = 0;

      $timings = [];

      if (($tagsetConfig['autoInfer'] ?? TRUE)) {
        // Contacts with no tags should get the inferred one.
        $sql = "INSERT INTO civicrm_entity_tag (entity_table, entity_id, tag_id)
          SELECT 'civicrm_contact' entity_table, id entity_id, $inferredRegionTagID tag_id
          FROM civicrm_contact
          WHERE is_deleted = 0 AND NOT EXISTS (
            SELECT entity_id
            FROM civicrm_entity_tag
            WHERE entity_table = 'civicrm_contact'
              AND entity_id = civicrm_contact.id
              AND tag_id IN($inferredRegionTagID, $regionTagIDs)
          )";
        $start = microtime(TRUE);
        CRM_Core_DAO::executeQuery($sql);
        $timings['autoInfer'] = ['took' => microtime(TRUE) - $start, 'affected' => NULL];
      }

      // Make a copy of the contacts for whom we need to infer regions.
      // This is because we can't do a DELETE query and use the same table in a sub query.
      $table = CRM_Utils_SQL_TempTable::build()
        ->setMemory(TRUE)
        ->createWithColumns('contact_id INT(10) UNSIGNED NOT NULL PRIMARY KEY');
      $inferredContactsTable = $table->getName();
      // Populate this table by finding contacts with an inferred tag.
      $sql = "INSERT INTO $inferredContactsTable
        SELECT entity_id
        FROM civicrm_entity_tag WHERE entity_table = 'civicrm_contact' AND tag_id = $inferredRegionTagID";
      $start = microtime(TRUE);
      $dao = CRM_Core_DAO::executeQuery($sql);
      $timings['identifySubjects'] = ['took' => microtime(TRUE) - $start, 'affected' => $dao->affectedRows()];

      // Delete all region tags on inferred contacts.
      $sql = "
        DELETE t1 FROM civicrm_entity_tag t1
        WHERE t1.entity_table = 'civicrm_contact' AND t1.tag_id IN ($regionTagIDs)
          AND t1.entity_id IN (SELECT contact_id FROM $inferredContactsTable t2)";
      $start = microtime(TRUE);
      $dao = CRM_Core_DAO::executeQuery($sql);
      $timings['deleteOldInferredTags'] = ['took' => microtime(TRUE) - $start, 'affected' => $dao->affectedRows()];

      $inactive_clause = '';
      $expired_clause = '';
      if (!($tagsetConfig['includeExpired'] ?? FALSE)) {
        // We need to EXCLUDE expired relationships.
        $expired_clause = 'AND (rel.start_date IS NULL OR rel.start_date <= CURRENT_DATE)
                           AND (rel.end_date IS NULL OR rel.end_date >= CURRENT_DATE)';
      }
      if (!($tagsetConfig['includeInactive'] ?? FALSE)) {
        // We need to EXCLUDE inactive relationships.
        $inactive_clause = 'AND rel.is_active = 1';
      }

      if (!empty($tagsetConfig['aSide'])) {
        $relTypes = implode(',', array_map(function($_) { return (int) $_; }, $tagsetConfig['aSide']));
        $sqlA = "
            INSERT IGNORE INTO civicrm_entity_tag (entity_table, entity_id, tag_id)
            SELECT 'civicrm_contact' entity_table, subject.contact_id entity_id, relatedRegionTag.tag_id tag_id

            FROM $inferredContactsTable subject
            INNER JOIN civicrm_relationship rel
              ON rel.relationship_type_id IN ($relTypes)
                 AND subject.contact_id = rel.contact_id_a
                 $inactive_clause $expired_clause
            INNER JOIN civicrm_entity_tag relatedRegionTag
              ON relatedRegionTag.entity_table = 'civicrm_contact'
                 AND rel.contact_id_b = relatedRegionTag.entity_id
                 AND relatedRegionTag.tag_id IN ($regionTagIDs)

            GROUP BY subject.contact_id, relatedRegionTag.tag_id
            ";
      }

      if (!empty($tagsetConfig['bSide'])) {
        $relTypes = implode(',', array_map(function($_) { return (int) $_; }, $tagsetConfig['bSide']));
        $sqlB = "
            INSERT IGNORE INTO civicrm_entity_tag (entity_table, entity_id, tag_id)
            SELECT 'civicrm_contact' entity_table, subject.contact_id entity_id, relatedRegionTag.tag_id tag_id

            FROM $inferredContactsTable subject
            INNER JOIN civicrm_relationship rel
              ON rel.relationship_type_id IN ($relTypes)
                 AND subject.contact_id = rel.contact_id_b
                 $inactive_clause $expired_clause
            INNER JOIN civicrm_entity_tag relatedRegionTag
              ON relatedRegionTag.entity_table = 'civicrm_contact'
                 AND rel.contact_id_a = relatedRegionTag.entity_id
                 AND relatedRegionTag.tag_id IN ($regionTagIDs)

            GROUP BY subject.contact_id, relatedRegionTag.tag_id
            ";
      }

      $iterations = 0;
      do {
        $rowCount = 0;
        if (isset($sqlA)) {
          $start = microtime(TRUE);
          $dao = CRM_Core_DAO::executeQuery($sqlA);
          $_ = $dao->affectedRows();
          $rowCount += $_;
          $timings["iteration$iterations:a"] = ['took' => microtime(TRUE) - $start, 'affected' => $_];
        }
        if (isset($sqlB)) {
          $start = microtime(TRUE);
          $dao = CRM_Core_DAO::executeQuery($sqlB);
          $_ = $dao->affectedRows();
          $rowCount += $_;
          $timings["iteration$iterations:b"] = ['took' => microtime(TRUE) - $start, 'affected' => $_];
        }
        $iterations++;
        $tagsAdded += $rowCount;
      } while ($iterations < 10 && $rowCount > 0);


      $timings['total'] = ['took' => microtime(TRUE) - $startOfTime, 'affected' => $tagsAdded];
    });

    return $timings;
  }
  /**
   * @return array
   */
  public function getConfig() {
    return static::$config;
  }
}
