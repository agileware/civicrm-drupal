<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

use CRM_Pdfapi_ExtensionUtil as E;

use iio\libmergepdf\Driver\DefaultDriver;
use iio\libmergepdf\Source\FileSource;

class CRM_Pdfapi_Utils {

  public static function mergePdfs($inputFiles, $outputFileName) {
    require_once(E::path(). '/vendor/autoload.php');
    $files = [];
    foreach($inputFiles as $file)  {
      $files[] = new FileSource($file);
    }
    $driver = new DefaultDriver();
    $createdPdf = $driver->merge(...$files);
    file_put_contents($outputFileName, $createdPdf);
  }

}
