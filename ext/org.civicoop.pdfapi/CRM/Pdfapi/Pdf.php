<?php
/**
 * Class to create the PDF
 *
 * @author Erik Hommel (CiviCooP) <erik.hommel@civicoop.org>
 * @date 3 Oct 2018
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

use CRM_Pdfapi_ExtensionUtil as E;

class CRM_Pdfapi_Pdf {
  protected $_apiParams = [];
  protected $_templateEmailId = NULL;
  protected $_emailSubject = NULL;
  protected $_htmlMessageEmail = NULL;
  protected $_messageTemplatesEmail = NULL;
  protected $_htmlMessage = NULL;
  protected $_subject = NULL;
  protected $_pdfsToBeGenerated = [];
  protected $_createdPdfs = [];
  protected $_createdFileIds = [];
  protected $_domain = NULL;
  protected $_version = NULL;
  protected $_toEmail = NULL;
  protected $_ccEmail = NULL;
  protected $_bccEmail = NULL;
  protected $_fromEmail = NULL;
  protected $_fromName  = NULL;
  protected $_contactIds = [];
  protected $_toContactIds = [];
  protected $_processedCaseIds = [];
  protected $_locationTypeId = NULL;
  protected $errors = [];

  public function __construct($params) {
    $this->_apiParams = $params;

    $this->_domain  = CRM_Core_BAO_Domain::getDomain();
    $this->_version = CRM_Core_BAO_Domain::version();
    [$fromName, $fromEmail] = CRM_Core_BAO_Domain::getNameAndEmail();
    $this->_fromName = isset($this->_apiParams['from_name']) && !empty($this->_apiParams['from_name']) ? $this->_apiParams['from_name'] : $fromName;
    $this->_fromEmail = isset($this->_apiParams['from_email']) && !empty($this->_apiParams['from_email']) ? $this->_apiParams['from_email'] : $fromEmail;
    $this->_locationTypeId = (int) $params['location_type_id'] ?? NULL;
    if (isset($this->_apiParams['to_email']) && !empty($this->_apiParams['to_email'])) {
      $this->_toEmail = $this->_apiParams['to_email'];
    }
    if (isset($this->_apiParams['contact_id'])) {
      $this->_toContactIds = explode(",", $this->_apiParams['contact_id']);
    }

    if (isset($this->_apiParams['cc_email']) && !empty($this->_apiParams['cc_email'])) {
      $this->_ccEmail = $this->_apiParams['cc_email'];
    }
    if (isset($this->_apiParams['bcc_email']) && !empty($this->_apiParams['bcc_email'])) {
      $this->_bccEmail = $this->_apiParams['bcc_email'];
    }

    // Optional template_email_id, if not default 0
    $this->_templateEmailId = CRM_Utils_Array::value('body_template_id', $this->_apiParams, 0);

    if ($this->_templateEmailId) {
      $this->_messageTemplatesEmail = $this->getMessageTemplatesEmail($this->_templateEmailId);
    }
  }

  /**
   * @return bool
   */
  public function hasErrors() {
    if (count($this->errors)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @return array
   */
  public function getErrors() {
    return $this->errors;
  }

  /**
   * Method to create the email with the pdf
   *
   * @param $overrideParams
   * @throws API_Exception
   * @throws CRM_Core_Exception
   */
  public function create($overrideParams=[]) {
    $schema = [];

    foreach($overrideParams as $key => $value) {
      $this->_apiParams[$key] = $value;
    }
    $this->validateCaseId();

    if (isset($this->_apiParams['contribution_id'])) {
	    $schema[] = 'contributionId';
      $context['contributionId'] = $this->_apiParams['contribution_id'];
    }
    elseif (!empty($this->_apiParams['extra_data']['contribution'])) {
 	    $schema[] = 'contributionId';
      $context['contributionId'] = $this->_apiParams['extra_data']['contribution']['contribution_id'];
    }

    if (isset($this->_apiParams['activity_id'])) {
	    $schema[] = 'activityId';
      $context['activityId'] = $this->_apiParams['activity_id'];
    }
	  elseif (isset($this->_apiParams['extra_data']['activity'])) {
	    $schema[] = 'activityId';
      $context['activityId'] = $this->_apiParams['extra_data']['activity']['id'];
    }

	  if (isset($this->_apiParams['case_id']) && !empty($this->_apiParams['case_id'])) {
      $schema[] = 'caseId';
	    $context['caseId'] = $this->_apiParams['case_id'];

      //CRM-4524
      // Hack for Case Token Extension
      \Civi::$statics['casetokens']['case_id'] = $this->_apiParams['case_id'];
    }
	  elseif (isset($this->_apiParams['extra_data']['case']) && !empty($this->_apiParams['extra_data']['case'])) {
	    $schema[] = 'caseId';
      $context['caseId'] = $this->_apiParams['extra_data']['case']['id'];
    }

    foreach ($this->_apiParams['extra_data'] as $entity => $entityData) {
      if (!in_array("{$entity}Id", $schema)){
	      $schema[] = "{$entity}Id";

        if (array_key_exists("id", $entityData)){
          $context["{$entity}Id"] = $entityData["id"];
        }
        elseif (array_key_exists("{$entity}_id", $entityData)){
          $context["{$entity}Id"] = $entityData["{$entity}_id"];
        }

	      $context[$entity] = $entityData;
	    }
    }

    $html = [];
    if (!preg_match('/[0-9]+(,[0-9]+)*/i', $this->_apiParams['contact_id'])) {
      throw new API_Exception('Parameter contact_id must be a unique id or a list of ids separated by comma');
    }
    $this->_contactIds = explode(",", $this->_apiParams['contact_id']);
    $messageTemplate = $this->getMessageTemplates();
    $this->_subject = $messageTemplate->msg_subject;
    $this->_htmlMessage = $this->formatMessage($messageTemplate);

    $filterdIds = [];
    // search for contacts which should not receive a mail
    $filterdContacts = \Civi\Api4\Contact::get(FALSE)
      ->addSelect('do_not_email', 'is_deceased', 'display_name', 'email.on_hold')
      ->addJoin('Email AS email', 'LEFT', ['email.contact_id', '=', 'id'])
      ->addWhere('id', 'IN', $this->_contactIds)
      ->addWhere('email.is_primary', '=', TRUE)
      ->addClause('OR', ['email.on_hold', '=', TRUE], ['is_deceased', '=', TRUE], ['do_not_email', '=', TRUE])
      ->execute();

    foreach ($filterdContacts as $contact) {
      if (count($this->_contactIds) == 1 && !($this->_toEmail ?? FALSE)){
        if ($contact['do_not_email']){
          throw new API_Exception('Suppressed creating pdf letter for: '.$contact['display_name'].' because DO NOT EMAIL is set');
        }
        if ($contact['is_deceased']){
          throw new API_Exception('Suppressed creating pdf letter for: '.$contact['display_name'].' because contact is deceased');
      }
        if ($contact['email.on_hold']){
          throw new API_Exception('Suppressed creating pdf letter for: '.$contact['display_name'].' because contact is on hold');
      }
      }

      $filterdIds[] = $contact['id'];
        }

	  $schema[] = 'contactId';

    $useSmarty = (bool) (defined('CIVICRM_MAIL_SMARTY') && CIVICRM_MAIL_SMARTY);
    $tokenProcessor = new \Civi\Token\TokenProcessor(\Civi::dispatcher(), [
      'controller' => __CLASS__,
      'schema' => $schema,
      'smarty' => $useSmarty,
    ]);

	  foreach($this->_contactIds as $contactId) {
      if (!in_array($contactId, $filterdIds)){
		    $context['contactId'] = $contactId;
        $tokenProcessor->addRow($context);
      }
      }

    $tokenProcessor->addMessage('messageSubject', $this->_subject, 'text/plain');
    $tokenProcessor->addMessage('html', $this->_htmlMessage, 'text/html');
    $tokenProcessor->evaluate();

    foreach ($tokenProcessor->getRows() as $row) {
      $this->_subject = $row->render('messageSubject');
      $this->_htmlMessage = $row->render('html');

      $html[] = $this->_htmlMessage;

      //create PDF activities if required
      if (isset($this->_apiParams['pdf_activity']) && $this->_apiParams['pdf_activity'] == TRUE) {
        $this->createPdfActivities($row->context['contactId'], $messageTemplate->pdf_format_id);
      }
    }

    $this->_pdfsToBeGenerated[] = [
      'title' => $messageTemplate->msg_title,
      'fileName' => CRM_Utils_String::munge($this->_subject) . '.pdf',
      'pdf_format_id' => $messageTemplate->pdf_format_id,
      'html' => $html,
    ];

    if (isset($this->_apiParams['case_id']) && !empty($this->_apiParams['case_id'])) {
      $this->_processedCaseIds[] = $this->_apiParams['case_id'];
    }
  }

  /**
   * Method to send the actual pdf as an email attachment
   *
   * @param $email
   * @param $contact_id
   */
  protected function sendPdf($email, $contact_id=null, $createEmailActivity=false) {
    $body_text = E::ts("CiviCRM has generated a PDF letter");
    $body_html = E::ts("CiviCRM has generated a PDF letter");
    $subject = E::ts('PDF Letter from Civicrm');

    if ($this->_messageTemplatesEmail) {
      $body_text = $this->_messageTemplatesEmail->msg_text;
      $body_html = $this->_messageTemplatesEmail->msg_html;
      if (!empty($this->_messageTemplatesEmail->msg_subject)) {
        $subject = $this->_messageTemplatesEmail->msg_subject;
      }
    }
    if (isset($this->_apiParams['email_subject']) && !empty($this->_apiParams['email_subject'])) {
      $subject = $this->_apiParams['email_subject'];
    }

    if (!$body_text) {
      $body_text = CRM_Utils_String::htmlToText($body_html);
    }

    $schema = [];

    if (isset($this->_apiParams['contribution_id'])) {
	    $schema[] = 'contributionId';
      $context['contributionId'] = $this->_apiParams['contribution_id'];
    }
    elseif (!empty($this->_apiParams['extra_data']['contribution'])) {
 	    $schema[] = 'contributionId';
      $context['contributionId'] = $this->_apiParams['extra_data']['contribution']['contribution_id'];
    }

    if (isset($this->_apiParams['activity_id'])) {
	    $schema[] = 'activityId';
      $context['activityId'] = $this->_apiParams['activity_id'];
    }
	  elseif (isset($this->_apiParams['extra_data']['activity'])) {
	    $schema[] = 'activityId';
      $context['activityId'] = $this->_apiParams['extra_data']['activity']['id'];
    }

	  if (isset($this->_apiParams['case_id']) && !empty($this->_apiParams['case_id'])) {
      $schema[] = 'caseId';
	    $context['caseId'] = $this->_apiParams['case_id'];

      //CRM-4524
      // Hack for Case Token Extension
      \Civi::$statics['casetokens']['case_id'] = $this->_apiParams['case_id'];
    }
	  elseif (isset($this->_apiParams['extra_data']['case']) && !empty($this->_apiParams['extra_data']['case'])) {
	    $schema[] = 'caseId';
      $context['caseId'] = $this->_apiParams['extra_data']['case']['id'];
    }

    foreach ($this->_apiParams['extra_data'] as $entity => $entityData) {
      if (!in_array("{$entity}Id", $schema)){
	      $schema[] = "{$entity}Id";

        if (array_key_exists("id", $entityData)){
          $context["{$entity}Id"] = $entityData["id"];
      }
        elseif (array_key_exists("{$entity}_id", $entityData)){
          $context["{$entity}Id"] = $entityData["{$entity}_id"];
        }

	      $context[$entity] = $entityData;
	    }
    }

    if ($contact_id) {
      $context['contactId'] = $contact_id;
      $schema[] = 'contactId';

      $contacts = \Civi\Api4\Contact::get(FALSE)
        ->addSelect('do_not_email', 'is_deceased', 'display_name', 'email.on_hold')
        ->addJoin('Email AS email', 'LEFT', ['email.contact_id', '=', 'id'])
        ->addWhere('id', '=', $contact_id)
        ->addWhere('email.is_primary', '=', TRUE)
        ->execute();
      $contact = $contacts->first();

      if ($contact){
        if ($contact['do_not_email']){
          throw new API_Exception('Suppressed sending pdf letter for: '.$contact['display_name'].' because DO NOT EMAIL is set');
        }
        if ($contact['is_deceased']){
          throw new API_Exception('Suppressed sending pdf letter for: '.$contact['display_name'].' because contact is deceased');
        }
        if ($contact['email.on_hold']){
          throw new API_Exception('Suppressed sending pdf letter for: '.$contact['display_name'].' because contact is on hold');
        }
      }else{
        throw new API_Exception('Could not find contact with ID: ' . $contact_id);
      }
    }

    $useSmarty = (bool) (defined('CIVICRM_MAIL_SMARTY') && CIVICRM_MAIL_SMARTY);
    $tokenProcessor = new \Civi\Token\TokenProcessor(\Civi::dispatcher(), [
      'controller' => __CLASS__,
      'schema' => $schema,
      'smarty' => $useSmarty,
    ]);

    $tokenProcessor->addRow($context);
    $tokenProcessor->addMessage('subject', $subject, 'text/plain');
    $tokenProcessor->addMessage('body_text', $body_text, 'text/plain');
    $tokenProcessor->addMessage('body_html',  $body_html, 'text/html');
    $tokenProcessor->evaluate();
    $tokenProcessorRow = $tokenProcessor->getRow(0);
    $this->_emailSubject = $tokenProcessorRow->render('subject');

    $mailParams = [
      'groupName' => 'PDF Letter API',
      'from' => $this->_fromName . ' <' . $this->_fromEmail . '>',
      'fromName' => $this->_fromName,
      'toEmail' => $email,
      'subject' => $this->_emailSubject,
      'html' => $tokenProcessorRow->render('body_html'),
      'text' => str_replace('&amp;', '&', $tokenProcessorRow->render('body_text')),
      'attachments' => $this->_createdPdfs,
    ];

    if (!empty($this->_ccEmail)) {
      $mailParams['cc'] = $this->_ccEmail;
    }
    if (!empty($this->_bccEmail)) {
      $mailParams['bcc'] = $this->_bccEmail;
    }

    $result = CRM_Utils_Mail::send($mailParams);
    if (!$result) {
      $message = ts('Could not send Email with PDF file as attachment in ' . __METHOD__);
      $this->logError($message);
    }

    if ($contact_id && $createEmailActivity) {
      if (count($this->_processedCaseIds)) {
        foreach (array_unique($this->_processedCaseIds) as $case_id) {
          $this->createEmailActivity($contact_id, $this->_createdFileIds, $case_id, $renderedSubject);
        }
      } else {
        $this->createEmailActivity($contact_id, $this->_createdFileIds, null, $renderedSubject);
      }
    }
  }

  /**
   * Method to send the actual pdf, either to all involved contacts or to specific email address
   *
   * @param bool $combine Combine the pdfs into one file?
   * @param array $apiParams
   *
   * @throws \Exception
   */
  public function processPdf($combine=false, $apiParams= []) {
    $createEmailActivity = $this->_apiParams['email_activity'] ?? FALSE;

    if (!$combine) {
      // Separate file for each PDF
      foreach ($this->_pdfsToBeGenerated as $pdfToBeGenerated) {
        $hash = md5(uniqid($pdfToBeGenerated['title'], TRUE));
        $pdfToBeGenerated = $this->alterTemplatePreRender($pdfToBeGenerated);
        $_fileName = $pdfToBeGenerated['fileName'] . '_' . $hash . '.pdf';
        $_cleanName = $pdfToBeGenerated['fileName'];
        $pdf = CRM_Utils_PDF_Utils::html2pdf($pdfToBeGenerated['html'], $_fileName, TRUE, $pdfToBeGenerated['pdf_format_id']);
        // if no email_activity, use temp folder otherwise use customFileUploadDir
        if ($createEmailActivity) {
          $config = CRM_Core_Config::singleton();
          $_fullPathName = $config->customFileUploadDir . $_fileName;
          $this->_createdFileIds[] = $this->createFileForPDF($_fileName);
        }
        else {
          $_fullPathName = CRM_Utils_File::tempnam();
        }
        file_put_contents($_fullPathName, $pdf);
        unset($pdf); //we don't need the temp file in memory
        $this->_createdPdfs[] = [
          'fullPath' => $_fullPathName,
          'mime_type' => 'application/pdf',
          'cleanName' => $_cleanName,
        ];
      }
    } else {
      // Combine each PDF into one file
      $pdfFiles = [];
      foreach ($this->_pdfsToBeGenerated as $pdfToBeGenerated) {
        $hash = md5(uniqid($pdfToBeGenerated['title'], TRUE));
        $pdfToBeGenerated = $this->alterTemplatePreRender($pdfToBeGenerated);
        $_fileName = $pdfToBeGenerated['fileName'] . '_' . $hash . '.pdf';
        $_cleanName = $pdfToBeGenerated['fileName'];
        $_pdf_format_id = $pdfToBeGenerated['pdf_format_id'];
        $pdf = CRM_Utils_PDF_Utils::html2pdf($pdfToBeGenerated['html'], $_fileName, TRUE, $_pdf_format_id);
        $_fullPathName = CRM_Utils_File::tempnam();
        file_put_contents($_fullPathName, $pdf);
        unset($pdf); //we don't need the temp file in memory
        $pdfFiles[] = $_fullPathName;
      }
      if (count($pdfFiles)) {
        // if no email_activity, use temp folder otherwise use customFileUploadDir
        if ($createEmailActivity) {
          $config = CRM_Core_Config::singleton();
          $_fullPathName = $config->customFileUploadDir . $_fileName;
          $this->_createdFileIds[] = $this->createFileForPDF($_fileName);
        }
        else {
          $_fullPathName = CRM_Utils_File::tempnam();
        }
        CRM_Pdfapi_Utils::mergePdfs($pdfFiles, $_fullPathName);

        $this->_createdPdfs[] = [
          'fullPath' => $_fullPathName,
          'mime_type' => 'application/pdf',
          'cleanName' => $_cleanName,
        ];
      }
    }

    if($apiParams['skip_send_email']) {return;}

    if ($this->_toEmail) {
      $contactId = null;
      if (isset($apiParams['contact_id'])) {
        $contactId = $apiParams['contact_id'];
      }
      $this->sendPdf($this->_toEmail, $contactId, $createEmailActivity);
    }
    elseif (isset($apiParams['contact_id'])) {
      $contactId = $apiParams['contact_id'];
      $email = $this->getContactEmail($contactId);
      if ($email) {
        $this->sendPdf($email, $contactId, $createEmailActivity);
      }
      else {
        $message = ts('Email with attached PDF not sent to contact ID ') . $contactId
          . ts(' as no primary email could be found for the contact, all emails for the contact are on hold, the contact opted out of mailing or the contact is deceased in ') . __METHOD__;
        $this->logError($message);
      }
    }
    else {
      foreach (array_unique($this->_toContactIds) as $contactId) {
        $email = $this->getContactEmail($contactId);
        if ($email) {
          $this->sendPdf($email, $contactId, $createEmailActivity);
        }
        else {
          $message = ts('Email with attached PDF not sent to contact ID ') . $contactId
            . ts(' as no primary email could be found for the contact, all emails for the contact are on hold, the contact opted out of mailing or the contact is deceased in ') . __METHOD__;
          $this->logError($message);
        }
      }
    }
  }

  /**
   * Alter the HTML template (title/html) before rendering as PDF
   * Useful to workaround wkhtmltopdf cannot access remote images for example.
   *
   * @param array $pdfHTMLTemplate
   *
   * @return array
   */
  protected function alterTemplatePreRender($pdfHTMLTemplate) {
    $pregReplacePattern = \Civi::settings()->get('pdfapi_pregreplace');
    $pregReplacePattern = explode(';', $pregReplacePattern);
    if (!empty($pregReplacePattern) && (count($pregReplacePattern) === 2)) {
      $pdfHTMLTemplate['title'] = preg_replace($pregReplacePattern[0], $pregReplacePattern[1], $pdfHTMLTemplate['title']);
      $pdfHTMLTemplate['html'] = preg_replace($pregReplacePattern[0], $pregReplacePattern[1], $pdfHTMLTemplate['html']);
    }
    return $pdfHTMLTemplate;
  }

  /**
   * Method to get email of contact.
   * Use location type id if specified, fall back to primary if not specified or not found.
   *
   * @param $contactId
   * @return bool|string
   */
  protected function getContactEmail($contactId) {
    $email = FALSE;
    $query = \Civi\Api4\Contact::get(FALSE)
      ->addSelect('email.email')
      ->addJoin('Email AS email', 'INNER')
      ->addWhere('do_not_email', '=', FALSE)
      ->addWhere('is_deceased', '=', FALSE)
      // Can't use "= 0" because of "On Hold Opt Out".
      ->addWhere('email.on_hold', '!=', 1)
      ->addWhere('id', '=', $contactId);

    if ($this->_locationTypeId) {
      // We clone the query to prevent clauses in this "if" statement affecting the second query below.
      $email = reset((clone $query)
        ->addWhere('email.location_type_id', '=', $this->_locationTypeId)
        ->execute()
        ->column('email.email'));
    }
    if (!$email) {
      $email = reset($query
        ->addWhere('email.is_primary', '=', TRUE)
        ->execute()
        ->column('email.email'));
    }
    return $email ?? FALSE;
  }

  /**
   * Method to save the PDF as a file in the customFileUploadDir
   * @param $filename
   */
  protected function createFileForPDF($filename) {
    try {
      $file = civicrm_api3('File', 'create', [
        'mime_type' => 'application/pdf',
        'uri' => $filename,
      ]);
      return $file['id'];
    }
    // if no joy, log error but continue processing
    catch (CiviCRM_API3_Exception $ex) {
      $message = ts('Could not save the created PFD as a File in civicrm in ' . __METHOD__
        . ', error message from API File create: ' . $ex->getMessage());
      $this->logError($message);
      return FALSE;
    }
  }

  /**
   * Method to create email activity for contact with PDF as attachment
   *
   * @param int $contactId
   * @param array $fileIds
   * @param $case_id
   */
  protected function createEmailActivity($contactId, $fileIds, $case_id=null) {
    $activityTypeId = $this->getActivityTypeId('email');
    if ($activityTypeId) {
      // first create activity
      $activityParams = [
        'source_contact_id' => $contactId,
        'activity_type_id' => $activityTypeId,
        'activity_date_time' => date('YmdHis'),
        'details' => $this->_htmlMessageEmail,
        'subject' => $this->_emailSubject,
      ];

      $activity = CRM_Activity_BAO_Activity::create($activityParams);
      if ($activity) {
        if($this->_version >= 4.4){
          $activityContacts = CRM_Core_OptionGroup::values('activity_contacts', FALSE, FALSE, FALSE, NULL, 'name');
          $targetId = CRM_Utils_Array::key('Activity Targets', $activityContacts);
          $activityTargetParams = [
            'activity_id' => $activity->id,
            'contact_id' => $contactId,
            'record_type_id' => $targetId,
          ];
          CRM_Activity_BAO_ActivityContact::create($activityTargetParams);
        }
        else {
          $activityTargetParams = [
            'activity_id' => $activity->id,
            'target_contact_id' => $contactId,
          ];
          CRM_Activity_BAO_Activity::createActivityTarget($activityTargetParams);
        }
        // add to case if required
        if ($case_id) {
          $this->processCaseActivity($activity->id, $case_id);
        }
        // add record to civicrm_entity_file to add attachment to activity
        foreach($fileIds as $fileId) {
          $this->createEntityFileForPDF($fileId, $activity->id);
        }
      }
    }
  }

  /**
   * Method to create an entity file record for the PDF and activity
   *
   * @param int $fileId
   * @param int $activityId
   */
  protected function createEntityFileForPdf($fileId, $activityId) {
    $params = [
      1 => ['civicrm_activity', 'String'],
      2 => [$activityId, 'Integer'],
      3 => [$fileId, 'Integer']
    ];
    // first check if we already have the record (should never happen but to be sure to be sure)
    $query = "SELECT COUNT(*) FROM civicrm_entity_file WHERE entity_table = %1 AND entity_id = %2 AND file_id = %3";
    $count = CRM_Core_DAO::singleValueQuery($query, $params);
    if ($count > 0) {
      $message = ts('Already found an attachment for activity_id ') . $activityId . ts(' and file_id ')
        . $fileId . ts(' in ') . __METHOD__;
      $this->logError($message);
    }
    else {
      $insert = "INSERT INTO civicrm_entity_file (entity_table, entity_id, file_id) VALUES(%1, %2, %3)";
      CRM_Core_DAO::executeQuery($insert, $params);
    }
  }

  /**
   * Method to add the Create PDF activity to the contact
   *
   * @param $contactId
   * @param $pdfFormatId
   * @throws API_Exception
   * @throws CRM_Core_Exception
   */
  protected function createPdfActivities($contactId, $pdfFormatId) {
    $activityTypeId = $this->getActivityTypeId('pdf');
    if ($activityTypeId) {
      $activityParams = [
        'source_contact_id' => $contactId,
        'activity_type_id' => $activityTypeId,
        'activity_date_time' => date('YmdHis'),
        'details' => $this->_htmlMessage,
        'subject' => $this->_subject,
      ];
      $activity = CRM_Activity_BAO_Activity::create($activityParams);
      if ($activity) {
        // Compatibility with CiviCRM >= 4.4
        if ($this->_version >= 4.4) {
          $activityContacts = CRM_Core_OptionGroup::values('activity_contacts', FALSE, FALSE, FALSE, NULL, 'name');
          $targetId = CRM_Utils_Array::key('Activity Targets', $activityContacts);
          $activityTargetParams = [
            'activity_id' => $activity->id,
            'contact_id' => $contactId,
            'record_type_id' => $targetId,
          ];
          CRM_Activity_BAO_ActivityContact::create($activityTargetParams);
        }
        else {
          $activityTargetParams = [
            'activity_id' => $activity->id,
            'target_contact_id' => $contactId,
          ];
          CRM_Activity_BAO_Activity::createActivityTarget($activityTargetParams);
        }
        // add to case if required
        if (isset($this->_apiParams['case_id']) && !empty($this->_apiParams['case_id'])) {
          $this->processCaseActivity($activity->id, $this->_apiParams['case_id']);
        }

        $hash = md5(uniqid($this->_subject, TRUE));
        $_fileName = CRM_Utils_String::munge($this->_subject) . '_' . $hash . '.pdf';
        $_cleanName = CRM_Utils_String::munge($this->_subject) . '.pdf';
        $pdf = CRM_Utils_PDF_Utils::html2pdf($this->_htmlMessage, $_fileName, TRUE, $pdfFormatId);
        // if no email_activity, use temp folder otherwise use customFileUploadDir
        $config = CRM_Core_Config::singleton();
        $_fullPathName = $config->customFileUploadDir . $_fileName;
        file_put_contents($_fullPathName, $pdf);
        unset($pdf); //we don't need the temp file in memory
        $fileID = $this->createFileForPDF($_fileName);
        $this->createEntityFileForPdf($fileID, $activity->id);
      }
    }
  }

  /**
   * Method to format the message
   *
   * @param $messageTemplate
   * @return string
   */
  protected function formatMessage($messageTemplate){
    $this->_htmlMessage = $messageTemplate->msg_html;

    //time being hack to strip '&nbsp;'
    //from particular letter line, CRM-6798
    $newLineOperators = [
      'p' => [
        'oper' => '<p>',
        'pattern' => '/<(\s+)?p(\s+)?>/m',
      ],
      'br' => [
        'oper' => '<br />',
        'pattern' => '/<(\s+)?br(\s+)?\/>/m',
      ],
    ];
    $htmlMsg = preg_split($newLineOperators['p']['pattern'], $this->_htmlMessage);
    foreach ($htmlMsg as $k => & $m) {
      $messages = preg_split($newLineOperators['br']['pattern'], $m);
      foreach ($messages as $key => & $msg) {
        $msg = trim($msg);
        $matches = [];
        if (preg_match('/^(&nbsp;)+/', $msg, $matches)) {
          $spaceLen = strlen($matches[0]) / 6;
          $trimMsg = ltrim($msg, '&nbsp; ');
          $charLen = strlen($trimMsg);
          $totalLen = $charLen + $spaceLen;
          if ($totalLen > 100) {
            $spacesCount = 10;
            if ($spaceLen > 50) {
              $spacesCount = 20;
            }
            if ($charLen > 100) {
              $spacesCount = 1;
            }
            $msg = str_repeat('&nbsp;', $spacesCount) . $trimMsg;
          }
        }
      }
      $m = implode($newLineOperators['br']['oper'], $messages);
    }
    $this->_htmlMessage = implode($newLineOperators['p']['oper'], $htmlMsg);
    return $this->_htmlMessage;
  }

  /**
   * Method to get the message templates depending on the version
   *
   * @return CRM_Core_DAO_MessageTemplate|CRM_Core_DAO_MessageTemplates
   * @throws
   */
  protected function getMessageTemplates() {
    // Compatibility with CiviCRM > 4.3
    if ($this->_version >= 4.4) {
      $messageTemplate =  new CRM_Core_DAO_MessageTemplate();
    } else {
      $messageTemplate = new CRM_Core_DAO_MessageTemplates();
    }
    $messageTemplate->id = $this->_apiParams['template_id'];
    if (!$messageTemplate->find(TRUE)) {
      throw new API_Exception('Could not find template with ID: ' . $this->_apiParams['template_id']);
    }
    // Optional pdf_format_id, if not default 0
    if (isset($this->_apiParams['pdf_format_id'])) {
      $messageTemplate->pdf_format_id = CRM_Utils_Array::value('pdf_format_id', $this->_apiParams, 0);
    }
    return $messageTemplate;
  }

  /**
   * Method to get the message template email
   *
   * @param $templateEmailId
   * @return CRM_Core_DAO_MessageTemplate|CRM_Core_DAO_MessageTemplates
   * @throws API_Exception
   */
  protected function getMessageTemplatesEmail($templateEmailId) {
    if($this->_version >= 4.4) {
      $this->_messageTemplatesEmail = new CRM_Core_DAO_MessageTemplate();
    } else {
      $this->_messageTemplatesEmail = new CRM_Core_DAO_MessageTemplates();
    }
    $this->_messageTemplatesEmail->id = $templateEmailId;
    if (!$this->_messageTemplatesEmail->find(TRUE)) {
      throw new API_Exception('Could not find template with ID: ' . $templateEmailId);
    }
    return $this->_messageTemplatesEmail;
  }

  /**
   * Method to get the activity type id based on the incoming type
   *
   * @param string $type
   * @return int
   * @throws
   */
  protected function getActivityTypeId($type) {
    try {
      switch ($type) {
        case 'pdf':
          return (int) civicrm_api3('OptionValue', 'getvalue', [
            'option_group_id' => 'activity_type',
            'name' => 'Print PDF Letter',
            'return' => 'value',
          ]);
          break;
        case 'email':
          return (int) civicrm_api3('OptionValue', 'getvalue', [
            'option_group_id' => 'activity_type',
            'name' => 'Email',
            'return' => 'value',
          ]);
          break;
      }
    }
    catch (CiviCRM_API3_Exception $ex) {
      throw new API_Exception(ts('Could not find an activity type for ' . $type . ' in ')
        . __METHOD__ . ts(', error from API OptionValue getvalue: ') . $ex->getMessage());
    }
  }

  /**
   * Method to add the case activities
   *
   * @param int $activityId
   * @param int $caseId
   */
  protected function processCaseActivity($activityId, $caseId) {
    $caseActivityDAO = new CRM_Case_DAO_CaseActivity();
    $caseActivityDAO->activity_id = $activityId;
    $caseActivityDAO->case_id = $caseId;
    $caseActivityDAO->find(TRUE);
    $caseActivityDAO->save();
  }

  /**
   * Method to check if case_id exists if passed and remove parameter case_id if not
   */
  protected function validateCaseId() {
    if (isset($this->_apiParams['case_id'])) {
      // if empty, log warning and remove
      if (empty($this->_apiParams['case_id'])) {
        unset($this->_apiParams['case_id']);
        $message = ts('Empty parameter case_id passed to API Create PDF, case_id ignored in ') . __METHOD__;
        $this->logError($message);
      }
      else {
        // check if case_id exists and if not, unset and warning
        $query = 'SELECT COUNT(*) FROM civicrm_case WHERE id = %1';
        $count = CRM_Core_DAO::singleValueQuery($query, [1 => [$this->_apiParams['case_id'], 'Integer']]);
        if ($count == 0) {
          $message = ts('Could not find a case with case ID ') . $this->_apiParams['case_id'] . ts(' in ')
            . __METHOD__ . ts(', activity will be logged to contact(s) instead, fix manually.');
          $this->logError($message);
          unset($this->_apiParams['case_id']);
        }
      }
    }
  }

  /**
   * Logs an error message.
   *
   * @param $message
   */
  protected function logError($message) {
    $this->errors[] = $message;
    if ($this->_version>= 4.7) {
      Civi::log()->warning($message);
    }
    else {
      CRM_Core_Error::debug_log_message($message);
    }
  }

  /**
   * Method to get the created pdf's
   *
   * @return array
   */
  public function getCreatedPdf() {
    return $this->_createdPdfs;
  }

}
