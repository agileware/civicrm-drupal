<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\Pdfapi\ActionProvider;

use Civi\ActionProvider\Action\AbstractAction;
use Civi\ActionProvider\Parameter\OptionGroupByNameSpecification;
use \Civi\ActionProvider\Parameter\ParameterBagInterface;
use Civi\ActionProvider\Parameter\SpecificationBag;
use Civi\ActionProvider\Parameter\Specification;
use Civi\ActionProvider\Utils\Files;
use Civi\ActionProvider\Utils\Tokens;
use CRM_Pdfapi_ExtensionUtil as E;

class CreatePdf extends AbstractAction {

  protected $files = [];

  protected $pdfLetterActivityType;

  public function doAction(ParameterBagInterface $parameters, ParameterBagInterface $output) {
    $participantId = $parameters->getParameter('participant_id');
    $message = $parameters->getParameter('message');
    $contactId = $parameters->getParameter('contact_id');
    $filename = $this->configuration->getParameter('filename');
    $fileNameWithoutContactId = $filename . '.pdf';
    $filenameWithContactId = $filename . '_' . $contactId . '.pdf';
    $contact = [];
    if ($participantId) {
      $contact['extra_data']['participant']['id'] = $participantId;
    }
    if ($parameters->doesParameterExists('case_id')) {
      $contact['case_id'] = $parameters->getParameter('case_id');
    }
    if ($parameters->doesParameterExists('contribution_id')) {
      $contact['contribution_id'] = $parameters->getParameter('contribution_id');
    }
    if ($parameters->doesParameterExists('activity_id')) {
      $contact['activity_id'] = $parameters->getParameter('activity_id');
    }

    $processedMessage = Tokens::replaceTokens($contactId, $message, $contact);
    if ($processedMessage === false) {
      return;
    }
    //time being hack to strip '&nbsp;'
    //from particular letter line, CRM-6798
    \CRM_Contact_Form_Task_PDFLetterCommon::formatMessage($processedMessage);
    $text = [$processedMessage];
    $pdfContents = \CRM_Utils_PDF_Utils::html2pdf($text, $fileNameWithoutContactId, TRUE);

    $file = $this->createActivity($contactId, $message, $pdfContents, $fileNameWithoutContactId, $parameters->getParameter('subject'));
    $this->files[] = $file['path'];

    $output->setParameter('filename', $file['name']);
    $output->setParameter('url', $file['url']);
    $output->setParameter('path', $file['path']);
  }

  /**
   * @param $contactId
   * @param $message
   * @param $pdfContents
   * @param $filename
   * @param $subject
   * @return array
   *   Returns the file array
   */
  protected function createActivity($contactId, $message, $pdfContents, $filename, $subject='') {
    $activityTypeId = $this->getPdfLetterActivityTypeId();
    $activityParams = [
      'activity_type_id' => $activityTypeId,
      'activity_date_time' => date('YmdHis'),
      'details' => $message,
      'target_contact_id' => $contactId,
      'subject' => $subject,
    ];
    $result = civicrm_api3('Activity', 'create', $activityParams);
    $attachment = civicrm_api3('Attachment', 'create', [
      'entity_table' => 'civicrm_activity',
      'entity_id' => $result['id'],
      'name' => $filename,
      'mime_type' => 'application/pdf',
      'content' => $pdfContents,
    ]);

    return reset($attachment['values']);
  }

  /**
   * @return array
   * @throws \CiviCRM_API3_Exception
   */
  protected function getPdfLetterActivityTypeId() {
    if (!$this->pdfLetterActivityType) {
      $activityTypeName = $this->configuration->getParameter('activity_type_id');
      $this->pdfLetterActivityType = civicrm_api3('OptionValue', 'getvalue', [
        'option_group_id' => 'activity_type',
        'name' => $activityTypeName,
        'return' => 'value'
      ]);
    }
    return $this->pdfLetterActivityType;
  }

  /**
   * This function initialize a batch.
   *
   * @param $batchName
   */
  public function initializeBatch($batchName) {
    parent::initializeBatch($batchName);
  }

  /**
   * This function finishes a batch and is called when a batch with actions is finished.
   *
   * @param $batchName
   * @param bool
   *   Whether this was the last batch.
   */
  public function finishBatch($batchName, $isLastBatch=false) {
    // Child classes could override this function
    // E.g. merge files in a directorys
    $subdir = Files::createRestrictedDirectory('createpdf/'.$batchName);
    $basePath = \CRM_Core_Config::singleton()->templateCompileDir . $subdir;
    if (count($this->files)) {
      $i = 0;
      do {
        $i ++;
        $fileName = str_pad($i, 6, '0', STR_PAD_LEFT).'.pdf';
      } while (file_exists($basePath.'/'.$fileName));
      \CRM_Pdfapi_Utils::mergePdfs($this->files, $basePath.'/'.$fileName);
    }
    if ($isLastBatch) {
      $downloadName = $this->configuration->getParameter('filename').'.pdf';
      $files = glob($basePath.'/*.pdf');
      \CRM_Pdfapi_Utils::mergePdfs($files, $basePath.'/'.$downloadName);
      $this->createDownloadStatusMessage($downloadName, $subdir, $downloadName);
    }
  }

  /**
   * Creates a status message with a download link.
   *
   * @param $filename
   * @param $subdir
   * @param $downloadName
   */
  protected function createDownloadStatusMessage($filename, $subdir, $downloadName) {
    $downloadUrl = \CRM_Utils_System::url('civicrm/actionprovider/downloadfile', [
      'filename' => $filename,
      'subdir' => $subdir,
      'downloadname' => $downloadName
    ]);
    \CRM_Core_Session::setStatus(E::ts('<a href="%1">Download document(s)<a/>', [1 => $downloadUrl]), E::ts('Created PDF'), 'success');
  }

  /**
   * Returns the specification of the parameters of the actual action.
   *
   * @return SpecificationBag
   */
  public function getParameterSpecification() {
    return new SpecificationBag([
      new Specification('contact_id', 'Integer', E::ts('Contact ID'), true),
      new Specification('message', 'String', E::ts('Message'), true),
      new Specification('activity_id', 'Integer', E::ts('Activity ID'), false),
      new Specification('contribution_id', 'Integer', E::ts('Contribution ID'), false),
      new Specification('case_id', 'Integer', E::ts('Case ID'), false),
      new Specification('participant_id', 'Integer', E::ts('Participant ID'), false),
      new Specification('subject', 'String', E::ts('Subject (for the activity)'), false),
    ]);
  }

  public function getConfigurationSpecification() {
    $filename = new Specification('filename', 'String', E::ts('Filename'), true, E::ts('document'));
    $filename->setDescription(E::ts('Without the extension .pdf or .zip'));
    $activity = new OptionGroupByNameSpecification('activity_type_id', 'activity_type', E::ts('PDF Letter Activity'), true, 'Print PDF Letter');

    return new SpecificationBag([
      $filename,
      $activity
    ]);
  }

  public function getOutputSpecification() {
    return new SpecificationBag([
      new Specification('filename', 'String', E::ts('Filename')),
      new Specification('url', 'String', E::ts('Download Url')),
      new Specification('path', 'String', E::ts('Path in filesystem')),
    ]);
  }

  /**
   * Returns a help text for this action.
   *
   * The help text is shown to the administrator who is configuring the action.
   * Override this function in a child class if your action has a help text.
   *
   * @return string|false
   */
  public function getHelpText() {
    return E::ts("
      This action generates PDF files for the contacts. <br />
      <br />
      Compared to the action 'Communication: Create PDF' this action is better creating one large pdf for multiple contacts.<br />
      Specially when the list of contacts is large.<br />
      <br />
      The input for this action is a contact ID and the message. You can use the <em>Find Message Template by name</em> action
      to retrieve the message.
    ");
  }

}
