(function(angular, $, _) {

  angular.module('emailunion').config(function($routeProvider) {
      $routeProvider.when('/emailunion', {
        controller: 'EmailunionEmailUnion',
        controllerAs: '$ctrl',
        templateUrl: '~/emailunion/EmailUnion.html',

        resolve: {
          stats: function(crmApi) {
            return crmApi('Email', 'getemailunionstats', { use_cache: 0 })
            .then(r => r.values);
          }
        }
      });
    }
  );

  // The controller uses *injection*. This default injects a few things:
  //   $scope -- This is the set of variables shared between JS and HTML.
  //   crmApi, crmStatus, crmUiHelp -- These are services provided by civicrm-core.
  //   myContact -- The current contact, defined above in config().
  angular.module('emailunion').controller('EmailunionEmailUnion', function($scope, crmApi, crmStatus, crmUiHelp, stats, crmApi4) {
    // The ts() and hs() functions help load strings for this module.
    var ts = $scope.ts = CRM.ts('emailunion');
    var hs = $scope.hs = crmUiHelp({file: 'CRM/emailunion/EmailUnion'}); // See: templates/CRM/emailunion/EmailUnion.hlp
    // Local variable for this controller (needed when inside a callback fn where `this` is not available).
    var ctrl = this;
    console.log({stats});

    this.message = stats.union.meta.message || '';
    this.hideDomainsOriginal = stats.hideDomains;
    this.hideDomains = stats.hideDomains;
    this.autoSubmit = stats.autoSubmit;
    this.ourHoldPercent = Math.round(stats.local[0].onHold / stats.local[0].total * 1000) / 10;
    this.ourTotal = stats.local[0].total;
    this.tableSort = 'onHold';
    this.tableSortDir = -1;
    this.lastSubmitted = stats.lastSubmitted || ts('Never');
    this.isSubmitting = false;

    this.updateSort = (k) => {
      if (this.tableSort === k) {
        // Inverse search polarity.
        this.tableSortDir *= -1;
      }
      else {
        // Clicked a different column.
        this.tableSort = k;
        this.tableSortDir = (k === 'domain') ? 1 : -1;
      }
      recalcRows();
    }

    // Strip off local averages.
    stats.local.shift();
    this.providerName = stats.provider;
    this.providerNameNew = '';

    // Get list of provider names
    const listProviders = () => {
      const knownProviders = [];
      for (const providerName in stats.union.aggregateRates.average) {
        if (stats.union.aggregateRates.average.hasOwnProperty(providerName) && providerName !== 'average') {
          knownProviders.push(providerName);
        }
      }
      if (this.providerName && knownProviders.indexOf(this.providerName) === -1) {
        knownProviders.push(this.providerName);
      }
      knownProviders.sort();
      knownProviders.push(ts('--new provider--'));
      this.knownProviders = knownProviders;
    };
    listProviders();

    this.updateProvider = (saveButton) => {
      if (this.providerName === ts('--new provider--')) {
        // Switching to new provider, don't do anything unless they clicked the save button.
        if (!saveButton) {
          return;
        }

        if (this.providerNameNew) {
          this.providerName = this.providerNameNew;
          listProviders();
        }
        else {
          // Bit rude
          alert(ts("Please enter a provider name"));
          return;
        }
      }
      crmApi4('Setting', 'set', { values: { emailunion_provider: this.providerName } });

      recalcRows();
    };
    this.saveHideDomains = () => {
      crmApi4('Setting', 'set', { values: { emailunion_hide_domains: this.hideDomains } })
      .then(() => {
        this.hideDomainsOriginal = this.hideDomains;
        CRM.alert(ts('Saved'), ts('Hidden domains setting'), 'success');
      });
    };
    this.saveAutoSubmit = () => {
      crmApi4('Setting', 'set', { values: { emailunion_auto_submit: this.autoSubmit } })
      .then( () => {
        CRM.alert(ts('Saved'), ts('Auto Submit Setting'), 'success');
      });
    };

    this.exportData = '';
    this.getExport = () => {
      crmApi('Email', 'getemailunionstats', {for_export: 1})
      .then(r => {
        this.exportData = JSON.stringify(r.values, null, 2);
      });
    };

    // Calculate rows.
    const recalcRows = () => {

      const avgs = stats.union.aggregateRates.average;
      if (avgs) {
        this.unionHoldPercent = avgs.average[0];
        this.providerHoldPercent = (this.providerName in avgs) ? avgs[this.providerName][0] : '?';
      }
      else {
        this.unionHoldPercent = '?';
        this.providerHoldPercent = '?';
      }

      this.rows = [];
      this.max = {
        onHold: 0,
        total: 0,
        localPercent: 0,
        providerPercent: 0,
        allPercent: 0,
      };
      stats.local.forEach(localRow => {
        const row = Object.assign({allPercent:null, providerPercent: null, localPercent: null}, localRow);
        if (row.total > 0) {
          row.localPercent = Math.round(row.onHold * 1000 / row.total) / 10;
        }
        // Do we have union stats for this domain?
        if (localRow.domain in stats.union.aggregateRates) {
          // Yep.
          const unionDomainStats = stats.union.aggregateRates[localRow.domain];
          // Do we have provider stats? @todo
          if (this.providerName in unionDomainStats) {
            row.providerPercent = unionDomainStats[this.providerName][0];
          }
          // Union average for this:
          row.allPercent = unionDomainStats.average[0];
        }
        ['onHold', 'total', 'localPercent', 'providerPercent', 'allPercent'].forEach(k => {
          if (row[k] && (this.max[k] < row[k])) {
            this.max[k] = row[k];
          }
        });
        this.rows.push(row);
      });

      // Apply sort.
      this.rows.sort((a, b) => {
        var v = 0;
        if (a[this.tableSort] < b[this.tableSort]) {
          v = -1
        }
        else if (a[this.tableSort] > b[this.tableSort]) {
          v = 1
        }
        v *= this.tableSortDir;
        return v;
      });
    };

    recalcRows();

    this.submit = (mode) => {
      this.isSubmitting = true;
      return crmStatus(
        // Status messages. For defaults, just use "{}"
        {start: ts('Submitting...'), success: (mode === 'delete') ? ts('Deleted') : ts('Thanks!')},
        // The submit action. Note that crmApi() returns a promise.
        crmApi('Email', 'Submitemailunionstats', { delete_data: (mode === 'delete') ? 1 : 0 })
        .then(r => {
          if (r.values.lastSubmitted) {
            this.lastSubmitted = r.values.lastSubmitted;
          }
        })
        .finally( () => {
          this.isSubmitting = false;
        })
      );
    };
  })
  .directive('bgBarchart', () => {
    return {
      restrict: 'E',
      scope: {
        val: '=val',
        max: '=max',
      },
      template: `<div class="barchart" ><div class="barchart-bar" ng-style="{width: (val / max * 100) + '%'}"></div><div class="barchart-text">{{val}}</div></div>`
    };
  });

})(angular, CRM.$, CRM._);
