# The Email Union!

![Screenshot](/images/screenshot.png)

- Understand how many emails you have on hold.

- Identify domain-specific (hotmail, yahoo, ...) issues.

- Measure how your SMTP gateway service (Sparkpost, Elastic Email, self hosted...) performs against others' experience.

- Opt-in to contribute your stats anonymously to make this work.

Emails get put on hold when they bounce. Sometimes even valid emails get put on hold because the email account provider (e.g. hotmail or yahoo) has decided that you're a spammer when you're not.

This extension shows you how may emails you have on hold on a per-domain basis which is helpful in identifying domain-specific problems. It shows you your data alongside data accumulated from the Email Union (i.e. those who install this extension and choose to submit data).

Screenshot of the settings options:

![Screenshot of settings](/images/settings-box.png)

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.2+
* CiviCRM 5.35 (probably works on 5.anything)

## Installation (Web UI)

Learn more about installing CiviCRM extensions in the [CiviCRM Sysadmin Guide](https://docs.civicrm.org/sysadmin/en/latest/customize/extensions/).

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl emailunion@https://github.com/FIXME/emailunion/archive/master.zip
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://lab.civicrm.org/extensions/emailunion.git
cv en emailunion
```

## Getting Started

1. Install and visit **Mailings » Email Union**.

2. Expand the *Info and settings* box and choose your SMTP service provider. If yours is not in the list then you can add it - more on that later.

3. This will show you how many emails you have on hold expressed as a percentage of all the emails in your CRM. You get these figures for each domain for which you have over 200 email addresses.

## What do the figures mean?

It counts emails that are on hold because of bounces (not opt-outs) and compares it to total emails. An email gets put on hold after certain types of bounces (or after a certain number of a certain type of bounce: e.g. if email is rejected as Spam it probably immediately gets put on hold, but if an email is rejected because the recipient's inbox is full, CiviCRM normally keeps trying for a few times before it puts the email on hold).

Once an email is on hold, it stays so unless it's manually released, deleted, or re-added.

This extension does *not* count bounces from your mailings; it does not use any data from your mailings.

Data is also pulled in from the Email Union database, a simple aggregation service provided by [Artful Robot](https://artfulrobot.uk). And you're encouraged to submit yours too; the more we have, the more helpful the info can be.

Note: we ignore emails that have been on hold for over a year. The reason for this is that we’re primarily interested in deliverability, and that changes over time. We don't need to care that last year some emails were on hold (and still are).

## Example please!

A real world example: I recently switched several clients to Elastic Email. I noticed a lot of bounces and as a result, a lot of on-hold emails.

This extension shows me that certain domains, e.g. btinternet, have a really high percentage of on-hold emails; this percentage is higher than other inbox providers, e.g. gmail. This suggests there's an issue specific to btinternet.

With data from the Email Union I might be able to see whether it's just *my* emails that are unlucky, or if this experience is typical for people using my email provider. If it's typical we could club together and complain to Elastic Email and/or btinternet, for example.

It also shows me the percentage for that domain across all SMTP services: if there's a mis-match here, it might be time to switch providers.

## Sharing data sounds dangerous? What about privacy and GDPR etc?

Email Union has been designed to be safe. We only deal with high level aggregate data.

- Only the email recipient domains are used - we don't use any actual email addresses.

- Only email recipient domains that you have over 500 of are used (note: you are shown domains with over 200 on your CRM, but only when you have over 500 are they included in the submission). So this automatically filters out rich@mypersonaldomain.uk - this data is only useful for the biggies anyway.

- You can provide a list of domains (with wildcards `%`) that you don't want to share data for. You'll still see them listed on your CRM but you won't be sharing those domains when you contribute your data.

- The above means **no personally identifiable data is shared**.

- For full transparency, there's a button you can press to see exactly what data would be sent from your site, should you choose to. So you can check your hidden domains are indeed hidden.

- Your site is given a unique dataset ID for the purposes of submission administration. This is a hash (i.e. a long code like `023bd23fa...`) that can't be used to identify your CRM site, but can be used to delete your old data when you contribute new data. This hash is not included in the data that is downloaded from the central service.


## Please consider enabling auto-submit for your data

From the Info and settings box you can choose to enable auto submission of your data. This will be super helpful to the project. This is done with a Scheduled Job that runs daily. That job must be enabled *and* this option enabled for auto-submission to have effect.

The scheduled job's log will show whether any data was submitted. If it's disabled it will say "Did NOT submit any stats because auto submit is not enabled in Email Union settings" for your reassurance.

## A note on data updates and caching

There are several layers of caching, and it's possible these change over time.

- Your CRM will cache your own sites's data for 1 hour, and will cache the shared data for 1 day.

- When you submit data your cache is cleared.

- The shared data JSON file is currently updated on every submission. Should this become problematic for performance (e.g. if this gets super popular) then it might change to being updated by a cron or something.

- You can clear your caches manually in the usual way.
