<?php
/**
 * Copyright (C) 2022  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\CaseArchive\Documents;

use Civi\CaseArchive\Documents\Entity\CaseDocuments;
use Civi\CaseArchive\Entity\CaseEntity;
use Civi\CaseArchive\Event\PrepareArchiveEvent;

class EventListener {

  /**
   * @param \Civi\CaseArchive\Event\PrepareArchiveEvent $event
   *
   * @return void
   */
  public static function onPrepare(PrepareArchiveEvent $event) {
    if ($event->entity instanceof CaseEntity) {
      $caseDocuments = new CaseDocuments($event->entity);
      $caseDocuments->onPrepare($event->archiveFile);
    }
  }


}
