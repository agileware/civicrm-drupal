<?php
/**
 * Copyright (C) 2022  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\CaseArchive\Documents\Section;

use Civi\CaseArchive\ArchiveFile;
use Civi\CaseArchive\Section\AbstractSection;
use Civi\CaseArchive\ArchiveFile\FileAttachment;
use Civi\CaseArchive\Documents\DocumentFacade;
use Civi\CaseArchive\Documents\Entity\CaseDocuments;
use Civi\CaseArchive\Entity\AbstractEntity;
use CRM_Documents_ExtensionUtil as E;

class CaseDocumentSection extends AbstractSection {

  /**
   * @var string
   */
  protected $templateFileName = 'CRM/CaseArchive/Documents/CaseDocumentSection.tpl';

  /**
   * @var \Civi\CaseArchive\Documents\Entity\CaseDocuments
   */
  protected $caseDocuments;

  /**
   * @var string
   */
  protected $documentsPath;

  /**
   * @param \Civi\CaseArchive\Entity\AbstractEntity $entity
   * @param \Civi\CaseArchive\Documents\Entity\CaseDocuments $caseDocuments
   * @param string $documentsPath
   */
  public function __construct(AbstractEntity $entity, CaseDocuments $caseDocuments, string $documentsPath) {
    parent::__construct($entity);
    $this->caseDocuments = $caseDocuments;
    $this->documentsPath = $documentsPath;
  }

  /**
   * Set the template file for this section.
   *
   * @param string $templateFileName
   *
   * @return void
   */
  public function setTemplateFileName(string $templateFileName) {
    $this->templateFileName = $templateFileName;
  }

  /**
   * @return string
   */
  public function getSectionTitle(): string {
    return E::ts('Documents');
  }

  /**
   * Method called when an archive file is build
   *
   * @param \Civi\CaseArchive\ArchiveFile $archiveFile
   *
   * @return void
   */
  public function onBuild(ArchiveFile $archiveFile) {
    $rootPath = $this->entity->getHtmlFile()->getRootPath();
    $documents = [];
    foreach($this->caseDocuments->getDocuments() as $document) {
      /** @var int $docId */
      $docId = $document->getId();
      $archiveAttachmentPath = $this->entity->getHtmlFile()->getAttachmentPath() . '/documents/document_'.$docId.'/current_version';
      $attachment = new FileAttachment($document->getCurrentVersion()->getAttachment()->fileID, $document->getCurrentVersion()->getAttachment()->filename, $archiveAttachmentPath);
      $archiveFile->addAttachment($attachment);
      $attachmentLink = $rootPath.$attachment->getRelativePath();
      $detailUrl = $this->documentsPath . $this->caseDocuments->getFileName($docId);
      $documentFacade = new DocumentFacade($document, $attachmentLink, $detailUrl);
      $documents[] = $documentFacade;
    }
    $vars = [
      'documents' => $documents,
    ];
    $this->content = $this->fetchTemplate($vars, $this->templateFileName);
    $name = 'caseDocuments_'.$this->entity->getEntityId();
    $this->entity->getHtmlFile()
      ->addContentSection($this->getSectionTitle(), $this->content, $name);
  }

}
