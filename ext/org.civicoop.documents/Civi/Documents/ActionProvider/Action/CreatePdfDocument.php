<?php
/**
 * Copyright (C) 2024  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
namespace Civi\Documents\ActionProvider\Action;

use Civi\ActionProvider\Parameter\ParameterBagInterface;
use Civi\ActionProvider\Parameter\Specification;
use Civi\ActionProvider\Parameter\SpecificationBag;
use Civi\ActionProvider\Utils\Tokens;
use CRM_Documents_ExtensionUtil as E;

class CreatePdfDocument extends UploadNewDocument {

  public function doAction(ParameterBagInterface $parameters, ParameterBagInterface $output) {
    $participantId = $parameters->getParameter('participant_id');
    $message = $parameters->getParameter('message');
    $contactId = $parameters->getParameter('contact_id');
    $filename = $this->configuration->getParameter('filename');
    $contact = [];
    if ($participantId) {
      $contact['extra_data']['participant']['id'] = $participantId;
    }
    if ($parameters->doesParameterExists('contribution_recur_id')) {
      $contact['extra_data']['contribution_recur']['id'] = $parameters->getParameter('contribution_recur_id');
    }
    if ($parameters->doesParameterExists('case_id')) {
      $contact['case_id'] = $parameters->getParameter('case_id');
    }
    if ($parameters->doesParameterExists('contribution_id')) {
      $contact['contribution_id'] = $parameters->getParameter('contribution_id');
    }
    if ($parameters->doesParameterExists('activity_id')) {
      $contact['activity_id'] = $parameters->getParameter('activity_id');
    }
    if ($parameters->doesParameterExists('membership_id')) {
      $contact['membership_id'] = $parameters->getParameter('membership_id');
    }
    $this->pdfFormat = null;
    if ($parameters->doesParameterExists('page_format_id')) {
      $this->pdfFormat = $parameters->getParameter('page_format_id');
    }

    $fileName = $filename . '.pdf';
    $processedMessage = Tokens::replaceTokens($contactId, $message, $contact, 'text/html');
    if ($processedMessage === false) {
      return;
    }
    //time being hack to strip '&nbsp;'
    //from particular letter line, CRM-6798
    \CRM_Contact_Form_Task_PDFLetterCommon::formatMessage($processedMessage);
    $this->messages[] = $processedMessage;
    $text = array($processedMessage);
    $pdfContents = \CRM_Utils_PDF_Utils::html2pdf($text, $fileName, TRUE, $this->pdfFormat);

    $document = $this->createDocument($parameters, [$contactId]);
    $version = $document->getCurrentVersion();
    $this->uploadFileContent($parameters, $version, $pdfContents, $fileName, 'application/pdf');
    $output->setParameter('document_id', $document->getId());
  }

  /**
   * Returns the specification of the parameters of the actual action.
   *
   * @return SpecificationBag
   */
  public function getParameterSpecification() {
    return new SpecificationBag(array(
      new Specification('contact_id', 'Integer', E::ts('Contact ID'), true),
      new Specification('message', 'String', E::ts('Message'), true),
      new Specification('activity_id', 'Integer', E::ts('Activity ID'), false),
      new Specification('contribution_id', 'Integer', E::ts('Contribution ID'), false),
      new Specification('contribution_recur_id', 'Integer', E::ts('Recurring Contribution ID'), false),
      new Specification('case_id', 'Integer', E::ts('Case ID'), false),
      new Specification('participant_id', 'Integer', E::ts('Participant ID'), false),
      new Specification('membership_id', 'Integer', E::ts('Membership ID'), false),
      new Specification('subject', 'String', E::ts('Subject'), false),
      new Specification('description', 'String', E::ts('Description'), false),
      new Specification('page_format_id', 'Integer', E::ts('Print Page (PDF) Format'), false),
    ));
  }

  public function getConfigurationSpecification() {
    $spec = parent::getConfigurationSpecification();
    $filename = new Specification('filename', 'String', E::ts('Filename'), true, E::ts('document'));
    $filename->setDescription(E::ts('Without the extension .pdf'));
    $spec->addSpecification($filename);
    return $spec;
  }

  public function getOutputSpecification() {
    return new SpecificationBag([
      new Specification('document_id', 'Integer', E::ts('Document ID')),
    ]);
  }

  /**
   * Returns a help text for this action.
   *
   * The help text is shown to the administrator who is configuring the action.
   * Override this function in a child class if your action has a help text.
   *
   * @return string|false
   */
  public function getHelpText() {
    return E::ts("This action generates PDF Files and stores it as a document either on the contact or on the case.");
  }

}
