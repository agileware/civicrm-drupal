<?php

namespace Civi\Documents\ActionProvider\Action;

use \Civi\ActionProvider\Action\AbstractAction;
use Civi\ActionProvider\Parameter\FileSpecification;
use Civi\ActionProvider\Parameter\OptionGroupSpecification;
use \Civi\ActionProvider\Parameter\ParameterBagInterface;
use \Civi\ActionProvider\Parameter\SpecificationBag;
use \Civi\ActionProvider\Parameter\Specification;
use \Civi\ActionProvider\Utils\CustomField;

use CRM_Documents_ExtensionUtil as E;

class ChangeDocument extends AbstractAction {

  /**
   * Run the action
   *
   * @param ParameterBagInterface $parameters
   *   The parameters to this action.
   * @param ParameterBagInterface $output
   *   The parameters this action can send back
   * @return void
   */
  protected function doAction(ParameterBagInterface $parameters, ParameterBagInterface $output) {
    $documentsRepo = \CRM_Documents_Entity_DocumentRepository::singleton();
    $document = $documentsRepo->getDocumentById($parameters->getParameter('document_id'));
    if ($parameters->getParameter('subject')) {
      $document->setSubject($parameters->getParameter('subject'));
    } elseif ($this->configuration->getParameter('subject')) {
      $document->setSubject($this->configuration->getParameter('subject'));
    }
    if ($parameters->getParameter('type')) {
      $document->setTypeId($parameters->getParameter('type'));
    } elseif ($this->configuration->getParameter('type')) {
      $document->setTypeId($this->configuration->getParameter('type'));
    }
    if ($parameters->getParameter('status')) {
      $document->setStatusId($parameters->getParameter('status'));
    } elseif ($this->configuration->getParameter('status')) {
      $document->setStatusId($this->configuration->getParameter('status'));
    }
    $documentsRepo->persist($document);
    $output->setParameter('document_id', $document->getId());
  }

  /**
   * Returns the specification of the configuration options for the actual action.
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag([
      new Specification('subject', 'String', E::ts('Subject'), false),
      new OptionGroupSpecification('type', 'document_type', E::ts('Document Type'), false),
      new OptionGroupSpecification('status', 'document_status', E::ts('Document Status'), false),
    ]);
  }

  /**
   * Returns the specification of the parameters of the actual action.
   *
   * @return SpecificationBag
   */
  public function getParameterSpecification() {
    $specs = new SpecificationBag([
      new Specification('subject', 'String', E::ts('Subject'), false),
      new OptionGroupSpecification('type', 'document_type', E::ts('Document Type'), false),
      new OptionGroupSpecification('status', 'document_status', E::ts('Document Status'), false),
      new Specification('document_id', 'Integer', E::ts('Document ID'), true, null, null, null, false)
    ]);
    return $specs;
  }

  /**
   * Returns the specification of the output parameters of this action.
   *
   * This function could be overriden by child classes.
   *
   * @return SpecificationBag
   */
  public function getOutputSpecification() {
    return new SpecificationBag([
      new Specification('document_id', 'Integer', E::ts('Document ID')),
    ]);
  }


}
