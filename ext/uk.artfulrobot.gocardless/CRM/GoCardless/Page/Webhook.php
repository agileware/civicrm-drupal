<?php
/**
 * @file
 * Provides webhook endpoint for GoCardless.
 */
require_once 'CRM/Core/Page.php';

class CRM_GoCardless_Page_Webhook extends CRM_Core_Page {

  /**
   * Main entry point for legacy webhooks.
   */
  public function run() {
    // As we don't know the payment processor, we pass in NULL.

    // We need to check the input against the test and live payment processors.
    $body = file_get_contents('php://input');
    if (!function_exists('getallheaders')) {
      // https://lab.civicrm.org/extensions/gocardless/-/issues/23
      // Some server configs do not provide getallheaders().
      // We only care about the Webhook-Signature header so try to extract that from $_SERVER.
      $headers = [];
      if (isset($_SERVER['HTTP_WEBHOOK_SIGNATURE'])) {
        $headers['Webhook-Signature'] = $_SERVER['HTTP_WEBHOOK_SIGNATURE'];
      }
    }
    else {
      $headers = getallheaders();
    }

    $handler = new CRM_Core_Payment_GoCardlessIPN();
    http_response_code($handler->handleRequest($headers, $body));
    CRM_Utils_System::civiExit();
  }

}
