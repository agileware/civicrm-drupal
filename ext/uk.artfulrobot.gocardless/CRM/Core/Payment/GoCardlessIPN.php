<?php
use Civi\Api4\PaymentprocessorWebhook;
use Civi\GoCardless\Exceptions\WebhookProcessingException;
use Civi\GoCardless\Exceptions\WebhookIgnoredException;

/**
 * Provides an IPN Class to handle webhooks.
 *
 */
class CRM_Core_Payment_GoCardlessIPN {

  /**
   * @var \Psr\Log\AbstractLogger
   */
  public $logger;

  /**
   * This is static as a cache. It is protected because otherwise an external
   * caller might think it's populated when it's not. It is populated in the
   * constructor.
   *
   * @var bool
   */
  protected static $webhookQueuingSupported;

  /**
   * @var arraylistofallimplementedwebhooks
   */
  public static $implemented_webhooks = [
    'payments'         => ['confirmed', 'failed'],
    'subscriptions'    => ['cancelled', 'finished'],
    'billing_requests' => ['fulfilled'],
  ];

  /**
   * @var bool*/
  protected $test_mode;

  /**
   * @var arrayofwebhookeventsthatwecanprocess
   */
  public $events;

  /**
   * @var CRM_Core_Payment_GoCardless
   */
  protected $paymentProcessorObject;

  /**
   * @var string
   */
  public $webhookID;
  /**
   * @var string
   */
  public $eventID;

  /**
   * For testing only.
   */
  public static function setWebhookQueuingSupport(bool $val) {
    static::$webhookQueuingSupported = $val;
  }

  /**
   * Constructor.
   *
   * @param NULL|CRM_Core_Payment_GoCardless $paymentClass
   *    NULL is for legacy use only.
   */
  public function __construct(?CRM_Core_Payment_GoCardless $paymentObject = NULL) {
    if ($paymentObject !== NULL && !($paymentObject instanceof CRM_Core_Payment_GoCardless)) {
      // This would be a coding error.
      throw new Exception(__CLASS__ . " constructor requires CRM_Core_Payment_GoCardless object (or NULL for legacy use).");
    }
    $this->paymentProcessorObject = $paymentObject;

    if (!isset(static::$webhookQueuingSupported)) {
      // Default is nope.
      static::$webhookQueuingSupported = FALSE;
      if ('installed' === CRM_Extension_System::singleton()->getManager()->getStatus('mjwshared')) {
        // It is installed, but is the version good enough?
        if (version_compare(CRM_Extension_System::singleton()
          ->getMapper()->keyToInfo('mjwshared')->version, '1.2') >= 0) {
          static::$webhookQueuingSupported = TRUE;
        }
      }
    }

    $this->logger = Civi::log('GoCardless');
  }

  /**
   * Handles the incoming http webhook request and returns a suitable http response code.
   */
  public function handleRequest(array $headers, string $body) :int {

    // Identify the webhookID, these begin with WB and can be looked up on GC's website.
    // Our logging uses the webhook and event IDs to prefix messages.
    $this->webhookID = $this->eventID = '';
    $webhookID = json_decode($body)->meta->webhook_id ?? NULL;
    $this->setWebhookID($webhookID);
    $requestURI = $_SERVER['REQUEST_URI'] ?? '<none>';
    $logContext = [
      'requestSentTo' => $requestURI,
      'headers' => $headers,
    ];
    if (!$webhookID) {
      // Reject webhook; should show as error at manage.gocardless.com
      $this->logger->error("FAILED: Webhook received lacks webhookID! This should not happen. Responding with 400 error. Raw body follows:\n$body", $logContext);
      return 400;
    }
    $this->logger->debug("START: Webhook received. Raw body follows\n$body", $logContext);

    try {
      $this->parseWebhookRequest($headers, $body);

      if (static::$webhookQueuingSupported) {
        // Queue the webhook events.
        $records = [];
        // $event does not properly implement JsonSerializable!
        foreach ($this->events as $event) {
          // Nb. we set trigger and identifier here mostly to help when troubleshooting.
          $records[] = [
            'event_id'   => $event->id,
            'trigger'    => ucfirst($event->resource_type ?? 'unknown') . ucfirst($event->action ?? 'unknown'),
            'identifier' => $this->webhookID, /* We don't strictly need this but it might be useful for troubleshooting */
            'data'       => json_encode($event),
          ];
        }
        if ($records) {
          // Store the events. They will receive status 'new'. Note that
          // because we filter out events we don't need, there may not be any
          // records to record.
          \Civi\Api4\PaymentprocessorWebhook::save(FALSE)
            ->setRecords($records)
            ->setDefaults(['payment_processor_id' => $this->paymentProcessorObject->getID(), 'created_date' => 'now'])
            ->execute();
        }

        $this->logger->info("OK: " . count($records) . " webhook events queued for background processing, returning 204 success.", ['=set' => []]);
      }
      else {
        $this->processWebhookEvents();
        $this->logger->info("OK: Completed processWebhookEvents, will return 204 success.", ['=set' => []]);
      }

      // Success, respond with 204, no content OK response code.
      return 204;
    }
    catch (\GoCardlessPro\Core\Exception\InvalidSignatureException $e) {
      // Log the invalid webhook call.
      $this->logger->error("FAILED: InvalidSignatureException " . $e->getMessage() . "\nWill respond with 400 http status");
      // Respond with Bad Request response code.
      // Note that the docs at https://developer.gocardless.com/getting-started/api/staying-up-to-date-with-webhooks/
      // still (28 Oct 2021) refer to issuing a non-standard 498 error. But we prefer 400.
      return 400;
    }
    catch (InvalidArgumentException $e) {
      // Log the invalid webhook call.
      $this->logger->error("FAILED: InvalidArgumentException " . $e->getMessage() . "\nWill respond with 400 http status");
      // Respond with Bad Request response code.
      return 400;
    }
    catch (\Exception $e) {
      // Something weird went on.
      $this->logger->error(
        "FAILED: " . get_class($e) . ": " . $e->getMessage() . "\nWill respond with 500 http status", [
          'exception' => $e,
        ]);
      // Respond with Server Error response code.
      return 500;
    }
  }

  /**
   * Check incoming input for validity and extract the data.
   *
   * Alters $this->events and sets
   * $this->paymentProcessorObject unless already set.
   *
   * http request
   *   -> paymentClass::handlePaymentNotification()
   *     -> this::handleRequest()
   *       -> parseWebhookRequest()
   *
   * @throws InvalidArgumentException if signature does not match.
   *
   * @param array $headers
   * @param string $raw_payload
   *
   * @return void
   */
  public function parseWebhookRequest($headers, $raw_payload) {

    // Check signature and find appropriate Payment Processor.
    // GoCardless announced in Jan 2020 that their headers would now be sent
    // lowercase and must be treated as case-insensitive.
    $provided_signature = NULL;
    foreach ($headers as $key => $value) {
      if (strtolower($key) === 'webhook-signature') {
        $provided_signature = $value;
        break;
      }
    }
    if (empty($provided_signature)) {
      throw new InvalidArgumentException("Unsigned API request.");
    }

    if ($this->paymentProcessorObject) {
      // Modern call, i.e. via civicrm/payment/ipn/<payment_processor_id>
      // We know which payment processor this is for, so the token must match.
      $config = $this->paymentProcessorObject->getPaymentProcessor();
      if (empty($config['signature'])) {
        throw new InvalidArgumentException("GoCardless Payment Processor ID $config[id] is misconfigured: no webhook secret.");
      }
      // Parse events: this may throw \GoCardlessPro\Core\Exception\InvalidSignatureException
      $events = $this->parseWebhookRequestEvents($raw_payload, $provided_signature, $config['signature']);

      // All valid, good to continue.
    }
    else {
      CRM_GoCardlessUtils::recordLegacyIPN();
      // Legacy call where the Payment Processor ID was not included in the webhook URL,
      // Loop through all GoCardless Payment Processors until we find one for which the signature is valid.
      $candidates = civicrm_api3('PaymentProcessor', 'get', ['payment_processor_type_id' => "GoCardless", 'is_active' => 1]);
      $valid = FALSE;
      foreach ($candidates['values'] as $payment_processor_id => $pp) {
        $webhook_secret = isset($pp['signature']) ? $pp['signature'] : '';
        if ($webhook_secret) {
          try {
            // This might throw InvalidSignatureException
            $events = $this->parseWebhookRequestEvents($raw_payload, $provided_signature, $webhook_secret);
            $valid = TRUE;
            $this->paymentProcessorObject = Civi\Payment\System::singleton()->getByProcessor($pp);
            break;
          }
          catch (\GoCardlessPro\Core\Exception\InvalidSignatureException $e) {
          }
        }
      }
      if (!$valid) {
        throw new InvalidArgumentException("Invalid signature in request. (Or payment processor is not active. Also: please update your webhook endpoint as you are using the deprecated version.)");
      }
    }

    // Filter for events that we can handle.
    //
    // Index by event id is safe because it's unique, and it makes testing easier :-)
    $this->events = [];
    foreach ($events as $event) {
      $this->setEventID($event->id);
      if (isset(static::$implemented_webhooks[$event->resource_type])
        && in_array($event->action, static::$implemented_webhooks[$event->resource_type])) {
        $this->events[$event->id] = $event;
      }
      else {
        $this->logger->debug("OK: Ignored $event->resource_type.$event->action (This is normal, we do not need to take action for this event.)");
      }
    }
    $this->setEventID('');
  }

  /**
   * Parse the webhook raw input (JSON) using GC library's code.
   *
   * Then, if we'll be queuing this, parse it into plain StdClass objects so we can store it safely on the queue.
   *
   */
  protected function parseWebhookRequestEvents(string $rawPayload, $providedSignature, $configuredSignature) :array {
    $events = \GoCardlessPro\Webhook::parse($rawPayload, $providedSignature, $configuredSignature);
    if (static::$webhookQueuingSupported) {
      // We will be queuing this, and we can't json_encode a GC Event resource object.
      // Now we have verified that the request came from GC, we can trust it fairly, so
      // we will use StdClass events instead:
      $events = json_decode($rawPayload)->events;
    }
    return $events;
  }

  /**
   * Loop the events and process them. This is called when running live, not in queued mode.
   *
   * It's the live version of processQueuedWebhookEvent
   *
   * @param bool $throw whether to silently log exceptions or chuck them up for
   * someone else to notice. Useful for phpunit tests.
   */
  public function processWebhookEvents($throw = FALSE) {
    foreach ($this->events as $event) {
      $processingResult = $this->processWebhookEvent($event);
      if ($throw && $processingResult->exception) {
        throw $processingResult->exception;
      }
    }
  }

  /**
   * Process a single *queued* event and update it.
   *
   * It's the queue version of processWebhookEvents
   *
   * Returns TRUE/FALSE for success.
   */
  public function processQueuedWebhookEvent(array $webhookEvent) :bool {
    $this->setWebhookID($webhookEvent['identifier']);

    // @todo consider instantiating the GC Event object here.
    $event = json_decode($webhookEvent['data']);

    $processingResult = $this->processWebhookEvent($event);
    // Update the stored webhook event.
    PaymentprocessorWebhook::update(FALSE)
      ->addWhere('id', '=', $webhookEvent['id'])
      ->addValue('status', $processingResult->ok ? 'success' : 'error')
      ->addValue('message', preg_replace('/^(.{250}).*/su', '$1 ...', $processingResult->message))
      ->addValue('processed_date', 'now')
      ->execute();

    return $processingResult->ok;
  }

  /**
   * Processes a single event, catches exceptions and returns an object.
   *
   * This is called by both queued (processQueuedWebhookEvent) and non-queued (processWebhookEvents) contexts.
   *
   * The result class includes keys:
   * - message string
   * - ok boolean
   * - exception if one occurred.
   */
  public function processWebhookEvent($event) :StdClass {
    $return = (object) ['message' => NULL, 'ok' => FALSE, 'exception' => NULL];
    // This event ID is only used for logging messages.
    $this->setEventID($event->id);
    try {
      $method = 'do' . ucfirst($event->resource_type) . ucfirst($event->action);
      $return->message = $this->$method($event);
      $return->ok = TRUE;
      $this->logger->info($return->message);
    }
    catch (Civi\GoCardless\Exceptions\Base $e) {
      $return->message = $e->getMessage();
      $return->ok = $e->isOk();
      $return->exception = $e;
    }
    catch (Exception $e) {
      $return->message = "FAILED: Had to skip webhook event. Reason: " . $e->getMessage() . "\n" . $e->getTraceAsString();
      $return->exception = $e;
      $this->logger->critical($return->message);
    }
    // Add message to log with appropriate value
    $this->setEventID('');
    return $return;
  }

  /**
   * Process webhook for 'payments' resource type, action 'confirmed'.
   *
   * A payment has been confirmed as successful.
   * We can look up the contribution recur record from the subscription id and
   * from then we can add a contribution.
   *
   * When the direct debit is first set up, e.g. by a Contribution page, the
   * first payment is already created with status incomplete. So for this
   * reason we look for a contribution like this and update that if we find one
   * instead of adding another.
   */
  public function doPaymentsConfirmed($event) :string {
    $this->logger->info("doPaymentsConfirmed {$event->links->payment}");
    $worker = new \Civi\GoCardless\Events\PaymentsConfirmed($this->paymentProcessorObject, $event->links->payment);
    return $worker->process();
  }

  /**
   * Process webhook for 'payments' resource type, action 'failed'.
   */
  public function doPaymentsFailed($event) :string {
    $this->logger->info("doPaymentsFailed {$event->links->payment}");
    $worker = new \Civi\GoCardless\Events\PaymentsFailed($this->paymentProcessorObject, $event->links->payment);
    return $worker->process();
  }

  /**
   * Process webhook for 'billing_requests' resource type, action 'fulfilled'.
   */
  public function doBilling_requestsFulfilled($event) :string {
    $this->logger->info("doBillingRequestsFulfilled {$event->links->billing_request}");
    $worker = new \Civi\GoCardless\Events\BillingRequestsFulfilled($this->paymentProcessorObject, $event->links->billing_request);
    return $worker->process();
  }

  /**
   * Process webhook for 'mandate' resource type, action 'finished'.
   *
   * In this case the subscription has come to its natural end.
   */
  public function doSubscriptionsFinished($event) :string {
    $this->logger->info("doSubscriptionsFinished {$event->links->subscription}");
    $subscription = $this->getAndCheckSubscription($event, 'finished');
    $recur = $this->getContributionRecurFromSubscriptionId($subscription->id);

    $update = [
      'id' => $recur['id'],
      'contribution_status_id' => 'Completed',
      'end_date' => !empty($subscription->end_date) ? $subscription->end_date : date('Y-m-d'),
    ];
    civicrm_api3('ContributionRecur', 'create', $update);
    $cancelledContribs = implode(', ', $this->cancelPendingContributions($recur));
    return "OK: SubscriptionsFinished completed. Recur: $recur[id] Contribution(s) cancelled: $cancelledContribs";
  }

  /**
   * Process webhook for 'mandate' resource type, action 'cancelled'.
   *
   * This covers a number of reasons. Typically, the supporter cancelled.
   */
  public function doSubscriptionsCancelled($event) :string {
    $this->logger->info("doSubscriptionsCancelled {$event->links->subscription}");
    $subscription = $this->getAndCheckSubscription($event, 'cancelled');
    $recur = $this->getContributionRecurFromSubscriptionId($subscription->id);
    $update = [
      'id' => $recur['id'],
      'contribution_status_id' => 'Cancelled',
      'end_date' => !empty($subscription->end_date) ? $subscription->end_date : date('Y-m-d'),
      'cancel_date' => !empty($subscription->end_date) ? $subscription->end_date : date('Y-m-d'),
    ];
    civicrm_api3('ContributionRecur', 'create', $update);
    $cancelledContribs = implode(', ', $this->cancelPendingContributions($recur));

    // Send a hook to allow custom integrations to do other things.
    CRM_GoCardless_Hook::GoCardlessSubscriptionCancelled((int) $recur['id']);
    return "OK: SubscriptionsCancelled completed. Recur: $recur[id] Contribution(s) cancelled: $cancelledContribs";
  }

  /**
   * Helper to load and return GC payment object.
   *
   * We check that the status is expected and that the payment belongs to
   * subscription.
   *
   * @throws \Civi\GoCardless\Exceptions\Base
   * @param GoCardlessPro\Resources\Event $event
   * @param array $expected_status array of acceptable stati
   * @return NULL|\GoCardless\Resources\Payment
   */
  public function getAndCheckGoCardlessPayment($event, $expected_status) {
    $gc_api = $this->paymentProcessorObject->getGoCardlessApi();
    // According to GoCardless we need to check that the status of the object
    // has not changed since the webhook was fired, so we re-load it and test.
    $payment = $gc_api->payments()->get($event->links->payment);
    if (!in_array($payment->status, $expected_status)) {
      // Payment status is no longer as expected, ignore this webhook.
      throw new WebhookIgnoredException("OK: Webhook out of date, expected status "
      . implode("' or '", $expected_status)
      . ", got '{$payment->status}'", 'notice');
    }

    // We expect a subscription link, but not all payments have this.
    if (empty($payment->links->subscription)) {
      // This payment is not part of a subscription. Assume it's not of interest to us.
      throw new WebhookIgnoredException("OK: Ignored payment that does not belong to a subscription (all CiviCRM-related ones would belong to a subscription).", 'notice');
    }

    return $payment;
  }

  /**
   * Looks up the ContributionRecur record for the given GC subscription Id.
   *
   * @throws WebhookIgnoredException
   * @param string $subscription_id
   * @return array
   */
  public function getContributionRecurFromSubscriptionId($subscription_id) {
    if (!$subscription_id) {
      throw new WebhookProcessingException("ERROR: No subscription_id data passed into getContributionRecurFromSubscriptionId");
    }

    // Find the recurring payment by the given subscription which will be
    // stored in the processor_id field.
    try {
      $recur = civicrm_api3('ContributionRecur', 'getsingle', [
        'processor_id' => $subscription_id,
      ]);
    }
    catch (CiviCRM_API3_Exception $e) {/* Note: this class is a class_alias and intelephense does not realise that */
      throw new WebhookProcessingException("ERROR: No matching recurring contribution record for processor_id {$subscription_id}");
    }
    return $recur;
  }

  /**
   * See if we have a pending contribution for the given contribution_record record.
   *
   * @param array $recur (only the 'id' key is used)
   * @return ?int Either the contribution id of the pending contribution, or NULL
   */
  public function getPendingContributionId($recur): ?int {
    // See if there's a Pending contribution we can update. xxx ??? No contribs at all?
    $incomplete_contribs = civicrm_api3('Contribution', 'get', [
      'sequential'             => 1,
      'contribution_recur_id'  => $recur['id'],
      'contribution_status_id' => "Pending",
      'is_test'                => $this->paymentProcessorObject->isTestMode() ? 1 : 0,
    ]);
    if ($incomplete_contribs['count'] > 0) {
      // Found one (possibly more than one, edge case - ignore and take first).
      return (int) $incomplete_contribs['values'][0]['id'];
    }
    return NULL;
  }

  /**
   * Get the first payment for this recurring contribution.
   *
   * This also attempts to handle Special case (Issue #82):
   * If the initial contrib Failed, we try to create a template contribution first.
   *
   * @param array $recur (only the 'id' key is used)
   * @return array The original contribution, if found. Plus key _was which can be:
   *   not_found (cannot proceed)
   *    found_completed
   *    found_template (includes created template)
   */
  public function getOriginalContribution($recur, $tryCreateTemplate = TRUE) {
    // Load the template (if exists) or latest contribution (which might be of any status, as of 5.50).
    $contrib = CRM_Contribute_BAO_ContributionRecur::getTemplateContribution($recur['id']);
    if (empty($contrib)) {
      $this->logger->error("FAILED: Did not find any Contributions at all for ContributionRecur (ID $recur[id]). This is not right.");
      $contrib = ['_was' => 'not_found'];
    }
    else {
      switch (CRM_Contribute_BAO_Contribution::buildOptions('contribution_status_id', 'validate')[$contrib['contribution_status_id']]) {
        case 'Completed':
          // Normal.
          $contrib['_was'] = 'found_completed';
          break;

        case 'Template':
          $contrib['_was'] = 'found_template';
          break;

        default:
          if ($tryCreateTemplate) {
            $this->logger->warning("UNEXPECTED: Did not find any completed or template Contributions but found a non-completed Contribution. Trying to create a template now for ContributionRecur (ID $recur[id]).");
            $newTemplateID = CRM_Contribute_BAO_ContributionRecur::ensureTemplateContributionExists($recur['id']);
            if ($newTemplateID) {
              $this->logger->info("Template created $newTemplateID");
              // Loop back, but don’t try this again. (Should not happen anyway, but we don’t like infinite loops.)
              return $this->getOriginalContribution($recur, FALSE);
            }
            else {
              $this->logger->warning("FAILED: Creating a template for recur $recur[id] failed.");
              return ['_was' => 'not_found'];
            }
          }
          else {
            $this->logger->warning("UNEXPECTED: Did not find any completed or template Contributions to use.");
            return ['_was' => 'not_found'];
          }
      }
    }
    return $contrib;
  }

  /**
   * Helper to load and return GC subscription object.
   *
   * We check that the status is expected.
   *
   * @param GoCardlessPro\Resources\Event $event
   * @param string $expected_status
   * @return NULL|\GoCardless\Resources\Subscription
   */
  public function getAndCheckSubscription($event, $expected_status) {
    $gc_api = $this->paymentProcessorObject->getGoCardlessApi();
    // According to GoCardless we need to check that the status of the object
    // has not changed since the webhook was fired, so we re-load it and test.
    $subscription = $gc_api->subscriptions()->get($event->links->subscription);
    if ($subscription->status != $expected_status) {
      // Payment status is no longer confirmed, ignore this webhook.
      throw new WebhookIgnoredException("OK: Skipping this event as it is out of date, expected subscription status '$expected_status', got '{$subscription->status}'");
    }

    return $subscription;
  }

  /**
   * Cancel any Pending Contributions from this recurring contribution.
   *
   * @param array $recur
   */
  public function cancelPendingContributions($recur) :array {
    // There should only be one, but just in case...
    $cancelled = [];
    while ($pending_contribution_id = $this->getPendingContributionId($recur)) {
      civicrm_api3('Contribution', 'create', [
        'id' => $pending_contribution_id,
        'contribution_status_id' => "Cancelled",
      ]);
      $cancelled[] = $pending_contribution_id;
    }
    return $cancelled;
  }

  /**
   *
   */
  protected function setWebhookID(string $webhookID) {
    $this->webhookID = $webhookID;
    $this->setLogID();
  }

  /**
   * Stores event ID and updates logger.
   */
  protected function setEventID(string $eventID) {
    $this->eventID = $eventID;
    $this->setLogID();
  }

  /**
   * Re-set the logger labels.
   */
  protected function setLogID() {
    $labels = [PHP_SAPI];
    if (!empty($this->webhookID)) {
      $labels[] = $this->webhookID;
    }
    if (!empty($this->eventID)) {
      $labels[] = $this->eventID;
    }
    $this->logger->info('', ['=set' => $labels]);
  }

}
