<?php
namespace Civi\GoCardless\Exceptions;

/**
 * @class
 * Exception for when an event in a webhook is ignored because
 * we don't need to process it.
 *
 * Safely ignorable, meh.
 */
class WebhookIgnoredException extends Base {

  public function __construct(string $message) {
    parent::__construct($message, 'debug');
  }

}
