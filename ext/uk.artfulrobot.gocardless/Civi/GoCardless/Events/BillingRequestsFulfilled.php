<?php
namespace Civi\GoCardless\Events;

use GoCardlessPro\Resources\Payment;
use GoCardlessPro\Resources\Subscription;
use GoCardlessPro\Resources\BillingRequest;
use CRM_Core_Payment_GoCardless;
use Civi;
use Civi\Api4\ContributionRecur;
use Civi\Api4\Contribution;
use Civi\Api4\Note;
use Brick\Money\Money;
use Civi\GoCardless\Exceptions\WebhookAlreadyProcessedException;
use Civi\GoCardless\Exceptions\WebhookProcessingException;

class BillingRequestsFulfilled {

  public CRM_Core_Payment_GoCardless $paymentProcessorObject;

  public \GoCardlessPro\Client $gcApi;

  public BillingRequest $brq;

  public ?Payment $payment = NULL;

  public ?Subscription $subscription = NULL;

  /**
   * Contribution ID
   */
  public ?int $cnID = NULL;

  /**
   * ContributionRecur
   *
   * This may be set externally after construct and before process.
   */
  public ?array $cr = NULL;

  /**
   * The IPN object that handles payments.confirmed, if the billing request
   * had a payment.
   */
  public ?PaymentsConfirmed $paymentsConfirmedWorker = NULL;

  public function __construct(CRM_Core_Payment_GoCardless $paymentProcessorObject, string $brqID) {
    $this->paymentProcessorObject = $paymentProcessorObject;
    $this->gcApi = $this->paymentProcessorObject->getGoCardlessApi();
    $this->brq = $this->gcApi->billingRequests()->get($brqID);
    Civi::log('GoCardless')->info("BillingRequestsFulfilled event processor: id: {id} Status: {status}", ['id' => $brqID, 'status' => $this->brq->status]);
  }

  public function process(): string {

    $this->ensureContributionRecurLoaded();

    if ($this->brq->status !== 'fulfilled') {
      $this->failRecurSetup("GoCardless billing request failed. Expected fulfilled but got {$this->brq->status}");
      throw new WebhookProcessingException("BillingRequestsFulfilled failed: got unexpected status: {$this->brq->status} on brqID {$this->brq->id}, set cr{$this->cr['id']} to failed");
    }

    // This is a big ugly print_r dump
    Civi::log('GoCardless')->debug("BillingRequestsFulfilled process: got brq: " . $this->brq);

    if (!empty($this->brq->payment_request) && $this->brq->links->payment_request_payment) {
      // The billing request included an instant payment (attempt) that seems to have worked.
      // Load this now since we may use it in calculating our recur's start date.
      $this->payment = $this->gcApi->payments()
        ->get($this->brq->links->payment_request_payment);
    }

    // If the recur's processor_id does not match the billing request then a subscription has already been setup.
    if ($this->cr['processor_id'] !== $this->brq->id) {
      Civi::log('GoCardless')->info("BillingRequestsFulfilled: {$this->brq->id} has already been processed on cr{$this->cr['id']}");
      $msg = "OK: Billing request already processed cr{$this->cr['id']}.";
    }
    else {
      $this->setupSubscription();
      Civi::log('GoCardless')->info("BillingRequestsFulfilled: cr{$this->cr['id']} now In Progress, starts {$this->cr['start_date']}");
      $msg = "OK: cr{$this->cr['id']} now In Progress, starts {$this->cr['start_date']}. ";
    }

    if ($this->payment) {
      $msg .= ' ' . $this->processInstantPayment();
    }

    return $msg;
  }

  protected function setupSubscription() {

    // Now try to set up a subscription.

    // Throw a spanner in the works if interval not supported by Go Cardless.
    // Note that earlier validation should prevent this ever being called.
    if (!in_array($this->cr['frequency_unit'], ['year', 'month', 'week'])) {
      throw new WebhookProcessingException("Invalid interval '{$this->cr['frequency_unit']}', must be year/month/week.");
    }
    $intervalUnit = $this->cr['frequency_unit'] . 'ly';

    // Direct Debits must be at most yearly
    if ($intervalUnit == 'yearly' && $this->cr['frequency_interval'] > 1 ||
    $intervalUnit == 'monthly' && $this->cr['frequency_interval'] > 12 ||
    $intervalUnit == 'weekly' && $this->cr['frequency_interval'] > 52) {
      throw new WebhookProcessingException("Interval must be at most yearly, not {$this->cr['frequency_interval']} {$this->cr['frequency_unit']}");
    }

    // Add some Civi data as metadata on the GoCardless object. JSON is fairly human-readable. See issue #79
    $params = [
    // Convert amount to pennies.
      'amount'        => Money::of($this->cr['amount'], 'GBP')->getMinorAmount()->toInt(),
      'currency'      => 'GBP',
      'name'          => $this->brq->mandate_request->description,
      'interval'      => (int) $this->cr['frequency_interval'],
      'interval_unit' => $intervalUnit,
      'links'         => ['mandate' => $this->brq->links->mandate_request_mandate],
      'metadata'      => [
        'civicrm' => json_encode([
          'contactID' => (int) $this->cr['contact_id'],
          'contributionRecurID' => (int) $this->cr['id'],
        ]),
      ],
    ];
    if (!empty($this->cr['installments'])) {
      $params['count'] = $this->cr['installments'];
    }

    if (!empty($this->cr['cycle_day'])) {
      $cycleDay = $this->cr['cycle_day'];
      if ($cycleDay == CRM_Core_Payment_GoCardless::CYCLE_DAY_FOR_LAST_DAY_OF_MONTH) {
        // GoCardless' API requires -1 for last day of month, but Civi can only
        // store positive integers for cycle_day.
        $params['day_of_month'] = -1;
      }
      elseif ($cycleDay > 0 && $cycleDay < 29) {
        $params['day_of_month'] = $cycleDay;
      }
      else {
        throw new WebhookProcessingException("BillingRequestsFulfilled: failed, cycle_day must be 1-28, or " . CRM_Core_Payment_GoCardless::CYCLE_DAY_FOR_LAST_DAY_OF_MONTH . " but it is $cycleDay");
      }
    }

    // Determine start date.
    if ($this->payment) {
      // It's started! We have an instant payment let's set the day_of_month
      // to be today, then the next payment should be then.
      $params['day_of_month'] = (int) substr($this->payment->charge_date, 8, 2);
      if ($params['day_of_month'] > 28) {
        $params['day_of_month'] = -1;
      }
    }
    else {
      // Prefer the requested start date unless it's before the earliest possible.
      $earliestStart = $this->gcApi->mandates()->get($this->brq->links->mandate_request_mandate)->next_possible_charge_date;
      $crStartDate = date('Y-m-d', strtotime($this->cr['start_date']));
      if ($crStartDate > $earliestStart) {
        // The CR is to start in the future. Note that CiviContribute doesn't offer this feature, but other APIs might want to use it.
        $params['start_date'] = $crStartDate;
      }
    }

    // Allow further manipulation of the arguments via custom hooks ...
    $input = [
      'context' => 'subscription_create',
      'billingRequestID' => $this->brq->id,
      'paymentID' => $this->payment ? $this->payment->id : NULL,
      'contribution_recur_id' => $this->cr['id'],
      'contact_id' => $this->cr['contact_id'],
    ];
    \CRM_Utils_Hook::alterPaymentProcessorParams(
      $this->paymentProcessorObject,
      $input,
      $params);
    \Civi::log('GoCardless')->debug("BillingRequestsFulfilled subscription create params:", $params);
    $this->subscription = $this->gcApi->subscriptions()->create(["params" => $params]);
    // Civi::log('GoCardless')->debug($this->subscription);

    $crStartDate = $this->payment ? $this->payment->charge_date : $this->subscription->start_date;

    // If the CR has a zero cycle_day, this was just to let us know it was supposed to be ASAP.
    // Replace it with the day of the month of the subscription start.
    $dayOfMonth = (int) date('j', strtotime($this->subscription->start_date));

    // Update recur to In Progress, and replace the BRQxxx id for the SUBxxx one.
    ContributionRecur::update(FALSE)
      ->addWhere('id', '=', $this->cr['id'])
      ->addValue('contribution_status_id:name', 'In Progress')
      ->addValue('processor_id', $this->subscription->id)
      ->addValue('start_date', $crStartDate)
      ->addValue('next_sched_contribution_date', $this->subscription->upcoming_payments[0]->charge_date)
      ->addValue('cycle_day', $this->cr['cycle_day'] ?: $dayOfMonth)
      ->execute();

    if (!$this->payment) {
      // Update the future date of the contribution to the start date returned by GC, as there isn't an instant payment.
      $this->updatePendingContribution(['receive_date' => $crStartDate]);
    }
  }

  protected function processInstantPayment(): string {

    // Since we have an instant payment, put its ID on the pending contribution for this recur.
    if ($this->cnID) {
      $cn = Contribution::update(FALSE)
        ->setValues(['trxn_id' => $this->payment->id])
        ->addWhere('id', '=', $this->cnID)
        ->addWhere('trxn_id', '<>', $this->payment->id)
        ->execute();
      if ($cn->count()) {
        Civi::log('GoCardless')->info("Updated cn{$this->cnID} trxn_id: {$this->payment->id}");
      }
    }

    if (in_array($this->payment->status, ['confirmed', 'paid_out'])) {
      // Since the payment has gone through, we can process it right away.
      // Process this payment with the IPN class. This should be able to find it
      // because the cn now has the payment ID.
      Civi::log('GoCardless')->info("BillingRequestsFulfilled: Calling payment confirmed worker");
      try {
        $this->paymentsConfirmedWorker = new PaymentsConfirmed($this->paymentProcessorObject, $this->payment->id);
        return $this->paymentsConfirmedWorker->process();
      }
      catch (WebhookAlreadyProcessedException $e) {
        // Silent on this.
      }
    }
    else {
      Civi::log('GoCardless')->info("BillingRequestsFulfilled: We have a payment but its status is {$this->payment->status}");
    }
    return '';
  }

  public function ensureContributionRecurLoaded() {
    // $ctId = $this->brq->metadata->contactID;
    $crID = $this->brq->metadata->contributionRecurID;
    if (!$crID) {
      throw new WebhookProcessingException("Billing request {$this->brq->id} metadata missing contributionRecurID");
    }
    $this->cnID = $this->brq->metadata->contributionID;
    if (!$this->cnID) {
      throw new WebhookProcessingException("Billing request {$this->brq->id} metadata missing contributionID");
    }
    if ($this->cr) {
      // If we're given a CR it MUST match the metadata.
      if ($this->cr['id'] != $crID) {
        throw new WebhookProcessingException("Coding error? Billing request {$this->brq->id} has contributionRecurID {$crID} but process() called with contributionRecurID {$this->cr['id']}");
      }
    }
    else {
      $this->cr = ContributionRecur::get(FALSE)
        ->addSelect('*', 'contribution_status_id:name')
        ->addWhere('id', '=', $crID)
        ->execute()->single();
      Civi::log('GoCardless')->debug("Loaded cr from metadata for brq: cr{$this->cr['id']}");
    }
  }

  public function updatePendingContribution(array $values) {
    $cn = Contribution::update(FALSE)
      ->setValues($values)
      ->addWhere('contribution_status_id:name', '=', 'Pending')
      ->addWhere('contribution_recur_id', '=', $this->cr['id'])
      ->addWhere('is_test', '=', $this->cr['is_test'])
      ->setLimit(1)
      ->execute()->first();
    Civi::log('GoCardless')->info("Updated cn{$cn['id']}", $values);
    $this->cnID = (int) $cn['id'];
    return $this->cnID;
  }

  public function failRecurSetup(string $adminFailReason) {

    // Something has gone wrong: update the CR and CN to failed.
    ContributionRecur::update(FALSE)
      ->addWhere('id', '=', $this->cr['id'])
      ->addValue('contribution_status_id:name', 'Failed')
      ->execute();

    $cn = Contribution::update(FALSE)
      ->addValue('contribution_status_id:name', 'Failed')
      ->addWhere('contribution_status_id:name', '=', 'Pending')
      ->addWhere('contribution_recur_id', '=', $this->cr['id'])
      ->addWhere('is_test', '=', $this->cr['is_test'])
      ->execute()->first();

    if (!empty($cn['id'])) {
      // Increase visibility of error for admins by adding a Note on the CN.
      Note::create(FALSE)
        ->addValue('entity_table', 'civicrm_contribution')
        ->addValue('entity_id', $cn['id'])
        ->addValue('note', $adminFailReason)
        ->execute();
    }

  }

}
