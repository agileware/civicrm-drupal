<?php
namespace Civi\GoCardless\Events;

use GoCardlessPro\Resources\Payment;
use CRM_Core_Payment_GoCardless;
use Civi\Api4\ContributionRecur;
use Civi\Api4\Contribution;
use Civi\GoCardless\Exceptions\WebhookIgnoredException;

abstract class PaymentsBase {

  public CRM_Core_Payment_GoCardless $paymentProcessorObject;

  public Payment $payment;

  /**
   * ContributionRecur
   */
  public ?array $cr = NULL;

  /**
   * Contribution
   */
  public ?array $cn = NULL;

  public bool $paymentHasSubscription = FALSE;
  public bool $paymentSubscriptionHasCr = FALSE;
  public bool $paymentHasCn = FALSE;
  public string $receiptPolicy;

  /**
   * This is only set when we have no match on trxn_id == paymentID.
   */
  public ?bool $crHasPendingCn = NULL;

  public function __construct(CRM_Core_Payment_GoCardless $paymentProcessorObject, string $paymentID) {
    $this->paymentProcessorObject = $paymentProcessorObject;
    $this->payment = $this->paymentProcessorObject->getGoCardlessApi()->payments()->get($paymentID);
    $this->receiptPolicy = \CRM_GoCardlessUtils::getSettings()['sendReceiptsForCustomPayments'];

    // Does this payment have a subscription?
    $links = $this->payment->links;
    if (!empty($links->subscription)) {
      $this->paymentHasSubscription = TRUE;
      // Find the recurring payment by the given subscription which will be
      // stored in the processor_id field.
      $this->loadCr($this->payment->links->subscription, 'processor_id');
      $this->paymentSubscriptionHasCr = (bool) $this->cr;
    }

    // Do we have this payment known by trxn_id?
    $this->cn = Contribution::get(FALSE)
      ->addSelect('*', 'contribution_status_id:name')
      ->addWhere('trxn_id', '=', $this->payment->id)
      ->addWhere('is_test', '=', $this->paymentProcessorObject->isTestMode())
      ->execute()->first();
    $this->paymentHasCn = (bool) $this->cn;

    if (empty($this->cr) && !empty($this->cn['contribution_recur_id'])) {
      // It's possible that the Payment does not belong to the subscription,
      // but the contribution does belong to a recur. (e.g. instant payment)
      $this->loadCr($this->cn['contribution_recur_id']);
    }

    if (!$this->cn) {
      if ($this->cr) {
        // Look up the recur's pending contribution.
        $this->cn = Contribution::get(FALSE)
          ->addSelect('*', 'contribution_status_id:name')
          ->addWhere('contribution_status_id:name', '=', 'Pending')
          ->addWhere('contribution_recur_id', '=', $this->cr['id'])
          ->addWhere('is_test', '=', $this->paymentProcessorObject->isTestMode())
          ->execute()->first();
        $this->crHasPendingCn = (bool) $this->cn;
      }
    }
  }

  protected function loadCr($value, string $field = 'id') {
    $this->cr = ContributionRecur::get(FALSE)
      ->addSelect('*', 'contribution_status_id:name')
      ->addWhere($field, '=', $value)
      ->addWhere('is_test', '=', $this->paymentProcessorObject->isTestMode())
      ->execute()->first();
  }

  abstract public function process(): string;

  protected function ignoreIfPaymentStatusNot(string ...$expected): void {
    if (!in_array($this->payment->status, $expected)) {
      throw new WebhookIgnoredException(
        "OK: Webhook out of date, expected status "
        . implode("' or '", $expected)
        . ", got '{$this->payment->status}'");
    }
  }

}
