<?php
namespace Civi\Api4;

use Civi\Api4\Action\GoCardless\FixCancelsPreV2;
use Civi\Api4\Generic\BasicGetFieldsAction;

/**
 * GoCardless entity.
 *
 * Provided by the GoCardless Direct Debit Payment Processor extension.
 *
 * @package Civi\Api4
 */
class GoCardless extends Generic\AbstractEntity {

  /**
   * @return \Civi\Api4\Action\GoCardless\FixCancelsPreV2
   */
  public static function fixCancelsPreV2(): FixCancelsPreV2 {
    return new FixCancelsPreV2('GoCardless', 'fixCancelsPreV2');
  }

  /**
   * @return \Civi\Api4\Generic\BasicGetFieldsAction
   */
  public static function getFields(): BasicGetFieldsAction {
    return (new Generic\BasicGetFieldsAction(__CLASS__, __FUNCTION__, function($getFieldsAction) {
      // We don't have any fields.
      return [];
    }));
  }

}
