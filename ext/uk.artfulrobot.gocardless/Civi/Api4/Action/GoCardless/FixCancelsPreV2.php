<?php
namespace Civi\Api4\Action\GoCardless;

use Civi\Api4\ContributionRecur;
use Civi\Api4\Generic\Result;

class FixCancelsPreV2 extends \Civi\Api4\Generic\AbstractAction {

  public function _run(Result $result) {

    // First, check cancelled subscriptions showing as overdue
    $recurs = ContributionRecur::get(FALSE)
      ->addSelect('processor_id', 'contribution_status_id:name', 'end_date', 'cancel_date', 'payment_processor_id')
      ->addWhere('payment_processor_id.payment_processor_type_id:name', '=', 'GoCardless')
      ->addWhere('is_test', 'IN', [0, 1])
      ->addWhere('contribution_status_id:name', '=', 'Overdue')
      ->addWhere('processor_id', 'IS NOT NULL')
      ->addWhere('end_date', 'IS NOT NULL')
      ->execute();

    $overdueToCancelled = [];
    foreach ($recurs as $recur) {
      $processor = \Civi\Payment\System::singleton()->getById($recur['payment_processor_id']);
      /** @var \CRM_Core_Payment_GoCardless $processor */
      $gc = $processor->getGoCardlessApi();
      $sub = $gc->subscriptions()->get($recur['processor_id']);
      if ($sub->status === 'cancelled') {
        ContributionRecur::update(FALSE)
          ->addWhere('id', '=', $recur['id'])
          ->addValue('contribution_status_id:name', 'Cancelled')
          ->addValue('cancel_date', $recur['end_date'])
          ->execute();
        $overdueToCancelled[] = $recur['id'];
      }
      // else, leave it alone.
    }
    \Civi::log('GoCardless')->info("Set the following 'Overdue' recurs to Cancelled based on looking up their subscription IDs.", compact('overdueToCancelled'));

    // Next update cancel_date where missing.
    $addedCancelDate = ContributionRecur::get(FALSE)
      ->addSelect('processor_id', 'contribution_status_id:name', 'end_date', 'cancel_date', 'payment_processor_id')
      ->addWhere('payment_processor_id.payment_processor_type_id:name', '=', 'GoCardless')
      ->addWhere('is_test', 'IN', [0, 1])
      ->addWhere('contribution_status_id:name', '=', 'Cancelled')
      ->addWhere('end_date', 'IS NOT NULL')
      ->addWhere('cancel_date', 'IS NULL')
      ->execute()->column('id');
    if ($addedCancelDate) {
      \CRM_Core_DAO::executeQuery('UPDATE civicrm_contribution_recur SET cancel_date = end_date WHERE id IN (%1)', [
        1 => [implode(',', $addedCancelDate), 'CommaSeparatedIntegers'],
      ]);
    }
    \Civi::log('GoCardless')->info("Set the following 'Cancelled' recurs' cancel_date to their end_date", compact('addedCancelDate'));

    $result[] = compact('addedCancelDate', 'overdueToCancelled');

  }

}
