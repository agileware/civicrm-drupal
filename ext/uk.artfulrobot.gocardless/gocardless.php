<?php
use CRM_GoCardless_ExtensionUtil as E;

require_once 'gocardless.civix.php';

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function gocardless_civicrm_config(&$config) {
  // Is this guard necessary?
  if (empty(Civi::$statics[__FUNCTION__])) {
    Civi::$statics[__FUNCTION__] = TRUE;
    Civi::dispatcher()->addListener(\Civi\API\Events::PREPARE, ['CRM_GoCardlessUtils', 'hookApiPrepare'], -100);
  }

  _gocardless_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * We set up the payment processor type and payment instrument types here.
 * (I tried to do this with `hook_civicrm_managed()` but failed because I need to relate the entities).
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function gocardless_civicrm_install() {
  _gocardless_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function gocardless_civicrm_enable() {
  _gocardless_civix_civicrm_enable();
}

/**
 * Various intercepts.
 *
 */
function gocardless_civicrm_buildForm($formName, &$form) {

  if ($formName === 'CRM_Contribute_Form_Contribution_Main') {
    // Inject Javascript to force recurring checkbox on, if configured to do so.
    CRM_GoCardlessUtils::handleContributeFormHacks();
  }
  elseif ($formName === 'CRM_Contribute_Form_Contribution_ThankYou') {
    if (!empty($_GET['brqID'])) {
      // Looks like a GoCardless thank you page called with a completed  billing request flow.
      CRM_GoCardlessUtils::handleContributeFormThanks();
    }
  }
}

/**
 * Functions below this ship commented out. Uncomment as required.
 *
 * /**
 * Implements hook_civicrm_preProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_preProcess
 *
 *
 * // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 */
function gocardless_civicrm_navigationMenu(&$menu) {
  _gocardless_civix_insert_navigation_menu($menu, 'Administer/CiviContribute', array(
    'label' => E::ts('GoCardless Settings', array('domain' => 'uk.artfulrobot.civicrm.gocardless')),
    'name' => 'gocardless_webhook_helper',
    'url' => 'civicrm/a#/gocardless',
    'permission' => 'administer payment processors',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _gocardless_civix_navigationMenu($menu);
}

/**
 * Implements hook_civicrm_validateForm().
 *
 * @param string $formName
 * @param array $fields
 * @param array $files
 * @param CRM_Core_Form $form
 * @param array $errors
 */
function gocardless_civicrm_validateForm($formName, &$fields, &$files, &$form, &$errors) {
  if ($formName === 'CRM_Admin_Form_PaymentProcessor') {
    if (empty($fields['payment_processor_type_id'])) {
      // huh?
      return;
    }
    $payment_processor_name = civicrm_api3('PaymentProcessorType', 'getvalue', ['return' => 'name', 'id' => $fields['payment_processor_type_id']]);
    if ('GoCardless' !== $payment_processor_name) {
      // Not a GoCardless payment processor form.
      return;
    }
    // Now we know it's a GoCardless payment processor form.
    if ($fields['signature'] === $fields['test_signature']) {
      $errors['test_signature'] = E::ts('Webhook secrets MUST be unique between test and live.');
    }
  }
}

/**
 * Implementation of hook_civicrm_check
 *
 * Add a check to the status page/System.check that the payment instrument has a financial account.
 * See https://lab.civicrm.org/extensions/gocardless/-/issues/51
 */
function gocardless_civicrm_check(&$messages) {
  if (!class_exists(\GoCardlessPro\Client::class)) {
    $messages[] = new CRM_Utils_Check_Message(
      'gocardless_missing_libs',
      E::ts("The GoCardlessPro classes are missing. This can happen if you install from the git repo and don‘t do composer install. This should never happen with an install from a released .zip/.gz file. Drupal 8+ admins will need to ensure this project‘s composer libs are installed."),
      E::ts('GoCardless extension incomplete install: will cause crashes!'),
      \Psr\Log\LogLevel::ERROR,
      'fa-flag'
    );
  }

  $result = civicrm_api3('OptionValue', 'getsingle', [
    'option_group_id' => "payment_instrument",
    'name' => "direct_debit_gc",
  ]);
  $financial_account_id = CRM_Contribute_PseudoConstant::getRelationalFinancialAccount($result['id'], NULL, 'civicrm_option_value');

  if (empty($financial_account_id)) {
    $messages[] = new CRM_Utils_Check_Message(
      'gocardless_missing_financial_account',
      E::ts('Please visit Administer » CiviContribute » Payment Methods and edit '
          . 'the entry called GoCardless Direct Debit. Select a suitable Financial Account '
          . 'and press Save. Without this you may see errors like "No Payments found for '
          . 'this contribution record".'),
      E::ts('Missing Financial Account for GoCardless'),
      \Psr\Log\LogLevel::WARNING,
      'fa-flag'
    );
  }

  $legacyLastUse = CRM_GoCardlessUtils::getLegacyIPNInUse();
  if ($legacyLastUse) {
    $messages[] = new CRM_Utils_Check_Message(
      'gocardless_legacy_ipns',
      E::ts('A GoCardless account is configured to send webhooks to the legacy endpoint. '
      . 'You must update the webhook settings on the manage.gocardless.com website (and manage-sandbox.gocardless.com) '
      . 'to use the new webhook endpoints which can be seen from Administer » CiviContribute » GoCardless Settings. '
      . 'This warning will continue to show until you have done this and cleared your cache.'),
      E::ts('Legacy webhook URLs used on %1', [1 => $legacyLastUse]),
      \Psr\Log\LogLevel::WARNING,
      'fa-flag'
    );

    if (defined('CIVI_GOCARDLESS_FORCE_SANDBOX_ENVIRONMENT')) {
      $messages[] = new CRM_Utils_Check_Message(
      'gocardless_force_sandbox',
      E::ts('This flag will cause live API calls to fail; it should not be set in production environments.'),
      E::ts('CIVI_GOCARDLESS_FORCE_SANDBOX_ENVIRONMENT set!'),
      \Psr\Log\LogLevel::ERROR,
      'fa-flag'
      );
    }
  }
}
