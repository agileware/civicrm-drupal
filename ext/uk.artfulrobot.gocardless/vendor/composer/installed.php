<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'reference' => NULL,
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'reference' => NULL,
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'gocardless/gocardless-pro' => array(
            'pretty_version' => '4.28.0',
            'version' => '4.28.0.0',
            'reference' => 'd843a4a94cbe20ab5c0533eb805c48a87600abdf',
            'type' => 'library',
            'install_path' => __DIR__ . '/../gocardless/gocardless-pro',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/guzzle' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '^6.0 | ^7.0',
            ),
        ),
    ),
);
