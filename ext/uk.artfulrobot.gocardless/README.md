# GoCardless Direct Debit integration for CiviCRM

**A CiviCRM extension to GoCardless integration to handle UK Direct Debits.**

Please see [documentation pages](https://docs.civicrm.org/gocardless)

This extension is working well for collecting regular weekly, monthly or yearly donations from UK supporters. Using it you can enable supporters to set up direct debits and every month when GoCardless takes payment this updates your CiviCRM contributions automatically. If someone cancels their Direct Debit this also updates your CiviCRM records. It also sends them a bunch of flowers thanking them for their support and asking them to reconsider their cancellation. Ok, it doesn't do that last bit.

[Artful Robot](https://artfulrobot.uk) stitches together open source websites and databases to help campaigns, charities, NGOs and other beautifully-minded people change the world. We specialise in CiviCRM and Drupal.

Other things to note

- 1.13.1 will be the last version to support PHP7.3 (which has been unsupported
  since 2021!). PHP7.4 is also unsupported now, but will be supported for a while longer by this extension.

- Avoid running CiviCRM 5.39 - 5.41 if you use memberships. This is not specific to this extension.

- Daily recurring is not supported by GoCardless, so you should not enable this option when configuring your forms. If you do users will get an error message: "Error Sorry, we are unable to set up your Direct Debit. Please call us."

- Taking one offs is [not supported/implemented yet](https://lab.civicrm.org/extensions/gocardless/-/issues/12).

- Membership organisations needing a way to change subscriptions in bulk - e.g.
  if you need to increase/decrease your membership fee at any point in future may be interested in the [Upgrade Recur extension](https://lab.civicrm.org/extensions/upgraderecur/); this GoCardless extension supports that process.

- Generally worth scanning the titles of the [Issue Queue](https://lab.civicrm.org/extensions/gocardless/-/issues/)

- Developers can drive it from a non-CiviCRM form, e.g. if you have a highly custom donate form that does not use CiviCRM's payment pages.

- There are some phpunit tests. You only get these by cloning the repo, not by downloading a release .tgz or .zip. Do not run these on a live database!

- Pull Requests (PR) welcome. Please ensure all existing tests run OK before making a PR :-)

- You can pay me to fix/implement a feature you need [contact me](https://artfulrobot.uk/contact)

- If you use this, consider joining the friendly [chat channel](https://chat.civicrm.org/civicrm/channels/gocardless) for announcements and support.


## Drupal 8+ / composer based installs

This extension requires some third party code. If you install from a release in a zip file, you'll get this automatically, but if not (e.g. if you used `git clone`) you'll need to fetch that yourself.

For non-composer based web applications (which I believe generally covers Drupal7, WordPress, Backdrop) you just need to run `composer install` from within the extension's dir.

For composer based applications (Drupal 8, 9, 10+, possibly some Joomla, Standalone), you will need to decide whether to do the above, or whether to merge the composer.json requirements into your main site's requirements. At the time of updating this README, you can do this from the dir where your site's main composer.json lives with `composer require gocardless/gocardless-pro='^4.28'` but always check this extension's composer.json file as I may forget to update this README!


