<?php

use Civi\Api4\PaymentProcessor;
use Civi\Api4\Contribution;
use Civi\Api4\ContributionRecur;
use Civi\Api4\Note;

/**
 * Job.Gocardlessfailabandoned API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC/API+Architecture+Standards
 */
function _civicrm_api3_job_Gocardlessfailabandoned_spec(&$spec) {
  $spec['timeout']['description'] = 'How long (hours) before we consider '
    . 'Pending ContributionRecur records as abandoned. Default: 2 hours';
  $spec['timeout']['api.default'] = 2;

  $spec['contribution_recur_id'] = [
    'description' => 'Provide a specific item to abandon (for testing purposes)',
  ];
}

/**
 * Job.Gocardlessfailabandoned API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_job_Gocardlessfailabandoned($params) {

  $hours = (float) ($params['timeout'] ?? 0);
  if (!($hours > 0)) {
    throw new API_Exception("Invalid timeout for Gocardlessfailabandoned. Should be an amount of hours > 0");
  }
  $too_old = time() - 60 * 60 * $hours;
  $returnValues = ['contribution_recur_ids' => []];

  // We need a list of GoCardless payment processors.
  $payment_processor_ids = PaymentProcessor::get(FALSE)
    ->addSelect('id')
    ->addWhere('payment_processor_type_id:name', '=', 'GoCardless')
    ->addWhere('is_test', 'IN', [0, 1])
    ->execute()->column('id');

  if ($payment_processor_ids) {
    if (!empty($params['contribution_recur_id'])) {
      // Hack for phpunit because we go on modified_date which is set by a trigger, I think.
      $oldCrs = ContributionRecur::get(FALSE)
        ->addWhere('contribution_status_id:name', '=', 'Pending')
        ->addWhere('id', '=', $params['contribution_recur_id'])
        ->addWhere('is_test', 'IN', [0, 1])
        ->execute()->indexBy('id')->getArrayCopy();
    }
    else {
      $oldCrs = ContributionRecur::get(FALSE)
        ->addWhere('contribution_status_id:name', '=', 'Pending')
        ->addWhere('payment_processor_id', 'IN', $payment_processor_ids)
        ->addWhere('modified_date', '<', date('Y-m-d H:i:s', $too_old))
        ->addWhere('is_test', 'IN', [0, 1])
        ->execute()->indexBy('id')->getArrayCopy();
    }

    if (count($oldCrs)) {
      Civi::log('GoCardless')->debug('Gocardlessfailabandoned: Begin processing assumed-abandoned ContributionRecur records', [
        'crIDs' => array_keys($oldCrs),
        '=start' => 'failAbandoned',
      ]);
      foreach ($oldCrs as $crID => $cr) {

        // Mark the ContributionRecur record as Failed
        ContributionRecur::update(FALSE)
          ->addWhere('id', '=', $crID)
          ->addValue('contribution_status_id:name', 'Failed')
          ->execute();
        Civi::log('GoCardless')->debug("cr $crID updated Pending → Failed");
        $returnValues['contribution_recur_ids'][] = $crID;

        // Find the pending payment and mark that as Cancelled.
        $contributions = Contribution::get(FALSE)
          ->addWhere('contribution_recur_id', '=', $crID)
          ->addWhere('contribution_status_id:name', '=', 'Pending')
          ->addWhere('contact_id', '=', $cr['contact_id'])
          ->addWhere('is_test', '=', $cr['is_test'])
          ->execute();
        if ($contributions->countFetched() === 1) {
          $cn = $contributions->first();
          Civi::log('GoCardless')->debug("cr $crID cn $cn[id] updated Pending → Cancelled");

          // We only expect one.
          Contribution::update(FALSE)
            ->addWhere('id', '=', $cn['id'])
            ->addValue('contribution_status_id:name', 'Cancelled')
            ->execute();
          Note::create(FALSE)
            ->addValue('entity_table', 'civicrm_contribution')
            ->addValue('entity_id', $cn['id'])
            ->addValue('note', 'Mandate setup abandoned or failed.')
            ->execute();
        }
      }
      Civi::log('GoCardless')->debug('Completed Gocardlessfailabandoned', ['=pop' => 1]);
    }
  }
  return civicrm_api3_create_success($returnValues, $params, 'Job', 'GoCardlessFailAbandoned');
}
