<?php

/**
 * Print a summary of subscriptions on GC
 *
 * Run as: `cv scr -U admin stats.php`
 */

if (php_sapi_name() != 'cli') {
  exit;
}

class GCStats {

  function __construct() {
    // Get a GoCardless API for the live endpoint.
    $processorConfig = civicrm_api3(
      'PaymentProcessor',
      'getsingle',
      ['payment_processor_type_id' => 'GoCardless', 'is_active' => 1, 'is_test' => 0]);
    $this->processor = Civi\Payment\System::singleton()->getByProcessor($processorConfig);

    $this->gcAPI = $this->processor->getGoCardlessApi();

  }

  public function print_stats() {
    $subscriptions = $this->gcAPI->subscriptions()->all();

    $s = [];
    foreach ($subscriptions as $subscription) {
      $s[] = $subscription;
    }
    $subscriptions = array_reverse($s);

    $stats = [];
    $count = 0;
    foreach ($subscriptions as $subscription) {
      print $subscription->start_date . "\n";
      $count++;
      $int = $subscription->interval . "-" . $subscription->interval_unit;
      $name = $subscription->name;
      // Some systems add an id to each sub name to make it unique.  Strip that.
      $name = preg_replace('/^(.*?)( \d{3,})$/', '$1 (multiple)', $name);

      $stats['1-status'][$subscription->status]++;
      $stats['2-interval'][$int]++;
      $stats['3-amount'][$subscription->amount]++;
      $stats['4-name'][$name]++;
      $stats['5-detail'][$name][$subscription->status][$int][$subscription->amount]++;
    }

    $this->print_array($stats);
    print "Total: $count\n";

  }

  public function print_array($array) {
    foreach ($this->squash($array) as $line) {
      print $line . "\n";
    }
  }

 /**
  * @param array $array
  *   multilevel array
  * @return  array
  *   key1 > key2 > ... = val
  */
  public function squash($array, $str = "") {
    $ret = [];
    ksort($array);
    foreach ($array as $key => $val) {

      if (is_array($val)) {
        foreach ($this->squash($val) as $line) {
          $ret[] = $key . ' > ' . $line;
        }
      }
      else {
        $ret[] = $key . ' = ' . $val;
      }
    }
    return $ret;
  }


}

$gcstats = new GCStats();
$gcstats->print_stats();

