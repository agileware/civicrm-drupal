<?php
ini_set("memory_limit", "512M");

use Civi\Api4\Contribution;
use Civi\Api4\PaymentProcessor;
use Civi\Api4\PaymentprocessorWebhook;

if (PHP_SAPI !== 'cli') {
  http_response_code(404);
  echo "404 Not Found.";
  exit;
}

$dryRun = (($argv[1] ?? '') !== 'act');

/**
 * This script attempts to clean up the mess left by Issue 141.
 * This was/is a CiviCRM Core bug that hit the GoCardless extension hard.
 *
 * Symptoms:
 *
 * Look at your Payment Processor Webhooks (mjwshared extension) and see you have lots of errors saying:
 *
 *     FAILED: Had to skip webhook event. Reason: Expected one Contribution but found 0
 *
 * View the details in the log and find the payment ID (PM....). Search for that in a contributions search
 * under Transaction ID. You should find a Pending contribution. This is because it created the
 * pending contribution then failed to update it because of the bug.
 *
 * Sometimes, e.g. monthly giving and it's been going on for more than a month, you can end up with finding a Completed
 * contribution whose trxn_id is a comma separated list of the original one and another one. What has happened here
 * is that the first has failed, leaving a Pending contribution, then the next one has come in and it's populated
 * the pending contribution with the new data. You can identify this case by looking at the payment - it's PM... ID should
 * match the 2nd ID listed in trxn_id.
 *
 * For these conditions, this script can help.
 *
 * - Pending contributions from crashed webhook runs are deleted.
 * - Contributions with two payment IDs on them, one failed, one successful, are updated to just have the successful ID.
 * - The webhooks are replayed for these.
 * - Pretty much everything is logged.
 *
 * Usage:
 *
 *     cv scr cli/fix-issue-141.php [act]
 *
 * If you don't include the optional act argument, a dry run does the whole thing and then rolls it back. Useful to take a look
 * at what it would do, and whether it would work! I recommend running this on a development copy of your data first, and
 * of course **taking backups**.
 *
 * If it finds things it can't handle it will report that and skip them.
 *
 * Note that this script cannot and shouldn't be run through/in a web browser.
 *
 */
class Fix141 {

  public CRM_Core_Payment_GoCardless $pp;

  public function __construct() {

    $gcPP = PaymentProcessor::get(FALSE)
      ->addWhere('payment_processor_type_id.name', '=', 'GoCardless')
      ->addWhere('is_test', '=', FALSE)
      ->execute()->single();
    $this->pp = \Civi\Payment\System::singleton()->getByProcessor($gcPP);

    if (!$this->pp) {
      fwrite(STDERR, "Failed to load payment processor by name 'GoCardless'\n");
      exit(1);
    }

    $extensionIsInstalled = 'installed' ===
      CRM_Extension_System::singleton()->getManager()->getStatus('mjwshared');
    if (!$extensionIsInstalled) {
      fwrite(STDERR, "The mjwshared extension is not installed, this script can only fix when it has been installed. (Installing it now won't help.)");
      exit(1);
    }
  }

  public function fix(bool $dryRun) {

    $affected = PaymentprocessorWebhook::get(FALSE)
      ->addWhere('payment_processor_id', '=', $this->pp->getID())
      ->addWhere('status', '=', "error")
      ->addWhere('message', 'LIKE', "FAILED: Had to skip webhook event. Reason: Expected one Contribution but found 0%")
      ->execute();

    $queueItems = [];
    $cnUpdates = [];
    $cns = $cts = [];
    $errors = [];
    foreach ($affected as $queueItem) {
      $queueItem['data'] = $data = json_decode($queueItem['data'] ?? '', TRUE) ?? [];
      try {
        if (($data['action'] ?? '') !== 'confirmed' || ($data['resource_type'] ?? '') !== 'payments') {
          throw new \NotPaymentsConfirmed("not a payments.confirmed webhook.");
        }
        $paymentID = $data['links']['payment'] ?? '';
        if (!$paymentID) {
          throw new \MissingExpectedData("Missing payment ID");
        }
        $webhookID = $queueItem['identifier'] ?? '';
        if (empty($webhookID)) {
          throw new \MissingExpectedData("Missing webhook ID");
        }
        $eventID = $data['id'] ?? '';
        if (empty($eventID)) {
          throw new \MissingExpectedData("Missing event ID");
        }

        // Look up a payment with this trxn_id
        $cn = Contribution::get(FALSE)
          ->addSelect('contribution_status_id:name', 'contact_id', 'trxn_id', 'invoice_id')
          ->addClause('OR', [
            ['trxn_id', 'LIKE', "%$paymentID%"],
            ['invoice_id', '=', $paymentID],
          ])
          ->addWhere('is_test', '=', FALSE)
          ->execute();
        if ($cn->countFetched() === 0) {
          throw new \MissingContribution("No Contribution found: this is a different issue.");
        }
        elseif ($cn->countFetched() > 1) {
          throw new \RuntimeException("multiple Contributions found: matching $paymentID this is a different issue.");
        }

        // only 1
        $cn = $cn->first();

        if (trim($cn['trxn_id']) !== $paymentID) {
          // Odd cases
          $multipleIDsInTrxn = preg_split('/\s*,\s*/', trim($cn['trxn_id']));
          if ($multipleIDsInTrxn[0] === $paymentID && count($multipleIDsInTrxn) === 2) {
            // We have a contribution with 2 payment IDs.
            // This can happen because the first came in, it crashed leaving it Pending,
            // then the next one came in and populated the Pending one.

            $payments = civicrm_api3('Payment', 'get', [
              'contribution_id' => $cn['id'],
              'sequential' => 1,
            ])['values'];

            if (count($payments) === 1 && $payments[0]['trxn_id'] === $multipleIDsInTrxn[1]) {
              // yes, this is the case.
              // We just need to remove the payment ID from the contribution.
              // But first check that the invoice id doesn't contain the old id.
              if (str_contains($cn['invoice_id'], $paymentID)) {
                throw new \ContributionHas2IDsAndUsedInvoiceID();
              }
              $cnUpdates[$cn['id']] = [
                'trxn_id' => $multipleIDsInTrxn[1],
                'removed' => $paymentID,
              ];
              $queueItems[] = $queueItem['id'];
              continue;
            }
            throw new \RuntimeException("$paymentID found in cn $cn[id] $cn[trxn_id] but is not a pattern we can deal with.");
          }
        }
        if ($cn['contribution_status_id:name'] !== 'Pending') {
          throw new \ContributionNotPending("Found Contribution $cn[id] matching $paymentID but its status is {$cn['contribution_status_id:name']}, not Pending.");
        }
        $cns[] = $cn['id'];
        $cts[$cn['contact_id']] = TRUE;
        // Ok, found one to re-process.
        $queueItems[] = $queueItem['id'];
        fwrite(STDOUT, "Pending cn{$cn['id']}: matches $paymentID → delete\n");
      }
      catch (\Exception $e) {
        fwrite(STDERR, "Warning: " . $e->getMessage() . "\n\t" . str_replace("\n", "\n\t", json_encode($queueItem, JSON_PRETTY_PRINT)) . "\n\n");
        $errors[get_class($e)]++;
        continue;
      }

    }

    $cts = array_keys($cts);
    fwrite(STDOUT, "Found " . count($queueItems) . " webhooks to retry, \nafter deleting " . count($cns)
      . " pending contributions, belonging to " . count($cts) . " contacts\nand after updating " . count($cnUpdates) . " contributions that have an erroneous paymentID\n"
      . json_encode($errors, JSON_PRETTY_PRINT) . "\n\n");

    fwrite(STDERR, "Processing...\n");
    if ($dryRun) {
      fwrite(STDERR, "(Dry run. Run with 'act' to force action.)\n");
    }

    CRM_Core_Transaction::create()->run(function($tx) use ($cnUpdates, $cns, $queueItems, $dryRun) {
      // Do this by SQL to be efficient and to avoid triggering anything else.
      // **this might not be appropriate in your setting**
      foreach ($cnUpdates as $cnID => $d) {
        fwrite(STDOUT, "Δ Update cn$cnID remove $d[removed] from trxn_id leaving just $d[trxn_id]\n");
        CRM_Core_DAO::executeQuery("UPDATE civicrm_contribution SET trxn_id = %1 WHERE id = %2", [
          1 => [$d['trxn_id'], 'String'],
          2 => [$cnID, 'Positive'],
        ]);
      }
      if (!empty($cns)) {
        fwrite(STDOUT, "✖ Delete " . count($cns) . " Pending Contributions.\n");
        Contribution::delete(FALSE)->addWhere('id', 'IN', $cns)->execute();
        $count = Contribution::get(FALSE)->addWhere('id', 'IN', $cns)->selectRowCount()->execute()->count();
        if ($count !== 0) {
          fwrite(STDERR, "✖ Delete failed, still $count remaining\n");
          $tx->rollback();
          exit;
        }
        $huh = Contribution::get(FALSE)->addWhere('id', '=', 11772)->execute()->first();
        if ($huh) {
          fwrite(STDERR, "✖ Still have 11772\n");
          if (in_array(11772, $cns)) {
            fwrite(STDERR, "but it was in the cns to delete...\n");
          }
          else {
            fwrite(STDERR, "it was NOT in the cns to delete...\n");
          }
          $tx->rollback();
          exit;
        }
      }
      if ($queueItems) {
        fwrite(STDOUT, "↻ requeue " . count($queueItems) . " webhooks\n");
        PaymentprocessorWebhook::update(FALSE)
          ->setValues([
            'status' => "new",
            'processed_date' => NULL,
            'message' => "Scheduled for retry",
          ])
          ->addWhere('id', 'IN', $queueItems)
          ->execute();
      }

      fwrite(STDOUT, " running webhooks\n");
      $result = civicrm_api3('Job', 'process_paymentprocessor_webhooks', ['delete_old' => '-1 year']);
      print_r($result['values']);

      if ($dryRun) {
        $tx->rollback(); fwrite(STDOUT, "rollback for dry run\n");
      }
    });

  }

}

class NotPaymentsConfirmed extends \Exception {};
class MissingExpectedData extends \Exception {};
class MissingContribution extends \Exception {};
class ContributionNotPending extends \Exception {};
class ContributionHas2IDsAndUsedInvoiceID extends \Exception {};

(new Fix141())->fix($dryRun);
