<?php

require_once 'mosaicotweaks.civix.php';
use CRM_Mosaicotweaks_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/ 
 */
function mosaicotweaks_civicrm_config(&$config) {
  _mosaicotweaks_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function mosaicotweaks_civicrm_install() {
  _mosaicotweaks_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function mosaicotweaks_civicrm_postInstall() {
  _mosaicotweaks_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function mosaicotweaks_civicrm_uninstall() {
  _mosaicotweaks_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function mosaicotweaks_civicrm_enable() {
  _mosaicotweaks_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function mosaicotweaks_civicrm_disable() {
  _mosaicotweaks_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function mosaicotweaks_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _mosaicotweaks_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function mosaicotweaks_civicrm_entityTypes(&$entityTypes) {
  _mosaicotweaks_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_civicrm_mosaicoConfig().
 *
 * See CRM_Mosaico_Page_Editor::createMosaicoConfig()
 * Note wierd format of plugins list
 */
function mosaicotweaks_civicrm_mosaicoConfig(&$config) {
  $config['tinymceConfig']['plugins'] = [ $config['tinymceConfig']['plugins'][0] . ' textcolor colorpicker' ];
  $config['tinymceConfig']['toolbar1'] = $config['tinymceConfig']['toolbar1'] . ' forecolor backcolor';

  $config['tinymceConfigFull']['plugins'] = [ $config['tinymceConfigFull']['plugins'][0] . ' colorpicker' ];
  $config['tinymceConfigFull']['toolbar1'] = $config['tinymceConfigFull']['toolbar1'] . ' | numlist bullist';
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 *

 // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 *
function mosaicotweaks_civicrm_navigationMenu(&$menu) {
  _mosaicotweaks_civix_insert_navigation_menu($menu, 'Mailings', array(
    'label' => E::ts('New subliminal message'),
    'name' => 'mailing_subliminal_message',
    'url' => 'civicrm/mailing/subliminal',
    'permission' => 'access CiviMail',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _mosaicotweaks_civix_navigationMenu($menu);
} // */
