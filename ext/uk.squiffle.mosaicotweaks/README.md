# Mosaico Tweaks

![Screenshot shows text colour tool with custom colours at the bottom.  Numbered and bulleted lists are shown at the end of the toolbar.](/images/screenshot.png)

This extension adds options to the text editor used in Mosaico:

* Add numbered and bulleted lists to the editing menu for text.
* Add custom colours to the text colour and background colour tools in editing menu for text.
* Add text colour and background colour with custom colours to the editing menu for headers

_... and no, 'colour' is not a typo!_

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.3+
* CiviCRM 5.32.0+

## Installation (Web UI)

This extension can be installed via the web UI. Look for 'Mosaico Tweaks'.

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl mosaicotweaks@https://lab.civicrm.org/extensions/mosaicotweaks/archive/master.zip
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://lab.civicrm.org/extensions/mosaicotweaks.git
cv en mosaicotweaks
```

## Usage

No configuration is required. When editing a Mosaico block, new options are added to the editing menu for both headers and text.

## Known Issues

None.

## See Also

This extension enables a particular set of features. If you want flexibility to configure the toolbar yourself, check out the [Mosaico Toolbar Configurator](https://github.com/ginkgostreet/com.ginkgostreet.mosaicotoolbarconfig) extension. Note that at the time of writing, that extension provides facilities to modify the editing menu for text but not for headers. You will need to know what plugins and toolbar options to add.

## Developer Note

See [TinyMCE Plugins](https://www.tiny.cloud/docs-4x/plugins/) for details of available plugins and configuration.
Note that Mosaico uses TinyMCE v4.

## ChangeLog

1.3 Changes for php v7.4 (thanks @kainuk)
