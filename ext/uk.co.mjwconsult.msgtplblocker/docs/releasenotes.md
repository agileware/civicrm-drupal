# Release notes

## 1.6.1

* Regenerate civix - minimum version 5.58

## 1.6

* Check for emailActivityID param before creating activity - allows for activity creation to be disabled by passing in `emailActivityID` (if the caller already created the email activity) works with eg. emailApi - see [emailapi!32](https://lab.civicrm.org/extensions/emailapi/-/merge_requests/32).
* Fix PHP notices when CC/BCC emails are defined without being linked to a contact.

## 1.5

* Replace CRM_Utils_Array with coalesce operator.
* Fixes to creating email activity and handle API errors.
* Make sure we always have a source contact ID when creating activity.
* Check if an Email activity already exists before creating it (emailapi extension creates an email activity, CiviCRM core sometimes create an email activity).

## 1.4

* Fix crash when creating activities if we don't have a source contact ID (eg. when triggered via API3 explorer). We now default to the current logged in contact ID if not set.

## 1.3

* Fix bug that email activities are not created if we are not blocking any messages.
* Add CC/BCC to email activity if available.

## 1.2

* Check for user message templates as well as system workflow - fixes the issue that user driven messagetemplates were ignored by this extension.
