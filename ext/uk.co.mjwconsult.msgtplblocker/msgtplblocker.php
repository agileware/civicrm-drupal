<?php

require_once 'msgtplblocker.civix.php';
use CRM_Msgtplblocker_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function msgtplblocker_civicrm_config(&$config) {
  _msgtplblocker_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function msgtplblocker_civicrm_install() {
  _msgtplblocker_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function msgtplblocker_civicrm_enable() {
  _msgtplblocker_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
function msgtplblocker_civicrm_navigationMenu(&$menu) {
  _msgtplblocker_civix_insert_navigation_menu($menu, 'Administer/Communications', [
    'label' => E::ts('Message Template Blocker'),
    'name' => 'msgtplblocker_settings',
    'url' => 'civicrm/admin/setting/msgtplblocker',
    'permission' => 'administer CiviCRM',
    'operator' => 'OR',
    'separator' => 0,
  ]);
  _msgtplblocker_civix_navigationMenu($menu);
}

/**
 * Implements hook_civicrm_alterMailParams().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterMailParams/
 */
function msgtplblocker_civicrm_alterMailParams(&$params, $context) {
  // We only handle message templates, not civimail, flexmailer, singleEmail
  // But we have to use the "singleEmail" context or the messagetemplate has not been built and we can't save an activity
  if ($context != 'singleEmail') {
    return;
  }
  // This checks if we are in messageTemplate context
  if (empty($params['valueName']) && empty($params['messageTemplateID'])) {
    return;
  }

  $createActivity = (boolean) Civi::settings()->get('msgtplblocker_createactivity');
  $blockAll = (boolean) Civi::settings()->get('msgtplblocker_blockall');
  $filter = Civi::settings()->get('msgtplblocker_types') ?? [];

  // Create an activity record of this email?
  if ($createActivity) {
    if (empty($params['abortMailSend'])) {
      $params['status_id'] = CRM_Core_PseudoConstant::getKey('CRM_Activity_BAO_Activity', 'status_id', 'Completed');
    }
    else {
      $params['status_id'] = CRM_Core_PseudoConstant::getKey('CRM_Activity_BAO_Activity', 'status_id', 'Cancelled');
    }
    $createActivity = new CRM_Msgtplblocker_Activity();
    if (!$createActivity->isActivityAlreadyCreated($params)) {
      $createActivity->createActivity($params);
    }
  }

  // Block this email?
  if (empty($params['valueName'])) {
    return;
  }
  if ($blockAll || (in_array($params['valueName'], $filter))) {
    Civi::log()->info('msgtplblocker blocked the email of type: ' . $params['valueName']);
    $params['abortMailSend'] = TRUE;
  }
}
