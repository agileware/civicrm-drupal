<?php

use CRM_Msgtplblocker_ExtensionUtil as E;

class CRM_Msgtplblocker_Utils {

  /**
   * Helper function to return a list of message template names
   * for the settings page.
   */
  public static function getOptionsForTemplateList() {
    $list = [];

    $dao = CRM_Core_DAO::executeQuery("SELECT ov.name, ov.label
      FROM civicrm_option_value ov
      INNER JOIN civicrm_option_group og on (og.id = ov.option_group_id)
      WHERE og.name like 'msg_tpl%'");

    while ($dao->fetch()) {
      $list[$dao->name] = $dao->label;
    }

    return $list;
  }

}
