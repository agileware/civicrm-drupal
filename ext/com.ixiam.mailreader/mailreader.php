<?php

require_once 'mailreader.civix.php';
use CRM_Mailreader_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function mailreader_civicrm_config(&$config) {
  _mailreader_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function mailreader_civicrm_install() {
  _mailreader_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function mailreader_civicrm_enable() {
  _mailreader_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 */
function mailreader_civicrm_navigationMenu(&$menu) {
  $parent_path = "Administer/Administration Console";
  _mailreader_civix_insert_navigation_menu($menu, $parent_path, array(
    'label' => E::ts('View E-mail Log'),
    'name' => 'stored_email_reader',
    'url' => 'civicrm/admin/mailreader',
    'permission' => 'administer CiviCRM',
    'operator' => '',
    'separator' => NULL,
    'active' => 1
  ));
  _mailreader_civix_navigationMenu($menu);
}
