<?php

/**
 *
 * +--------------------------------------------------------------------+
 * |                                                                    |
 * | Developed by: iXiam                                                |
 * | @ iXiam Global Solutions (info@ixiam.com)                          |
 * | Last Modification Date: 2021-09-30                                 |
 * |                                                                    |
 * +--------------------------------------------------------------------+
 * | Copyright iXiam Global Solutions (c) 2007-2021                     |
 * | http://www.ixiam.com                                               |
 * +--------------------------------------------------------------------+
 *
 */

use CRM_Mailreader_ExtensionUtil as E;

class CRM_Mailreader_Form_Mailreader extends CRM_Core_Form {

  public $_action;
  private static $_limitRows = 20;

  public function buildQuickForm() {

    // Variables
    $params = $data = $this->exportValues();
    $warning = '';

    // Set page title
    CRM_Utils_System::setTitle(E::ts('Stored Email viewer'));

    // Fetch the mailing preferences
    $mailingInfo = CRM_Core_BAO_Setting::getItem(CRM_Core_BAO_Setting::MAILING_PREFERENCES_NAME, 'mailing_backend');

    if ($mailingInfo['outBound_option'] != '5') {
      // Issue a warning and do nothing
      $url = CRM_Utils_System::url('civicrm/admin/setting/smtp', 'reset=1');
      $warning = E::ts('Your <a href="%1">CiviCRM mailer option</a> is not set to <strong>"Redirect to Database"</strong>. I got nothing to do.', ["1" => $url]);
    }

    if (array_key_exists('exportid', $_GET)) {
      $exportID = $_GET['exportid'];
      $selected_record = $this->readlog($exportID, 0, TRUE, TRUE);
      $exportable_filename = 'mail_' . date_timestamp_get(date_create());
      $output_file = self::exportEML($selected_record, $exportable_filename);
    }

    // Limit to X rows by default
    $this->add('text', 'limit_to', E::ts('Limit to: ', ['domain' => 'com.ixiam.modules.mailreader']), ['size' => 6, 'maxlength' => 6]);
    // Show last rows first, by default
    $this->add('checkbox', 'show_last', E::ts('Show in DESCending order'));
    // Count how many rows do we have
    $countrows = $this->countlog();
    // Populate the table
    $limit_to = CRM_Utils_Request::retrieve('limit', 'Positive', $this);
    if (isset($_GET['order']) && $_GET['order'] == 'ASC') {
      $sort_order = FALSE;
    }
    elseif (isset($_GET['order']) && ($_GET['order'] == 'DESC') || !isset($_GET['order'])) {
      $sort_order = TRUE;
    }
    if ($limit_to) {
      $records = $this->readlog($limit_to, 0, $sort_order, FALSE);
    }
    else {
      // Default value-limit to $_limitRows
      $records = $this->readlog(self::$_limitRows, 0, $sort_order, FALSE);
    }
    // Add the export button
    $this->addButtons([
      [
        'type' => 'refresh',
        'subName' => 'refresh',
        'name' => E::ts('Refresh listing'),
      ],
      [
        'type' => 'submit',
        'subName' => 'delete_all',
        'name' => E::ts('Delete ALL entries'),
      ],
    ]);

    // Assign values to template
    $this->assign('limit_to', $limit_to);
    $this->assign('records', $records);
    $this->assign('total_rows', $countrows);
    if (isset($show_last)) {
      $this->assign('show_last', $show_last);
    }

    // We need this variable in all cases
    $this->assign('warning', $warning);
    $this->setDefaults();
    parent::buildQuickForm();
  }

  /**
   * This function sets the default values for the form.
   * default values are retrieved from the database
   *
   * @return array
   */
  public function setDefaultValues() {

    $limit_to = CRM_Utils_Request::retrieve('limit', 'Positive', $this);
    // Predefined value
    $defaults['show_last'] = TRUE;

    if ((isset($_GET['order']) && $_GET['order'] == 'DESC') || !isset($_GET['order'])) {
      $defaults['show_last'] = TRUE;
    }
    else {
      $defaults['show_last'] = FALSE;
    }
    if ($limit_to) {
      $defaults['limit_to'] = $limit_to;
    }
    else {
      $defaults['limit_to'] = self::$_limitRows;
    }

    return $defaults;
  }

  public function postProcess() {
    $params = $this->exportValues();

    if (isset($params['_qf_Mailreader_submit_delete_all'])) {
      // We will now be deleting all the log entries
      $this->deletelog();
      // After deletion, redirect
      CRM_Utils_System::redirect(CRM_Utils_System::url('civicrm/admin/mailreader', '&reset=1'));
    }

    if (isset($params['limit_to']) && isset($params['_qf_Mailreader_refresh_refresh'])) {

      $urlParams = 'limit=' . $params['limit_to'];
      if (isset($params['show_last'])) {
        $urlParams .= '&order=DESC';
      }
      else {
        $urlParams .= '&order=ASC';
      }
      $urlParams .= '&reset=1';
      CRM_Utils_System::redirect(CRM_Utils_System::url('civicrm/admin/mailreader', $urlParams));
    }

  }

  /**
   * This function returns the stored entries from a table.
   *
   * @access private
   * @param int $count
   * @param int $offset
   * @param mixed $order - Sort order: TRUE = DESC / FALSE = ASC
   * @param bool $single
   *
   * @return mixed
   *   id,
   *   job_id,
   *   recipient_email,
   *   headers,
   *   body,
   *   added_at,
   *   removed_at
   *
   * */
  private function readlog($count, $offset, $order, $single) {

    if ($order) {
      $sort_order = 'DESC';
    }
    else {
      $sort_order = 'ASC';
    }
    $sql_default = "
    SELECT  id,
            job_id,
            recipient_email,
            headers,
            body,
            added_at,
            removed_at
    FROM civicrm_mailing_spool";

    $sql = $sql_default . " ORDER BY id {$sort_order}";

    if ($count) {
      $sql .= " LIMIT {$count}";
    }
    if ($offset) {
      $sql .= " OFFSET {$offset}";
    }

    // If $single is TRUE, $count is taking the role of column 'id'
    if ($single) {
      $sql = $sql_default . " WHERE id = {$count}";
    }

    $fsql = CRM_Core_DAO::executeQuery($sql);
    $entries = [];

    while ($fsql->fetch()) {
      $entries[$fsql->id]['id'] = $fsql->id;
      $entries[$fsql->id]['job_id'] = $fsql->job_id;
      $entries[$fsql->id]['recipient_email'] = htmlspecialchars($fsql->recipient_email);
      $entries[$fsql->id]['headers'] = $fsql->headers;
      $entries[$fsql->id]['body'] = htmlspecialchars($fsql->body);
      $entries[$fsql->id]['attachments'] = $this->parseAttachments($fsql->headers . "\n\n" . $fsql->body);
      $entries[$fsql->id]['added_at'] = $fsql->added_at;
      $entries[$fsql->id]['removed_at'] = $fsql->removed_at;
    }

    return $entries;
  }

  /**
   * Parse the mail and return a list of attachments.
   */
  private function parseAttachments($body) {
    $files = [];

    if (!class_exists('ezcMailVariableSet')) {
      Civi::log()->debug('mailreader: class ezcMailVariableSet was not found. Was it removed from Core?');
      return $files;
    }

    if (!function_exists('getmypid')) {
      // Internal PHP function getmypid is not found or blacklisted.
      // We are avoiding logging anything as this will cause more log noise in environments that do not support this function (eg. blacklisted)
      return $files;
    }

    $set = new ezcMailVariableSet($body);
    $parser = new ezcMailParser();
    $parser->options->parseTextAttachmentsAsFiles = TRUE;
    $mail = $parser->parseMail($set);

    if (!empty($mail[0])) {
      $parts = $mail[0]->fetchParts();

      foreach ($parts as $part) {
        if (get_class($part) == 'ezcMailFile') {
          $files[] = $part->contentDisposition->fileName;
        }
      }
    }

    return $files;
  }

  /**
   * Private function that will delete ALL rows from table civicrm_mailing_spool
   *
   */
  private function deletelog() {
    $sql = "DELETE FROM civicrm_mailing_spool";
    $fsql = CRM_Core_DAO::executeQuery($sql);

    return TRUE;
  }

  /**
   * Private function that counts how many rows we have
   *
   */
  private function countlog() {
    $sql = "SELECT count(*) as countrows FROM civicrm_mailing_spool";
    $fsql = CRM_Core_DAO::executeQuery($sql);
    while ($fsql->fetch()) {
      $count = $fsql->countrows;
    }
    return $count;
  }

  /**
   * exportEML
   * Export selected email to file
   * Writes defined output to EML file
   *
   * @param  mixed $data
   * @param  mixed $filenameEML
   * @return void
   */
  private function exportEML($data, $filenameEML) {

    if (is_array($data)) {
      // We are interested only on the first value
      $data = array_shift($data);
      // Lets construct the full EML, by joining the headers with the body, separated by double line feed
      $emlData = $data['headers'] . "\n\n" . $data['body'];

      CRM_Utils_System::download(CRM_Utils_String::munge($filenameEML),
        'application/octet-stream',
        $emlData,
        'eml',
        TRUE
      );
    }
  }

}
