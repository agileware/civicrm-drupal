# Mailgunny - Mailgun bounce processing.

Mailgun is one of many emailing services (e.g. SMTP relays). While many services
offer to send all your bounced email to a particular email address (e.g. so
CiviCRM can process bounces), Mailgun does not.

However, Mailgun can be configured to send bounce information directly to
CiviCRM using webhooks, enabling the normal CiviMail mailing reports.

This extension provides this functionality.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

### How do I *send* email from CiviCRM using Mailgun?

To send email, configure CiviCRM to send email using an SMTP server - https://docs.civicrm.org/sysadmin/en/latest/setup/civimail/outbound/#smtp

A typical configuration might look like:

* SMTP server: ssl://smtp.eu.mailgun.org
* Port: 465
* Username/Password: *You'll need to create new SMTP credential in your mailgun account*

## Installation

See: https://docs.civicrm.org/sysadmin/en/latest/customize/extensions/#installing-a-new-extension

## Usage

### Step 0: Log in to Civi and Mailgun in separate tabs/browsers

In CiviCRM, visit the settings page at: **Administer » CiviMail » Mailgunny Settings** (path `civicrm/mailgunny/settings`).

### Step 1: configure your webhooks at Mailgun

Log in to Mailgun's website and find the Webhooks page (*not* the "legacy webhooks"). As of Nov 2021, this is under **Sending » Webhooks** in the side panel.

For **Permanent Failure** and **Temporary Failure** events, enter the webhook URL for your site, which **you can copy from the Mailgunny Settings page** and will look like:

- Drupal 7: `https://example.com/civicrm/mailgunny/webhook`
- Wordpress: `https://example.com/?page=CiviCRM&q=civicrm/mailgunny/webhook`
- Joomla: `https://example.com/index.php?option=com_civicrm&task=civicrm/mailgunny/webhook`

### Step 2: enter your Mailgun "HTTP webhook signing key" in your CiviCRM Mailgunny settings page

Nb. the "HTTP webhook signing key" key is *not* your Mailgun password (nor your domain's SMTP password). You can find it on the Webhooks page.

Put it in the box and press Save.

If you don't want your API key exposed here, you can put it in a constant, e.g. in `civicrm.settings.php`:

```php
define('MAILGUN_API_KEY', 'xxxxxxxxxxxxxxxxx');
```

You'll also have to supply the API end point. This should be one of:

- `https://api.eu.mailgun.net/v3/` Use this if your domain is listed with an EU
  flag in the Mailgun UI.
- `https://api.mailgun.net/v3/` Use this otherwise.


## Hey what's with the name?

Gunny is a strong coarse material. Such that you might make sacks out of. Like
post/mail sacks. And this is about mailings. So Mailgunny. And, what we're
**not** interested in at all is guns, so it's a deliberate subversion of
Mailgun's name. After all, who wants to be shot by email?

## Mailgun suppressions are deleted when emails are edited in CiviCRM if not on hold.

Whenever an email address is edited in CiviCRM, e.g. from the Contact screen (but also via the API), then it is assumed that the data just entered is correct, and that if the current data in CiviCRM says it's not *on hold* then any suppression ("bounces" or "compaints") at Mailgun should be removed.

Before this feature was added (extension versions before 1.4.0) then a hard bounce would mean that Mailgun suppressed the email, then told Civi which put it on hold. An admin later who knows that email is OK, removes the hold in Civi, but because it's suppressed in Mailgun, your email still won't get through.

If you edit an email and leave it or put it on hold, no suppressions are deleted.

## Bulk sync of suppressions and CiviCRM's on hold.

There is an API (see below) that can be used to remove all suppressions at Mailgun where there isn't a matching on-hold email in CiviCRM. Take care using this; it could unblock emails that have not been used in a long while.

## Api4: Email.RemoveMailgunSuppression

You can call this for a single email address (set the `address` parameter only) to delete Mailgun suppressions for that address. It will report what it had to do, if anything.

You can also call this to perform the bulk sync, by setting the `removeAllNotOnHold` parameter TRUE. There is also a `dryRun` parameter which will let you preview what *would* have been done.

## DKIM, SPF and sub domains

Mailgun would have you set up a subdomain, e.g. email.example.org and use that for your mailings. This is OK but it does result in emails showing up as "via: email.example.org" on some mail clients, which is confusing cruft.

After a long support thread Mailgunny now offers a feature so that you can achieve the following holy grail:

- `Return-Path:` header using the subdomain (e.g. email.example.org).

   - ✔ Good: delayed bounces will be handled by Mailgun (and fed back to Civi)

   - ✔ Good: your dedicated subdomain SPF record handles SPF authentication for this subdomain.

- `From:` header using a real email address in your main domain, e.g. wilma@example.org

   - ✔ Good: your email comes from a normal address

- `DKIM-Signature:` header uses the main domain (`example.org`) for the signature, and there's no `Sender:` header.

   - ✔ Good: gets rid of 'via...' and looks like a proper, normal email.

You can achieve this by:

1. 'Verifying' your root domain (example.org) with Mailgun.

   - Add a DKIM public key
   - Add `include:mailgun.org` into your domain's SPF. (Yes you apparently have to do this even though we won't be using SPF on the root domain)
   - **Do NOT** change the MX records to Mailgun for your root domain. That would be very bad (as in Mailgun would receive *all* your inbound email...)
   - You do not need to add 'tracking' records.

2. Next, verify your **subdomain** (e.g. may be _email_.example.org)

   - Add a DKIM public key for the subdomain.
   - Add an SPF record for the subdomain
   - Add the MX records to Mailgun for your subdomain.
   - You do not need to add 'tracking' records.

3. In CiviCRM, visit **Administer » CiviMail » Mailgunny Settings** and tick the box about the "Mailgun native send header"

**Note:** If you did (2) before (1), you'll need to contact Mailgun or [use their API](https://documentation.mailgun.com/en/latest/api-domains.html#domains) and explain what you want. (By the way, this took me 13 emails...).

Testing: Unfortunately, you can't properly test this by sending a test email from the CiviCRM SMTP settings page, as that does not call the hook that this extension uses. The easiest way is to install the [Test-Send Message Templates](https://lab.civicrm.org/extensions/msgtpltester) extension, and send yourself an email that way. Or maybe just send an email by adding an Email activity.

In the headers, if successful, you should see the above headers, plus one called `x-mailgun-native-send: true`.


