<?php
namespace Civi\Api4\Action\Email;

use Civi;
use Civi\Api4\Generic\AbstractAction;
use Civi\Api4\Generic\Result;
use Civi\Mailgunny\MailgunApiClient;
use CRM_Core_DAO;

class RemoveMailgunSuppression extends AbstractAction {

  /**
   * Email address to remove suppression for.
   *
   * @var string
   */
  protected $address;

  /**
   * Remove all suppressions that are not on hold in CiviCRM
   *
   * @default FALSE
   * @var bool
   */
  protected $removeAllNotOnHold = FALSE;

  /**
   * Don't really remove suppressions
   *
   * @default FALSE
   * @var bool
   */
  protected $dryRun = FALSE;

  public function _run(Result $result) {
    if ($this->removeAllNotOnHold && $this->address) {
      throw new \API_Exception("Either provide email address, OR choose removeAllNotOnHold. Not both.");
    }
    if (empty($this->address) && empty($this->removeAllNotOnHold)) {
      throw new \API_Exception("You must provide either email address OR choose removeAllNotOnHold.");
    }

    if ($this->address) {
      if ($this->dryRun) {
        throw new \API_Exception("dryRun not supported for single address.");
      }
      $api = new MailgunApiClient();
      $result[] = [
        'result' => $api->removeSuppression($this->address)
      ];
    }
    else {
      $this->sync($result);
    }
  }

  /**
   */
  protected function sync(Result $result) {
    $api = new MailgunApiClient();
    $this->unsuppress($api, $result, 'bounces');
    $this->unsuppress($api, $result, 'complaints');
  }

  protected function unsuppress(MailgunApiClient $api, Result $result, string $type) {
    if (!in_array($type, ['bounces', 'complaints'])) {
      throw new \BadMethodCallException('$type must be bounces|complaints');
    }

    \CRM_Core_DAO::executeQuery('CREATE TEMPORARY TABLE mailgunny_emails (email VARCHAR(255) NOT NULL DEFAULT "" PRIMARY KEY);');
    $n = 0;
    $url = $type;
    do {
      $response = $api->requestQuiet('GET', $url);
      // Civi::log()->debug("Loaded " . count($response['items']) . " $type, $n done");
      $url = ($response['paging']['next'] === $response['paging']['last'])
            ? NULL
            : $response['paging']['next'];
      $rows = [];
      $params = [];
      $i = 1;
      foreach ($response['items'] as $item) {
        $rows[] = "(%$i)";
        $params[$i] = [$item['address'], 'String'];
        $i++;
      }
      $n += $i-1;
      if ($i > 1) {
        CRM_Core_DAO::executeQuery('INSERT IGNORE INTO mailgunny_emails VALUES ' . implode(',', $rows), $params);
      }
    } while ($url && $i > 1);

    Civi::log()->debug("Loaded $n $type suppressions");
    // Find the emails that are NOT on hold in Civi, but are suppressed at MG.
    $dao = CRM_Core_DAO::executeQuery(<<<SQL
      SELECT mg.email
      FROM mailgunny_emails mg
      WHERE NOT EXISTS (
        SELECT email
        FROM civicrm_email c
        WHERE c.email = mg.email AND on_hold > 0
      )
      SQL
    );
    $freed = [];
    while ($dao->fetch()) {
      $freed[] = $dao->email;
      // sense check.
      if (empty($dao->email) || ($dao->email === 'null')) {
        throw new \API_Exception("Encountered something that is not an email: '$dao->email'");
      }
      if ($this->dryRun) {
        continue;
      }
      $api->requestQuiet('DELETE', "$type/$dao->email");
    }
    CRM_Core_DAO::executeQuery('DROP TEMPORARY TABLE mailgunny_emails');

    Civi::log()->info("Found "
      . count($freed)
      . " emails not on hold in Civi but suppressed ($type) in Mailgun. "
      . ($this->dryRun ? ' DRY RUN, no suppressions deleted.' : 'Suppressions were deleted.')
    );
    $result[] = ['dryRun' => $this->dryRun, 'type' => $type, 'count' => count($freed), 'emails' => $freed];
  }
}
