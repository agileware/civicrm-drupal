<?php
namespace Civi\Mailgunny;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\TransferException;
use Civi;
use Civi\Mailgunny\Config;

class MailgunApiClient {

  protected GuzzleClient $gz;

  public function __construct() {
    $apiKey = Config::getApiKey();
    $endpointWithTrailingSlash = Config::getApiEndpoint() . Config::getMailDomain() . '/';
    $this->gz = new GuzzleClient([
      'base_uri'    => $endpointWithTrailingSlash,
      'http_errors' => TRUE, // Get exceptions from guzzle requests.
      'timeout' => 5,
      'headers' => [
        'Authorization' => ['Basic '.base64_encode("api:$apiKey")],
      ],
    ]);
  }

  public function request(string $method, string $path, $query=null, $json=null): array {
    $params = [];
    if ($query) {
      $params['query'] = $query;
    }
    if ($json) {
      $params['json'] = $json;
    }

    $response = $this->gz->request($method, $path, $params);
    if ($this->isJsonResponse($response)) {
      // OK, return the JSON.
      $responseArray = json_decode($response->getBody(), TRUE);
      $responseArray['httpStatusCode'] = $response->getStatusCode();
      return $responseArray;
    }
    else {
      throw new \RuntimeException("Expected JSON, got something else." . $response->getBody()->getContents());
    }
  }

  protected function isJsonResponse($response) {
    $json_returned = ($response->hasHeader('Content-Type') && preg_match(
      '@^application/(problem\+)?json\b@i',
      $response->getHeader('Content-Type')[0]));
    return $json_returned;
  }

  public function requestQuiet(string $method, string $path, $query=null, $json=null): array {
    try {
      return $this->request(... func_get_args());
    }
    catch (ClientException $e) {
      // 4xx errors
      if ($this->isJsonResponse($e->getResponse())) {
        return [
          'errorResponseMessage' => json_decode($e->getResponse()->getBody()->getContents(), TRUE)['message'],
          'errorResponseCode' => $e->getCode(),
          'httpStatusCode' => $e->getResponse()->getStatusCode(),
        ];
      }
      return [
        'errorResponseMessage' => $e->getResponse()->getBody()->getContents(),
        'errorResponseCode' => $e->getCode(),
        'httpStatusCode' => $e->getResponse()->getStatusCode(),
      ];
    }
    catch (TransferException $e) {
      return [
        'errorResponseMessage' => $e->getMessage(),
        'errorResponseCode' => $e->getCode(),
        'httpStatusCode' => 500,
      ];
    }
    catch (\Exception $e) {
      return [
        'errorResponseMessage' => $e->getMessage(),
        'errorResponseCode' => $e->getCode(),
        'httpStatusCode' => 500,
      ];
    }
  }

  /**
   * Check if the address is found in bounces/compaints
   * and if so, remove it.
   */
  public function removeSuppression(string $address): string {
    $output = [];

    $result = $this->requestQuiet('DELETE', "bounces/$address");
    if ($result['httpStatusCode'] == 200) {
      Civi::log()->info("mailgun: '$address' $result[message]");
      $output[] = 'removed bounce';
    }

    $result = $this->requestQuiet('DELETE', "complaints/$address");
    if ($result['httpStatusCode'] == 200) {
      Civi::log()->info("mailgun: '$address' $result[message]");
      $output[] = 'removed complaint';
    }

    return $output ? implode(', ', $output) : 'was not suppressed';
  }

}

