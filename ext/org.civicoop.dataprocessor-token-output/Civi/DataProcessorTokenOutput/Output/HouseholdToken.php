<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessorTokenOutput\Output;

use Civi\DataProcessor\DataFlow\EndOfFlowException;
use Civi\DataProcessor\DataFlow\Sort\SortCompareFactory;
use Civi\DataProcessor\DataFlow\SqlDataFlow;
use Civi\DataProcessor\Exception\DataSourceNotFoundException;
use Civi\DataProcessor\Exception\FieldNotFoundException;
use Civi\DataProcessor\Output\OutputInterface;

use Civi\DataProcessor\ProcessorType\AbstractProcessorType;
use CRM_DataprocessorTokenOutput_ExtensionUtil as E;

class HouseholdToken extends AbstractToken {

  /**
   * Returns the title of the ID field.
   *
   * @return String
   */
  protected function getIdFieldTitle() {
    return E::ts('Contact ID Field');
  }

  /**
   * @return string
   */
  protected function getIdFieldName() {
    return 'contact_id_field';
  }

  /**
   * @return string
   */
  protected function getTokenProcessorIdFieldName() {
    return 'contactId';
  }

  /**
   * When this output type has additional configuration you can add
   * the fields on the form with this function.
   *
   * @param \CRM_Core_Form $form
   * @param array $output
   */
  public function buildConfigurationForm(\CRM_Core_Form $form, $output = []) {
    parent::buildConfigurationForm($form, $output);

    $relationshipTypeApi = civicrm_api3('RelationshipType', 'get', array('is_active' => 1, 'options' => array('limit' => 0)));
    $relationshipTypes = array();
    foreach($relationshipTypeApi['values'] as $relationship_type) {
      if ($relationship_type['contact_type_a'] == 'Individual' && $relationship_type['contact_type_b'] == 'Household') {
        $relationshipTypes['a_b_' . $relationship_type['name_a_b']] = $relationship_type['label_a_b'];
      }
      if ($relationship_type['contact_type_a'] == 'Household' && $relationship_type['contact_type_b'] == 'Individual') {
        $relationshipTypes['b_a_'.$relationship_type['name_b_a']] = $relationship_type['label_b_a'];
      }
    }

    $form->add('select', 'relationship_types', E::ts('Household relationship'), $relationshipTypes, true, array(
      'style' => 'min-width:250px',
      'class' => 'crm-select2 huge',
      'placeholder' => E::ts('- Show all roles -'),
      'multiple' => true,
    ));

    $defaults = array();
    if ($output) {
      if (isset($output['configuration']) && is_array($output['configuration'])) {
        if (isset($output['configuration']['relationship_types'])) {
          $defaults['relationship_types'] = $output['configuration']['relationship_types'];
        }
      }
    }

    if (!isset($defaults['relationship_types'])) {
      $defaults['relationship_types'] = array(
        'a_b_Head of Household for',
        'a_b_Household Member of'
      );
    }
    $form->setDefaults($defaults);
  }

  /**
   * When this output type has configuration specify the template file name
   * for the configuration form.
   *
   * @return false|string
   */
  public function getConfigurationTemplateFileName() {
    return "CRM/DataprocessorTokenOutput/Form/HouseholdTokenConfiguration.tpl";
  }

  /**
   * Process the submitted values and create a configuration array
   *
   * @param $submittedValues
   * @param array $output
   *
   * @return array $output
   */
  public function processConfiguration($submittedValues, &$output) {
    $configuration = parent::processConfiguration($submittedValues, $output);
    $configuration['relationship_types'] = $submittedValues['relationship_types'];
    return $configuration;
  }

  /**
   * Function to set a different contact ID to be used in the data processor.
   * This function is here so child classes can override it.
   *
   * @param int $contactId
   * @param array $values
   * @param array $configuration
   *
   * @return int|null
   */
  protected function getIdForToken($contactId, $values, $configuration) {
    $householdContactId = FALSE;
    try {
      $contact = civicrm_api3('Contact', 'getsingle', ['id' => $contactId]);
    } catch (\CiviCRM_API3_Exception $e) {
      return null;
    }
    if ($contact['contact_type'] == 'Individual') {
      $householdContactId = $this->retrieveHousehold($contactId, $configuration['relationship_types']);
    }
    elseif ($contact['contact_type'] == 'Household') {
      $householdContactId = $contactId;
    }
    return $householdContactId;
  }

  /**
   * Function to set a different contact ID to be used in the data processor.
   * This function is here so child classes can override it.
   *
   * @param \Civi\Token\TokenRow $tokenRow
   * @param $configuration
   *
   * @return int|null
   */
  protected function getIdForTokenFromTokenRow($tokenRow, $configuration) {
    if (isset($tokenRow->context[$this->getTokenProcessorIdFieldName()])) {
      $contactId = $tokenRow->context[$this->getTokenProcessorIdFieldName()];
      $householdContactId = FALSE;
      try {
        $contact = civicrm_api3('Contact', 'getsingle', ['id' => $contactId]);
      } catch (\CiviCRM_API3_Exception $e) {
        return null;
      }
      if ($contact['contact_type'] == 'Individual') {
        $householdContactId = $this->retrieveHousehold($contactId, $configuration['relationship_types']);
      }
      elseif ($contact['contact_type'] == 'Household') {
        $householdContactId = $contactId;
      }
      return $householdContactId;
    }
    return null;
  }

  protected function retrieveHousehold($contact_id, $relationship_types) {
    $sql['a_b'] = "SELECT c.id
            FROM civicrm_contact c
            INNER JOIN civicrm_relationship r ON r.contact_id_b = c.id
            INNER JOIN civicrm_relationship_type t on r.relationship_type_id = t.id
            WHERE c.is_deleted = 0 AND r.is_active = 1 AND r.contact_id_a = %1 AND c.contact_type = 'Household'";
    $sql['b_a'] = "SELECT c.id
            FROM civicrm_contact c
            INNER JOIN civicrm_relationship r ON r.contact_id_a = c.id
            INNER JOIN civicrm_relationship_type t on r.relationship_type_id = t.id
            WHERE c.is_deleted = 0 AND r.is_active = 1 AND r.contact_id_b = %1 AND c.contact_type = 'Household'";

    $relationship_type_ids = array();
    if (is_array($relationship_types)) {
      foreach($relationship_types as $rel_type) {
        $dir = substr($rel_type, 0, 4);
        $rel_type_name = substr($rel_type, 4);
        if ($dir == 'a_b_') {
          $rel_type_id = civicrm_api3('RelationshipType', 'getvalue', ['return' => 'id', 'name_a_b' => $rel_type_name]);
        } else {
          $rel_type_id = civicrm_api3('RelationshipType', 'getvalue', ['return' => 'id', 'name_b_a' => $rel_type_name]);
        }
        $relationship_type_ids[] = array('dir' => $dir, 'id' => $rel_type_id);
      };
    }

    if (count($relationship_type_ids)) {
      $relationShipTypeIdsA_B = array();
      $relationShipTypeIdsB_A = array();
      foreach($relationship_type_ids as $rel_type) {
        if ($rel_type['dir'] == 'a_b_') {
          $relationShipTypeIdsA_B[] = $rel_type['id'];
        } else {
          $relationShipTypeIdsB_A[] = $rel_type['id'];
        }
      }
      if (count($relationShipTypeIdsA_B)) {
        $sql['a_b'] .= " AND t.id IN (" . implode(", ", $relationShipTypeIdsA_B) . ") ";
      } else {
        unset($sql['a_b']);
      }
      if (count($relationShipTypeIdsB_A)) {
        $sql['b_a'] .= " AND t.id IN (" . implode(", ", $relationShipTypeIdsB_A) . ") ";
      } else {
        unset($sql['b_a']);
      }
    }

    if (count($sql)) {
      $sql = implode(" UNION ", $sql);
      $sqlParams[1] = [$contact_id, 'Integer'];
      $dao = \CRM_Core_DAO::executeQuery($sql, $sqlParams);
      while ($dao->fetch()) {
        return $dao->id;
      }
    }

    return false;
  }


}
