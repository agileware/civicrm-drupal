<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessorTokenOutput\Output;

use Civi\DataProcessor\DataFlow\EndOfFlowException;
use Civi\DataProcessor\DataFlow\Sort\SortCompareFactory;
use Civi\DataProcessor\DataFlow\SqlDataFlow;
use Civi\DataProcessor\Exception\DataSourceNotFoundException;
use Civi\DataProcessor\Exception\FieldNotFoundException;
use Civi\DataProcessor\Output\OutputInterface;

use Civi\DataProcessor\ProcessorType\AbstractProcessorType;
use CRM_DataprocessorTokenOutput_ExtensionUtil as E;

class EventToken extends AbstractToken {

  /**
   * Array containing all event ids
   *
   * This property is set when executed from an participant search action.
   *
   * @var array
   */
  private static $_eventIds = null;

  /**
   * @return string
   */
  protected function getTokenProcessorIdFieldName() {
    return 'eventId';
  }

  /**
   * Function to set a different contact ID to be used in the data processor.
   * This function is here so child classes can override it.
   *
   * @param $contactId
   * @param $configuration
   *
   * @return mixed
   */
  protected function getIdForToken($contactId, $values, $configuration) {
    if (isset($values['extra_data']['event']['id'])) {
      // Coming from CiviRules
      return $values['extra_data']['event']['id'];
    }
    if (isset($values['extra_data']['participant']['id'])) {
      // Coming from CiviRules
      $participant_id = $values['extra_data']['participant']['id'];
      try {
        $event_id = civicrm_api3('Participant', 'getvalue', [
          'id' => $participant_id,
          'return' => 'event_id'
        ]);
      } catch (\CiviCRM_API3_Exception $e) {
        return false;
      }
      return $event_id;
    } elseif (is_array(self::$_eventIds) && count(self::$_eventIds)) {
      foreach(self::$_eventIds as $key => $val) {
        if ($val['contact_id'] == $contactId) {
          unset(self::$_eventIds[$key]);
          return $val['event_id'];
        }
      }
      return array_shift(self::$_eventIds);
    }
    return false;
  }

  /**
   * Returns the title of the ID field.
   *
   * @return String
   */
  protected function getIdFieldTitle() {
    return E::ts('Event ID Field');
  }

  /**
   * @return string
   */
  protected function getIdFieldName() {
    return 'event_id_field';
  }

  /**
   * Extract the event ids from the event task forms.
   * We need this as soon as the task is a send letter or send e-mail from the action
   * after a search for participants.
   *
   * We can then use those event ids during the processing of the tokens.
   *
   * @param $form
   */
  public static function extractParticipantIdsFromEventFormTasks(\CRM_Core_Form $form) {
    if ($form instanceof \CRM_Event_Form_Task) {
      $participantIds = $form->getVar('_participantIds');

      self::$_eventIds = array();
      $IDs = implode(',', $participantIds);
      $query = "SELECT contact_id, event_id, id FROM civicrm_participant WHERE id IN ($IDs)";
      $dao = \CRM_Core_DAO::executeQuery($query);
      while ($dao->fetch()) {
        $key = array_search($dao->id, $participantIds);
        if ($key !== false) {
          self::$_eventIds[$key] = ['contact_id' => $dao->contact_id, 'event_id' => $dao->event_id];
        }
      }
      ksort(self::$_eventIds);
    }
  }

}
