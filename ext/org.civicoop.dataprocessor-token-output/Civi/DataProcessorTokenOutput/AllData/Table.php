<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessorTokenOutput\AllData;

use Civi\DataProcessor\FieldOutputHandler\HTMLFieldOutput;

class Table {

  public static function render($records, $availableFields) {
    $output = '<table>';
    $output .= '<tr>';
    foreach($availableFields as $field) {
      $output .= '<th>'.$field->title.'</th>';
    }
    $output .= '</tr>';
    foreach($records as $record) {
      $output .= '<tr>';
      foreach($availableFields as $field) {
        if ($record[$field->alias] instanceof HTMLFieldOutput) {
          $output .= '<td>'.$record[$field->alias]->getMarkupOut().'</td>';
        } else {
          $output .= '<td>' . $record[$field->alias]->formattedValue . '</td>';
        }
      }
      $output .= '</tr>';
    }
    $output .= '</table>';
    return $output;
  }

}
