<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessorTokenOutput;

class TokenFactory {

  protected $tokenOutputTypes = [];

  public function getTokenOutputTypes() {
    return $this->tokenOutputTypes;
  }

  public function addTokenOutputType($type) {
    $this->tokenOutputTypes[] = $type;
  }
}
