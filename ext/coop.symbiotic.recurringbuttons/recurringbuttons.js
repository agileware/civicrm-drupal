(function($, _, ts){

  // This is mostly to handle invoice payment
  // In most other cases, the JS is not loaded.
  if ($('#is_recur').size() == 0) {
    return;
  }

  // Hide the core checkbox, and move the recurring option above the priceset
  $('.crm-section.is_recur-section').hide();
  $('#priceset').before($('.crm-section.is_recur_radio-section'));

  // Move the help text next to the new radio buttons
  $('#recurHelp').appendTo($('.crm-section.is_recur_radio-section .content'));

  // Synchronize the values of the radio and checkbox
  $('input[name=is_recur_radio]').on('change', function() {
    var interval = $('input[name=is_recur_radio]:checked').val();
    var checked = (interval != '');
    $('#is_recur').prop('checked', checked).trigger('change');

    // Frequency unit, if enabled
    if ($('#frequency_unit').size() > 0) {
      $('#frequency_unit').val(interval).trigger('change');
    }
  });

  // Initial state - notably to help with the radiobuttons (extension) integration
  var recur_unit_default = CRM.vars.recurringbuttons.recur_unit_default;

  if ($('#is_recur').prop('checked')) {
    $('input[name="is_recur_radio"][value=""]').prop('checked', false);
    $('input[name="is_recur_radio"][value="' + recur_unit_default + '"]').trigger('click');
  }
  else {
    $('input[name="is_recur_radio"][value=""]').trigger('click');
    $('input[name="is_recur_radio"][value="' + recur_unit_default + '"]').prop('checked', false);
  }

})(CRM.$, CRM._, CRM.ts('recurringbuttons'));
