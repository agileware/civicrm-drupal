<?php

require_once 'recurringbuttons.civix.php';
use CRM_Recurringbuttons_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config
 */
function recurringbuttons_civicrm_config(&$config) {
  _recurringbuttons_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function recurringbuttons_civicrm_install() {
  _recurringbuttons_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function recurringbuttons_civicrm_enable() {
  _recurringbuttons_civix_civicrm_enable();
}

/**
 * Check if recur unit enabled on the contribution page
 * is supported by the extension.
 *
 * @param array $values
 */
function recurringbuttons_isRecurUnitSupported($values) {
  // As of 2020, these are all the core options
  // in theory, someone could have custom intervals.
  $supportedUnits = ['day', 'week', 'month', 'year'];
  $enabledUnits = explode(CRM_Core_DAO::VALUE_SEPARATOR, $values['recur_frequency_unit']);
  if (array_diff($enabledUnits, $supportedUnits)) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Implements hook_civicrm_buildForm().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_buildForm
 */
function recurringbuttons_civicrm_buildForm($formName, &$form) {
  if ($formName == 'CRM_Contribute_Form_Contribution_Main') {
    if (!$form->elementExists('is_recur') || !recurringbuttons_isRecurUnitSupported($form->_values)) {
      return;
    }

    if (!empty(CRM_Utils_Request::retrieveValue('snippet', 'String'))) {
      return;
    }

    if (function_exists('radiobuttons_convert_element')) {
      radiobuttons_convert_element('.is_recur_radio-section .content', [
        'mandatory_field' => true,
        'button_width' => 200,
      ]);
    }

    // Check if the checkbox is frozen (hidden)
    // Although this does not help for invoice payments, where it exists, but somehow not displayed.
    // We handle that in JS later.
    $e = $form->getElement('is_recur');

    if (isset($e->_flagFrozen) && $e->_flagFrozen == true) {
      return;
    }

    // Re-create a new widget
    $recur_options = [];
    $recur_options[''] = E::ts('One-Time');

    $labels = [
      'day' => E::ts('Daily'),
      'week' => E::ts('Weekly'),
      'month' => E::ts('Monthly'),
      'year' => E::ts('Yearly'),
    ];

    $enabledUnits = explode(CRM_Core_DAO::VALUE_SEPARATOR, $form->_values['recur_frequency_unit']);

    foreach ($enabledUnits as $unit) {
      $recur_options[$unit] = $labels[$unit];
    }

    $form->addRadio('is_recur_radio', E::ts('Select Donation Type'), $recur_options);

    CRM_Core_Region::instance('form-body')->add([
      'template' => 'CRM/Recurringbuttons/Contribute/Form/Contribution/Main.tpl',
    ]);

    // FIXME: If the form is reloading because of an error, does this cause problems?
    // Maybe mitigated by the initial state check in JS
    $defaults = [];
    $form_id = $form->get('id');
    $defaults['is_recur'] = Civi::settings()->get('recurringbuttons_default_' . $form_id) ?? 0;

    if ($unit = CRM_Utils_Request::retrieveValue('frequency_unit', 'String')) {
      $defaults['is_recur'] = 1;
      $defaults['frequency_unit'] = $unit;
    }

    if (!empty($defaults)) {
      $form->setDefaults($defaults);
    }

    // If set to default to recurring, then default to the first recurring unit
    Civi::resources()->addVars('recurringbuttons', [
      'is_recur' => $defaults['is_recur'] ?? 0,
      'recur_unit_default' => $defaults['frequency_unit'] ?? $enabledUnits[0],
      'frequency_unit' => $defaults['frequency_unit'] ?? '',
    ]);

    Civi::resources()->addScriptFile('recurringbuttons', 'recurringbuttons.js');
  }
  elseif($formName == 'CRM_Contribute_Form_ContributionPage_Amount') {
    CRM_Recurringbuttons_Contribute_Form_ContributionPage_Amount::buildForm($form);
  }
}

/**
 * Implementation of hook_civicrm_postProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postProcess
 */
function recurringbuttons_civicrm_postProcess($formName, &$form) {
  if ($formName == 'CRM_Contribute_Form_ContributionPage_Amount') {
    CRM_Recurringbuttons_Contribute_Form_ContributionPage_Amount::postProcess($form);
  }
}
