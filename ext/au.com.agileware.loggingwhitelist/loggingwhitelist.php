<?php

require_once 'loggingwhitelist.civix.php';
use CRM_Loggingwhitelist_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/ 
 */
function loggingwhitelist_civicrm_config(&$config) {
  _loggingwhitelist_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function loggingwhitelist_civicrm_install() {
  _loggingwhitelist_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function loggingwhitelist_civicrm_postInstall() {
  _loggingwhitelist_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function loggingwhitelist_civicrm_uninstall() {
  _loggingwhitelist_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function loggingwhitelist_civicrm_enable() {
  _loggingwhitelist_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function loggingwhitelist_civicrm_disable() {
  _loggingwhitelist_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function loggingwhitelist_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _loggingwhitelist_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function loggingwhitelist_civicrm_entityTypes(&$entityTypes) {
  _loggingwhitelist_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_alterLogTables().
 *
 * @param array $logTableSpec
 */
function loggingwhitelist_civicrm_alterLogTables(&$logTableSpec) {
    CRM_LoggingWhitelist::alterLogTables($logTableSpec);   
}
