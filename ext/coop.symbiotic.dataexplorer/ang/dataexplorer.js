(function(angular, $, _) {
  // Declare a list of dependencies.
  // Making angular conditional until we figure out how to more cleanly integrate with dashlets
  if (angular) {
    angular.module('dataexplorer', CRM.angRequires('dataexplorer'));
  }

  /**
   * Helper function to format query values
   *
   * An object of type {foo:'a', bar:'b'} will return "foo-a,bar-b"
   * so that we can send http requests such as "groupby=foo-a,bar-b".
   */
  CRM.dataexplorer_format_values = function dataexplorer_format_values(values) {
    // FIXME: if a value is an object (ex: date), it must first be converted to a string.
    var v = _.cloneDeep(values);

    $.each(v, function(index, value) {
      if (typeof value == 'object') {
        v[index] = value.toISOString().slice(0, 10);
      }
    });

    // Converts the object to foo=1&bar=2
    var str = $.param(v);

    // Special case, not too sure maybe we should generalize all elements to this format
    str = str.replace(/period-start=/, 'period-start:');
    str = str.replace(/period-end=/, 'period-end:');

    // This convert period=year to period-year
    str = str.replace(/=/g, '-');
    str = str.replace(/\&/g, ',');

    return str;
  };

  /**
   * Load preferences from localStorage.
   */
  CRM.dataexplorer_load_preferences = function dataexplorer_load_preferences($scope, data) {
    if (!data) {
      return;
    }

    data = JSON.parse(data);

    if (typeof data.view_mode) {
      $.each(data.view_mode, function(index, value) {
        $scope.view_mode[index] = value;
      });
    }

    if (typeof data.measure != 'undefined') {
      // This is the select2 element
      $scope.measure = Object.keys(data.measure).join(',');

      $.each(data.measure, function(index, value) {
        $scope.measure_values[index] = value;
      });

      // FIXME This is copied from measure.watch (in Dataexplorer.js)
      // So that filters are visible. Either make a function or cleaup.
      var all_filters = [];

      _.each(CRM.vars.dataexplorer.filters, function(g, key) {
        var formatted = {id: key, text: g.label};
        all_filters.push(formatted);
      });
      $scope.all_filters = all_filters;

      // @todo See above
      var all_groups = [];

      _.each(CRM.vars.dataexplorer.groups, function(g, key) {
        var formatted = {id: key, text: g.label};
        all_groups.push(formatted);
      });
      $scope.all_groups = all_groups;
    }

    if (typeof data.filter != 'undefined') {
      var tmp_filters = [];

      $.each(data.filter, function(index, value) {
        var parts = index.split('-');

        // This is for the select2 element
        // Handle date filters such as 'period-start' or 'period-end'
        if (parts.length > 1) {
          tmp_filters.push(parts[0]);

          if (value.match(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/)) {
            $scope.filter_values[index] = new Date(value);
          }
          else {
            $scope.filter_values[index] = value;
          }
        }
        else {
          tmp_filters.push(index);
          $scope.filter_values[index] = value;
        }
      });

      if (tmp_filters.length > 0) {
        $scope.filter = tmp_filters.join(',');
      }
    }

    if (typeof data.groupby != 'undefined') {
      var tmp_groupbys = [];

      $.each(data.groupby, function(index, value) {
        $scope.groupby_values[index] = value;
        tmp_groupbys.push(index);
      });

      if (tmp_groupbys.length > 0) {
        $scope.groupby = tmp_groupbys.join(',');
      }
    }

    // TODO: do we need to update the 'depends'? (show filters relevant for the measures)

    // View mode
    var viewmodes = ['table', 'barchart', 'linechart', 'piechart', 'map'];

    viewmodes.forEach(function(element, index, array) {
      if ($scope.dz.get('vizmode-' + element)) {
        $scope.view_mode[element] = true;
      }
    });

    $scope.dz.reloading = false;
  };

  /**
   * Removes values from all checkboxes, dates, etc.
   */
  function dataexplorer_reset_ui_prefs() {
    // Unselect viz modes and most checkboxes
    $('a.item-selected')
      .removeClass('item-selected');

    // Trinary exclude/include/only items
    $('#dataexplorer-menu-nav > .list-group > .list-group-item > .list-group > .list-group-item > .btn-group > label.btn')
      .not('.dataexplorer-checkbox-label-0')
      .removeClass('active')
      .removeClass('btn-primary');

    // Reset all badges
    $('a .badge').html('');
  };

  /**
   * Loads known presets in a widget.
   * FIXME: presets should be renamed to 'bookmarks' and stored in the DB
   */
  function dataexplorer_presets_ui() {
    var preset_ids = dz.getAllIDs();

    $('#dataexplorerPresets').html('');

    if (preset_ids.length <= 0) {
      return;
    }

    // Add 'new' button
    var div = $('<div/>')
      .addClass('list-group-item')
      .appendTo('#dataexplorerPresets');

    var a = $('<a/>')
      .html('<i class="fa fa-plus-circle"></i>')
      .attr('title', 'Nouveau')
      .attr('href', '#')
      .addClass('dataexplorer-preset-new')
      .appendTo(div);

    // Add user presets
    $.each(preset_ids, function(index, value) {
      var desc = dz.getDescriptionForID(value);

      if (! desc || typeof desc == 'undefined') {
        return;
      }

      var div = $('<div/>')
        .addClass('list-group-item')
        .appendTo('#dataexplorerPresets');

      var a = $('<a/>')
        .html('<i class="fa fa-trash"></i>')
        .attr('title', 'Supprimer')
        .data('dataexplorer-preset-id', value)
        .attr('href', '#')
        .addClass('dataexplorer-preset-delete')
        .appendTo(div);

      var a = $('<a/>')
        .html(desc.html_title + ' <span>#' + value + '</span>')
        .addClass('dataexplorer-preset-title')
        .data('dataexplorer-preset-id', value)
        .attr('href', '#')
        .appendTo(div);

      // a.get(0).innerHTML += desc.html_description;
    });

    // FIXME: put this somewhere else?
    // TODO: do not allow to delete the current graph?
    $('#dataexplorerPresets a.dataexplorer-preset-delete').click(function(e) {
      var known_ids = dz.getAllIDs();
      var id = $(this).data('dataexplorer-preset-id');

      dz.reset(id);
      dz.deleteID(id);

      // Remove the menu/preset item.
      $(this).parent().fadeOut(500, function() {
        $(this).remove();
      });

      e.preventDefault();
      return false;
    });

    // FIXME This is dormant, bookmarks need a rewrite
/*
    $('#dataexplorerPresets a.dataexplorer-preset-title').click(function(e) {
      var id = $(this).data('dataexplorer-preset-id');

      dz.id = id;
      window.location.hash = id;

      dataexplorer_reset_ui_prefs();
      CRM.dataexplorer_load_preferences();
      CRM.dataexplorer_refresh(); // FIXME dz and scope arg

      e.preventDefault();
      return false;
    });

    $('#dataexplorerPresets').hide();
*/
  };

  /**
   * Helper funtion to update the badge of date filters.
   */
  function dataexplorer_update_date_filter_badge() {
    var badgehtml = '';
    var date_start = $('input[name=period_start]').val();
    var date_end = $('input[name=period_end]').val();

    if (date_start || date_end) {
      date_start = date_start || '...';
      date_end = date_end || '...';

      badgehtml = date_start + ' <br> ' + date_end;
    }

    // NB: if date start/end if empty, we still want to update the badge,
    // because we might be clearing its value.
    $('input[name=period_start]').closest('.list-group-type-dates').find('.badge').html(badgehtml);
  };

  /**
   * Refresh the graph/table/map.
   * Usually called after a measure/filter/groupby/viewmode was changed.
   */
  CRM.dataexplorer_refresh = function dataexplorer_refresh(dz, $scope) {
    // do not refresh during page load.
    if (dz.reloading) {
      return;
    }

    // do not refresh during batch operations (select all/none).
    if (dz.batching) {
      return;
    }

    // TODO cleaner way to reload?
    $('#barChartContainer' + $scope.div_suffix + ' svg').remove();
    $('#lineChartContainer' + $scope.div_suffix + ' svg').remove();
    $('#doughnutChartContainer' + $scope.div_suffix + ' svg').remove();
    $('#barChartContainer' + $scope.div_suffix + ' *').remove();
    $('#lineChartContainer' + $scope.div_suffix + ' *').remove();
    $('#doughnutChartContainer' + $scope.div_suffix + ' *').remove();
    $('#tableContainer' + $scope.div_suffix + ' *').remove();
    $('#mapContainer' + $scope.div_suffix + ' #map').remove();

    $('#barChartContainerWrapper' + $scope.div_suffix).hide();
    $('#lineChartContainerWrapper' + $scope.div_suffix).hide();
    $('#doughnutChartContainerWrapper' + $scope.div_suffix).hide();
    $('#mapContainerWrapper' + $scope.div_suffix).hide();

    $('#dataexplorer-loading' + $scope.div_suffix).show();

    // Get the measures, filters, groupbys.
    var measures = CRM.dataexplorer_format_values($scope.measure_values);
    var filters = CRM.dataexplorer_format_values($scope.filter_values);
    var groupbys = CRM.dataexplorer_format_values($scope.groupby_values);

    // User must select at least one measure and one display mode
    var trigger_data_refresh = false;
    var found_display_mode = false;

    if (measures.length > 0) {
      trigger_data_refresh = true;
    }

    $.each($scope.view_mode, function(index, value) {
      if (value) {
        found_display_mode = true;
      }
    });

    if (trigger_data_refresh && found_display_mode) {
      var base_query_url = CRM.url("civicrm/dataexplorer-data.csv?reset=1&measure=" + measures + "&filter=" + filters + "&groupby=" + groupbys);
      var url = base_query_url + '&format=json';

      $.get(url, function(data) {
        var t = JSON.parse(data);

        if ($scope.view_mode.map) {
          CRM.dataexplorer_refresh_map(t, measures, filters, groupbys, dz, $scope);
        }
        if ($scope.view_mode.piechart) {
          CRM.dataexplorer_refresh_diagram(t, measures, filters, groupbys, dz, $scope, 'doughnut');
        }
        if ($scope.view_mode.barchart) {
          CRM.dataexplorer_refresh_diagram(t, measures, filters, groupbys, dz, $scope, 'bar');
        }
        if ($scope.view_mode.linechart) {
          CRM.dataexplorer_refresh_diagram(t, measures, filters, groupbys, dz, $scope, 'line');
        }
        if ($scope.view_mode.table) {
          CRM.dataexplorerDisplayTable(t);
        }

        $('#dataexplorer-loading' + $scope.div_suffix).hide();
      });
    }

    // Page title and description
/*
    var desc = dz.getDescription();

    if (desc) {
      $('#dataexplorer-description > .panel-heading > h3').html(desc.html_title);
      $('#dataexplorer-description > .panel-body').html(desc.html_description);
      $('#dataexplorer-description').show();
    }
    else {
      $('#dataexplorer-description').hide();
    }
*/

    if (found_display_mode && trigger_data_refresh) {
      $('#dataexplorer-intro-help').fadeOut();
    }
    else {
      $('#dataexplorer-intro-help').fadeIn();
      $('#dataexplorer-loading').hide();
    }

    // FIXME: presets should be renamed to 'bookmarks' and stored in the DB
    // dataexplorer_presets_ui();
  };

  /**
   * Generate a ChartJS graph.
   */
  CRM.dataexplorer_refresh_diagram = function dataexplorer_refresh_diagram(data, measures, filters, groupbys, dz, $scope, chart_type) {
    $('#' + chart_type + 'ChartContainerWrapper' + $scope.div_suffix).show();
    var ctx = $('#' + chart_type + 'ChartContainer' + $scope.div_suffix);

    var x_label = data.headers[0].axis_x.label;
    var x;

    var labels = [];
    var datasets = [];
    var dataset_key = 0;

    var y_axes = [];
    var map_series_to_axes = {};

    // Add the labels from the main x axis
    $.each(data.data, function(i, v) {
      labels.push(v.id);
    });

    $.each(data.headers, function(i, v) {
      $.each(v.axis_y, function(ii, vv) {
        var y_label = vv.label;
        var y_id = vv.id;

        if (typeof map_series_to_axes[vv.series] == 'undefined') {
          map_series_to_axes[vv.series] = y_axes.length;

          y_axes.push({
            type: 'linear',
            display: true,
            position: (map_series_to_axes[vv.series] == 0 ? 'left' : 'right'),
            id: 'y-axis-' + map_series_to_axes[vv.series],
          });
        }

        if (typeof datasets[dataset_key] == 'undefined') {
          datasets[dataset_key] = {
            label: '',
            data: [],
            yAxisID: 'y-axis-' + map_series_to_axes[vv.series]
          };
        }

        datasets[dataset_key].label = y_label;

        $.each(data.data, function(iii, vvv) {
          if (typeof vvv[y_id] != 'undefined') {
            datasets[dataset_key].data.push(vvv[y_id]);
          }
          else {
            datasets[dataset_key].data.push(vvv[y_label]);
          }
        });
        dataset_key++;
      });
    });

    // Enable colours only if there is more than one axis/dataset
    if (datasets.length > 1) {
      var seq = palette('tol', dataset_key);

      for (let i = 0; i < dataset_key; i++) {
        datasets[i].backgroundColor = '#' + seq[i];
        datasets[i].borderColor = '#' + seq[i];
        datasets[i].fill = false;
      }
    }

    if (typeof $scope.charts[chart_type] == 'undefined') {
      $scope.charts[chart_type] = new Chart(ctx, {
        type: chart_type,
        responsive: true,
        options: {
          maintainAspectRatio: false,
          hover: {
            mode: 'single'
          },
          // FIXME: https://www.chartjs.org/samples/latest/charts/bar/multi-axis.html
          tooltips: {
            mode: 'index',
            intersect: true
          },
          scales: {
            yAxes: y_axes
          }
        },
        data: {
          labels: labels,
          datasets: datasets
        }
      });
    }
    else {
      $scope.charts[chart_type].data.labels = labels;
      $scope.charts[chart_type].data.datasets = datasets;
      $scope.charts[chart_type].update();
    }
  };

  /**
   * Map the data.
   * Source: http://leafletjs.com/examples/choropleth.html
   *
   * FIXME: This needs a lot of fixing.
   */
  CRM.dataexplorer_refresh_map = function dataexplorer_refresh_map(data, measures, filters, groupbys, dz, $scope) {
    $('#mapContainer' + $scope.div_suffix).append('<div id="map"></div>');

    accounting.settings = {
      currency: {
        symbol: '$',
        thousand: ' ',
        decimal: ',',
        precision: '0',
        format: {
          pos: '%v %s',
          neg: '-%v %s',
          zero: '%v %s'
        }
      },
      number: {
        precision: 0,
        thousand: ' ',
        decimal: ','
      }
    }

// @todo cleanup
//    d3.csv(base_query_url + "&format=csv", function (data) {
//      // Get the chart headers/labels
//      jQuery.ajax({
//        dataType: "json",
//        // FIXME: get=config has been removed
//        url: base_query_url + '&get=config&format=json',
//        success: function(config) {

          // Calculate totals of the primary measure.
          var totals = {};
          var max_series = {};
          var primary_measure = data.headers[0].axis_y[0].label; // FIXME: this takes the first yy value. Make configurable? 2020: should it be axis_y[1] ?
          var primary_type = data.headers[0].axis_y[0].type; // FIXME
          var max_value = 1;

          // If we are grouping on something else than diocese/province/admregion,
          // we need to calculate the totals.
          // Although.. do we really? it's kind of the user's fault...

          // we probably should just use 'max_value'
          max_series[primary_measure] = 1;

          // For each series totals, calculate the max value (for % of colors/opacity).
          // Uhm.. we only need to calculate this for the primary_measure.
          data.data.forEach(function(data_element, data_index) {
            var m = parseFloat(data_element[primary_measure]);
            if (m > max_series[primary_measure]) {
              max_series[primary_measure] = m;
              max_value = m;
            }
          });

          // Force the map to stay within these coordinates.
          var southWest = L.latLng(42, -145),
              northEast = L.latLng(68, -40),
              bounds = L.latLngBounds(southWest, northEast);

          var map = L.map('map', {
            // @todo This was restricting to Canada, make configurable?
            // maxBounds: bounds,
            attributionControl: false
          }).setView([45.5, -10.0], 2); // @todo centers on the Atlantic? configurable?

/*
          L.tileLayer('https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png', {
            maxZoom: 7,
            minZoom: 3,
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
              '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
              'Imagery © <a href="http://mapbox.com">Mapbox</a>',
            id: 'examples.map-20v6611k'
          }).addTo(map);
*/

          // Manually placing the attributions on the left (default is on the right, but Ontario is in the way).
          L.control.attribution({
            position: 'bottomleft'
          }).addTo(map);

          // control that shows state info on hover
          var info = L.control();

          info.onAdd = function (map) {
            this._div = L.DomUtil.create('div', 'info');
            this.update();
            return this._div;
          };

          info.update = function (props) {
            this._div.innerHTML = '';

            if (props) {
              this._div.innerHTML += '<div><strong>' + props.name + '</strong></div>';
              this._div.innerHTML += '<div>' + primary_measure + ' : ' + Math.round(dataexplorerFindObjectValueInArray(data.data, 'id', props.name, primary_measure)) + '</div>';
            }
            else {
              this._div.innerHTML += '<span>' + 'Highlight a region' + '</span>'; /* @todo ts */
            }
          };

          info.addTo(map);

          function getColor(v) {
            return v > 90 ? '#800026' :
                   v > 80  ? '#BD0026' :
                   v > 70  ? '#E31A1C' :
                   v > 60  ? '#FC4E2A' :
                   v > 40   ? '#FD8D3C' :
                   v > 30   ? '#FEB24C' :
                   v > 20   ? '#FED976' :
                              '#FFEDA0';
          }

          function style(feature) {
            var v = 0;
            var t = 0;

            // if (Math.round(dataexplorerFindObjectValueInArray(data.data, /* data.headers[0].axis_x.label */ 'id', feature.properties.name, primary_measure))) {
            //   v = Math.round(dataexplorerFindObjectValueInArray(data.data, /* data.headers[0].axis_x.label */ 'id', feature.properties.name, primary_measure) / max_series[primary_measure] * 100);
            // }

            if (t = Math.round(dataexplorerFindObjectValueInArray(data.data, 'id', feature.properties.name, primary_measure))) {
              v = Math.round(t / max_series[primary_measure] * 100);
            }

            var s = {
              weight: 2,
              opacity: 1,
              color: 'white',
              dashArray: '3',
              fillOpacity: 0.8, // (v / 100) - 0.2,
              fillColor: 'rgb(10%, ' + v + '%' + ', 10%)'
            };

            if (v == 0) {
              s.fillColor = 'rgb(220, 220, 220)';
              s.fillOpacity = '1.0';
            }
            else {
              s.fillColor = getColor(v);
            }

            return s;
          }

          /**
           * Called when the mouse hovers an area.
           */
          function highlightFeature(e) {
            var layer = e.target;

            layer.setStyle({
              weight: 5,
              color: '#666',
              dashArray: '',
              fillOpacity: 0.7
            });

            if (!L.Browser.ie && !L.Browser.opera) {
              layer.bringToFront();
            }

            info.update(layer.feature.properties);
          }

          var geojson;

          function resetHighlight(e) {
            geojson.resetStyle(e.target);
            info.update();
          }

          function zoomToFeature(e) {
            map.fitBounds(e.target.getBounds());
          }

          // This is probably a bit sketchy, data-countries.js is loaded globally
          var layerData = dataexplorerCountriesData;

          // FIXME  I guess we could load various data overlays, ex: a specific country?
          // should we check $scope.groupby_values instead?
          // if (groupbys.indexOf('region-district') != -1) {
          //  layerData = montrealDistricts;
          // }

          geojson = L.geoJson(layerData, {
            style: style,
            onEachFeature: function(feature, layer) {
              layer.on({
                mouseover: highlightFeature,
                mouseout: resetHighlight,
              });

              var v = dataexplorerFindObjectValueInArray(data.data, 'id', feature.properties.name, primary_measure);

              var unit = '';
              if (primary_type == 'money') {
                unit = '';
                v = accounting.formatMoney(v);
              }

              popupOptions = {maxWidth: 600};

              // @todo Does not display anything.
              layer.bindPopup("<div id='dataexplorer-dataviz-popup'><i class='fa fa-cog fa-spin'></i></div>");
              // layer.bindPopup("<p><strong>" + feature.properties.name + "</strong></p>"
              //  + "<p><strong>" + primary_measure + ": </strong>" + v + unit + "</p>"
              //  + "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rhoncus imperdiet ipsum sagittis faucibus. Morbi ac urna volutpat, pulvinar metus ac, iaculis neque.",
              //  popupOptions
              // );
            }
          }).addTo(map);

          // Map Legend
          var legend = L.control({position: 'bottomright'});

          legend.onAdd = function (map) {
              var div = L.DomUtil.create('div', 'info map-legend'),
                  grades = [0, 20, 30, 40, 50, 60, 70, 80, 90],
                  labels = [];

              // loop through our density intervals and generate a label with a colored square for each interval
              for (var i = 0; i < grades.length; i++) {
                div.innerHTML += '<i style="background:' + getColor(grades[i] + 1) + '"></i> ';

                if (primary_type == 'money') {
                  div.innerHTML += accounting.formatMoney(max_series[primary_measure] * grades[i] / 100)
                    + (grades[i + 1] ? '&ndash;' + accounting.formatMoney(max_series[primary_measure] * grades[i + 1] / 100) + '<br>' : '+');
                }
                else {
                  div.innerHTML += (max_series[primary_measure] * grades[i] / 100)
                    + (grades[i + 1] ? '&ndash;' + (max_series[primary_measure] * grades[i + 1] / 100) + '<br>' : '+');
                }
              }

              return div;
          };

          legend.addTo(map);

          // @todo Force a refresh, otherwise map does not display?
          map._onResize();

          // map.attributionControl.addAttribution('data &copy; <a href="http://example.org/">To be determined</a>');
//        }
//      });
//    });

    $('#mapContainerWrapper' + $scope.div_suffix).show();
    $('#dataexplorer-loading' + $scope.div_suffix).hide();
  };

  CRM.dataexplorer = CRM.dataexplorer || {};

  // FIXME Bookmarks and URL params need a rework
  // CRM.dataexplorer_load_preferences();
  // CRM.dataexplorer_refresh();

  // Presets popup
  $('#dataexplorer-presets-button').click(function(e) {
    $('#dataexplorerPresets').toggle();
  });

})(typeof angular !== 'undefined' ? angular : null, CRM.$, CRM._);
