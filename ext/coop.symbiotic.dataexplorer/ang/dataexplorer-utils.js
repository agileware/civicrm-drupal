/**
 * @file
 * Helper functions to manage UI settings.
 *
 * The code is pre-AngularJS, so there are a lot of redundancies
 * that need to be cleaned up.
 */

function dzSettings(id, data) {
  this.id = id;
  this.state = {};
  this.title = 'Data visualisation';

  // NB: keep in singular, must match the prefixes on menu items.
  this.settings = {
    vizmode : {},
    measure : {},
    filter: {},
    groupby: {}
  };

  // Reloading the whole page
  // We disable part of the work in 'click' events, because we load the prefs
  // from localStorage, and we want to avoid re-writing again, and reloading graphs
  // until we are done.
  this.reloading = false;

  // Doing a batch operation, such as "select all" or "select none",
  // so we want to disable the reloading of the graphe, but we still want
  // the localStorage writes to happen.
  this.batching = false;
}

(function ($, _) {
  "use strict";

  dzSettings.prototype.get = function(name) {
    return this.state[name];
  };

  dzSettings.prototype.set = function(name, value) {
    // Extract if : measure, filter, groupby.
    var parts = name.split('-');
    var config_type = parts[0];
    var realname = name.substr(config_type.length + 1);

    this.settings[config_type][realname] = value;
    this.state[name] = value;
  };

  dzSettings.prototype.remove = function(name) {
    // Extract if : measure, filter, groupby.
    var parts = name.split('-');
    var config_type = parts[0];
    var realname = name.substr(config_type.length + 1);

    delete this.settings[config_type][realname];
    delete this.state[name];
  };

  // deprecated
  dzSettings.prototype.getMeasures = function() {
    var ret = [];
    $.each(this.settings.measure, function(index, value) {
      ret.push(index);
    });
    return ret;
  };

  // deprecated
  dzSettings.prototype.getFilters = function() {
    var ret = [];
    $.each(this.settings.filter, function(index, value) {
      // Emergencies are the same as campaigns, but split visually in the menus.
      var parts = index.split('-');
      if (parts[0] == 'emergencies') {
        index = 'campaigns-' + parts[1];
      }
      ret.push(index + ':' + value);
    });
    return ret;
  };

  /**
   * Generate an html table with the data.
   */
  CRM.dataexplorerDisplayTable = function dataexplorerDisplayTable(data) {
    var columns = [];
    var seen = [];

    // This code assumes that if two columns have the same name, then they are the
    // same thing. Ex: if selecting two types of data and they both have a a 'Year'
    // column, then we only add it once.
    _.each(data.headers, function(element, index, list) {
      if (!_.contains(seen, element.axis_x.label)) {
        var is_3d_table = (typeof element.translate != 'undefined');

        columns.push({
          title: element.axis_x.label,
          // It's a bit unpredictable how data is keyed, but the Json generator
          // uses 'id' for the field that is used for the key.
          // Ex, said key might be "Total" if not grouping, but if grouping by
          // month, then it might be 2020-12. So we hardcoded 'id' instead.
          field: 'id', // element.axis_x.label
        });
        seen.push(element.axis_x.label);
      }
 
      // We can have 1 or 2 axis_y
      _.each(element.axis_y, function(e2, i2, list2) {
        if (typeof e2.translate != 'undefined') {
          _.each(e2.translate, function(tr_label, tr_key, list3) {
            if (!_.contains(seen, tr_label)) {
              var label = tr_label;
              columns.push({
                title: label,
                field: tr_key
              });
              seen.push(label);
            }
          });
        }
        else {
          if (!_.contains(seen, e2.label)) {
            var label = e2.label;
            columns.push({
              title: label,
              field: label
            });
            seen.push(label);
          }
        }
      });
    });

    var table = new Tabulator("#tableContainer", {
      data: data.data,
      columns: columns
    });

    // This used to support Excel, which can be done with sheetjs, but
    // it does not support non-English well. Better option might be to
    // have some integration to convert the CSV to XLS with civiexcelexport.
    // For now, this works well in LibreOffice.
    $('#tableContainer').append('<div style="text-align: right;">'
      + '<div href="#" id="crm-dataexplorer-link-csv" class="btn btn-default">CSV</div></div>');

    $('#crm-dataexplorer-link-csv').click(function(event) {
      table.download('csv', 'data.csv', {bom: true});
    });
  };

})(CRM.$, CRM._);

/**
 * Given an array of objects (ex: array of dioceses/provinces),
 * returns the object that matches i[keyname] == value.
 * Ex: find the object representing the diocese of Montreal.
 *
 * Returns null otherwise.
 *
 * @deprecated
 */
function dataexplorerFindObjectValueInArray(items, keyname, value, lookup_property) {
  var found = items.filter(function(item) {
    return (item[keyname] == value);
  });

  if (found.length == 1) {
    return found[0][lookup_property];
  }

  // Object not found, value probably zero.
  return 0;
}
