<?php

use CRM_Dataexplorer_ExtensionUtil as E;

class CRM_Dataexplorer_Explore_Generator_Mailingrecipient extends CRM_Dataexplorer_Explore_Generator {
  use CRM_Dataexplorer_Explore_Generator_DateTrait;

  protected $_grouping_ignore_dates = FALSE;

  /**
   *
   */
  function config($options = []) {
    if ($this->_configDone) {
      return $this->_config;
    }

    $defaults = [
      'y_label' => E::ts('Mailings'),
      'y_series' => 'Mailings',
      'y_type' => 'number',
    ];

    $this->_options = array_merge($defaults, $options);

    // It helps to call this here as well, because some filters affect the groupby options.
    // FIXME: see if we can call it only once, i.e. remove from data(), but not very intensive, so not a big deal.
    $params = [];
    $this->whereClause($params);

    $this->_from[] = "civicrm_mailing_event_delivered";

    // We can only have 2 groupbys, otherwise would be too complicated
    switch (count($this->_groupBy)) {
      case 0:
        $this->_config['axis_x'] = [
          'label' => 'Total',
          'type' => 'number',
        ];
        $this->_config['axis_y'][] = [
          'label' => $this->_options['y_label'],
          'type' => $this->_options['y_type'],
          'series' => $this->_options['y_series'],
          'id' => 15, // FIXME c.f. CSV
        ];

        $this->_select[] = '"Total" as x';
        break;

      case 1:
      case 2:
        // Find all the labels for this type of group by
        if (in_array('period-year', $this->_groupBy)) {
          $this->configGroupByPeriodYear('time_stamp');
        }
        if (in_array('period-month', $this->_groupBy)) {
          $this->configGroupByPeriodMonth('time_stamp');
        }
        if (in_array('period-day', $this->_groupBy)) {
          $this->configGroupByPeriodDay('time_stamp');
        }
        if (in_array('period-hour', $this->_groupBy)) {
          $this->configGroupByPeriodHour('time_stamp');
        }
        if (in_array('region-arrondissement', $this->_groupBy)) {
          $this->configGroupByRegionArrondissement();
        }
        if (in_array('region-district', $this->_groupBy)) {
          $this->configGroupByRegionDistrict();
        }
        if (in_array('other-campaign', $this->_groupBy)) {
          $this->configGroupByOtherCampaign();
        }
        if (in_array('other-gender', $this->_groupBy)) {
          $this->configGroupByOtherGender();
        }
        if (in_array('other-fiche', $this->_groupBy)) {
          $this->configGroupByOtherContactSubType();
        }

        break;

      default:
        CRM_Core_Error::fatal('Cannot groupby on ' . count($this->_groupBy) . ' elements. Max 2 allowed.');
    }

    // This happens if we groupby 'period' (month), but nothing else.
    if (empty($this->_config['axis_y'])) {
      $this->_config['axis_y'][] = array(
        'label' => $this->_options['y_label'],
        'type' => $this->_options['y_type'],
        'series' => $this->_options['y_series'],
        'id' => 15, // FIXME CSV
      );
    }

    $this->_configDone = TRUE;
    return $this->_config;
  }

  /**
   *
   */
  function data() {
    $data = [];
    $params = [];

    // This makes it easier to check specific exceptions later on.
    $this->config();
 
    // Generate an array with the number of periods that we want.
    $periods = [];

    if (in_array('period-year', $this->_groupBy)) {
      $periods = $this->getListOfYearsFromFilter('period');
    }
    if (in_array('period-month', $this->_groupBy)) {
      $this->queryAlterPeriod('month', 'time_stamp');
    }
    if (in_array('period-day', $this->_groupBy)) {
      $this->queryAlterPeriod('day', 'time_stamp');
    }
    if (in_array('period-hour', $this->_groupBy)) {
      $this->queryAlterPeriod('hour', 'time_stamp');
    }

    if ($periods) {
      $this->_grouping_ignore_dates = TRUE;
      $where = $this->whereClause($params);

      foreach ($periods as $p) {
        // date_field >= (filter date start)
        // date_field <= (filter date end)
        $where_tmp = $where . ' AND time_stamp <= ' . $p['end'] . ' AND time_stamp >= ' . $p['start'];

        // The 'x' passed is used to ensure the correct X-avis label, ex: 2015, not 2015-01-01.
        $this->runQuery($where_tmp, $params, $data, $p['x']);
      }
    }
    else {
      $where = $this->whereClause($params);
      $this->runQuery($where, $params, $data, NULL);
    }

    return $data;
  }

  function whereClause(&$params) {
    $where_clauses = [];
    $where_extra = '';

    $this->whereClauseCommon($params);

    foreach ($this->_filters as $filter) {
      // foo[0] will have 'period-start' and foo[1] will have 2014-09-01
      $foo = explode(':', $filter);

      // bar[0] will have 'period' and bar[1] will have 'start'
      // bar[0] will have 'diocese' and bar[1] will have '1'
      $bar = explode('-', $foo[0]);

      if ($bar[0] == 'period' && ! $this->_grouping_ignore_dates) {
        // Transform to MySQL date: remove the dashes in the date (2014-09-01 -> 20140901).
        if (in_array('memberships-new:1', $this->_filters)) {
          // Only new members
          $foo[1] = str_replace('-', '', $foo[1]);

          if ($bar[1] == 'start' && ! empty($foo[1])) {
            $params[1] = array($foo[1], 'Timestamp');
            $where_clauses[] = 'time_stamp >= %1';
          }
          elseif ($bar[1] == 'end' && ! empty($foo[1])) {
            $params[2] = array($foo[1] . '235959', 'Timestamp');
            $where_clauses[] = 'time_stamp <= %2';
          }
        }
        elseif (in_array('memberships-new:2', $this->_filters)) {
          // Exclude new members
          $foo[1] = str_replace('-', '', $foo[1]);

          if ($bar[1] == 'start' && ! empty($foo[1])) {
            $params[1] = array($foo[1], 'Timestamp');
            $where_clauses[] = 'time_stamp >= %1';
          }
          elseif ($bar[1] == 'end' && ! empty($foo[1])) {
            $params[2] = array($foo[1] . '235959', 'Timestamp');
            $where_clauses[] = 'time_stamp <= %2 AND time_stamp <= %1';
          }
        }
        else {
          $foo[1] = str_replace('-', '', $foo[1]);

          if ($bar[1] == 'start' && ! empty($foo[1])) {
            $params[1] = array($foo[1], 'Timestamp');
            $where_clauses[] = 'time_stamp >= %1';
          }
          elseif ($bar[1] == 'end' && ! empty($foo[1])) {
            $params[2] = array($foo[1] . '235959', 'Timestamp');
            $where_clauses[] = 'time_stamp <= %2';
          }
        }
      }
      elseif ($bar[0] == 'relative_date' && !$this->_grouping_ignore_dates) {
        // @todo Not tested
        $dates = CRM_Utils_Date::getFromTo($bar[1], NULL, NULL);

        $params[1] = array($dates[0], 'Timestamp');
        $where_clauses[] = 'time_stamp >= %1';

        $params[2] = array($dates[1], 'Timestamp');
        $where_clauses[] = 'time_stamp <= %2';
      }
    }

    if (! empty($this->_config['filters']['campaigns'])) {
      $where_clauses[] = 'campaign IN (' . implode(',', $this->_config['filters']['campaigns']) . ')';
    }

    if (! empty($this->_config['filters']['contact_sub_type'])) {
      $where_clauses[] = 'contact_sub_type IN (' . implode(',', $this->_config['filters']['contact_sub_type']) . ')';
    }

    if (! empty($this->_config['filters']['gender'])) {
      $where_clauses[] = 'gender IN (' . implode(',', $this->_config['filters']['gender']) . ')';
    }

    $where = implode(' AND ', $where_clauses);
    $where = trim($where);

    return $where;
  }

}
