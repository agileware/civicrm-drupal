<?php

use CRM_Dataexplorer_ExtensionUtil as E;

class CRM_Dataexplorer_Explore_Generator_Contribution extends CRM_Dataexplorer_Explore_Generator {
  use CRM_Dataexplorer_Explore_Generator_DateTrait;
  use CRM_Dataexplorer_Explore_Generator_FinancialTrait;

  function config($options = []) {
    if ($this->_configDone) {
      return $this->_config;
    }

    $defaults = [
      'y_label' => E::ts('Amount $'),
      'y_series' => 'Contributions',
      'y_type' => 'money',
    ];

    $this->_options = array_merge($defaults, $options);

    // It helps to call this here as well, because some filters affect the groupby options.
    // FIXME: see if we can call it only once, i.e. remove from data(), but not very intensive, so not a big deal.
    $params = [];
    $this->whereClause($params);

    // We can only have 2 groupbys, otherwise would be too complicated
    switch (count($this->_groupBy)) {
      case 0:
        $this->addAxis([
          'label' => E::ts('Total'),
          'type' => 'number',
        ]);
        $this->addAxis([
          'label' => $this->_options['y_label'],
          'type' => $this->_options['y_type'],
          'series' => $this->_options['y_series'],
        ]);

        $this->_select[] = '"Total" as x';
        break;

      case 1:
      case 2:
        // Find all the labels for this type of group by
        if (in_array('period-year', $this->_groupBy)) {
          $this->configGroupByPeriodYear('receive_date');
        }
        if (in_array('period-month', $this->_groupBy)) {
          $this->configGroupByPeriodMonth('receive_date');
        }
        if (in_array('period-week', $this->_groupBy)) {
          $this->configGroupByPeriodWeek('receive_date');
        }
        if (in_array('period-day', $this->_groupBy)) {
          $this->configGroupByPeriodDay('receive_date');
        }
        if (in_array('period-hour', $this->_groupBy)) {
          $this->configGroupByPeriodHour('receive_date');
        }
        if (in_array('contribution-ft', $this->_groupBy)) {
          $this->configGroupByContributionsFt();
        }
        if (in_array('contribution-payment_instrument_id', $this->_groupBy)) {
          $this->configGroupByContributionsPaymentInstrumentId();
        }
        if (in_array('contribution-payment_processor_id', $this->_groupBy)) {
          $this->configGroupByContributionsPaymentProcessorId();
        }
        if (in_array('contribution-contribution_page_id', $this->_groupBy)) {
          $this->configGroupByContributionsPageId();
        }
        if (in_array('contribution-contribution_status_id', $this->_groupBy)) {
          $this->configGroupByContributionsStatusId();
        }
        if (in_array('region-district', $this->_groupBy)) {
          $this->configGroupByRegionDistrict();
        }
        if (in_array('other-campaign', $this->_groupBy)) {
          $this->configGroupByOtherCampaign();
        }
        if (in_array('other-gender', $this->_groupBy)) {
          $this->configGroupByOtherGender();
        }
        if (in_array('other-fiche', $this->_groupBy)) {
          $this->configGroupByOtherContactSubType();
        }

        break;

      default:
        throw new Exception('Cannot groupby on ' . count($this->_groupBy) . ' elements. Max 2 allowed.');
    }

    $this->_config['axis_y'][] = [
      'label' => $this->_options['y_label'],
      'type' => $this->_options['y_type'],
      'series' => $this->_options['y_series'],
      'id' => 1,
    ];

    $this->_configDone = TRUE;
    return $this->_config;
  }

  function data() {
    $data = [];
    $params = [];

    // This makes it easier to check specific exceptions later on.
    $this->config();

    // Make sure it's the first element
    array_unshift($this->_from, "civicrm_contribution as contrib
      LEFT JOIN civicrm_contact as c ON (c.id = contrib.contact_id)");

    if (in_array('period-year', $this->_groupBy)) {
      $this->queryAlterPeriod('year', 'receive_date');
    }
    if (in_array('period-month', $this->_groupBy)) {
      $this->queryAlterPeriod('month', 'receive_date');
    }
    if (in_array('period-week', $this->_groupBy)) {
      $this->queryAlterPeriod('week', 'receive_date');
    }
    if (in_array('period-day', $this->_groupBy)) {
      $this->queryAlterPeriod('day', 'receive_date');
    }
    if (in_array('period-hour', $this->_groupBy)) {
      $this->queryAlterPeriod('hour', 'receive_date');
    }
    if (in_array('contribution-ft', $this->_groupBy)) {
      $this->queryAlterContributionsFt();
    }
    if (in_array('contribution-payment_instrument_id', $this->_groupBy)) {
      $this->queryAlterContributionsPaymentInstrumentId();
    }
    if (in_array('contribution-payment_processor_id', $this->_groupBy)) {
      $this->queryAlterGroupContributionsPaymentProcessorId();
    }
    if (in_array('contribution-contribution_status_id', $this->_groupBy)) {
      $this->queryAlterGroupContributionsStatusId();
    }
    if (in_array('contribution-contribution_page_id', $this->_groupBy)) {
      $this->queryAlterGroupContributionsPageId();
    }
    if (in_array('region-district', $this->_groupBy)) {
      $this->queryAlterRegionDistrict();
    }
    if (in_array('other-campaign', $this->_groupBy)) {
      $this->queryAlterOtherCampaign();
    }
    if (in_array('other-gender', $this->_groupBy)) {
      $this->queryAlterOtherGender();
    }

    // Force to select 'where' clauses, otherwise queries will be too heavy.
    if ($where = $this->whereClause($params)) {
      $has_data = FALSE;

      $where = $this->whereClause($params);
      $this->runQuery($where, $params, $data, NULL);

      // FIXME: if we don't have any results, and we are querying two
      // types of data, the 2nd column of results (CSV) might get bumped into
      // the first column. This really isn't ideal, should fix the CSV merger.
      // @todo Also, it has not really been tested, since multi-groupby is currently broken.
      if (empty($data)) {
        $tlabel = $this->_config['axis_x']['label'];
        $data[$tlabel][$ylabel] = 0;
      }
    }

    return $data;
  }

  function whereClause(&$params) {
    $where_clauses = [];
    $where_extra = '';

    $this->whereClauseCommon($params);

    $where_clauses[] = 'contrib.total_amount > 0';

    $financial_types = [];
    $payment_processor_ids = [];
    $payment_instrument_ids = [];
    $contribution_status_ids = [];
    $contribution_page_ids = [];

    foreach ($this->_filters as $filter) {
      // foo[0] will have 'period-start' and foo[1] will have 2014-09-01
      // FIXME: rename variable to something clearer
      $foo = explode(':', $filter);

      // bar[0] will have 'period' and bar[1] will have 'start'
      // bar[0] will have 'diocese' and bar[1] will have '1'
      $bar = explode('-', $foo[0]);

      if ($bar[0] == 'period') {
        // Transform to MySQL date: remove the dashes in the date (2014-09-01 -> 20140901).
        $foo[1] = str_replace('-', '', $foo[1]);

        if ($bar[1] == 'start' && ! empty($foo[1])) {
          $params[1] = array($foo[1], 'Timestamp');
          $where_clauses[] = 'receive_date >= %1';
        }
        elseif ($bar[1] == 'end' && ! empty($foo[1])) {
          $params[2] = array($foo[1] . '235959', 'Timestamp');
          $where_clauses[] = 'receive_date <= %2';
        }
      }
      elseif ($bar[0] == 'relative_date') {
        $dates = CRM_Utils_Date::getFromTo($bar[1], NULL, NULL);

        $params[1] = array($dates[0], 'Timestamp');
        $where_clauses[] = 'receive_date >= %1';

        $params[2] = array($dates[1], 'Timestamp');
        $where_clauses[] = 'receive_date <= %2';
      }
      elseif ($bar[0] == 'contributions') {
        /* @todo Not implemented in the UI? */
        if ($bar[1] == 'monthly') {
          if ($foo[1] == 1) {
            $where_clauses[] = 'contribution_recur_id IS NOT NULL';
          }
          elseif ($foo[1] == 2) {
            $where_clauses[] = 'contribution_recur_id IS NULL';
          }
        }
      }
      elseif ($bar[0] == 'ft' && !empty($bar[1])) {
        $financial_types[] = $bar[1];
      }
      elseif ($bar[0] == 'payment_instrument_id' && !empty($bar[1])) {
        $payment_instrument_ids[] = $bar[1];
      }
      elseif ($bar[0] == 'payment_processor_id' && !empty($bar[1])) {
        $payment_processor_ids[] = $bar[1];
      }
      elseif ($bar[0] == 'contribution_status_id' && !empty($bar[1])) {
        $contribution_status_ids[] = $bar[1];
      }
      elseif ($bar[0] == 'contribution_page_id' && !empty($bar[1])) {
        $contribution_page_ids[] = $bar[1];
      }
    }

    if (!empty($financial_types)) {
      $where_clauses[] = 'financial_type_id IN (' . implode(',', $financial_types) . ')';
    }

    if (!empty($payment_instrument_ids)) {
      $where_clauses[] = 'payment_instrument_id IN (' . implode(',', $payment_instrument_ids) . ')';
    }

    if (!empty($payment_processor_ids)) {
      $where_clauses[] = 'payment_processor_id IN (' . implode(',', $payment_processor_ids) . ')';
      $this->queryAlterJoinContributionsPaymentProcessorId();
    }

    if (!empty($contribution_page_ids)) {
      $where_clauses[] = 'contrib.contribution_page_id IN (' . implode(',', $contribution_page_ids) . ')';
    }

    if (!empty($contribution_status_ids)) {
      $where_clauses[] = 'contrib.contribution_status_id IN (' . implode(',', $contribution_status_ids) . ')';
    }
    elseif (!in_array('contribution-contribution_status_id', $this->_groupBy)) {
      // If not filtering/grouping by Contribution Status ID, then default to displaying only completed transactions
      $where_clauses[] = 'contrib.contribution_status_id = 1';
    }

    /* @todo Cleanup */
    if (!empty($this->_config['filters']['provinces'])) {
      $where_clauses[] = 'province_id IN (' . implode(',', $this->_config['filters']['provinces']) . ')';
    }

    if (!empty($this->_config['filters']['gender'])) {
      $where_clauses[] = 'gender_id IN (' . implode(',', $this->_config['filters']['gender']) . ')';
    }

    $where = implode(' AND ', $where_clauses);
    $where = trim($where);

    return $where;
  }

}
