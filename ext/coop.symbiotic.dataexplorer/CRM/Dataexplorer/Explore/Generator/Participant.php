<?php

use CRM_Dataexplorer_ExtensionUtil as E;

class CRM_Dataexplorer_Explore_Generator_Participant extends CRM_Dataexplorer_Explore_Generator {
  use CRM_Dataexplorer_Explore_Generator_DateTrait;
  use CRM_Dataexplorer_Explore_Generator_FinancialTrait;

  function config($options = []) {
    if ($this->_configDone) {
      return $this->_config;
    }

    $defaults = [
      'y_label' => E::ts('Participants'),
      'y_series' => 'Participants',
      'y_type' => 'number',
    ];

    $this->_options = array_merge($defaults, $options);

    // It helps to call this here as well, because some filters affect the groupby options.
    // FIXME: see if we can call it only once, i.e. remove from data(), but not very intensive, so not a big deal.
    $params = [];
    $this->whereClause($params);

    $this->_from[] = "civicrm_participant
      LEFT JOIN civicrm_contact as c ON (c.id = civicrm_participant.contact_id) ";

    // We can only have 2 groupbys, otherwise would be too complicated
    switch (count($this->_groupBy)) {
      case 0:
        $this->_config['axis_x'] = [
          'label' => E::ts('Total'),
          'type' => 'number',
        ];
        $this->_config['axis_y'][] = [
          'label' => $this->_options['y_label'],
          'type' => $this->_options['y_type'],
          'series' => $this->_options['y_series'],
        ];

        $this->_select[] = '"Total" as x';
        break;

      case 1:
      case 2:
        // Find all the labels for this type of group by
        if (in_array('period-year', $this->_groupBy)) {
          $this->configGroupByPeriodYear('register_date');
        }
        if (in_array('period-month', $this->_groupBy)) {
          $this->configGroupByPeriodMonth('register_date');
        }
        if (in_array('period-week', $this->_groupBy)) {
          $this->configGroupByPeriodWeek('register_date');
        }
        if (in_array('period-day', $this->_groupBy)) {
          $this->configGroupByPeriodDay('register_date');
        }
        if (in_array('event-ft', $this->_groupBy)) {
          $this->configGroupByContributionsFt();
        }
        if (in_array('participant-event', $this->_groupBy)) {
          $this->configGroupByEvent();
        }
        if (in_array('other-campaign', $this->_groupBy)) {
          $this->configGroupByOtherCampaign();
        }
        if (in_array('other-gender', $this->_groupBy)) {
          $this->configGroupByOtherGender();
        }

        break;

      default:
        CRM_Core_Error::fatal('Cannot groupby on ' . count($this->_groupBy) . ' elements. Max 2 allowed.');
    }

    // This happens if we groupby 'period' (month), but nothing else.
    if (empty($this->_config['axis_y'])) {
      $this->_config['axis_y'][] = array(
        'label' => $this->_options['y_label'],
        'type' => $this->_options['y_type'],
        'series' => $this->_options['y_series'],
      );
    }

    $this->_configDone = TRUE;
    return $this->_config;
  }

  function data() {
    $data = [];
    $params = [];

    // This makes it easier to check specific exceptions later on.
    $this->config();

    // Generate an array with the number of periods that we want.
    $periods = [];

    if (in_array('period-year', $this->_groupBy)) {
      $this->queryAlterPeriod('year', 'register_date');
    }
    if (in_array('period-month', $this->_groupBy)) {
      $this->queryAlterPeriod('month', 'register_date');
    }
    if (in_array('period-week', $this->_groupBy)) {
      $this->queryAlterPeriod('week', 'register_date');
    }
    if (in_array('period-day', $this->_groupBy)) {
      $this->queryAlterPeriod('day', 'register_date');
    }
    if (in_array('participant-ft', $this->_groupBy)) {
      $this->queryAlterFt();
    }
    if (in_array('participant-event', $this->_groupBy)) {
      $this->queryAlterEvent();
    }
    if (in_array('other-campaign', $this->_groupBy)) {
      $this->queryAlterOtherCampaign();
    }
    if (in_array('other-gender', $this->_groupBy)) {
      $this->queryAlterOtherGender();
    }

    if ($periods) {
      $this->_grouping_ignore_dates = TRUE;
      $where = $this->whereClause($params);

      foreach ($periods as $p) {
        // date_field >= (filter date start)
        // date_field <= (filter date end)
        $where_tmp = $where . ' AND date <= ' . $p['end'] . ' AND date >= ' . $p['start'];

        // The 'x' passed is used to ensure the correct X-avis label, ex: 2015, not 2015-01-01.
        $this->runQuery($where_tmp, $params, $data, $p['x']);
      }
    }
    else {
      $where = $this->whereClause($params);
      $this->runQuery($where, $params, $data, NULL);
    }

    return $data;
  }

  function whereClause(&$params) {
    $where_clauses = [];
    $where_extra = '';

    $this->whereClauseCommon($params);

    // completed transactions
    $where_clauses[] = 'status_id = 1';
    $where_clauses[] = 'is_test = 0';

    $financial_types = [];

    foreach ($this->_filters as $filter) {
      // foo[0] will have 'period-start' and foo[1] will have 2014-09-01
      // FIXME: rename variable to something clearer
      $foo = explode(':', $filter);

      // bar[0] will have 'period' and bar[1] will have 'start'
      // bar[0] will have 'something' and bar[1] will have '1'
      $bar = explode('-', $foo[0]);

      if ($bar[0] == 'period') {
        // Transform to MySQL date: remove the dashes in the date (2014-09-01 -> 20140901).
        $foo[1] = str_replace('-', '', $foo[1]);

        // Remove timzone bit, workaround because somehow the timezone is being saved in db
        $test = explode('T', $foo[1]);

        if (count($test) == 2) {
          $foo[1] = $test[0];
        }

        if ($bar[1] == 'start' && ! empty($foo[1])) {
          $params[1] = array($foo[1], 'Timestamp');
          $where_clauses[] = 'register_date >= %1';
        }
        elseif ($bar[1] == 'end' && ! empty($foo[1])) {
          $params[2] = array($foo[1] . '235959', 'Timestamp');
          $where_clauses[] = 'register_date <= %2';
        }
      }
      elseif ($bar[0] == 'relative_date') {
        $dates = CRM_Utils_Date::getFromTo($bar[1], NULL, NULL);

        $params[1] = array($dates[0], 'Timestamp');
        $where_clauses[] = 'register_date >= %1';

        $params[2] = array($dates[1], 'Timestamp');
        $where_clauses[] = 'register_date <= %2';
      }
      elseif ($bar[0] == 'contributions') {
        /* @todo Not implemented? */
        if ($bar[1] == 'monthly') {
          if ($foo[1] == 1) {
            $where_clauses[] = 'contribution_recur_id IS NOT NULL';
          }
          elseif ($foo[1] == 2) {
            $where_clauses[] = 'contribution_recur_id IS NULL';
          }
        }
      }
      elseif ($bar[0] == 'ft' && !empty($bar[1])) {
        $financial_types[] = $bar[1];
      }
    }

    if (!empty($financial_types)) {
      $where_clauses[] = 'financial_type_id IN (' . implode(',', $financial_types) . ')';
    }

    if (!empty($this->_config['filters']['provinces'])) {
      $where_clauses[] = 'province_id IN (' . implode(',', $this->_config['filters']['provinces']) . ')';
    }

    if (!empty($this->_config['filters']['gender'])) {
      $where_clauses[] = 'gender_id IN (' . implode(',', $this->_config['filters']['gender']) . ')';
    }

    $where = implode(' AND ', $where_clauses);
    $where = trim($where);

    return $where;
  }

  function configGroupByEvent() {
    $this->_config['axis_x'] = [
      'label' => E::ts('Event'),
      'type' => $this->_options['y_type'],
      'series' => $this->_options['y_series'],
      'id' => 'event',
    ];

    // FIXME: This should filter with the date range, if specified?
    $events = civicrm_api3('Event', 'get', [
      'return' => 'title',
      'options' => ['limit' => 0],
    ])['values'];

    foreach ($events as $key => $val) {
      $this->_config['x_translate'][$key] = $val['title'];
    }

    $this->_select[] = "civicrm_participant.event_id as x";
  }

  function queryAlterFt() {
    $this->_from[] = 'LEFT JOIN civicrm_participant_payment cpp ON (cpp.participant_id = civicrm_participant.id)';
    $this->_from[] = 'LEFT JOIN civicrm_contribution contrib ON (contrib.id = cpp.contribution_id)';
    $this->_group[] = "financial_type_id";
  }

  function queryAlterEvent() {
    $this->_from[] = 'LEFT JOIN civicrm_event e ON (e.id = civicrm_participant.event_id)';
    $this->_group[] = "e.id";
  }

}
