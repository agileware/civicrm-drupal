<?php

use CRM_Dataexplorer_ExtensionUtil as E;

class CRM_Dataexplorer_Explore_Generator_Activity_Count extends CRM_Dataexplorer_Explore_Generator_Activity {

  /**
   *
   */
  function config($options = []) {
    $options['y_label'] = E::ts('Activities');
    $options['y_series'] = 'Activities';
    $options['y_type'] = 'number';

    $this->_select[] = "count(*) as y";

    return parent::config($options);
  }

}
