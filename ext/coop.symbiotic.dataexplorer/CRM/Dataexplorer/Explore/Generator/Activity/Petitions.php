<?php

class CRM_Dataexplorer_Explore_Generator_Activity_Petition extends CRM_Dataexplorer_Explore_Generator_Activity {
  function __construct() {
    parent::__construct();
  }

  function config($options = array()) {
    $defaults = array(
      'y_label' => 'Petitions',
      'y_series' => 'Activities',
      'y_type' => 'number',
    );

    return parent::config($defaults);
  }

  function data() {
    $this->_select[] = "count(*) as y";

    return parent::data();
  }

  function whereClause(&$params) {
    $where = parent::whereClause($params);

    if (! empty($where)) {
      $where .= ' AND ';
    }

    $where .= ' number_of_petition_signatures_66 > 0';
    return $where;
  }
}
