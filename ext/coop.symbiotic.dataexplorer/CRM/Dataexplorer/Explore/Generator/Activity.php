<?php

use CRM_Dataexplorer_ExtensionUtil as E;

class CRM_Dataexplorer_Explore_Generator_Activity extends CRM_Dataexplorer_Explore_Generator {
  use CRM_Dataexplorer_Explore_Generator_DateTrait;
  use CRM_Dataexplorer_Explore_Generator_CustomFieldTrait;

  function config($options = []) {
    if ($this->_configDone) {
      return $this->_config;
    }

    $defaults = [
      'y_label' => E::ts('Activities'),
      'y_series' => 'Activities',
      'y_type' => 'number',
    ];

    $this->_options = array_merge($defaults, $options);

    $this->_from[] = "civicrm_activity as a";

    // It helps to call this here as well, because some filters affect the groupby options.
    // FIXME: see if we can call it only once, i.e. remove from data(), but not very intensive, so not a big deal.
    $params = [];
    $this->whereClause($params);

    // We can only have 2 groupbys, otherwise would be too complicated
    switch (count($this->_groupBy)) {
      case 0:
        $this->_config['axis_x'] = [
          'label' => E::ts('Total'),
          'type' => 'number',
        ];
        $this->_config['axis_y'][] = [
          'label' => $this->_options['y_label'],
          'type' => $this->_options['y_type'],
          'series' => $this->_options['y_series'],
        ];

        $this->_select[] = '"Total" as x';
        break;

      case 1:
      case 2:
        // Find all the labels for this type of group by
        // Check for period-month or equivalent
        foreach ($this->_groupBy as $g) {
          $parts = explode('-', $g);
          if ($parts[0] == 'period') {
            $this->configGroupByPeriod($parts[1], 'activity_date_time');
          }
        }

        // This function does its own validation
        $this->configGroupByCustomField('Activity');

        break;

      default:
        throw new Exception('Cannot groupby on ' . count($this->_groupBy) . ' elements. Max 2 allowed.');
    }

    // This happens if we groupby 'period' (month), but nothing else.
    if (empty($this->_config['axis_y'])) {
      $this->_config['axis_y'][] = array(
        'label' => $this->_options['y_label'],
        'type' => $this->_options['y_type'],
        'series' => $this->_options['y_series'],
      );
    }

    $this->_configDone = TRUE;
    return $this->_config;
  }

  function data() {
    $data = [];
    $params = [];

    // This makes it easier to check specific exceptions later on.
    $this->config();

    // Generate an array with the number of periods that we want.
    $periods = [];

    if (in_array('period-year', $this->_groupBy)) {
      $periods = $this->getListOfYearsFromFilter('period');
    }
    if (in_array('period-month', $this->_groupBy)) {
      $this->queryAlterPeriod('month', 'activity_date_time');
    }
    if (in_array('period-week', $this->_groupBy)) {
      $this->queryAlterPeriod('week', 'activity_date_time');
    }
    if (in_array('period-day', $this->_groupBy)) {
      $this->queryAlterPeriod('day', 'activity_date_time');
    }
    if (in_array('period-hour', $this->_groupBy)) {
      $this->queryAlterPeriod('hour', 'activity_date_time');
    }

    if ($periods) {
      $this->_grouping_ignore_dates = TRUE;
      $where = $this->whereClause($params);

      foreach ($periods as $p) {
        // date_field >= (filter date start)
        // date_field <= (filter date end)
        $where_tmp = $where . ' AND date <= ' . $p['end'] . ' AND date >= ' . $p['start'];

        // The 'x' passed is used to ensure the correct X-avis label, ex: 2015, not 2015-01-01.
        $this->runQuery($where_tmp, $params, $data, $p['x']);
      }
    }
    else {
      $where = $this->whereClause($params);
      $this->runQuery($where, $params, $data, NULL);
    }

    return $data;
  }

  function whereClause(&$params) {
    $where_clauses = [];
    $where_extra = '';

    $this->whereClauseCommon($params);

    foreach ($this->_filters as $filter) {
      // foo[0] will have 'period-start' and foo[1] will have 2014-09-01
      // FIXME: rename variable to something clearer
      $foo = explode(':', $filter);

      // bar[0] will have 'period' and bar[1] will have 'start'
      // bar[0] will have 'group_id' and bar[1] will have '1'
      $bar = explode('-', $foo[0]);

      if ($bar[0] == 'period') {
        // Transform to MySQL date: remove the dashes in the date (2014-09-01 -> 20140901).
        $foo[1] = str_replace('-', '', $foo[1]);

        // Remove timzone bit, workaround because somehow the timezone is being saved in db
        $test = explode('T', $foo[1]);

        if (count($test) == 2) {
          $foo[1] = $test[0];
        }

        if ($bar[1] == 'start' && ! empty($foo[1])) {
          $params[1] = array($foo[1], 'Timestamp');
          $where_clauses[] = 'activity_date_time >= %1';
        }
        elseif ($bar[1] == 'end' && ! empty($foo[1])) {
          $params[2] = array($foo[1] . '235959', 'Timestamp');
          $where_clauses[] = 'activity_date_time <= %2';
        }
      }
      elseif ($bar[0] == 'relative_date') {
        $dates = CRM_Utils_Date::getFromTo($bar[1], NULL, NULL);

        $params[1] = array($dates[0], 'Timestamp');
        $where_clauses[] = 'activity_date_time >= %1';

        $params[2] = array($dates[1], 'Timestamp');
        $where_clauses[] = 'activity_date_time <= %2';
      }
      elseif ($bar[0] == 'activity_type_id') {
        $params[3] = [$bar[1], 'Positive'];
        $where_clauses[] = 'activity_type_id = %3';
      }
      elseif ($bar[0] == 'activity_status_id') {
        $params[4] = [$bar[1], 'Positive'];
        $where_clauses[] = 'status_id = %4';
      }
      elseif ($bar[0] == 'activity_subject') {
        $params[4] = [$bar[2], 'String'];
        $where_clauses[] = 'subject LIKE %4';
      }
    }

    // Perhaps the params and where_clauses should be on $this
    $this->whereClauseCustomFields('Activity', $params, $where_clauses);

    $where = implode(' AND ', $where_clauses);
    $where = trim($where);

    return $where;
  }

}
