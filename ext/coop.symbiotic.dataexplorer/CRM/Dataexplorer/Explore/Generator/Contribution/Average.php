<?php

use CRM_Dataexplorer_ExtensionUtil as E;

class CRM_Dataexplorer_Explore_Generator_Contribution_Average extends CRM_Dataexplorer_Explore_Generator_Contribution {

  function config($options = []) {
    $options['y_label'] = E::ts('Average contribution $');
    $options['y_series'] = 'DonsMoy';
    $options['y_type'] = 'money';

    return parent::config($options);
  }

  function data() {
    $this->_select[] = "avg(contrib.total_amount) as y";
    return parent::data();
  }

}
