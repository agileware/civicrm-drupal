<?php

/* @todo This isn't used anymore? */

class CRM_Dataexplorer_Explore_Generator_Contribution_Monthly extends CRM_Dataexplorer_Explore_Generator {

  function config() {
    $config = [
      'axis_x' => [
        'label' => ts('Date'),
        'type' => 'date',
      ],
      'axis_y' => [],
    ];

    // We can only have 2 groupbys, otherwise would be too complicated
    switch (count($this->_groupBy)) {
      case 0:
        $config['axis_y'][] = [
          'label' => 'Recurring Contributions',
          'type' => 'money',
          'id' => 1,
        ];
        break;
      case 1:
      case 2:
        // TODO: Not implemented.
        break;
      default:
        CRM_Core_Error::fatal('Cannot groupby on ' . count($this->_groupBy) . ' elements. Max 2 allowed.');
    }

    return $config;
  }

  function data() {
    $data = array();
    $params = array();

    // Get contributions by day
    $select = "SELECT DATE_FORMAT(receive_date, '%Y-%m-01') as x, sum(contrib.total_amount) as y
                 FROM civicrm_contribution ";

    $where_clauses = array();
    $where_extra = 'AND contribution_recur_id is not NULL';
    $groupby = "GROUP BY DATE_FORMAT(receive_date, '%Y-%m-01')";

    // FIXME need to cleanup..
    // Filters are sent as, for example:
    // ?filter=period-start=2014-09-01,period-end=2014-12-31,diocese-1=1,diocese-2=1
    $filters = explode(',', $_REQUEST['filter']);

    foreach ($filters as $filter) {
      // foo[0] will have 'period-start' and foo[1] will have 2014-09-01
      $foo = explode('=', $filter);

      // bar[0] will have 'period' and bar[1] will have 'start'
      // bar[0] will have 'diocese' and bar[1] will have '1'
      $bar = explode('-', $foo[0]);

      if ($bar[0] == 'period') {
        // Transform to MySQL date: remove the dashes in the date (2014-09-01 -> 20140901).
        $foo[1] = str_replace('-', '', $foo[1]);

        if ($bar[1] == 'start') {
          $params[1] = array($foo[1], 'Timestamp');
          $where_clauses[] = 'receive_date >= %1';
        }
        elseif ($bar[1] == 'end') {
          $params[2] = array($foo[1], 'Timestamp');
          $where_clauses[] = 'receive_date <= %2';
        }
      }
      elseif ($bar[0] == 'relative_date') {
        $dates = CRM_Utils_Date::getFromTo($bar[1], NULL, NULL);

        $params[1] = array($dates[0], 'Timestamp');
        $where_clauses[] = 'receive_date >= %1';

        $params[2] = array($dates[1], 'Timestamp');
        $where_clauses[] = 'receive_date <= %2';
      }
    }

    // Force to select 'where' clauses, otherwise queries will be too heavy.
    if (empty($where_clauses) && empty($dioceses) && empty($campaigns)) {
      return $data;
    }

    $sql = $select . ' WHERE ' . implode(' AND ', $where_clauses) . ' ' . $where_extra . ' ' . $groupby;

    $dao = $this->executeQuery($sql, $params);

    while ($dao->fetch()) {
      if ($dao->x && $dao->y) {
        if (! empty($dao->yy)) {
          $data[$dao->x][$dao->yy] = $dao->y;
        }
        else {
          $data[$dao->x]['Dons mensuels $'] = $dao->y;
        }
      }
    }

    return $data;
  }
}
