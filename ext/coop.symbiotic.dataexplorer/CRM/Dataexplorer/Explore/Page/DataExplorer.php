<?php

use CRM_Dataexplorer_ExtensionUtil as E;
use Civi\DataExplorer\Event\DataExplorerEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;

class CRM_Dataexplorer_Explore_Page_DataExplorer extends CRM_Core_Page {

  public function run() {
    CRM_Utils_System::setTitle(E::ts('Data Explorer'));
    CRM_Dataexplorer_Utils::addGraphicsResources();

    $event = new DataExplorerEvent();
    \Civi::service('dispatcher')->dispatch(DataExplorerEvent::NAME, $event);

    $vars = [];
    $vars['measures'] = $event->getDataSources();
    $vars['filters'] = $event->getFilters();
    $vars['groups'] = $event->getGroupBy();

    // Copied from api4 explorer: this will help us set filters/groups dynamically
    $vars['schema'] = (array) \Civi\Api4\Entity::get()->setChain(['fields' => ['$name', 'getFields']])->execute();

    // List available existing visualisations
    $vars['visualisations'] = \Civi\Api4\Dataexplorer::get()->execute()->indexBy('id');

    Civi::resources()
      ->addVars('dataexplorer', $vars);
 
    $loader = Civi::service('angularjs.loader');
    $loader->setPageName('civicrm/dataexplorer');
    $loader->addModules(['crmApp', 'dataexplorer']);
    $loader->useApp([
      'defaultRoute' => '/dataexplorer',
    ]);
    $loader->load();
    parent::run();
  }

}
