<?php

namespace Civi\Api4;

/**
 * Dataexplorer entity.
 *
 * Based on the 'Mock' entities from api4 until we create proper DAO/BAOs.
 *
 * @package Civi\Api4
 */
class Dataexplorer extends Generic\AbstractEntity {

  /**
   * @return Generic\BasicGetFieldsAction
   */
  public static function getFields() {
    return new Generic\BasicGetFieldsAction(static::class, __FUNCTION__, function() {
      return [
        [
          'name' => 'id',
          // Api4 does not result these fields if we define the type?
          // although it's old code, should be updated with proper entities
          // 'type' => 'Integer',
        ],
        [
          'name' => 'contact_id',
          // 'type' => 'Integer',
        ],
        [
          'name' => 'label',
        ],
        [
          'name' => 'data',
        ],
        [
          'name' => 'share_key',
        ],
        [
          'name' => 'share_expire_date',
        ],
      ];
    });
  }

  /**
   * @return Generic\BasicGetAction
   */
  public static function get() {
    return new Generic\BasicGetAction('Dataexplorer', __FUNCTION__, [\CRM_Dataexplorer_BAO_Dataexplorer::CLASS, 'get']);
  }

  /**
   * @return Generic\BasicCreateAction
   */
  public static function create() {
    return new Generic\BasicCreateAction('Dataexplorer', __FUNCTION__, [\CRM_Dataexplorer_BAO_Dataexplorer::CLASS, 'create']);
  }

  /**
   * @return Generic\BasicUpdateAction
   */
  public static function update() {
    return new Generic\BasicUpdateAction('Dataexplorer', __FUNCTION__, 'id', [\CRM_Dataexplorer_BAO_Dataexplorer::CLASS, 'update']);
  }

}
