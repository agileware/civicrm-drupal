# Data Explorer

![Screenshot](https://civicrm.org/sites/civicrm.org/files/styles/large/public/dataexplorer.png)

Data Explorer aims to provide an easy to use interface that can generate custom
visualizations of CiviCRM data. The visualizations can then be added to the
CiviCRM Dashboard, shared to external people without access to CiviCRM (using a
secret key), or (eventually) as an HTML snippet that can be embedded in website
content.

Please see the "Known Issues" section below. Patches or sponsors welcome!

The extension is licensed under [AGPL-3.0](LICENSE.txt).

Credits: the code of this extension was heavily influenced by the CiviCRM Api4
Explorer by Coleman Watts, and the [reportplus](https://lab.civicrm.org/extensions/reportplus)
extension by iXiam. User interface design by Myriam Antaki. Initially sponsored
by [Development and Peace](https://www.devp.org). Maintained by [Coop Symbiotic](https://www.symbiotic.coop/en).

## Requirements

* PHP v7.2+
* CiviCRM Latest

The feature to export data to Excel depends on the [civiexcelexport](https://lab.civicrm.org/extensions/civiexportexcel/) extension.

## Installation

Install as a regular CiviCRM extension.

If installing from Git, you also have to run:

```
npm install
```

## Usage

Go to: Reports > Data Explorer.

## Hooks

Other extensions can declare their own measures, filters and groupbys.

Example hook implementation:

The [timetrack](https://lab.civicrm.org/extensions/timetrack) extension
implements these hooks.

Extensions must implement:

* Civi/DataExplorer/Events.php
* CRM/DataExplorer/Generator/[...]

(todo: needs better docs)

## Debugging

Users with the "view report sql" can see a log of SQL queries in the ajax calls.

## Known Issues

The extension is pretty basic and has a lot of legacy code from a previous
project.

* The user interface needs a lot more polishing. The "display" (groupby)
  options need work. Some options might be displayed but might be experimental.
* APIv4 would probably be a much better fit to replace the SQL in the 'Generator' classes. Todo: how could we groupby day/week/month/year?
* .. or it could be interesting to leverage reports somehow (if the user had a
  way to specify the axises, the [reportplus](https://lab.civicrm.org/extensions/reportplus)
  extension might do that already).
* .. or the dataprocessor extension?
* The code of the "generator" classes is very messy. They are slowly being cleaned up,
  hopefully one day replaced by api4 calls.

Also see the TODO section below.

## TODO

* Core: Fix saving/setting of date filters.
  * They are saving the full timestamp in the database, which then breaks dashlets (c.f. the Groupsubscription generator for a workaround).
* Core: Having multiple groupbys is mostly broken. The groupby mechanism is not checking if it's adding the first x axis (well, x/y/yy/yyy/etc). c.f. configGroupByContributionsFt
* Display: Fix basic map display
* Filters: Add support for all entity fields
* Display: Add support for all entity fields
* Display: Expose settings for graph colors
* Core: allow non-admins to access visualizations
* Core: get rid of the 'dz' object
* Dashlets: improve loading of CSS/JS files, which will be loaded multiple times if multiple dashlets
* Dashlets: show the "more info" link, if set.
* Dashlets: add support for CiviCRM permissions? (currently dashlets rely on the share_key, which does not offer much granularity)
* Core: provide a way to embed an HTML/JS snippet into another site, similar to what dashlets do. That way they could be integrated in a website as part of the content.
* Core: have the concept of exposed filters (kind of like Views)

## Support

Please post bug reports in the issue tracker of this project on CiviCRM's Gitlab:  
https://lab.civicrm.org/extensions/dataexplorer/issues

This extension was written thanks to the financial support of organisations
using it, as well as the very helpful and collaborative CiviCRM community.

While we do our best to provide free community support for this extension,
please consider financially contributing to support or development of this
extension.

Support via Coop Symbiotic:  
https://www.symbiotic.coop/en

Coop Symbiotic is a worker-owned co-operative based in Canada. We have a strong
experience working with non-profits and CiviCRM. We provide affordable, fast,
turn-key hosting with regular upgrades and proactive monitoring, as well as custom
development and training.
