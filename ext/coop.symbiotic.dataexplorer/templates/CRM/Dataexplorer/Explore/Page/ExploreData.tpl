{* @deprecated - replaced by AngularJS *}

{* Required for date inputs *}
{include file="CRM/Form/validate.tpl"}

{crmScope extensionKey= 'dataexplorer'}
<div id="crm-dataexplorer-container">
  <div id="dataexplorer-menu-nav">
    <div class="dataexplorermode-viewmode-wrapper">
      <div style="display: inline-block;">
      <a href="#" class="dataexplorer-viewmode dataexplorer-viewmode-table" data-dataexplorermode="vizmode-table" title="Table">
        <span class="fa-stack fa-2x">
          <i class="fa fa-square-o fa-stack-2x"></i>
          <i class="fa fa-table fa-stack-1x"></i>
        </span>
      </a>

      <a href="#" class="dataexplorer-viewmode dataexplorer-viewmode-diagram" data-dataexplorermode="vizmode-diagram" title="Diagramme">
        <span class="fa-stack fa-2x">
          <i class="fa fa-square-o fa-stack-2x"></i>
          <i class="fa fa-bar-chart fa-stack-1x"></i>
        </span>
      </a>
      </div>

      <div style="display: inline-block;">
      <a href="#" class="dataexplorer-viewmode dataexplorer-viewmode-pie" data-dataexplorermode="vizmode-pie" title="Tarte">
        <span class="fa-stack fa-2x">
          <i class="fa fa-square-o fa-stack-2x"></i>
          <i class="fa fa-pie-chart fa-stack-1x"></i>
        </span>
      </a>

      <a href="#" class="dataexplorer-viewmode dataexplorer-viewmode-map" data-dataexplorermode="vizmode-map" title="Carte">
        <span class="fa-stack fa-2x">
          <i class="fa fa-square-o fa-stack-2x"></i>
          <i class="fa fa-map-marker fa-stack-1x"></i>
        </span>
      </a>
      </div>
    </div>

    <div class="dataexplorer-menu-nav-header">{ts}Measure{/ts}</div>
    <div class="list-group dataexplorer-menu-first-level" id="dataexplorer-group-measure">
      {foreach from=$measures key="groupkey" item="m"}
        <div class="list-group-item" id="dataexplorer-measures-{$groupkey}"><a href="#"><span class="dataexplorer-label">{$m.label}</span><span class="badge pull-right"></span></a>
          <div class="menu-popup list-group dataexplorer-menu-second-level">
            {foreach from=$m.items key="key" item="val"}
              <a class="list-group-item" id="item-measure-{$key}" href="#" data-itemname="measure-{$key}" data-groupname="measure-{$groupkey}">{$val}</a>
            {/foreach}
          </div>
        </div>
      {/foreach}
    </div>

    <div class="dataexplorer-menu-nav-header">{ts}Filter{/ts}</div>
    <div class="list-group dataexplorer-menu-first-level" id="dataexplorer-group-filter">
      {foreach from=$filters key="groupkey" item="m"}
        <div class="list-group-item list-group-type-{$m.type}" id="dataexplorer-filters-{$groupkey}"><a href="#"><span class="dataexplorer-label">{$m.label}</span><span class="badge pull-right"></span></a>
          <div class="menu-popup list-group dataexplorer-menu-second-level{if count($m.items3)} dataexplorer-menu-second-level-items3{/if}">
            {if count($m.items)}
              <div class="list-group-item dataexplorer-menu-buttons-wrapper">
                <button class="btn btn-xs dataexplorer-button-all" type="button" href="#all">Tous</button>
                <button class="btn btn-xs dataexplorer-button-none" type="button" href="#none">Aucun</button>
              </div>
            {/if}

            {* Binary items with checkboxes *}
            {foreach from=$m.items key="key" item="val"}
              <a class="list-group-item" id="item-filter-{$groupkey}-{$key}" href="#" data-itemname="filter-{$groupkey}-{$key}">{$val}</a>
            {/foreach}

            {* Trinary items with on/off/either *}
            {foreach from=$m.items3 key="key" item="val"}
              <div class="list-group-item">
                {* NB: the 'id' does not make particular sense, but helps for the JS that picks up the labels to show the description. *}
                <div class="dataexplorer-items3-label" id="item-filter-{$key}">{$val.label}</div>
                <div class="btn-group" data-toggle="buttons" data-dataexplorericon="{$val.icon}">
                  {foreach from=$val.options item="option" key="optkey"}
                    {if $optkey eq 0}
                      {* By default, the 'all' option will be checked/active *}
                      <label id="dataexplorer-checkbox-label-filter-{$key}-{$optkey}" class="dataexplorer-checkbox-label-{$optkey} btn btn-default{if $optkey eq 0} btn-primary active{/if}"><input type="radio" name="item-{$key}" id="item-filter-{$key}-{$optkey}" value="{$optkey}" checked="checked" class="dataexplorer-checkbox-{$optkey} dataexplorer-checkbox" data-itemname="filter-{$key}" /><span class="dataexplorer-label">{$option}</span></label>
                    {else}
                      <label id="dataexplorer-checkbox-label-filter-{$key}-{$optkey}" class="dataexplorer-checkbox-label-{$optkey} btn btn-default"><input type="radio" name="item-{$key}" id="item-filter-{$key}-{$optkey}" value="{$optkey}" class="dataexplorer-checkbox-{$optkey} dataexplorer-checkbox" data-itemname="filter-{$key}" /><span class="dataexplorer-label">{$option}</span></label>
                    {/if}
                  {/foreach}
                </div>
              </div>
            {/foreach}

            {* Extra widgets, such as date fields *}
            {if count($m.widgets)}
              <div class="list-group-item dataexplorer-menu-buttons-wrapper">
                <button class="btn btn-xs dataexplorer-button-cleardates" type="button" href="#none">Effacer <i class="fa fa-times"></i></button>
              </div>
            {/if}
            {foreach from=$m.widgets key="key" item="val"}
              <div class="list-group-item">
                <label style="width: 2em; display: inline-block;">{$val.label}</label>
                {if $val.type eq 'date'}
                  <form novalidate method="get">
                    <input id="item-filter-{$groupkey}-{$key}" href="#" data-itemname="filter-{$groupkey}-{$key}" name="{$groupkey}_{$key}" id="{$groupkey}_{$key}" class="dataexplorer-dateplugin dateplugin" autocomplete="off" type="text" format="yy-mm-dd">
                  </form>
                {/if}
              </div>
            {/foreach}
          </div>
        </div>
      {/foreach}
    </div>

    <div class="dataexplorer-menu-nav-header">{ts}Display{/ts}</div>
    <div class="list-group dataexplorer-menu-first-level" id="dataexplorer-group-show">
      {foreach from=$groups key="groupkey" item="m"}
        <div class="list-group-item"><a href="#"><span class="dataexplorer-label">{$m.label}</span><span class="badge pull-right"></span></a>
          <div class="menu-popup list-group dataexplorer-menu-second-level">
            {foreach from=$m.items key="key" item="val"}
              <a class="list-group-item" id="item-groupby-{$groupkey}-{$key}" href="#" data-itemname="groupby-{$groupkey}-{$key}">{$val}</a>
            {/foreach}
          </div>
        </div>
      {/foreach}
    </div>
  </div>
  <div role="main">
    <div class="panel panel-primary" id="dataexplorer-description">
      <div class="panel-heading">
        <a id="dataexplorer-presets-button" href="#" data-toggle="tooltip" data-placement="left" title="Historique et signets"><i class="fa fa-cog fa-2x"></i></a>
        <h3 class="panel-title"></h3>
      </div>
      <div class="panel-body"></div>
    </div>

    <div class="jumbotron" id="dataexplorer-intro-help">
      <p>À partir du menu sur le côté gauche de l'écran, sélectionner des critères de données à mesurer (dons, matériels, etc), puis des filtres et (optionnellement) affiner les critères d'affichage.</p>
      <p>Vous pourrez ensuite sélectionner un affichage différent (diagramme, tarte, carte, exporter) à partir du menu situé sur le côté droit de l'écran.</p>
      <p><small><em>(texte non-final.. exemple seulement)</em></small></p>
    </div>

    <div class="jumbotron" id="dataexplorer-loading">
      <p>Chargement ...</p>
    </div>

    <div id="mapContainerWrapper">
      <div id="mapContainer"></div>
      <p style="text-align: right;">
        <a href="#" class="btn btn-default dataexplorer-button-map-to-image" data-dataexplorerexportformat="jpeg">JPG</a>
        <a href="#" class="btn btn-default dataexplorer-button-map-to-image" data-dataexplorerexportformat="png">PNG</a>
      </p>

      <canvas id="mapCanvass" width="1199" height="550" class="hide"></canvas>
      <div id="svgmapdataurl" class="hide"></div>
    </div>

    <div id="chartContainerWrapper">
      <div id="chartContainer"></div>
      <p style="text-align: right;">
        <a href="#" class="btn btn-default dataexplorer-button-canvas-to-image" data-dataexplorerexportformat="jpeg">JPG</a>
        <a href="#" class="btn btn-default dataexplorer-button-canvas-to-image" data-dataexplorerexportformat="png">PNG</a>
      </p>

      <canvas id="chartCanvass" width="1199" height="550" class="hide"></canvas>
      <div id="svgchartdataurl" class="hide"></div>
    </div>

    <div id="tableContainer"></div>

    <div id="dataexplorerPresets" class="list-group dataexplorer-menu-first-level"></div>
  </div>
</div>
{/crmScope}

<script>
  {literal}
    // Required to enable Bootstrap tooltips on the graph-mode icons.
/* FIXME
    jQuery(function ($) {
      jQuery("a.dataexplorer-viewmode").tooltip();
      jQuery("a#dataexplorer-presets-button").tooltip();
    });
*/
  {/literal}
</script>
