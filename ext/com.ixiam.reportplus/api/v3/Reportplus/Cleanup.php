<?php
use CRM_Reportplus_ExtensionUtil as E;

/**
 * Reportplus.cleanup API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 *
 * @see https://docs.civicrm.org/dev/en/latest/framework/api-architecture/
 */
function _civicrm_api3_reportplus_cleanup_spec(&$spec) {

}

/**
 * Reportplus.cleanup API
 *
 * This api cleans up all the Reportplus temp tables.
 *
 * @param array $params
 *
 * @return array
 *   API result descriptor
 *
 * @see civicrm_api3_create_success
 *
 * @throws API_Exception
 */
function civicrm_api3_reportplus_cleanup($params) {
  CRM_Reportplus_Utils_Report::clearTempTables();
  $returnValues = [];
  return civicrm_api3_create_success($returnValues, $params, 'Reportplus', 'cleanup');
}
