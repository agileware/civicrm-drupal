<?php
use CRM_Reportplus_ExtensionUtil as E;

/**
 * Collection of upgrade steps.
 */
class CRM_Reportplus_Upgrader extends CRM_Extension_Upgrader_Base {

  /**
   * Upgrade hook for release 2.5.1
   */
  public function upgrade_2501() {
    CRM_Core_Invoke::rebuildMenuAndCaches();
    return TRUE;
  }

  public function install() {
    CRM_Core_DAO::executeQuery("INSERT INTO civicrm_component (name, namespace) VALUES ('ReportPlus', 'CRM_Reportplus');");

    CRM_Core_Invoke::rebuildMenuAndCaches();
  }

}
