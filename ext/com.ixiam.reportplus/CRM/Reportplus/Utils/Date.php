<?php

/**
 * Class CRM_Reportplus_Utils_Date
 */
class CRM_Reportplus_Utils_Date {

  /**
   * Get start and end date for a given year / week number
   *
   * @param string $week
   * @param string $year
   *
   * @return array
   */
  public static function getStartAndEndDateforWeek($week, $year) {
    $dto = new DateTime();
    $dto->setISODate($year, $week);
    $ret['from'] = $dto->format('Ymd');
    $dto->modify('+6 days');
    $ret['to'] = $dto->format('Ymd');
    return $ret;
  }

  /**
   * Get start and end date for a given year / quarter number
   *
   * @param string $quarter
   * @param string $year
   *
   * @return array
   */
  public static function getStartAndEndDateforQuarter($quarter, $year) {
    $ret = ["from" => "", "to" => ""];
    switch ($quarter) {
      case '1':
        $ret = ["from" => "{$year}0101", "to" => "{$year}0331"];
        break;

      case '2':
        $ret = ["from" => "{$year}0401", "to" => "{$year}0630"];
        break;

      case '3':
        $ret = ["from" => "{$year}0701", "to" => "{$year}0930"];
        break;

      case '4':
        $ret = ["from" => "{$year}1001", "to" => "{$year}1231"];
        break;
    }

    return $ret;
  }

}
