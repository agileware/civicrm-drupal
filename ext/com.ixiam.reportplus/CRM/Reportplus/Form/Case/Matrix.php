<?php

class CRM_Reportplus_Form_Case_Matrix extends CRM_Reportplus_Form_Matrix {

  /**
   * Class constructor.
   */
  public function __construct() {
    $this->_customGroupExtends = ['Case', 'Contact', 'Individual', 'Household', 'Organization', 'Address'];
    $this->_customGroupGroupBy = TRUE;

    $this->_columns = [
      'civicrm_statistics' => [
        'dao' => 'CRM_Case_DAO_Case',
        'fields' => [
          'count' => [
            'title' => ts('Count'),
            'default' => TRUE,
            'dbAlias' => '*',
          ],
        ],
        'grouping' => 'statistics-fields',
      ],
      'civicrm_case' => [
        'group_title' => ts('Case'),
        'dao' => 'CRM_Case_DAO_Case',
        'fields' => [],
        'grouping' => 'case-fields',
        'group-title' => 'Case',
        'group_bys' => [
          'subject' => [
            'title' => ts('Case Subject'),
          ],
          'status_id' => [
            'title' => ts('Case Status'),
          ],
          'start_date' => [
            'title' => ts('Start Date'),
          ],
          'end_date' => [
            'title' => ts('End Date'),
          ],
        ],
        'filters' => [
          'subject' => [
            'title' => ts('Case Subject'),
            'type' => CRM_Utils_Type::T_STRING,
            'operatorType' => CRM_Report_Form::OP_STRING,
          ],
          'start_date' => [
            'title' => ts('Start Date'),
            'operatorType' => CRM_Report_Form::OP_DATE,
            'type' => CRM_Utils_Type::T_DATE,
          ],
          'end_date' => [
            'title' => ts('End Date'),
            'operatorType' => CRM_Report_Form::OP_DATE,
            'type' => CRM_Utils_Type::T_DATE,
          ],
          'status_id' => [
            'title' => ts('Case Status'),
            'type' => CRM_Utils_Type::T_INT,
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options' => CRM_Case_BAO_Case::buildOptions('status_id', 'search'),
          ],
          'case_type_id' => [
            'title' => ts('Case Type'),
            'type' => CRM_Utils_Type::T_INT,
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options' => CRM_Case_PseudoConstant::caseType(),
          ],
          'case_is_deleted' => [
            'title' => ts('Deleted?'),
            'type' => CRM_Utils_Type::T_BOOLEAN,
            'dbAlias' => 'case_civireport.is_deleted',
            'default' => 0,
          ],
        ],
      ],
      'civicrm_case_type' => [
        'group_title' => ts('Case'),
        'dao' => 'CRM_Case_DAO_CaseType',
        'fields' => [],
        'grouping' => 'case-fields',
        'group_bys' => [
          'title' => ['title' => ts('Case Type')],
        ],
      ],
      'civicrm_contact' => [
        'dao' => 'CRM_Contact_DAO_Contact',
        'fields' => [],
        'grouping' => 'contact-fields',
        'group-title' => ts('Contacts'),
        'group_bys' => $this->addContactGroupBysFields(),
      ],
    ] + $this->addAddressFields(TRUE, FALSE, TRUE, [], FALSE);

    parent::__construct();
  }

  public function from($entity = NULL) {
    parent::from($entity);

    $case = $this->_aliases['civicrm_case'];
    $type = $this->_aliases['civicrm_case_type'];
    $contact = $this->_aliases['civicrm_contact'];

    $extendedCaseQuery = "
          INNER JOIN civicrm_case_contact civireport_case_contact on civireport_case_contact.contact_id = {$contact}.id
          INNER JOIN civicrm_case $case ON {$case}.id = civireport_case_contact.case_id
          INNER JOIN civicrm_case_type {$type} ON {$case}.case_type_id = {$type}.id";

    $this->_from = $this->_from . $extendedCaseQuery;
  }

}
