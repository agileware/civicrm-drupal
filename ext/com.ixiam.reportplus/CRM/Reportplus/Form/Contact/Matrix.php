<?php

class CRM_Reportplus_Form_Contact_Matrix extends CRM_Reportplus_Form_Matrix {

  /**
   * Class constructor.
   */
  public function __construct() {
    $this->_customGroupExtends = ['Contact', 'Individual', 'Organization', 'Address'];
    $this->_customGroupGroupBy = TRUE;
    $this->_drilldownReport = ['contact/summary/plus' => 'Link to Detail Report'];

    $this->_columns = [
      'civicrm_statistics' => [
        'dao' => 'CRM_Contact_DAO_Contact',
        'fields' => [
          'count' => [
            'title' => ts('Count'),
            'default' => TRUE,
            'dbAlias' => '*',
          ],
        ],
        'grouping' => 'statistics-fields',
      ],
      'civicrm_contact' => [
        'dao' => 'CRM_Contact_DAO_Contact',
        'fields' => [],
        'grouping' => 'contact-fields',
        'group-title' => ts('Contacts'),
        'group_bys' => $this->addContactGroupBysFields(),
      ],
    ] + $this->addAddressFields(TRUE, FALSE, TRUE, [], FALSE);

    parent::__construct();
  }

  public function from($entity = NULL) {
    parent::from($entity);
  }

}
