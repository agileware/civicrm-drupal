<?php
use CRM_Reportplus_ExtensionUtil as E;

class CRM_Reportplus_Form_Activity_Matrix extends CRM_Reportplus_Form_Matrix {

  protected $_activityTableAlias = [
    'activity_contact_target' => 'activity_contact_target_civireport',
    'civicrm_contact_target' => 'contact_target_civireport',
  ];

  /**
   * Class constructor.
   */
  public function __construct() {
    $this->_customGroupExtends = ['Activity'];
    $this->_customGroupGroupBy = TRUE;
  
    $this->_columns = [
      'civicrm_statistics' => [
        'dao' => 'CRM_Activity_DAO_Activity',
        'fields' => [
          'count_total' => [
            'title' => E::ts('Count Activities'),
            'default' => TRUE,
            'dbAlias' => 'COUNT(DISTINCT `activity_civireport`.`id`)',
          ],
          'count_target' => [
            'title' => E::ts('Count Target Contacts'),
            'dbAlias' => "COUNT(DISTINCT `{$this->_activityTableAlias['activity_contact_target']}`.`contact_id`)",
          ],
        ],
        'grouping' => 'statistics-fields',
      ],
      'civicrm_activity' => [
        'dao' => 'CRM_Activity_DAO_Activity',
        'fields' => [],
        'filters' => [
          'activity_date_time' => [
            'operatorType' => CRM_Report_Form::OP_DATE,
          ],
          'activity_subject' => ['title' => ts('Activity Subject')],
          'activity_type_id' => [
            'title' => ts('Activity Type'),
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options' => CRM_Core_PseudoConstant::activityType(TRUE, TRUE, FALSE, 'label', TRUE),
          ],
          'status_id' => [
            'title' => ts('Activity Status'),
            'type' => CRM_Utils_Type::T_STRING,
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options' => CRM_Core_PseudoConstant::activityStatus(),
          ],
          'location' => [
            'title' => ts('Location'),
            'type' => CRM_Utils_Type::T_TEXT,
          ],
          'details' => [
            'title' => ts('Activity Details'),
            'type' => CRM_Utils_Type::T_TEXT,
          ],
          'priority_id' => [
            'title' => ts('Activity Priority'),
            'type' => CRM_Utils_Type::T_STRING,
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options' => CRM_Core_PseudoConstant::get('CRM_Activity_DAO_Activity', 'priority_id'),
          ],
          'is_test' => [
            'title' => ts('Is Test'),
            'type' => CRM_Utils_Type::T_BOOLEAN,
            'default' => 0,
          ],
        ],
        'group_bys' => [
          'activity_date_time' => [
            'title' => ts('Activity Date'),
            'frequency' => TRUE,
          ],
          'activity_type_id' => [
            'title' => ts('Activity Type'),
          ],
          'status_id' => [
            'title' => ts('Activity Status'),
          ],
          'priority_id' => [
            'title' => ts('Activity Priority'),
          ],
        ],
        'order_bys' => [
          'activity_date_time' => [
            'title' => ts('Activity Date'),
          ],
          'activity_type_id' => [
            'title' => ts('Activity Type'),
          ],
        ],
        'grouping' => 'activity-fields',
        'alias' => 'activity',
      ],
      'civicrm_activity_contact_target' => [
        'dao' => 'CRM_Contact_DAO_Contact',
        'fields' => [],
        'grouping' => 'civicrm_activity_contact_target-fields',
        'group_title' => E::ts('Target Contacts'),
        'group_bys' => [
          'contact_type' => [
            'title' => E::ts('Contact Type'),
            'dbAlias' => "`{$this->_activityTableAlias['civicrm_contact_target']}`.`contact_type`",
          ],
          'contact_sub_type' => [
            'title' => E::ts('Contact Subtype'),
            'dbAlias' => "`{$this->_activityTableAlias['civicrm_contact_target']}`.`contact_sub_type`",
          ],
          'gender_id' => [
            'title' => E::ts('Contact Gender'),
            'dbAlias' => "`{$this->_activityTableAlias['civicrm_contact_target']}`.`gender_id`",
          ],
          'age' => [
            'title' => E::ts('Contact Age'),
            'dbAlias' => "TIMESTAMPDIFF(YEAR, `{$this->_activityTableAlias['civicrm_contact_target']}`.`birth_date`, CURDATE())",
          ],
        ],
      ],
      'civicrm_contact_target' => [
        'dao' => 'CRM_Contact_DAO_Contact',
        'filters' => $this->getBasicContactFilters(),
        'grouping' => 'civicrm_contact_target-fields',
        'group_title' => E::ts('Target Contacts'),
      ],
    ];

    parent::__construct();

    // In this report default "contacts / tags / groups" settings are not useful, because we have 3 different contact tables to handle
    unset($this->_columns["civicrm_contact"]);
    unset($this->_columns["civicrm_tag"]);
    unset($this->_columns["civicrm_group"]);

    $this->addCampaignFields('civicrm_activity', TRUE, FALSE, TRUE, FALSE);
  }

  /**
   * Set select clause.
   */
  public function select() {
    $select = $this->_columnHeaders = [];
    $this->_colGroupByFreq = $this->_params['group_bys_column_freq'];
    $this->_rowGroupByFreq = $this->_params['group_bys_row_freq'];

    foreach ($this->_columns as $tableName => $table) {
      if (array_key_exists('fields', $table)) {
        foreach ($table['fields'] as $fieldName => $field) {
          if (!empty($field['required']) || !empty($this->_params['fields'][$fieldName])) {
            if ($tableName == 'civicrm_statistics') {
              $select[] = "{$field['dbAlias']} as 'value'";
            }
          }
        }
      }
      if (array_key_exists('group_bys', $table)) {
        foreach ($table['group_bys'] as $fieldName => $field) {
          $groupByFreq = NULL;
          if ($this->_params['group_bys_column'] == $fieldName) {
            $this->_colField = $field;
            $fieldType = 'coly';
          }
          elseif ($this->_params['group_bys_row'] == $fieldName) {
            $this->_rowField = $field;
            $fieldType = 'rowx';
          }
          else {
            $fieldType = NULL;
          }

          if ($fieldType) {
            if ($tableName == 'civicrm_address') {
              $this->_selectedTables[] = 'civicrm_address';
            }

            if (!empty($field['type'])) {
              if ($field['type'] & CRM_Utils_Type::T_DATE) {
                if ($fieldType == 'coly') {
                  $groupByFreq = $this->_colGroupByFreq;
                }
                if ($fieldType == 'rowx') {
                  $groupByFreq = $this->_rowGroupByFreq;
                }
              }
            }

            switch ($groupByFreq) {
              case 'YEARWEEK':
                $select[] = "DATE_FORMAT({$field['dbAlias']}, '%Y-%u') as '$fieldType'";
                break;

              case 'YEAR':
                $select[] = "YEAR({$field['dbAlias']}) as '$fieldType'";
                break;

              case 'MONTH':
                $select[] = "DATE_FORMAT({$field['dbAlias']}, '%Y-%m') as '$fieldType'";
                break;

              case 'QUARTER':
                $select[] = "CONCAT(YEAR({$field['dbAlias']}), '-', QUARTER({$field['dbAlias']}))  as '$fieldType'";
                break;

              case 'DAY':
                $select[] = "DATE_FORMAT({$field['dbAlias']}, '%Y-%m-%d') as '$fieldType'";
                break;

              default:
                $select[] = $field['dbAlias'] . " as '$fieldType'";
                break;
            }
          }
        }
      }
    }
    $this->_select = "SELECT " . implode(', ', $select) . " ";
  }

  /**
   * Generate from clause.
   *
   * @param object $entity
   */
  public function from($entity = NULL) {
    $activityContacts = CRM_Activity_BAO_ActivityContact::buildOptions('record_type_id', 'validate');
    $targetID = CRM_Utils_Array::key('Activity Targets', $activityContacts);

    $this->_from = "
        FROM civicrm_activity {$this->_aliases['civicrm_activity']}

         LEFT JOIN civicrm_activity_contact {$this->_aliases['civicrm_activity_contact_target']}
                ON {$this->_aliases['civicrm_activity']}.id = {$this->_aliases['civicrm_activity_contact_target']}.activity_id AND
                   {$this->_aliases['civicrm_activity_contact_target']}.record_type_id = {$targetID}
         LEFT JOIN civicrm_contact {$this->_aliases['civicrm_contact_target']}
                ON {$this->_aliases['civicrm_activity_contact_target']}.contact_id = {$this->_aliases['civicrm_contact_target']}.id
    ";

    $this->_from .= "{$this->_aclFrom}";
  }

}
