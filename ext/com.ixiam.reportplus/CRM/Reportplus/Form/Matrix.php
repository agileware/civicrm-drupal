<?php
use CRM_Reportplus_ExtensionUtil as E;

class CRM_Reportplus_Form_Matrix extends CRM_Reportplus_Form {
  protected $_groupByType = [];

  protected $_rowField;
  protected $_colField;
  protected $_colOptionsWeight = [];
  protected $_rowOptionsWeight = [];
  protected $_colGroupByFreq;
  protected $_rowGroupByFreq;
  protected $_rowHeaders = [];
  protected $_rowDecorators = [];

  protected $_showTotals = FALSE;
  protected $_addressField = FALSE;
  protected $_doNotCacheTables = FALSE;
  protected $_cache;

  public $_drilldownReport = [];

  /**
   * Class constructor.
   */
  public function __construct() {
    $this->_groupByDateFreq = [
      'YEAR' => ts('Year'),
      'QUARTER' => E::ts('Quarter'),
      'YEARWEEK' => ts('Week'),
      'MONTH' => ts('Month'),
      'DAY' => ts('Day'),
    ];
    $this->_groupByType = [
      '' => ' - ',
      'col' => E::ts("columns"),
      'row' => E::ts("rows"),
    ];

    // If we have a campaign, build out the relevant elements
    if ($this->_campaignEnabled && !empty($this->_activeCampaigns)) {
      $this->_columns['civicrm_contribution']['fields']['campaign_id'] = [
        'title' => 'Campaign',
        'default' => 'false',
      ];
      $this->_columns['civicrm_contribution']['filters']['campaign_id'] = [
        'title' => ts('Campaign'),
        'operatorType' => CRM_Report_Form::OP_MULTISELECT,
        'options' => $this->_activeCampaigns,
      ];
      $this->_columns['civicrm_contribution']['group_bys']['campaign_id'] = ['title' => ts('Campaign')];
    }

    $this->_tagFilter = TRUE;
    $this->_groupFilter = TRUE;
    parent::__construct();

    $this->_columns['civicrm_contact']['fields'] = [];
    $this->_columns['civicrm_contact']['filters'] = $this->getBasicContactFilters();

    $this->_options['hideEmptyHeaders'] = [
      'type' => 'checkbox',
      'title' => E::ts('Hide Empty Headers'),
    ];
    $this->_options['showTotals'] = [
      'type' => 'checkbox',
      'title' => E::ts('Show Totals'),
    ];
    $this->_options['contactSummaryEnabled'] = [
      'type' => 'checkbox',
      'title' => E::ts('Available for Contact Summary block'),
    ];
    $this->_options['doNotCacheTables'] = [
      'type' => 'checkbox',
      'title' => E::ts('Do Not Cache MySQL results table when Paging'),
    ];
    $this->_options['linkDrilldownReports'] = [
      'type' => 'checkbox',
      'title' => E::ts('Link results to drilldown reports'),
    ];

    $this->_chartJSEnabled = TRUE;

    // init cache
    $this->_cache = CRM_Utils_Cache::create([
      'type' => ['SqlGroup', 'ArrayCache'],
      'name' => 'ReportPlus',
    ]);

  }

  /**
   * Add group by options to the report.
   */
  public function addGroupBys() {
    $groupsBys = $freqElements = [];

    foreach ($this->_columns as $tableName => $table) {
      if (array_key_exists('group_bys', $table)) {
        foreach ($table['group_bys'] as $fieldName => $field) {
          if (!empty($field)) {
            $groupsBys[$fieldName] = $table['group_title'] . " :: " . $field['title'];
            $fieldType = isset($field['type']) ? intval($field['type']) : 0;
            if ($fieldType & CRM_Utils_Type::T_DATE) {
              $freqElements[] = $fieldName;
            }
          }
        }
      }
    }
    $this->addElement('select', 'group_bys_column', E::ts('Select Columns'), ['' => E::ts('-- Select --')] + $groupsBys, ['class' => 'crm-select2 huge']);
    $this->addElement('select', 'group_bys_row', E::ts('Select Rows'), ['' => E::ts('-- Select --')] + $groupsBys, ['class' => 'crm-select2 huge']);
    $this->addElement('select', "group_bys_column_freq", ts('Frequency'), $this->_groupByDateFreq);
    $this->addElement('select', "group_bys_row_freq", ts('Frequency'), $this->_groupByDateFreq);

    $this->assign('freqElements', $freqElements);

    if (!empty($groupsBys)) {
      $this->tabs['GroupBy'] = [
        'title' => ts('Grouping'),
        'tpl' => 'GroupByPlus',
        'div_label' => 'group-by-elements',
      ];
    }
  }

  /**
   * Set select clause.
   */
  public function select() {
    $select = $this->_columnHeaders = [];
    $this->_colGroupByFreq = $this->_params['group_bys_column_freq'];
    $this->_rowGroupByFreq = $this->_params['group_bys_row_freq'];

    foreach ($this->_columns as $tableName => $table) {
      if (array_key_exists('fields', $table)) {
        foreach ($table['fields'] as $fieldName => $field) {
          if (!empty($field['required']) || !empty($this->_params['fields'][$fieldName])) {
            if ($tableName == 'civicrm_statistics') {
              $select[] = $this->selectStatsFields($fieldName, $field);
            }
          }
        }
      }
      if (array_key_exists('group_bys', $table)) {
        foreach ($table['group_bys'] as $fieldName => $field) {
          $groupByFreq = NULL;
          if ($this->_params['group_bys_column'] == $fieldName) {
            // Store original fieldName for later use (drilldown urls)
            $field['fieldName'] = $fieldName;
            $this->_colField = $field;
            $fieldType = 'coly';
          }
          elseif ($this->_params['group_bys_row'] == $fieldName) {
            $field['fieldName'] = $fieldName;
            $this->_rowField = $field;
            $fieldType = 'rowx';
          }
          else {
            $fieldType = NULL;
          }

          if ($fieldType) {
            if ($tableName == 'civicrm_address') {
              $this->_selectedTables[] = 'civicrm_address';
            }

            if (!empty($field['type'])) {
              if ($field['type'] & CRM_Utils_Type::T_DATE) {
                if ($fieldType == 'coly') {
                  $groupByFreq = $this->_colGroupByFreq;
                }
                if ($fieldType == 'rowx') {
                  $groupByFreq = $this->_rowGroupByFreq;
                }
              }
            }

            switch ($groupByFreq) {
              case 'YEARWEEK':
                $select[] = "DATE_FORMAT({$field['dbAlias']}, '%Y-%u') as '$fieldType'";
                break;

              case 'YEAR':
                $select[] = "YEAR({$field['dbAlias']}) as '$fieldType'";
                break;

              case 'MONTH':
                $select[] = "DATE_FORMAT({$field['dbAlias']}, '%Y-%m') as '$fieldType'";
                break;

              case 'QUARTER':
                $select[] = "CONCAT(YEAR({$field['dbAlias']}), '-', QUARTER({$field['dbAlias']}))  as '$fieldType'";
                break;

              case 'DAY':
                $select[] = "DATE_FORMAT({$field['dbAlias']}, '%Y-%m-%d') as '$fieldType'";
                break;

              default:
                $select[] = "COALESCE(" . $field['dbAlias'] . ", '') as '$fieldType'";
                break;
            }
          }
        }
      }
    }
    $this->_select = "SELECT " . implode(', ', $select) . " ";
  }

  /**
   * Build select clause for statistics fields
   */
  protected function selectStatsFields($fieldName, $field) {
    switch ($fieldName) {
      case 'sum':
        $statField = "SUM({$field['dbAlias']}) as 'value'";
        $this->_currencyColumn = 'civicrm_contribution_currency';
        break;

      case 'count':
        $statField = "COUNT({$field['dbAlias']}) as 'value'";
        break;

      case 'avg':
        $statField = "ROUND(AVG({$field['dbAlias']}),2) as 'value'";
        $this->_currencyColumn = 'civicrm_contribution_currency';
        break;

      case 'max':
        $statField = "MAX({$field['dbAlias']}) as 'value'";
        $this->_currencyColumn = 'civicrm_contribution_currency';
        break;

      case 'min':
        $statField = "MIN({$field['dbAlias']}) as 'value'";
        $this->_currencyColumn = 'civicrm_contribution_currency';
        break;

      case 'count_distinct':
        $statField = "COUNT(DISTINCT " . $this->_aliases[$field['alias']] . ".{$field['dbAlias']}) as 'value'";
        break;
    }

    return $statField;
  }

  /**
   * Build from clause.
   *
   * @param string $entity
   */
  public function from($entity = NULL) {
    $this->_from = "
        FROM civicrm_contact  {$this->_aliases['civicrm_contact']} {$this->_aclFrom}
    ";

    $this->joinAddressFromContact();
  }

  /**
   * Build order by clause.
   */
  public function orderBy() {
    $this->_orderBy = "ORDER BY rowx, coly";
  }

  /**
   * Build group by clause.
   */
  public function groupBy() {
    $this->_groupBy = "GROUP BY coly, rowx";

    if (isset($this->_params['hideEmptyHeaders'])) {
      $this->_having = ' HAVING coly IS NOT NULL AND rowx IS NOT NULL ';
    }
  }

  /**
   * Override to set limit is 10
   * @param int $rowCount
   */
  public function limit($rowCount = self::ROW_COUNT_LIMIT) {
    $rowCount = 9999999;
    parent::limit($rowCount);
  }

  /**
   * Set limit to Matrix reports. Replaces limit() function
   *
   * @param int $rowCount
   *
   * @return array
   */
  public function limitMatrix($rowCount = self::ROW_COUNT_LIMIT) {
    // lets do the pager if in html mode
    $this->_limit = NULL;

    // CRM-14115, over-ride row count if rowCount is specified in URL
    if ($this->_dashBoardRowCount) {
      $rowCount = $this->_dashBoardRowCount;
    }
    if ($this->addPaging) {
      $pageId = CRM_Utils_Request::retrieve('crmPID', 'Integer');

      // @todo all http vars should be extracted in the preProcess
      // - not randomly in the class
      if (!$pageId && !empty($_POST)) {
        if (isset($_POST['PagerBottomButton']) && isset($_POST['crmPID_B'])) {
          $pageId = max((int) $_POST['crmPID_B'], 1);
        }
        elseif (isset($_POST['PagerTopButton']) && isset($_POST['crmPID'])) {
          $pageId = max((int) $_POST['crmPID'], 1);
        }
        unset($_POST['crmPID_B'], $_POST['crmPID']);
      }

      $pageId = $pageId ? $pageId : 1;
      $this->set(CRM_Utils_Pager::PAGE_ID, $pageId);
      $offset = ($pageId - 1) * $rowCount;

      $offset = CRM_Utils_Type::escape($offset, 'Int');
      $rowCount = CRM_Utils_Type::escape($rowCount, 'Int');

      $this->_limit = " LIMIT $offset, $rowCount";
      return [$offset, $rowCount];
    }
    if ($this->_limitValue) {
      if ($this->_offsetValue) {
        $this->_limit = " LIMIT {$this->_offsetValue}, {$this->_limitValue} ";
      }
      else {
        $this->_limit = " LIMIT " . $this->_limitValue;
      }
    }
  }

  /**
   * BeginPostProcess function run in both report mode and non-report mode (api).
   */
  public function beginPostProcessCommon() {
    $this->_showTotals = isset($this->_params['showTotals']) ?? 0;
    $this->_doNotCacheTables = isset($this->_params['doNotCacheTables']) ?? 0;
  }

  /**
   * Build report statistics.
   *
   * Override this method to build your own statistics.
   *
   * @param array $rows
   *
   * @return array
   */
  public function statistics(&$rows) {
    $statistics = parent::statistics($rows);
    $this->fieldStat($statistics);

    return $statistics;
  }

  /**
   * Add count statistics.
   *
   * @param array $statistics
   * @param int $count
   */
  public function countStat(&$statistics, $count) {
    parent::countStat($statistics, $count);
    $statistics['counts']['rowCount']['title'] = ts('Total Row(s)');
    unset($statistics['counts']['rowsFound']);
  }

  /**
   * Add field statistics.
   *
   * @param array $statistics
   */
  public function fieldStat(&$statistics) {
    foreach ($this->_columns as $tableName => $table) {
      if (array_key_exists('fields', $table)) {
        foreach ($table['fields'] as $fieldName => $field) {
          if (!empty($field['required']) || !empty($this->_params['fields'][$fieldName])) {
            if ($tableName == 'civicrm_statistics') {
              $statistics['fields']['stats'] = [
                'title' => ts('Statistics'),
                'value' => $field['title'],
              ];
              return;
            }
          }
        }
      }
    }
  }

  /**
   * Add group by statistics.
   *
   * @param array $statistics
   */
  public function groupByStat(&$statistics) {
    foreach ($this->_columns as $tableName => $table) {
      if (array_key_exists('group_bys', $table)) {
        foreach ($table['group_bys'] as $fieldName => $field) {
          if ($this->_params['group_bys_column'] == $fieldName) {
            $combinations[] = E::ts('Columns') . ": " . $field['title'];
          }
          elseif ($this->_params['group_bys_row'] == $fieldName) {
            $combinations[] = E::ts('Rows') . ": " . $field['title'];
          }
        }
      }
    }

    $statistics['groups'][] = [
      'title' => ts('Grouping(s)'),
      'value' => implode('<br>', $combinations),
    ];
  }

  /**
   * Post process function.
   */
  public function postProcess() {
    // get ready with post process params
    $this->beginPostProcess();

    // build query
    $sql = $this->buildQuery();

    // build array of result based on column headers. This method also allows
    // modifying column headers before using it to build result set i.e $rows.
    $rows = [];
    // Matrix Report needs a pre-process before building rows
    $tableName = $this->preBuildRows($sql);
    $this->buildRows($tableName, $rows);

    // format result set.
    $this->formatDisplay($rows);

    // assign variables to templates
    $this->doTemplateAssignment($rows);

    // do print / pdf / instance stuff if needed
    $this->endPostProcess($rows);
  }

  /**
   * Build the report query.
   *
   * @param bool $applyLimit
   *
   * @return string
   */
  public function buildQuery($applyLimit = TRUE) {
    // override main buildquery to not limit
    return parent::buildQuery(FALSE);
  }

  /**
   * Pre process before build output rows.
   *
   * @param string $sql
   */
  public function preBuildRows($sql) {
    $qfKey = CRM_Utils_Request::retrieve('qfKey', 'String', $this);

    // If it's paging we keep tmp table to avoid calculate all over again just to change page
    if (!($tmp2TableName = $this->_getCachedTable($qfKey))) {
      // First create Totals tmp table, one value per row
      $this->createTemporaryTable('civicrm_reportplus_matrix_tmp1', $sql, FALSE, TRUE);
      $tmp1TableName = $this->temporaryTables['civicrm_reportplus_matrix_tmp1']['name'];

      // Get Report columns from col values of tmp1 table
      $sqlCols = "SELECT DISTINCT coly FROM {$tmp1TableName} ORDER BY coly";
      $dao = CRM_Core_DAO::executeQuery($sqlCols);
      $allColHeaders = $dao->fetchAll();

      // Second pivots Totals tmp table, to get a Matrix, grouped by row
      $i = 1;
      foreach ($allColHeaders as $tableCol) {
        $columnName = 'col_' . $i;
        $sumCol[] = "SUM(`" . $tableCol['coly'] . "`) AS '" . $columnName . "'";
        $selectCol[] = "CASE WHEN COALESCE(coly, '')='" . $tableCol['coly'] . "' THEN value ELSE 0 END AS '" . $tableCol['coly'] . "'";

        // set columns headers
        if (!array_key_exists($columnName, $this->_columnHeaders)) {
          $this->_columnHeaders[$columnName]['title'] = !empty($tableCol['coly']) ? $tableCol['coly'] : ts('N/A');
          $this->_columnHeaders[$columnName]['value'] = $tableCol['coly'];
          if ($this->_currencyColumn) {
            $this->_columnHeaders[$columnName]['type'] = 1024;
          }
          else {
            $this->_columnHeaders[$columnName]['type'] = self::OP_INT;
          }
        }
        $i++;
      }

      if (!empty($allColHeaders)) {
        $sqlMatrix = "SELECT rowx, %s FROM ( SELECT COALESCE(rowx, '') AS rowx, %s FROM %s ) AS tmp2 GROUP BY rowx";
        $sqlMatrix = sprintf($sqlMatrix, implode(', ', $sumCol), implode(', ', $selectCol), $tmp1TableName);
      }
      else {
        // empty resultset, just create an empty table
        $sqlMatrix = "SELECT rowx FROM {$tmp1TableName} AS tmp2";
      }

      $isMemory = ($this->_doNotCacheTables) ?? TRUE;
      $this->createTemporaryTable('civicrm_reportplus_matrix_tmp2', $sqlMatrix, FALSE, $isMemory);
      $tmp2TableName = $this->temporaryTables['civicrm_reportplus_matrix_tmp2']['name'];

      // Cache some stuff to avoid calculate everything again on page change
      if (!$this->_doNotCacheTables) {
        $cacheParams = [
          'columnHeaders' => $this->_columnHeaders,
          'tableName' => $tmp2TableName,
        ];
        $this->_cache->set($qfKey, $cacheParams);
      }
    }

    // Recreate columns from cache
    if (empty($this->_columnHeaders)) {
      $cacheParams = $this->_cache->get($qfKey);
      $this->_columnHeaders = $cacheParams['columnHeaders'] ?? [];
    }

    return $tmp2TableName;
  }

  /**
   * Build output rows.
   *
   * @param string $tableName
   * @param array $rows
   */
  public function buildRows($tableName, &$rows) {
    // Finally, select values from Matrix Temp Table with pagination
    $this->_rowsFound = CRM_Core_DAO::singleValueQuery("SELECT COUNT(*) FROM {$tableName}");
    $sqlReport = "SELECT * FROM {$tableName} ";
    $limit = empty($this->_params['limit']) ? self::ROW_COUNT_LIMIT : $this->_params['limit'];
    $this->limitMatrix($limit);
    $sqlReport .= $this->_limit;
    CRM_Utils_Hook::alterReportVar('sql', $sqlReport, $this);
    $dao = CRM_Core_DAO::executeQuery($sqlReport);

    if (!is_array($rows)) {
      $rows = [];
    }
    $rows = $dao->fetchAll();

    $j = 1;
    foreach ($rows as $key => $value) {
      // set row headers
      $rowName = "row_" . $j;
      if (!array_key_exists($rowName, $this->_rowHeaders)) {
        $this->_rowHeaders[$rowName]['title'] = !empty($value['rowx']) ? $value['rowx'] : ts('N/A');
        $this->_rowHeaders[$rowName]['value'] = $value['rowx'];
        if ($this->_currencyColumn) {
          // Currency type
          $this->_rowHeaders[$rowName]['type'] = 1024;
        }
        else {
          $this->_rowHeaders[$rowName]['type'] = self::OP_INT;
        }
      }

      // replace by rowHeaderName for nicer presentation
      $rows[$key]['row'] = $rowName;
      $j++;
    }

    // use this method to modify $this->_columnHeaders
    $this->modifyColumnHeaders();
  }

  public function buildChartJS(&$rows) {
    if (!empty($this->_params['charts'])) {
      Civi::resources()->addScriptFile('com.ixiam.modules.reportplus', 'js/chart.min.js');

      $this->_chartJSType = $this->_params['charts'];
      CRM_Reportplus_Utils_ChartJS::chart($rows, $this->_chartJSType, $this->_columnHeaders, $this->_rowHeaders, $this->_params);

      if ($this->_chartJSType == 'horizontalBar') {
        $this->_chartJSType = 'bar';
      }
      $this->assign('chartJSType', $this->_chartJSType);
    }
  }

  /**
   * Modify column headers.
   */
  public function modifyColumnHeaders() {
    $this->_setHeaderLabels($this->_columnHeaders, $this->_colField, $this->_colOptionsWeight);
    $this->_setHeaderLabels($this->_rowHeaders, $this->_rowField, $this->_rowOptionsWeight);

    if ($this->_showTotals) {
      $lastCol = end($this->_columnHeaders);
      $this->_columnHeaders['col_total'] = [
        'title' => ts('Total'),
        'value' => 'col_total',
        'type' => $lastCol['type'],
      ];

      $lastRow = end($this->_rowHeaders);
      $this->_rowHeaders['row_total'] = [
        'title' => ts('Total'),
        'value' => 'row_total',
        'type' => $lastRow['type'],
      ];
    }
  }

  /**
   * Alter display of rows.
   *
   * Iterate through the rows retrieved via SQL and make changes for display purposes,
   * such as rendering contacts as links.
   *
   * @param array $rows
   *   Rows generated by SQL, with an array for each row.
   */
  public function alterDisplay(&$rows) {
    if (in_array($this->_outputMode, ['excel2007', 'csv'])) {
      $tempRows = [];
      foreach ($rows as $key => $row) {
        $tempRows[$key] = $row;
        $tempRows[$key]['row'] = $this->_rowHeaders[$row['row']]['title'];
      }

      $rowsItem = [
        'row' => [
          'value' => '',
          'title' => '',
          'type' => 2,
        ],
      ];
      $this->_columnHeaders = $rowsItem + $this->_columnHeaders;
      $rows = $tempRows;
    }

    // setup row decorators in a different array
    if (isset($this->_params['linkDrilldownReports']) && !empty($this->_drilldownReport)) {
      foreach ($rows as $rowNum => $row) {
        $rowValue = "";
        foreach ($row as $cellKey => $cellValue) {
          if ($cellKey == 'row') {
            $rowValue = $this->_rowHeaders[$cellValue]['value'];
            continue;
          }
          $colValue = $this->_columnHeaders[$cellKey]['value'];
          $this->_rowDecorators[$rowNum][$cellKey]['url'] = $this->getDrillDownUrl($rowValue, $colValue);
        }
      }
    }

    if ($this->_showTotals) {
      $this->calculateTotals($rows);
    }

    $this->assign('rowDecorators', $this->_rowDecorators);
    $this->assign('rowHeaders', $this->_rowHeaders);
  }

  /**
   * Calculate cols/rows subtotal values
   *
   * @param array $rows
   */
  public static function calculateTotals(&$rows) {
    $tempRows = $rows;
    $tempRows['row_total'] = [
      'col_total' => 0
    ];

    foreach ($rows as $keyR => $valueR) {
      foreach ($valueR as $key => $value) {
        if (strpos($key, 'col_') === 0 && is_numeric($value)) {
          $floatval = floatval($value);
          if (!isset($tempRows['row_total'][$key])) {
            $tempRows['row_total'][$key] = 0;
          }
          if (!isset($tempRows[$keyR]['col_total'])) {
            $tempRows[$keyR]['col_total'] = 0;
          }

          $tempRows['row_total'][$key] += $floatval;
          $tempRows['row_total']['col_total'] += $floatval;
          $tempRows[$keyR]['col_total'] += $floatval;
        }
      }
    }
    $tempRows['row_total']['row'] = "row_total";
    $tempRows['row_total']['rowx'] = "total";
    $tempRows['row_total']['class'] = "report-label";

    $rows = $tempRows;
  }

  /**
   * Validate form with specific rules
   *
   * @param array $fields
   * @param array $files
   * @param object $self
   *
   * @return array $errors
   */
  public static function formRule($fields, $files, $self) {
    $errors = [];

    if (count($fields['fields']) == 0) {
      $errors['fields'] = E::ts("Please select one statistic to be displayed");
    }
    elseif (count($fields['fields']) > 1) {
      $errors['fields'] = E::ts("Please select only one statistic to be displayed");
    }
    elseif (!$fields['group_bys_row']) {
      $errors['group_bys_row'] = E::ts("Please select one field to be the rows");
    }
    elseif (!$fields['group_bys_column']) {
      $errors['group_bys_column'] = E::ts("Please select one field to be the columns");
    }

    if (!empty($fields['chartjs_enabled']) && empty($fields['charts'])) {
      $errors['charts'] = E::ts("Please select one Chart JS type");
    }

    return $errors;
  }

  public function addOrderBys() {
  }

  public function addContactGroupBysFields() {
    $groupbys = [
      'id' => [
        'title' => ts('Contact ID'),
      ],
      'display_name' => [
        'title' => ts('Display Name'),
      ],
      'contact_type' => [
        'title' => ts('Contact Type'),
      ],
      'contact_sub_type' => [
        'title' => ts('Contact Subtype'),
      ],
      'gender_id' => [
        'title' => ts('Gender'),
      ],
      'age' => [
        'title' => ts('Age'),
        'dbAlias' => 'TIMESTAMPDIFF(YEAR, contact_civireport.birth_date, CURDATE())',
      ],
      'birth_date' => [
        'title' => ts('Birth Date'),
      ],
      'is_deceased' => [
        'title' => ts('Contact is Deceased'),
      ],
      'deceased_date' => [
        'title' => ts('Deceased Date'),
      ],
    ];
    return $groupbys;
  }

  /**
   * Function to assign the tabs to the matrix template in the correct order.
   *
   * We want the tabs to wind up in this order (if not overridden).
   *
   *  FieldSelection
   *  GroupBy
   *  ReportOptions
   *  Filters
   *  + Extra Tabs
   */
  protected function assignTabs() {
    $order = [
      'FieldSelection',
      'GroupBy',
      'ReportOptions',
      'Filters',
    ];
    $order = array_intersect_key(array_fill_keys($order, 1), $this->tabs);
    $order = array_merge($order, $this->tabs);
    $this->assign('tabs', $order);
  }

  /**
   * Add custom data to the columns.
   *
   * @param bool $addFields
   * @param array $permCustomGroupIds
   */
  public function addCustomDataToColumns($addFields = TRUE, $permCustomGroupIds = []) {
    if (empty($this->_customGroupExtends)) {
      return;
    }
    if (!is_array($this->_customGroupExtends)) {
      $this->_customGroupExtends = [$this->_customGroupExtends];
    }

    $customGroupWhere = '';
    if (!empty($permCustomGroupIds)) {
      $customGroupWhere = "cg.id IN (" . implode(',', $permCustomGroupIds) .
        ") AND";
    }
    $sql = "
      SELECT cg.table_name, cg.title, cg.extends, cf.id as cf_id, cf.label,
             cf.column_name, cf.data_type, cf.html_type, cf.option_group_id, cf.time_format
      FROM   civicrm_custom_group cg
      INNER  JOIN civicrm_custom_field cf ON cg.id = cf.custom_group_id
      WHERE cg.extends IN ('" . implode("','", $this->_customGroupExtends) . "') AND
            {$customGroupWhere}
            cg.is_active = 1 AND
            cf.is_active = 1 AND
            cf.is_searchable = 1 AND
            cf.serialize = 0
      ORDER BY cg.weight, cf.weight";
    $customDAO = CRM_Core_DAO::executeQuery($sql);

    $curTable = NULL;
    while ($customDAO->fetch()) {
      if ($customDAO->table_name != $curTable) {
        $curTable = $customDAO->table_name;
        $curFields = $curFilters = [];

        // dummy dao object
        $this->_columns[$curTable]['dao'] = 'CRM_Contact_DAO_Contact';
        $this->_columns[$curTable]['extends'] = $customDAO->extends;
        $this->_columns[$curTable]['grouping'] = $customDAO->table_name;
        $this->_columns[$curTable]['group_title'] = $customDAO->title;

        foreach ([
          'filters',
          'group_bys',
        ] as $colKey) {
          if (!array_key_exists($colKey, $this->_columns[$curTable])) {
            $this->_columns[$curTable][$colKey] = [];
          }
        }
      }
      $fieldName = 'custom_' . $customDAO->cf_id;

      if ($addFields) {
        // this makes aliasing work in favor
        $curFields[$fieldName] = [
          'name' => $customDAO->column_name,
          'title' => $customDAO->label,
          'dataType' => $customDAO->data_type,
          'htmlType' => $customDAO->html_type,
        ];
      }
      if ($this->_customGroupFilters) {
        // this makes aliasing work in favor
        $curFilters[$fieldName] = [
          'name' => $customDAO->column_name,
          'title' => $customDAO->label,
          'dataType' => $customDAO->data_type,
          'htmlType' => $customDAO->html_type,
        ];
      }

      switch ($customDAO->data_type) {
        case 'Date':
          // filters
          $curFilters[$fieldName]['operatorType'] = CRM_Report_Form::OP_DATE;
          $curFilters[$fieldName]['type'] = CRM_Utils_Type::T_DATE;
          // CRM-6946, show time part for datetime date fields
          if ($customDAO->time_format) {
            $curFields[$fieldName]['type'] = CRM_Utils_Type::T_TIMESTAMP;
          }
          break;

        case 'Boolean':
          $curFilters[$fieldName]['operatorType'] = CRM_Report_Form::OP_SELECT;
          $curFilters[$fieldName]['options'] = [
            '' => ts('- select -'),
            1 => ts('Yes'),
            0 => ts('No'),
          ];
          $curFilters[$fieldName]['type'] = CRM_Utils_Type::T_INT;
          break;

        case 'Int':
          $curFilters[$fieldName]['operatorType'] = CRM_Report_Form::OP_INT;
          $curFilters[$fieldName]['type'] = CRM_Utils_Type::T_INT;
          break;

        case 'Money':
          $curFilters[$fieldName]['operatorType'] = CRM_Report_Form::OP_FLOAT;
          $curFilters[$fieldName]['type'] = CRM_Utils_Type::T_MONEY;
          break;

        case 'Float':
          $curFilters[$fieldName]['operatorType'] = CRM_Report_Form::OP_FLOAT;
          $curFilters[$fieldName]['type'] = CRM_Utils_Type::T_FLOAT;
          break;

        case 'String':
          $curFilters[$fieldName]['type'] = CRM_Utils_Type::T_STRING;

          if (!empty($customDAO->option_group_id)) {
            if (in_array($customDAO->html_type, [
              'Multi-Select',
              'AdvMulti-Select',
              'CheckBox',
            ])) {
              $curFilters[$fieldName]['operatorType'] = CRM_Report_Form::OP_MULTISELECT_SEPARATOR;
            }
            else {
              $curFilters[$fieldName]['operatorType'] = CRM_Report_Form::OP_MULTISELECT;
            }
            if ($this->_customGroupFilters) {
              $curFilters[$fieldName]['options'] = [];
              $ogDAO = CRM_Core_DAO::executeQuery("SELECT ov.value, ov.label FROM civicrm_option_value ov WHERE ov.option_group_id = %1 ORDER BY ov.weight", [
                1 => [
                  $customDAO->option_group_id,
                  'Integer',
                ],
              ]);
              while ($ogDAO->fetch()) {
                $curFilters[$fieldName]['options'][$ogDAO->value] = $ogDAO->label;
              }
              CRM_Utils_Hook::customFieldOptions($customDAO->cf_id, $curFilters[$fieldName]['options'], FALSE);
            }
          }
          break;

        case 'StateProvince':
          $curFilters[$fieldName]['type'] = CRM_Utils_Type::T_STRING;
          if (in_array($customDAO->html_type, [
            'Multi-Select State/Province',
          ])) {
            $curFilters[$fieldName]['operatorType'] = CRM_Report_Form::OP_MULTISELECT_SEPARATOR;
          }
          else {
            $curFilters[$fieldName]['operatorType'] = CRM_Report_Form::OP_MULTISELECT;
          }
          $curFilters[$fieldName]['options'] = CRM_Core_PseudoConstant::stateProvince();
          break;

        case 'Country':
          $curFilters[$fieldName]['type'] = CRM_Utils_Type::T_STRING;
          if (in_array($customDAO->html_type, [
            'Multi-Select Country',
          ])) {
            $curFilters[$fieldName]['operatorType'] = CRM_Report_Form::OP_MULTISELECT_SEPARATOR;
          }
          else {
            $curFilters[$fieldName]['operatorType'] = CRM_Report_Form::OP_MULTISELECT;
          }
          $curFilters[$fieldName]['options'] = CRM_Core_PseudoConstant::country();
          break;

        case 'ContactReference':
          $curFilters[$fieldName]['type'] = CRM_Utils_Type::T_STRING;
          $curFilters[$fieldName]['name'] = 'display_name';
          $curFilters[$fieldName]['alias'] = "contact_{$fieldName}_civireport";

          $curFields[$fieldName]['type'] = CRM_Utils_Type::T_STRING;
          $curFields[$fieldName]['name'] = 'display_name';
          $curFields[$fieldName]['alias'] = "contact_{$fieldName}_civireport";
          break;

        default:
          $curFields[$fieldName]['type'] = CRM_Utils_Type::T_STRING;
          $curFilters[$fieldName]['type'] = CRM_Utils_Type::T_STRING;
      }

      if (!array_key_exists('type', $curFields[$fieldName])) {
        $curFields[$fieldName]['type'] = CRM_Utils_Array::value('type', $curFilters[$fieldName], []);
      }

      if ($this->_customGroupFilters) {
        $this->_columns[$curTable]['filters'] = array_merge($this->_columns[$curTable]['filters'], $curFilters);
      }
      if ($this->_customGroupGroupBy) {
        $this->_columns[$curTable]['group_bys'] = array_merge($this->_columns[$curTable]['group_bys'], $curFields);
      }
    }
  }

  /**
   * Build custom data from clause.
   */
  public function customDataFrom($joinsForFiltersOnly = FALSE) {
    if (empty($this->_customGroupExtends)) {
      return;
    }
    $mapper = CRM_Core_BAO_CustomQuery::$extendsMap;

    foreach ($this->_columns as $table => $prop) {
      if (substr($table, 0, 13) == 'civicrm_value' ||
        substr($table, 0, 12) == 'custom_value'
      ) {
        $extendsTable = $mapper[$prop['extends']];

        // check field is in params
        // Check field is required for rendering the report.
        if ((!$this->isFieldSelected($prop)) || ($joinsForFiltersOnly && !$this->isFieldFiltered($prop))) {
          continue;
        }
        $baseJoin = CRM_Utils_Array::value($prop['extends'], $this->_customGroupExtendsJoin, "{$this->_aliases[$extendsTable]}.id");

        $customJoin = is_array($this->_customGroupJoin) ? $this->_customGroupJoin[$table] : $this->_customGroupJoin;
        $this->_from .= "
{$customJoin} {$table} {$this->_aliases[$table]} ON {$this->_aliases[$table]}.entity_id = {$baseJoin}";
        // handle for ContactReference
        if (array_key_exists('group_bys', $prop)) {
          foreach ($prop['group_bys'] as $fieldName => $field) {
            if (CRM_Utils_Array::value('dataType', $field) ==
              'ContactReference'
            ) {
              $columnName = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_CustomField', CRM_Core_BAO_CustomField::getKeyID($fieldName), 'column_name');
              $this->_from .= "
LEFT JOIN civicrm_contact {$field['alias']} ON {$field['alias']}.id = {$this->_aliases[$table]}.{$columnName} ";
            }
          }
        }
      }
    }
  }

  public function isFieldSelected($prop) {
    if (empty($prop)) {
      return FALSE;
    }

    if (!empty($this->_params['fields']) && isset($prop['fields'])) {
      foreach (array_keys($prop['fields']) as $fieldAlias) {
        $customFieldId = CRM_Core_BAO_CustomField::getKeyID($fieldAlias);
        if ($customFieldId) {
          if (array_key_exists($fieldAlias, $this->_params['fields'])) {
            return TRUE;
          }

          //might be survey response field.
          if (!empty($this->_params['fields']['survey_response']) &&
            !empty($prop['fields'][$fieldAlias]['isSurveyResponseField'])
          ) {
            return TRUE;
          }
        }
      }
    }

    if ($this->_customGroupGroupBy) {
      foreach (array_keys($prop['group_bys']) as $fieldAlias) {
        if (CRM_Core_BAO_CustomField::getKeyID($fieldAlias)) {
          if (($this->_params['group_bys_column'] == $fieldAlias) ||
            ($this->_params['group_bys_row'] == $fieldAlias)) {
            return TRUE;
          }
        }
      }
    }

    if (!empty($this->_params['order_bys'])&& !empty($prop['fields'])) {
      foreach (array_keys($prop['fields']) as $fieldAlias) {
        foreach ($this->_params['order_bys'] as $orderBy) {
          if ($fieldAlias == $orderBy['column'] &&
            CRM_Core_BAO_CustomField::getKeyID($fieldAlias)
          ) {
            return TRUE;
          }
        }
      }
    }

    if (!empty($prop['filters']) && $this->_customGroupFilters) {
      foreach ($prop['filters'] as $fieldAlias => $val) {
        foreach ([
          'value',
          'min',
          'max',
          'relative',
          'from',
          'to',
        ] as $attach) {
          if (isset($this->_params[$fieldAlias . '_' . $attach]) &&
            (!empty($this->_params[$fieldAlias . '_' . $attach])
              || ($attach != 'relative' &&
                $this->_params[$fieldAlias . '_' . $attach] == '0')
            )
          ) {
            return TRUE;
          }
        }
        if (!empty($this->_params[$fieldAlias . '_op']) &&
          in_array($this->_params[$fieldAlias . '_op'], ['nll', 'nnll'])
        ) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Join address table from custom address table
   */
  public function joinAddressFromContact($prefix = '', $extra = []) {
    $this->addAddressTable();

    // Join row from custom address table
    if (substr($this->_params['group_bys_row'], 0, 7) === "custom_") {
      // Extract custom field id
      $customID = substr($this->_params['group_bys_row'], strrpos($this->_params['group_bys_row'], "_") + 1);
      // Get extension of custom table
      $result = civicrm_api3('CustomField', 'get', ['sequential' => 1, 'return' => ["custom_group_id.extends"], 'id' => $customID]);
      if ($result['values'][0]['custom_group_id.extends'] == 'Address') {
        $this->_selectedTables[] = 'civicrm_address';
      }
    }
    // Join col from custom address table
    if (substr($this->_params['group_bys_column'], 0, 7) === "custom_") {
      // Extract custom field id
      $customID = substr($this->_params['group_bys_column'], strrpos($this->_params['group_bys_column'], "_") + 1);
      // Get extension of custom table
      $result = civicrm_api3('CustomField', 'get', ['sequential' => 1, 'return' => ["custom_group_id.extends"], 'id' => $customID]);
      if ($result['values'][0]['custom_group_id.extends'] == 'Address') {
        $this->_selectedTables[] = 'civicrm_address';
      }
    }

    parent::joinAddressFromContact($prefix, $extra);
  }

  /**
   * Callback function for columns sorting
   *
   * @param string $a
   * @param string $b
   *
   * @return int
   */
  private function _columnHeadersSort($a, $b) {
    return ($this->_colOptionsWeight[$a['title']] > $this->_colOptionsWeight[$b['title']]) ? +1 : -1;
  }

  /**
   * Callback function for datetime sorting
   *
   * @param string $a
   * @param string $b
   *
   * @return int
   */
  private function _columnHeadersSortByDate($a, $b) {
    return ($a['title'] > $b['title']) ? +1 : -1;
  }

  /**
   * Callback function for rows sorting
   *
   * @param string $a
   * @param string $b
   *
   * @return int
   */
  private function _rowsSort($a, $b) {
    return ($this->_rowOptionsWeight[$a] > $this->_rowOptionsWeight[$b]) ? +1 : -1;
  }

  /**
   * Get option value through api call
   *
   * @param string $columnName
   *
   * @return array
   */
  private function _getOptionValues($columnName) {
    $params = [
      'return'      => 'id, custom_group_id',
      'column_name' => $columnName,
      'options'     => [
        'limit' => 0,
      ],
      'api.CustomGroup.getsingle' => [
        'id' => "\$value.custom_group_id",
      ],
    ];
    $result  = civicrm_api3('CustomField', 'getsingle', $params);

    $id = $result['id'];
    $entity = $result['api.CustomGroup.getsingle']['extends'];

    // Get options for custom field
    $result = civicrm_api3($entity, 'getoptions', [
      'field' => "custom_" . $id,
      'context' => "get",
    ]);

    $options = $result['values'];

    return $options;
  }

  /**
   * Set Column labels
   *
   * @param array $headers
   * @param array $labels
   */
  private function _setColumnLabel(&$headers, $labels) {
    foreach ($headers as $key => $value) {
      if (!empty($labels[$value['value']])) {
        $headers[$key]['title'] = $labels[$value['value']];
      }
    }
  }

  /**
   * Set Header labels
   *
   * @param array $headers
   * @param array $field
   * @param array $weights
   */
  private function _setHeaderLabels(&$headers, $field, &$weights) {
    switch ($field['name']) {
      case 'gender_id':
        $genders = CRM_Core_PseudoConstant::get('CRM_Contact_DAO_Contact', 'gender_id', ['localize' => TRUE]);
        $this->_setColumnLabel($headers, $genders);
        break;

      case 'country_id':
        $countries = CRM_Core_PseudoConstant::country(FALSE, FALSE);
        $this->_setColumnLabel($headers, $countries);
        break;

      case 'state_province_id':
        $states = CRM_Core_PseudoConstant::stateProvince(FALSE, FALSE);
        $this->_setColumnLabel($headers, $states);
        break;

      case 'county_id':
        $counties = CRM_Core_PseudoConstant::county(FALSE);
        $this->_setColumnLabel($headers, $counties);
        break;

      case 'membership_type_id':
        $membershipTypes = CRM_Member_PseudoConstant::membershipType(FALSE, FALSE);
        $this->_setColumnLabel($headers, $membershipTypes);
        break;

      case 'status_id':
        if ($field['table_name'] == 'civicrm_activity') {
          $status = CRM_Core_PseudoConstant::activityStatus();
        }
        elseif ($field['table_name'] == 'civicrm_case') {
          $status = CRM_Core_PseudoConstant::get('CRM_Case_DAO_Case', 'status_id', ['localize' => TRUE]);
        }
        elseif ($field['table_name'] == 'civicrm_participant') {
          $status = CRM_Event_PseudoConstant::participantStatus(NULL, NULL, "label");
        }
        else {
          $status = CRM_Member_PseudoConstant::membershipStatus(NULL, NULL, 'label');
        }
        $this->_setColumnLabel($headers, $status);
        break;

      case 'contribution_status_id':
        $status = CRM_Contribute_PseudoConstant::contributionStatus();
        $this->_setColumnLabel($headers, $status);
        break;

      case 'financial_type_id':
        $status = CRM_Contribute_PseudoConstant::financialType();
        $this->_setColumnLabel($headers, $status);
        break;

      case 'payment_instrument_id':
        $payment_instrument = CRM_Contribute_PseudoConstant::paymentInstrument();
        $this->_setColumnLabel($headers, $payment_instrument);
        break;

      case 'activity_type_id':
        $type = CRM_Core_PseudoConstant::activityType(TRUE, TRUE, FALSE, 'label', FALSE);
        $this->_setColumnLabel($headers, $type);
        break;

      case 'campaign_id':
        $campaigns = $this->_activeCampaigns + $this->_disabledCampaigns;
        $this->_setColumnLabel($headers, $campaigns);
        break;

      case 'role_id':
        $roles = CRM_Event_PseudoConstant::participantRole();
        $this->_setColumnLabel($headers, $roles);
        break;

      case 'event_id':
        $events = \Civi\Api4\Event::get()->addSelect('id', 'title')->execute()->indexBy('id')->column('title');
        $this->_setColumnLabel($headers, $events);
        break;

      case 'event_type_id':
        $event_types = CRM_Core_OptionGroup::values('event_type');
        $this->_setColumnLabel($headers, $event_types);
        break;

      case 'price_field_id':
        $price_fields = \Civi\Api4\PriceField::get()->execute()->indexBy('id')->column('label');
        $this->_setColumnLabel($headers, $price_fields);
        break;

      case 'price_field_value_id':
        $price_field_values = \Civi\Api4\PriceFieldValue::get()->execute()->indexBy('id')->column('label');
        $this->_setColumnLabel($headers, $price_field_values);
        break;

      default:
        $type = CRM_Utils_Type::typeToString(CRM_Utils_Array::value('type', $field));
        if (!empty($type)) {
          if ($type == 'Timestamp') {
            uasort($headers, ['CRM_Reportplus_Form_Matrix', '_columnHeadersSortByDate']);
          }
          elseif ($type == 'String') {
            if (isset($field['htmlType'])) {
              if (in_array($field['htmlType'], ['Select', 'Radio', 'Autocomplete-Select']) && $field['dataType'] != 'ContactReference') {
                $options = $this->_getOptionValues($field['name']);
                $optionLabels = [];
                foreach ($options as $key => $value) {
                  $optionLabels[$key] = $value;
                }
                uasort($headers, ['CRM_Reportplus_Form_Matrix', '_columnHeadersSort']);
                $this->_setColumnLabel($headers, $optionLabels);
              }
            }
          }
          elseif ($type == 'Int') {
            if (isset($field['dataType']) && $field['dataType'] == 'Boolean') {
              foreach ($headers as $keyHeader => $header) {
                switch ($header['value']) {
                  case '':
                    $headers[$keyHeader]['title'] = ts('N/A');
                    break;

                  case 0:
                    $headers[$keyHeader]['title'] = ts('No');
                    break;

                  case 1:
                    $headers[$keyHeader]['title'] = ts('Yes');
                    break;
                }
              }
            }
          }
        }
        else {
          if (isset($field['dataType']) && $field['dataType'] == 'Country') {
            $countries = CRM_Core_PseudoConstant::country(FALSE, FALSE);
            $this->_setColumnLabel($headers, $countries);
          }
        }
        break;
    }
  }

  /**
   * Return cached table name if exists
   *
   * @param string $qfKey
   *
   * @return string
   */
  protected function _getCachedTable($qfKey = NULL) {
    // only use cached table when paging, new submits needs re-build
    if ($this->addPaging && !$this->_actionButtonName && !$this->_doNotCacheTables) {
      $pageId = CRM_Utils_Request::retrieve('crmPID', 'Integer');
      if (!empty($pageId) && !empty($qfKey)) {
        $cacheParams = $this->_cache->get($qfKey);
        $tableName = $cacheParams['tableName'] ?? NULL;
        if (!empty($tableName)) {
          $checkTableSql = "show tables like '{$tableName}'";
          $foundName = CRM_Core_DAO::singleValueQuery($checkTableSql);
          if ($foundName == $tableName) {
            return $tableName;
          }
        }
      }
    }
    return FALSE;
  }

  /**
   * Get Drill Down report url with existing filters
   *
   * @param string $rowValue
   * @param string $colValue
   *
   * @return bool
   */
  public function getDrillDownUrl($rowValue, $colValue) {
    $criteriaQueryParams = CRM_Reportplus_Utils_Report::getPreviewCriteriaQueryParams($this->_defaults, $this->_params);
    $drilldownReportUrl = array_key_first($this->_drilldownReport);
    if ($drilldownReportUrl) {
      $colFieldName = $this->_colField['fieldName'];
      $rowFieldName = $this->_rowField['fieldName'];

      $colFilterQuery = $rowFilterQuery = "";
      if (isset($this->_colField['type']) && ($this->_colField['type'] & CRM_Utils_Type::T_DATE)) {
        if (!empty($colValue)) {
          $colFieldValue = $this->getDateQueryFilterValues($colValue, $this->_colGroupByFreq);
          $colFilterQuery = "&{$colFieldName}_from={$colFieldValue['from']}&{$colFieldName}_to={$colFieldValue['to']}";
        }
        else {
          $colFilterQuery = "&{$colFieldName}_relative=nll";
        }
      }
      else {
        $colOperator = empty($colValue) ? 'nll' : 'eq';
        $colFilterQuery = "&{$colFieldName}_op={$colOperator}&{$colFieldName}_value={$colValue}";
      }

      if (isset($this->_rowField['type']) && ($this->_rowField['type'] & CRM_Utils_Type::T_DATE)) {
        if (!empty($rowValue)) {
          $rowFieldValue = $this->getDateQueryFilterValues($rowValue, $this->_rowGroupByFreq);
          $rowFilterQuery = "&{$rowFieldName}_from={$rowFieldValue['from']}&{$rowFieldName}_to={$rowFieldValue['to']}";
        }
        else {
          $rowFilterQuery = "&{$rowFieldName}_relative=nll";
        }
      }
      else {
        $rowOperator = empty($rowValue) ? 'nll' : 'eq';
        $rowFilterQuery = "&{$rowFieldName}_op={$rowOperator}&{$rowFieldName}_value={$rowValue}";
      }

      $url = CRM_Report_Utils_Report::getNextUrl($drilldownReportUrl,
        "reset=1&force=1&{$criteriaQueryParams}{$colFilterQuery}{$rowFilterQuery}",
        $this->_absoluteUrl, $this->_id, $this->_drilldownReport
      );
    }

    return $url;
  }

  /**
   * Get Date Values (from / to) for querystring filters
   *
   * @param string $value
   * @param string $freq
   *
   * @return array
   */
  private function getDateQueryFilterValues($value, $freq) {
    $dateValues = [
      "from" => NULL,
      "to" => NULL,
    ];

    switch ($freq) {
      case 'YEAR':
        $dateValues = [
          "from" => $value . "0101",
          "to" => $value . "1231",
        ];
        break;

      case 'QUARTER':
        [$year, $quarter] = explode("-", $value);
        $dateValues = CRM_Reportplus_Utils_Date::getStartAndEndDateforQuarter($quarter, $year);
        break;

      case 'YEARWEEK':
        [$year, $week] = explode("-", $value);
        $dateValues = CRM_Reportplus_Utils_Date::getStartAndEndDateforWeek($week, $year);
        break;

      case 'MONTH':
        $date = str_replace("-", "", $value);
        $dateValues = [
          "from" => $date . "01",
          "to" => date("Ymt", strtotime($value)),
        ];
        break;

      case 'DAY':
        $date = str_replace("-", "", $value);
        $dateValues = [
          "from" => $date,
          "to" => $date,
        ];
        break;
    }

    return $dateValues;

  }

  /**
   * Check if table name has columns in SELECT, WHERE or GROUP BY clause.
   *
   * @param string $tableName
   *   Name of table (index of $this->_columns array).
   *
   * @return bool
   */
  public function isTableSelected($tableName) {
    $isInSelect = parent::isTableSelected($tableName);
    $isInGroupBy = ($this->_rowField['table_name'] == $tableName or $this->_colField['table_name'] == $tableName);
    return $isInSelect or $isInGroupBy;
  }

}
