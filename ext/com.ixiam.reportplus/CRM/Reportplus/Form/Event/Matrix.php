<?php
use CRM_Reportplus_ExtensionUtil as E;

class CRM_Reportplus_Form_Event_Matrix extends CRM_Reportplus_Form_Matrix {

  protected $_isFeeLevelStats = FALSE;

  /**
   * Class constructor.
   */
  public function __construct() {
    $this->_customGroupExtends = ['Contact', 'Individual', 'Household', 'Organization', 'Address', 'Event', 'Participant'];
    $this->_customGroupGroupBy = TRUE;

    $this->_columns = [
      'civicrm_statistics' => [
        'dao' => 'CRM_Event_DAO_Participant',
        'fields' => [
          'count' => [
            'title' => ts('Count'),
            'default' => TRUE,
            'dbAlias' => '*',
          ],
          'count_distinct' => [
            'title' => E::ts('Unique Contacts'),
            'default' => FALSE,
            'alias' => 'civicrm_participant',
            'dbAlias' => 'contact_id',
          ],
          'min' => [
            'title' => E::ts('Min Participant Fee'),
            'default' => FALSE,
            'dbAlias' => 'fee_amount',
          ],
          'max' => [
            'title' => E::ts('Max Participant Fee'),
            'default' => FALSE,
            'dbAlias' => 'fee_amount',
          ],
          'sum' => [
            'title' => E::ts('Total Participant Fee'),
            'default' => FALSE,
            'dbAlias' => 'fee_amount',
          ],
          'avg' => [
            'title' => E::ts('Avg Participant Fee'),
            'default' => FALSE,
            'dbAlias' => 'fee_amount',
          ],
          'count_level' => [
            'title' => ts('Count Fee Level'),
            'default' => FALSE,
            'dbAlias' => 'qty',
          ],
          'min_level' => [
            'title' => E::ts('Min Fee Level'),
            'default' => FALSE,
            'dbAlias' => 'line_total',
          ],
          'max_level' => [
            'title' => E::ts('Max Fee Level'),
            'default' => FALSE,
            'dbAlias' => 'line_total',
          ],
          'sum_level' => [
            'title' => E::ts('Total Fee Level'),
            'default' => FALSE,
            'dbAlias' => 'line_total',
          ],
          'avg_level' => [
            'title' => E::ts('Avg Fee Level'),
            'default' => FALSE,
            'dbAlias' => 'line_total',
          ],
        ],
        'grouping' => 'statistics-fields',
      ],
      'civicrm_contact' => [
        'dao' => 'CRM_Contact_DAO_Contact',
        'fields' => [],
        'grouping' => 'contact-fields',
        'group-title' => ts('Contacts'),
        'group_bys' => $this->addContactGroupBysFields(),
      ],
      'civicrm_participant' => [
        'dao' => 'CRM_Event_DAO_Participant',
        'grouping' => 'event-fields',
        'group-title' => ts('Event'),
        'fields' => [],
        'filters' => [
          'event_id' => [
            'name' => 'event_id',
            'title' => ts('Event'),
            'operatorType' => CRM_Report_Form::OP_ENTITYREF,
            'type' => CRM_Utils_Type::T_INT,
            'attributes' => [
              'entity' => 'Event',
              'select' => ['minimumInputLength' => 0],
            ],
          ],
          'sid' => [
            'name' => 'status_id',
            'title' => ts('Participant Status'),
            'type' => CRM_Utils_Type::T_INT,
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options' => CRM_Event_PseudoConstant::participantStatus(NULL, NULL, 'label'),
          ],
          'rid' => [
            'name' => 'role_id',
            'title' => ts('Participant Role'),
            'type' => CRM_Utils_Type::T_INT,
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options' => CRM_Event_PseudoConstant::participantRole(),
          ],
          'participant_register_date' => [
            'title' => ts('Registration Date'),
            'operatorType' => CRM_Report_Form::OP_DATE,
          ],
          'fee_currency' => [
            'title' => ts('Fee Currency'),
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options' => CRM_Core_OptionGroup::values('currencies_enabled'),
            'default' => NULL,
            'type' => CRM_Utils_Type::T_STRING,
          ],
          'registered_by_id' => [
            'title' => ts('Registered by Participant ID'),
            'type' => CRM_Utils_Type::T_STRING,
            'operator' => 'like',
          ],
          'source' => [
            'title' => ts('Source'),
            'type' => CRM_Utils_Type::T_STRING,
            'operator' => 'like',
          ],
          'is_test' => [
            'title' => ts('Is Test'),
            'type' => CRM_Utils_Type::T_BOOLEAN,
            'default' => 0,
          ],
        ],
        'group_bys' => [
          'event_id' => [
            'title' => ts('Event'),
          ],
          'status_id' => [
            'title' => ts('Participant Status'),
          ],
          'role_id' => [
            'title' => ts('Participant Role'),
          ],
          'register_date' => [
            'title' => ts('Registration Date'),
            'frequency' => TRUE,
            'type' => 12,
          ],
        ],
      ],
      'civicrm_event' => [
        'dao' => 'CRM_Event_DAO_Event',
        'grouping' => 'event-fields',
        'fields' => [],
        'filters' => [
          'event_type_id' => [
            'name' => 'event_type_id',
            'title' => ts('Event Type'),
            'type' => CRM_Utils_Type::T_INT,
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options' => CRM_Core_OptionGroup::values('event_type'),
          ],
          'event_start_date' => [
            'title' => ts('Event Start Date'),
            'operatorType' => CRM_Report_Form::OP_DATE,
          ],
          'event_end_date' => [
            'title' => ts('Event End Date'),
            'operatorType' => CRM_Report_Form::OP_DATE,
          ],
        ],
        'group_bys' => [
          'event_type_id' => [
            'title' => ts('Event Type'),
            'default' => FALSE,
          ],
          'event_start_date' => [
            'title' => ts('Event Start Date'),
            'default' => FALSE,
            'frequency' => TRUE,
            'type' => 12,
          ],
          'event_end_date' => [
            'title' => ts('Event End Date'),
            'default' => FALSE,
            'frequency' => TRUE,
            'type' => 12,
          ],
        ],
      ],
      'civicrm_line_item' => [
        'dao' => 'CRM_Price_DAO_LineItem',
        'grouping' => 'priceset-fields',
        'group-title' => ts('Price Set'),
        'filters' => [
          'price_field_value_id' => [
            'name' => 'price_field_value_id',
            'title' => ts('Fee Price Field Value'),
            'type' => CRM_Utils_Type::T_INT,
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'options' => $this->getPriceLevels(),
          ],
        ],
        'group_bys' => [
          'price_field_value_id' => [
            'title' => ts('Price Fee Level'),
          ],
        ],
      ],
    ] + $this->addAddressFields(TRUE, FALSE, TRUE, [], FALSE);

    parent::__construct();
    $this->addCampaignFields('civicrm_participant', TRUE, FALSE, TRUE, FALSE);
  }

  /**
   * Set select clause with specific civicrm_line_item's fields stats.
   */
  protected function selectStatsFields($fieldName, $field) {
    $statsField = parent::selectStatsFields($fieldName, $field);
    if (!isset($statsField)) {
      switch ($fieldName) {
        case 'sum_level':
          $statsField = "SUM({$this->_aliases['civicrm_line_item']}.{$field['dbAlias']}) as 'value'";
          $this->_currencyColumn = 'civicrm_contribution_currency';
          $this->_isFeeLevelStats = TRUE;
          break;

        case 'count_level':
          $statsField = "SUM({$this->_aliases['civicrm_line_item']}.{$field['dbAlias']}) as 'value'";
          $this->_isFeeLevelStats = TRUE;
          break;

        case 'avg_level':
          $statsField = "ROUND(AVG({$this->_aliases['civicrm_line_item']}.{$field['dbAlias']}),2) as 'value'";
          $this->_currencyColumn = 'civicrm_contribution_currency';
          $this->_isFeeLevelStats = TRUE;
          break;

        case 'max_level':
          $statsField = "MAX({$this->_aliases['civicrm_line_item']}.{$field['dbAlias']}) as 'value'";
          $this->_currencyColumn = 'civicrm_contribution_currency';
          $this->_isFeeLevelStats = TRUE;
          break;

        case 'min_level':
          $statsField = "MIN({$this->_aliases['civicrm_line_item']}.{$field['dbAlias']}) as 'value'";
          $this->_currencyColumn = 'civicrm_contribution_currency';
          $this->_isFeeLevelStats = TRUE;
          break;
      }
    }
    return $statsField;
  }

  public function from($entity = NULL) {
    parent::from($entity);

    $this->_from .= "
             INNER JOIN civicrm_participant {$this->_aliases['civicrm_participant']}
                ON {$this->_aliases['civicrm_contact']}.id = {$this->_aliases['civicrm_participant']}.contact_id";

    if ($this->isTableSelected('civicrm_event')) {
      $this->_from .= "
             INNER JOIN civicrm_event {$this->_aliases['civicrm_event']}
                ON {$this->_aliases['civicrm_participant']}.event_id = {$this->_aliases['civicrm_event']}.id";
    }

    if ($this->isTableSelected('civicrm_line_item') || $this->_isFeeLevelStats) {
      $this->_from .= "
             LEFT JOIN civicrm_line_item {$this->_aliases['civicrm_line_item']}
                ON {$this->_aliases['civicrm_participant']}.id ={$this->_aliases['civicrm_line_item']}.entity_id AND
                  {$this->_aliases['civicrm_line_item']}.entity_table = 'civicrm_participant' ";
    }
  }

  /**
   * Searches database for priceset values.
   *
   * @return array
   */
  public function getPriceLevels() {
    $query = "
SELECT CONCAT(cv.label, ' (', ps.title, ' - ', cf.label , ')') label, cv.id
FROM civicrm_price_field_value cv
LEFT JOIN civicrm_price_field cf
  ON cv.price_field_id = cf.id
LEFT JOIN civicrm_price_set_entity ce
  ON ce.price_set_id = cf.price_set_id
LEFT JOIN civicrm_price_set ps
  ON ce.price_set_id = ps.id
WHERE ce.entity_table = 'civicrm_event'
ORDER BY  cv.label
";
    $dao = CRM_Core_DAO::executeQuery($query);
    $elements = [];
    while ($dao->fetch()) {
      $elements[$dao->id] = "$dao->label\n";
    }

    return $elements;
  }

}
