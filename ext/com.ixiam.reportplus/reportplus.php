<?php

require_once 'reportplus.civix.php';
// phpcs:disable
use CRM_Reportplus_ExtensionUtil as E;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
// phpcs:enable

/**
 * Implementation of hook_civicrm_permission().
 *
 * Create ad-hoc ReportPlus permission
 */
function reportplus_civicrm_permission(array &$permissions) {
  $prefix = ts('ReportPlus') . ': ';
  $permissions['access ReportPlus'] = [
    'label' => $prefix . 'access ReportPlus',
    'description' => E::ts('access templates and instances from ReportPlus Extension'),
  ];
  $permissions['administer ReportPlus'] = [
    'label' => $prefix . 'administer ReportPlus',
    'description' => E::ts('administer templates from ReportPlus Extension'),
  ];
}

/**
 * Implementation of hook_civicrm_pageRun().
 *
 * Used to display ReportPlus templates and instances based on permissions
 */
function reportplus_civicrm_pageRun(&$page) {
  $pageName = $page->getVar('_name');

  if (CRM_Core_Permission::check([['access ReportPlus', 'administer ReportPlus']])) {
    // Add Reportplus templates to the list (if permissions)
    if ($pageName == 'CRM_Report_Page_TemplateList') {

      $sql = "
        SELECT  v.id, v.value, v.label, v.description, v.component_id, comp.name as component_name, v.grouping, inst.id as instance_id
        FROM    civicrm_option_value v
        INNER JOIN civicrm_option_group g
                ON (v.option_group_id = g.id AND g.name = 'report_template')
        LEFT  JOIN civicrm_report_instance inst
                ON v.value = inst.report_id
        LEFT  JOIN civicrm_component comp
                ON v.component_id = comp.id
        ";
      $sql .= " WHERE v.is_active = 1 AND comp.name = 'ReportPlus' ";
      $sql .= " ORDER BY  v.label ";

      $dao = CRM_Core_DAO::executeQuery($sql);
      $rows = [];
      $config = CRM_Core_Config::singleton();
      while ($dao->fetch()) {
        $rows[$dao->component_name][$dao->value]['title'] = ts($dao->label);
        $rows[$dao->component_name][$dao->value]['description'] = ts($dao->description);
        $rows[$dao->component_name][$dao->value]['url'] = CRM_Utils_System::url('civicrm/report/' . trim($dao->value, '/'), 'reset=1');
        if ($dao->instance_id) {
          $rows[$dao->component_name][$dao->value]['instanceUrl'] = CRM_Utils_System::url('civicrm/report/list',
            "reset=1&ovid={$dao->id}"
          );
        }
      }

      $oldRows = CRM_Core_Smarty::singleton()->getTemplateVars('list') ?? [];
      $newRows = $oldRows + $rows;
      $page->assign('list', $newRows);
    }
    // Add Reportplus instances to the list
    elseif ($pageName == 'CRM_Report_Page_InstanceList') {
      $report = '';

      $sql = "
          SELECT inst.id, inst.title, inst.report_id, inst.description, v.label, v.grouping, comp.name as compName, v.name as class_name
            FROM civicrm_option_group g
            LEFT JOIN civicrm_option_value v
                   ON v.option_group_id = g.id AND
                      g.name  = 'report_template'
            LEFT JOIN civicrm_report_instance inst
                   ON v.value = inst.report_id
            LEFT JOIN civicrm_component comp
                   ON v.component_id = comp.id

            WHERE v.is_active = 1
                  AND inst.domain_id = %1
                  AND comp.name = 'ReportPlus'
            ORDER BY  v.weight";

      $dao = CRM_Core_DAO::executeQuery($sql, [
        1 => [CRM_Core_Config::domainID(), 'Integer'],
      ]);

      $config = CRM_Core_Config::singleton();
      $rows = [];
      $url = 'civicrm/report/instance';
      while ($dao->fetch()) {
        $enabled = TRUE;
        //filter report listings by permissions
        if (!($enabled && CRM_Report_Utils_Report::isInstancePermissioned($dao->id))) {
          continue;
        }
        //filter report listing by group/role
        if (!($enabled && CRM_Report_Utils_Report::isInstanceGroupRoleAllowed($dao->id))) {
          continue;
        }

        if (trim($dao->title)) {
          if (isset($form->ovID)) {
            $form->title = ts("Report(s) created from the template: %1", [1 => $dao->label]);
          }
          $rows[$dao->compName][$dao->id]['title'] = $dao->title;
          $rows[$dao->compName][$dao->id]['label'] = $dao->label;
          $rows[$dao->compName][$dao->id]['description'] = $dao->description;
          $rows[$dao->compName][$dao->id]['url'] = CRM_Utils_System::url("{$url}/{$dao->id}", "reset=1");
          $rows[$dao->compName][$dao->id]['viewUrl'] = CRM_Utils_System::url("{$url}/{$dao->id}", 'force=1&amp;reset=1');
          $rows[$dao->compName][$dao->id]['actions'] = CRM_Reportplus_Utils_Report::getActionLinks($dao->id, $dao->class_name);
        }
      }

      $oldRows = CRM_Core_Smarty::singleton()->getTemplateVars('list') ?? [];
      $newRows = $oldRows + $rows;
      $page->assign('list', $newRows);
    }
    elseif ($pageName == 'CRM_Contact_Page_View_Summary') {
      // Load angular dependencies for reportplus widgets
      if (version_compare('5.40.0', CRM_Utils_System::version(), '>')) {
        $loader = new \Civi\Angular\AngularLoader();
        $loader->setModules(['reportplus']);
        $loader->load();
      }
      else {
        /** @var \Civi\Angular\AngularLoader $loader */
        $loader = \Civi::service('angularjs.loader');
        $loader->addModules(['reportplus']);
      }
    }
  }
}

/**
 * Implementation of hook_civicrm_alterContent().
 *
 * Add ChartJS script for dashlets
 */
function reportplus_civicrm_alterContent(&$content, $context, $tplName, &$object) {
  if ($tplName == "CRM/Contact/Page/DashBoard.tpl") {
    $content .= "<script src='" . E::url("js/chart.min.js") . "'></script>";
  }
}

/**
 * Implements hook_civicrm_contactSummaryBlocks().
 *
 * @link https://github.com/civicrm/org.civicrm.contactlayout
 */
function reportplus_civicrm_contactSummaryBlocks(&$blocks) {
  Civi::cache()->set(__CLASS__ . 'contact_summary_reportlets', NULL);
  $reportlets = Civi::cache()->get(__CLASS__ . 'contact_summary_reportlets');
  if ($reportlets === NULL) {
    $reportlets = civicrm_api3('ReportInstance', 'get', ['form_values' => ['LIKE' => '%contactSummaryEnabled";s:1:"1";%']])['values'];
    Civi::cache()->set(__CLASS__ . 'contact_summary_reportlets', $reportlets);
  }
  if (empty($reportlets)) {
    return;
  }
  $blocks += [
    'reportplus' => [
      'title' => ts('ReportPlus report'),
      'icon' => 'fa-file-text',
      'blocks' => [],
    ],
  ];
  foreach ($reportlets as $report) {
    $blocks['reportplus']['blocks']['report_' . $report['id']] = [
      'id' => 'report_' . $report['id'],
      'icon' => 'crm-i fa-bar-chart',
      'title' => $report['title'],
      'tpl_file' => 'CRM/Reportplus/Page/Inline/Reportplus.tpl',
      'edit' => FALSE,
      'instance_id' => $report['id'],
      'collapsible' => TRUE,
    ];
  }

}

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function reportplus_civicrm_config(&$config) {
  _reportplus_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function reportplus_civicrm_install() {
  _reportplus_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function reportplus_civicrm_enable() {
  _reportplus_civix_civicrm_enable();
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 */
//function reportplus_civicrm_preProcess($formName, &$form) {
//
//}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
//function reportplus_civicrm_navigationMenu(&$menu) {
//  _reportplus_civix_insert_navigation_menu($menu, 'Mailings', [
//    'label' => E::ts('New subliminal message'),
//    'name' => 'mailing_subliminal_message',
//    'url' => 'civicrm/mailing/subliminal',
//    'permission' => 'access CiviMail',
//    'operator' => 'OR',
//    'separator' => 0,
//  ]);
//  _reportplus_civix_navigationMenu($menu);
//}
