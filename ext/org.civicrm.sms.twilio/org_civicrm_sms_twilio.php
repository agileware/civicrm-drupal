<?php

// Load the official Twilio library
require_once 'Services/Twilio/autoload.php';
use Twilio\Rest\Client;

class org_civicrm_sms_twilio extends CRM_SMS_Provider {

  /**
   * api type to use to send a message
   * @var string
   */
  protected $_apiType = 'http';

  /**
   * provider details
   * @var string
   */
  protected $_providerInfo = [];

  public $_apiURL = "https://api.twilio.com/";

  protected $_messageType = [];
  protected $_messageStatus = [];

  /**
   * Twilio client object
   * @var Service_Twilio
   */
  protected $_twilioClient = null;

  /**
   * We only need one instance of this object. So we use the singleton
   * pattern and cache the instance in this variable
   *
   * @var object
   * @static
   */
  static private $_singleton = [];

  /**
   * Constructor
   *
   * Create and auth a Twilio session.
   * This is not needed for Twilio
   *
   * @return void
   */ 
  function __construct($provider = [], $skipAuth = TRUE) {
    // initialize vars
    $this->_apiType = CRM_Utils_Array::value('api_type', $provider, 'http');
    $this->_providerInfo = $provider;

    // Instantiate the Twilio client
    if ($this->_apiType == 'http' &&
        array_key_exists('username', $this->_providerInfo) &&
        array_key_exists('password', $this->_providerInfo)
    ) {
      $sid = $this->_providerInfo['username'];
      $token = $this->_providerInfo['password'];
      $this->_twilioClient = new Client($sid, $token);
    }

    if ($skipAuth) {
      return TRUE;
    }

    $this->authenticate();
  }

  /**
   * singleton function used to manage this object
   *
   * @return object
   */
  static function &singleton($providerParams = [], $force = FALSE) {
    $providerID = $providerParams['provider_id'] ?? NULL;
    $skipAuth   = $providerID ? FALSE : TRUE;
    $cacheKey   = (int) $providerID;

    if (!isset(self::$_singleton[$cacheKey]) || $force) {
      $provider = [];
      if ($providerID) {
        $provider = CRM_SMS_BAO_Provider::getProviderInfo($providerID);
      }
      self::$_singleton[$cacheKey] = new org_civicrm_sms_twilio($provider, $skipAuth);
    }
    return self::$_singleton[$cacheKey];
  }

  /**
   * Authenticate to the Twilio Server.
   * Not needed in Twilio
   * @return boolean TRUE
   * @access public
   * @since 1.1
   */
  function authenticate() { 
    return TRUE;
  }

  /**
   * Send an SMS Message via the Twilio API Server
   *
   * @param array the message with a to/from/text
   *
   * @return mixed SID on success or PEAR_Error object
   * @access public
   */
  function send($recipients, $header, $message, $jobID = NULL, $userID = NULL) {
    if ($this->_apiType == 'http') {
      $from = '';
      if (array_key_exists('From', $this->_providerInfo['api_params'])) {
        $from = $this->_providerInfo['api_params']['From'];
        if (preg_match('/\|/', $from)) {
          $froms = explode('|', $from);
          $key = array_rand($froms);
          $from = $froms[$key];
        }

      }

      try {
        $twilioMessage = $this->_twilioClient->messages->create(
          $header['To'],
          [
            'from' => $from,
            'body' => $message
          ]
        );
      } catch (Exception $e) {
        $errMsg = $e->getMessage();
        return PEAR::raiseError(
          $errMsg,
          $e->getCode(),
          PEAR_ERROR_RETURN
        );
      }

      $sid = $twilioMessage->sid;
      $this->createActivity($sid, $message, $header, $jobID, $userID);
      return $sid;
    }
  }

  function callback() {
    return TRUE;
  }

  function inbound() {
    $like      = "";
    $fromPhone = $this->retrieve('From', 'String');
    return parent::processInbound($fromPhone, $this->retrieve('Body', 'String'), NULL, $this->retrieve('SmsSid', 'String'));
  }

}
