<?php return array(
    'root' => array(
        'name' => 'civicrm/cronplus',
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'reference' => NULL,
        'type' => 'civicrm-ext',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'civicrm/cronplus' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'reference' => NULL,
            'type' => 'civicrm-ext',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'dragonmantank/cron-expression' => array(
            'pretty_version' => 'v3.3.3',
            'version' => '3.3.3.0',
            'reference' => 'adfb1f505deb6384dc8b39804c5065dd3c8c8c0a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../dragonmantank/cron-expression',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'mtdowling/cron-expression' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '^1.0',
            ),
        ),
        'webmozart/assert' => array(
            'pretty_version' => '1.11.0',
            'version' => '1.11.0.0',
            'reference' => '11cb2199493b2f8a3b53e7f19068fc6aac760991',
            'type' => 'library',
            'install_path' => __DIR__ . '/../webmozart/assert',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
