<table id="crm-pricesetvisibility-recurring" style="display: none;">
  <tr class="crm-price-field-form-block-pricesetvisibility-recurring">
   <td class="label"><label>{$form.recur_visibility.label}</label></td>
   <td>{$form.recur_visibility.html}</td>
  </tr>
</table>

{literal}
<script>
  CRM.$('#crm-pricesetvisibility-recurring tr').appendTo(CRM.$('form.CRM_Price_Form_Option table > tbody'));
  CRM.$('#crm-pricesetvisibility-recurring').remove();
</script>
{/literal}
