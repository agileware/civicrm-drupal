<?php

use CRM_Pricesetvisibility_ExtensionUtil as E;

/**
 * Collection of upgrade steps.
 */
class CRM_Pricesetvisibility_Upgrader extends CRM_Extension_Upgrader_Base {

  // By convention, functions that look like "function upgrade_NNNN()" are
  // upgrade tasks. They are executed in order (like Drupal's hook_update_N).

  /**
   * Run an external SQL script when the module is installed.
   */
  public function install() {
    $this->executeSqlFile('sql/install.sql');
  }

  /**
   * Remove our SQL table for field configurations.
   */
  public function uninstall() {
    CRM_Core_DAO::executeQuery('DROP TABLE civicrm_price_set_visibility');
  }

  /**
   * Convert the recur_visibility field from int to varchar
   * to allow selecting multiple values.
   *
   * @return TRUE on success
   * @throws Exception
   */
  public function upgrade_1001() {
    $this->ctx->log->info('Applying pricesetvisibility update 1001');

    CRM_Core_DAO::executeQuery('ALTER TABLE civicrm_price_set_visibility CHANGE `recur_visibility` `recur_visibility_tmp` tinyint(4) DEFAULT NULL');
    CRM_Core_DAO::executeQuery('ALTER TABLE civicrm_price_set_visibility ADD `recur_visibility` varchar(128) DEFAULT NULL');

    // @todo convert existing value
    CRM_Core_DAO::executeQuery('UPDATE civicrm_price_set_visibility SET recur_visibility = "always" WHERE recur_visibility_tmp = 1');
    CRM_Core_DAO::executeQuery('UPDATE civicrm_price_set_visibility SET recur_visibility = "onetime" WHERE recur_visibility_tmp = 2');
    CRM_Core_DAO::executeQuery('UPDATE civicrm_price_set_visibility SET recur_visibility = "recurring" WHERE recur_visibility_tmp = 3');

    // Drop the temp column
    CRM_Core_DAO::executeQuery('ALTER TABLE civicrm_price_set_visibility DROP `recur_visibility_tmp`');

    return TRUE;
  }

}
