CREATE TABLE `civicrm_price_set_visibility` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique Price Set Visibility ID',
  `price_field_id` int(10) unsigned NOT NULL COMMENT 'FK to civicrm_price_field',
  `price_field_value_id` int(10) unsigned NULL COMMENT 'FK to civicrm_price_field_value',
  `recur_visibility` varchar(128) DEFAULT NULL COMMENT 'Visibility options, separated by VALUE_SEPARATOR',
  PRIMARY KEY (`id`),
  KEY `FK_civicrm_price_set_visibility_price_field_id` (`price_field_id`),
  KEY `FK_civicrm_price_set_visibility_price_field_value_id` (`price_field_value_id`),
  CONSTRAINT `FK_civicrm_price_set_visibility_price_field_id` FOREIGN KEY (`price_field_id`) REFERENCES `civicrm_price_field` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_civicrm_price_set_visibility_price_field_value_id` FOREIGN KEY (`price_field_value_id`) REFERENCES `civicrm_price_field_value` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
