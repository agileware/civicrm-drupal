# pricesetvisibility

Provides admin options to determine priceset (field/value) visibility.

Currently the only use-case supported to allow admins to determine whether to
show a price field option for regular and/or recurring contributions. i.e. it
lets admins decide which price options to display depending on whether the donor
wants to do a one-time or recurring contribution.

This extension works well in combination with [recurringbuttons](https://lab.civicrm.org/extensions/recurringbuttons),
so that the donor selects the donation type before deciding the amount.

It also provides a few extras:

* On full pricesets (which are required for this extension to work, see 'Usage' below),
  the "Other Amount" works as it should be, deselecting any previously selected amount,
  and vice-versa.
* ~~It displays the currency inside the "Other Amount" field (for CAD/USD/EUR).~~ (disabled for now)
* It disables autocomplete on the "Other Amount" field and changes its type to "number" (better for mobile)

This extension aims to keep the admin interface simple. More complicated use-cases
should go in [fieldconditions](https://lab.civicrm.org/extensions/fieldconditions).

Initially developed by [Coop Symbiotic](https://www.symbiotic.coop/en) and
Roshani Kothari ([@roshani](https://chat.civicrm.org/civicrm/messages/@roshani)), with
funding of the [Plastic Pollution Coalition](https://www.plasticpollutioncoalition.org/).

![Screenshot](/images/screenshot.gif)

The extension is licensed under [AGPL-3.0](LICENSE.txt).

Similar extensions:

* https://civicrm.org/extensions/pricesetfrequency
* https://civicrm.org/extensions/profile-conditionals

## Requirements

* PHP v7.2+
* CiviCRM 5.latest

## Installation

Install as a regular CiviCRM extension.

## Usage

Once the extension is enabled, new settings will be visible from the price set option settings.

The contribution page must use a full priceset (not a "quick" priceset), meaning that if your
contribution page has amounts that are set directly from the "Amounts" tab, you will need to
click there link on that page to convert the priceset. Once that is done, you can manage the
options from Administer > CiviContribute > Manage Price Sets.

## Known issues

It does not currently have any support for more advanced forms of recurring payments.
We have only tested situations that have one recurring option (ex: one-time or monthly).

If you have an "Other Amount" field, please make sure that it matches exactly either
"Other Amount" or "Other". Translations are supported. If not, entering an other amount
will not be deselected correctly if a radio button is then selected.

## Roadmap

Other use-cases which might be relevant for this extension:

* Display some options only to members or non-members (see also: https://civicrm.org/extensions/group-based-pricing).

## Support

Please post bug reports in the issue tracker of this project on CiviCRM's Gitlab:  
https://lab.civicrm.org/extensions/pricesetvisibility/issues

This extension was written thanks to the financial support of organisations
using it, as well as the very helpful and collaborative CiviCRM community.

While we do our best to provide free community support for this extension,
please consider financially contributing to support or development of this
extension.

Support via Coop Symbiotic:  
https://www.symbiotic.coop/en

Coop Symbiotic is a worker-owned co-operative based in Canada. We have a strong
experience working with non-profits and CiviCRM. We provide affordable, fast,
turn-key hosting with regular upgrades and proactive monitoring, as well as custom
development and training.
