<table class="crm-form-block crm-contribution-forcerecurring">
  <tr>
    <td class="label">{$form.contribution_force_recurring.label}</td>
    <td class="content">
      {$form.contribution_force_recurring.html}
    </td>
  </tr>
</table>

{literal}
<script>
  CRM.$(function($) {
    // Move the fields to the right place
    $('#recurFields > td > table tr:last').after($('.crm-contribution-forcerecurring tr'));
    $('.crm-contribution-forcerecurring').remove();
  });
</script>
{/literal}
