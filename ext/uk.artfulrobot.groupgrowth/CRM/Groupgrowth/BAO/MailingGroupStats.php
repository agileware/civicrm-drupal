<?php
use CRM_Groupgrowth_ExtensionUtil as E;

class CRM_Groupgrowth_BAO_MailingGroupStats extends CRM_Groupgrowth_DAO_MailingGroupStats {

  /**
   * Create a new MailingGroupStats based on array-data
   *
   * @param array $params key-value pairs
   * @return CRM_Groupgrowth_DAO_MailingGroupStats|NULL
   *
  public static function create($params) {
    $className = 'CRM_Groupgrowth_DAO_MailingGroupStats';
    $entityName = 'MailingGroupStats';
    $hook = empty($params['id']) ? 'create' : 'edit';

    CRM_Utils_Hook::pre($hook, $entityName, CRM_Utils_Array::value('id', $params), $params);
    $instance = new $className();
    $instance->copyValues($params);
    $instance->save();
    CRM_Utils_Hook::post($hook, $entityName, $instance->id, $instance);

    return $instance;
  } */

}
