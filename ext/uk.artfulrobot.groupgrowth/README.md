# Group Growth v2

![Screenshot of chart](./images/groupgrowth-v2.png)

![Screenshot of chart showing detail](./images/groupgrowth-v2-detail.png)

Shows how your group has grown (or shrunk!) over time in a chart.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.3+
* CiviCRM 5.19+

## Usage

Configure the groups you want to monitor (see below).

Enable the scheduled job `Job.Collectmailinggroupstats`.

Navigate to **Contacts » Group Growth** and from there select a group.

## Similar extensions

- <https://lab.civicrm.org/extensions/mailingsubscriptions>

## Version 2

Adds a daily snapshot of stats for all *Mailing List* groups including:

- how many contacts in the group (inc. smart groups)
- how many contacts in the group are mailable (this should be very similar to the number of mailing recipients)
- how many have missing emails so won't be mailable
- how many have on hold emails and won't be mailable
- how many have 'opt-out' set so won't be mailable
- how many emails belonging to contacts in this group got put on hold on that day.
- how many emails belonging to contacts in this group were released from hold on that day.
   Note that someone put on hold, then off hold on the same day counts as 0 on holds and 1 release, because of the way the data is stored.
- how many contacts were manually added to the group that day
- how many contacts were removed from the group that day (e.g. unsubscribes)

Note that for smart groups, we can't know exactly when they became included in the group because it's not the same as a manual 'added' to group. So if you use this on a smart group, the total may rise by more than the 'manually added' number, or fall by more than the 'removed' number.

The mailable total might be different to recipient count in an actual mailing because these stats do not de-dupe by email. So if you have 2 contacts that share an email, CiviMail will send one mailing, but we will count them here as 2.

### Deprecated constant

If you had this `define` in your civicrm.settings.php file, from v1 of this extension, you can remove it now as it is no longer used:

```
define('GROUPGROWTH_GROUP_IDS', '5,123');
```
