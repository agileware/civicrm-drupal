<?php

namespace Civi\Api4;

/**
 * MailingGroupStats entity.
 *
 * Provided by the Group Growth chart extension.
 *
 * @package Civi\Api4
 */
class MailingGroupStats extends Generic\DAOEntity {

  /**
   * generateDailyStats
   *
   * @param bool $checkPermissions
   *
   * @return Generic\BasicGetAction
   */
  public static function generateDailyStats($checkPermissions = TRUE) {
    return (new \Civi\Api4\Action\MailingGroupStats\GenerateDailyStats('MailingGroupStats', 'generateDailyStats'))
      ->setCheckPermissions($checkPermissions);
  }

  /**
   * backFill
   *
   * @param bool $checkPermissions
   *
   * @return Generic\BasicGetAction
   */
  public static function backFill($checkPermissions = TRUE) {
    return (new \Civi\Api4\Action\MailingGroupStats\BackFill('MailingGroupStats', 'backFill'))
      ->setCheckPermissions($checkPermissions);
  }
}
