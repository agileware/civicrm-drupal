<?php

namespace Civi\Api4\Action\MailingGroupStats;

use Civi\Api4\Generic\Result;
use Civi\Api4\MailingGroupStats;
use Civi\Api4\Group;
use API_Exception;

/**
 * Generate stats for the given group at this point in time.
 *
 * This should be called at the end of each day.
 */
class GenerateDailyStats extends \Civi\Api4\Generic\AbstractAction {
  /**
   * Group ID to generate stats for.
   *
   * @var int
   */
  protected int $group_id = 0;

  public function _run(Result $result) {

    if (empty($this->group_id)) {
      throw new API_Exception("group_id not set in MailingGroupStats.GenerateDailyStats");
    }

    // Check the group exists.
    $group = Group::get(FALSE)->addWhere('id', '=', $this->group_id)->execute()->first();
    if (!$group) {
      throw new API_Exception("Group $this->group_id does not exist");
    }

    $isSmartGroup = (bool) $group['saved_search_id'];

    if ($isSmartGroup) {
      // Nb. The first argument is typically a CRM_Contact_BAO_Group object, but
      // only the 'id' property is used, so casting to object should work:
      try {
        \CRM_Contact_BAO_GroupContactCache::load((object) $group, FALSE);
      }
      catch (\Exception $e) {
        \Civi::log()->error("Exception while running smart group $this->group_id $group[title]");
        return;
      }
      $groupTable = 'civicrm_group_contact_cache';
      $groupTableStatusClause = "";
    }
    else {
      $groupTable = 'civicrm_group_contact';
      $groupTableStatusClause = "AND g.status = 'Added'";
    }

    $today = date('Y-m-d');

    $sql = <<<SQL
      WITH emailInfoByContact AS (
        SELECT e.contact_id,
          (COUNT(*) - SUM(on_hold > 0)) = 0 AS all_emails_on_hold,
          SUM(on_hold > 0 AND hold_date >= '$today') AS held_count,
          SUM(on_hold = 0 AND reset_date >= '$today') AS released_count
        FROM civicrm_email e
        INNER JOIN $groupTable g ON e.contact_id = g.contact_id AND g.group_id = $this->group_id $groupTableStatusClause
        WHERE (is_primary = 1 OR is_bulkmail = 1)
        GROUP BY e.contact_id
      ),
      membershipChanges AS (
        SELECT SUM(status = 'Added') additions_count,
               SUM(status = 'Removed') removals_count
        FROM civicrm_subscription_history
        WHERE group_id = $this->group_id
          AND `date` >= '$today'
      )
      SELECT
        COUNT(*) AS members_count,
        SUM(c.is_deceased) AS deceased_count,
        SUM(c.is_opt_out) AS opt_out_count,
        SUM(emailInfoByContact.contact_id IS NULL) AS missing_email_count,
        SUM(COALESCE(emailInfoByContact.all_emails_on_hold, 0)) AS on_hold_count,
        COALESCE(membershipChanges.additions_count, 0) additions_count,
        COALESCE(membershipChanges.removals_count, 0) removals_count,
        SUM(COALESCE(emailInfoByContact.held_count, 0)) AS held_count,
        SUM(COALESCE(emailInfoByContact.released_count, 0)) AS released_hold_count,
        SUM(COALESCE(c.is_deceased = 0 AND c.is_opt_out = 0 AND emailInfoByContact.all_emails_on_hold = 0, 0)) AS mailable_count

      FROM $groupTable g
      INNER JOIN civicrm_contact c ON c.id = g.contact_id AND c.is_deleted = 0
      LEFT JOIN emailInfoByContact ON emailInfoByContact.contact_id = g.contact_id
      CROSS JOIN membershipChanges
      WHERE g.group_id = $this->group_id $groupTableStatusClause
    SQL;
    $dao = \CRM_Core_DAO::executeQuery($sql);
    $dao->fetch();
    // Note: we record the exact time stats were saved here.
    $values = ['group_id' => $this->group_id, 'day_ended' => date('Y-m-d H:i:s')] + $dao->toArray();

    // We wish to replace any stats made earlier this day for this group.
    MailingGroupStats::delete(FALSE)
      ->addWhere('group_id', '=', $this->group_id)
      ->addWhere('day_ended', '>=', $today)
      ->execute();

    try {
      $statsResult = MailingGroupStats::create(FALSE)->setValues($values)->execute()->first();
    }
    catch (\Exception $e) {
      \Civi::log()->warning("GenerateDailyStats group {$this->group_id} FAILED to create stats row: " . $e->getMessage(), $values);
      $statsResult = ['error' => $e->getMessage(), 'group_id' => $this->group_id];
    }
    // For some reason the api returns it like Ymd but I prefer Y-m-d
    $statsResult['day_ended'] = $today;
    $result->exchangeArray($statsResult);

    return $result;
  }

}
