<?php

namespace Civi\Api4\Action\MailingGroupStats;

use Civi\Api4\Generic\Result;
use Civi\Api4\MailingGroupStats;
use Civi\Api4\Group;
use API_Exception;

/**
 * Generate stats for the given group prior to v2 of this extension running.
 */
class BackFill extends \Civi\Api4\Generic\AbstractAction {
  /**
   * Group ID to generate stats for.
   *
   * @var int
   */
  protected $group_id;

  public function _run(Result $result) {

    // Check the group exists.
    $group = Group::get(FALSE)->addWhere('id', '=', $this->group_id)->execute()->first();
    if (!$group) {
      throw new API_Exception("Group $this->group_id does not exist");
    }

    $isSmartGroup = (bool) $group['saved_search_id'];
    if ($isSmartGroup) {
      throw new API_Exception("Group $this->group_id is a smart group; back-filling is likely to create unhelpful results.");
    }

    $data = civicrm_api3('groupgrowth', 'get', [
      'group_id' => $this->group_id,
      'since'    => 'today - 2 years',
      'period'   => 'day',
    ])['values'] ?? [];

    // Find the earliest MailingGroupStats record we have, we'll only import things earlier than this.
    $earliest = \Civi\Api4\MailingGroupStats::get(FALSE)
      ->addSelect('day_ended')
      ->addWhere('group_id', '=', $this->group_id)
      ->addOrderBy('day_ended', 'ASC')
      ->setLimit(1)
      ->execute()->first()['day_ended'] ?? '';
    if ($earliest) {
      $earliest = substr($earliest, 0, 10);
    }

    $defaults = [
      'group_id' => $this->group_id,
      'mailable_count' => 0,
      'opt_out_count' => 0,
      'held_count' => 0,
      'on_hold_count' => 0,
      'released_hold_count' => 0,
      'missing_email_count' => 0,
    ];
    $values = [];
    foreach ($data as $row) {
      $date = date('Y-m-d', strtotime($row['period']));
      if ($date >= ($earliest ?: '3000-00-00')) continue;
      $values[] = [
        'day_ended' => "$date 23:59:00",
        'members_count' => $row['start'] + $row['gain'] - $row['loss'],
        'removals_count' => $row['loss'],
        'additions_count' => $row['gain'],
      ];
    }
    // print json_encode($values, JSON_PRETTY_PRINT);
    if ($values) {
      $r = MailingGroupStats::save(FALSE)->setRecords($values)->setDefaults($defaults)->execute();
      $result->exchangeArray($r);
    }

    return $result;
  }
}
