<?php
use CRM_Groupgrowth_ExtensionUtil as E;

/**
 * Job.Collectmailinggroupstats API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 *
 * @see https://docs.civicrm.org/dev/en/latest/framework/api-architecture/
 */
function _civicrm_api3_job_Collectmailinggroupstats_spec(&$spec) {
}

/**
 * Job.Collectmailinggroupstats API
 *
 * @param array $params
 *
 * @return array
 *   API result descriptor
 *
 * @see civicrm_api3_create_success
 *
 * @throws API_Exception
 */
function civicrm_api3_job_Collectmailinggroupstats($params) {

  // Get all mailing lists.
  $listIDs = \Civi\Api4\Group::get(FALSE)
    ->addSelect('id')
    ->addWhere('group_type:name', 'CONTAINS', 'Mailing group')
    ->execute()->column('id');
  $returnValues = [];
  foreach ($listIDs as $groupID) {
    /** @var \Civi\Api4\Generic\Result */
    $returnValues[$groupID] = \Civi\Api4\MailingGroupStats::generateDailyStats(FALSE)
      ->setGroup_id($groupID)
      ->execute()->getArrayCopy();
  }

  return civicrm_api3_create_success($returnValues, $params, 'Job', 'Collectmailinggroupstats');
}
