<?php

use CRM_Radiobuttons_ExtensionUtil as E;

return [
  'radiobuttons_amounts' => [
    'name' => 'radiobuttons_amounts',
    'type' => 'Boolean',
    'quick_form_type' => 'YesNo',
    'default' => 0,
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Enhance the Contribution Page and Event amounts?'),
    'description' => E::ts('This will change the HTML and visual of the amounts (radio) options on public payment pages.'),
    'html_attributes' => [],
    'settings_pages' => [
      'radiobuttons' => [
        'weight' => 10,
      ]
    ],
  ],
  'radiobuttons_pp' => [
    'name' => 'radiobuttons_pp',
    'type' => 'Boolean',
    'quick_form_type' => 'YesNo',
    'default' => 0,
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Enhance the Payment Processor options?'),
    'description' => E::ts('This will change the HTML and visual of the Payment Processor options on public payment pages.'),
    'html_attributes' => [],
    'settings_pages' => [
      'radiobuttons' => [
        'weight' => 20,
      ]
    ],
  ],
  'radiobuttons_fix_other_amount' => [
    'name' => 'radiobuttons_fix_other_amount',
    'type' => 'Boolean',
    'quick_form_type' => 'YesNo',
    'default' => 0,
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Fix Other Amount field?'),
    'description' => E::ts('On Contribution Page, if there is an Other Amount field and the user first selects a price option, then enters an amount, unselect the price option. If the pricefieldvisility extension is enabled, it already does this automatically.'),
    'html_attributes' => [],
    'settings_pages' => [
      'radiobuttons' => [
        'weight' => 20,
      ]
    ],
  ],
];
