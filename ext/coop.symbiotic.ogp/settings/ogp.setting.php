<?php

use CRM_Ogp_ExtensionUtil as E;

$settings = [];
$weight = 10;

$settings['ogp_title'] = [
  'name' => 'ogp_title',
  'type' => 'Boolean',
  'default' => 1,
  'html_type' => 'checkbox',
  'add' => '1.0',
  'title' => E::ts('Generate og:title?'),
  'description' => E::ts('The og:title meta tag might already be added by the CMS.'),
  'is_domain' => 1,
  'is_contact' => 0,
  'settings_pages' => [
    'ogp' => [
      'weight' => $weight++,
    ],
  ],
];

$settings['ogp_twitter'] = [
  'name' => 'ogp_twitter',
  'type' => 'String',
  'default' => '',
  'html_type' => 'text',
  'add' => '1.0',
  'title' => E::ts('Twitter handle'),
  'description' => E::ts('If set, twitter-specific meta tags will be added to pages. Example: @YourOrganization'),
  'is_domain' => 1,
  'is_contact' => 0,
  'settings_pages' => [
    'ogp' => [
      'weight' => $weight++,
    ],
  ],
];

if (CRM_Core_I18n::isMultilingual()) {
  $languages = CRM_Core_I18n::languages(true);

  foreach ($languages as $key => $label) {
    $settings['ogp_sitelogo_' . $key] = [
      'name' => 'ogp_sitelogo_' . $key,
      'type' => 'String',
      'default' => null,
      'html_type' => 'text',
      'add' => '1.0',
      'title' => E::ts('Default site logo (%1)', [1 => $label]),
      'description' => E::ts('Enter the full URL to the image, ex: http://example.org/logo.png - It might be a different logo than the logo displayed on your website, because Facebook requires that the image be at least 200x200 pixels.'),
      'is_domain' => 1,
      'is_contact' => 0,
      'settings_pages' => [
        'ogp' => [
          'weight' => $weight++,
        ],
      ],
    ];
  }
}
else {
  $settings['ogp_sitelogo'] = [
    'name' => 'ogp_sitelogo',
    'type' => 'String',
    'default' => null,
    'html_type' => 'text',
    'add' => '1.0',
    'title' => E::ts('Default site logo'),
    'description' => E::ts('Enter the full URL to the image, ex: http://example.org/logo.png - It might be a different logo than the logo displayed on your website, because Facebook requires that the image be at least 200x200 pixels.'),
    'is_domain' => 1,
    'is_contact' => 0,
    'settings_pages' => [
      'ogp' => [
        'weight' => $weight++,
      ],
    ],
  ];
}

$pcp_pages = [];

// TODO move to a function
$dao = CRM_Core_DAO::executeQuery("select p.id, cp.title from civicrm_pcp_block as p left join civicrm_contribution_page cp on (cp.id = p.target_entity_id) where p.target_entity_type = 'contribute' and p.is_active = 1");

while ($dao->fetch()) {
  $pcp_pages[$dao->id] = $dao->title;
}

// events
$dao = CRM_Core_DAO::executeQuery("select p.id, cp.title from civicrm_pcp_block as p left join civicrm_contribution_page cp on (cp.id = p.target_entity_id) where p.target_entity_type = 'event' and p.is_active = 1");

while ($dao->fetch()) {
  $pcp_pages[$dao->id] = $dao->title;
}

if (CRM_Core_I18n::isMultilingual()) {
  $languages = CRM_Core_I18n::languages(true);

  foreach ($pcp_pages as $pcp_id => $title) {
    foreach ($languages as $lang => $langlabel) {
      $settings['ogp_pcp_' . $pcp_id . '_' . $lang] = [
        'name' => 'ogp_pcp_' . $pcp_id . '_' . $lang,
        'type' => 'String',
        'default' => null,
        'html_type' => 'text',
        'add' => '1.0',
        'title' => E::ts('Default PCP logo for: %1 (%2)', [1 => $title, 2 => $langlabel]),
        'is_domain' => 1,
        'is_contact' => 0,
        'settings_pages' => [
          'ogp' => [
            'weight' => $weight++,
          ],
        ],
      ];
    }
  }
}
else {
  foreach ($pcp_pages as $pcp_id => $title) {
    $settings['ogp_pcp_' . $pcp_id] = [
      'name' => 'ogp_pcp_' . $pcp_id,
      'type' => 'String',
      'default' => null,
      'html_type' => 'text',
      'add' => '1.0',
      'title' => E::ts('Default PCP logo for: %1', [1 => $title]),
      'is_domain' => 1,
      'is_contact' => 0,
      'settings_pages' => [
        'ogp' => [
          'weight' => $weight++,
        ],
      ],
    ];
  }
}

return $settings;
