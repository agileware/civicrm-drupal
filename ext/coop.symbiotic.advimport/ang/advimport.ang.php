<?php

// This file declares an Angular module which can be autoloaded
// in CiviCRM.

return array (
  'js' => array (
    0 => 'ang/advimport.js',
    1 => 'ang/advimport/*.js',
    2 => 'ang/advimport/*/*.js',
  ),
  'css' => array (
    0 => 'ang/advimport.css',
  ),
  'partials' => array (
    0 => 'ang/advimport',
  ),
  'settings' => array (
  ),
);
