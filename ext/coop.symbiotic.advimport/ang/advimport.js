(function(angular, $, _) {

  // Declare a list of dependencies.
  angular.module('advimport', [
    'crmUi', 'crmUtil', 'ngRoute', 'crmResource'
  ]);

})(angular, CRM.$, CRM._);
