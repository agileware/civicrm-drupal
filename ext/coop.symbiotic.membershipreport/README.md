# Membership Report

![Screenshot](/images/screenshot.png)

It's the membership report you always wanted.

Given a date range, it provides the following numbers:

* New members
* Lapsed renewals (members who renewed after their membership had expired)
* Regular renewals with the same membership type
* Regular renewals but with a different membership type
* Members currently in grace
* Total active members (the sum of all of the above)
* Expired members.

All of these numbers are for the specific date range, meaning that you can go
back and time to generate reports about the membership overview at a given
time period (which most built-in CiviCRM reports cannot do).

It also supports the following use-cases:

* If the Preferred Language column is displayed, it will display a breakdown by language.
* If the Primary Membership Type column is displayed, and we have filtered for a specific sub-membership-type (ex: a division/chapter), it will display a breakdown by the primary membership type (assuming that the primary is linked to the main organization contact record, and sub-memberships are not). If multiple sub-membership-types were selected, the information is displayed in the popup. This can be very useful for displaying, for example, the status of a specific chapter members, with a breakdown of the regular, student and retired members.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.0+
* CiviCRM 5.latest

## Installation

Install as a regular CiviCRM extension.

## Usage

You can access the report under Reports > All Reports > New Report.

## Known Issues

* There are currently some hardcoded membership status IDs (grace, cancelled, etc).
* It does not make a distinction between regular and inherited memberships (maybe have a filter option for it?).
* It might not handle monthly memberships very well, if generating stats for a year, for example. It has not been tested.

## Roadmap

* Configuration options for the membership statuses? (c.f. "todo" in the code)
* It would be interesting to have an 'Export to CSV/Excel' link from the Report Audit popup.
* Maybe we should spin the Report Audit popup into its own extension, but for now I don't know if how re-usable it would really be.
* Or maybe we should get rid of the Report Audit popup and instead link to custom searches. That way it's easy to run other actions, such as export, email, add to group, etc.
* Performance improvements. The report always generates all columns. It does not respect the columns selected by the user (they might not display, but they are still calculated). Some data should also be cached because we use the same base query to calculate different columns.
* Integration with dataexplorer?
* Link from the Membership Dashboard?
* Replace the member dashboard? (generate dashlets, display dashlets on that page)

## Related extensions

If you enjoy this extension, you may also like:

* [membership churn report](https://civicrm.org/extensions/membership-churn-reports)
* [contact membership logs](https://civicrm.org/extensions/contact-membership-logs)

# Support

Please post bug reports in the issue tracker of this project on CiviCRM's Gitlab:
https://lab.civicrm.org/extensions/membershipreport/issues

This is a community contributed extension written thanks to the financial
support of organisations using it, as well as the very helpful and collaborative
CiviCRM community. It's team work. Please consider giving back either by contributing
features, documentation, helping with support, promoting the extension or by
supporting financially.

Commercial support is available through Coop SymbioTIC:  
https://www.symbiotic.coop/en

# Copyright

License: AGPL 3

Copyright (C) 2020 Mathieu Lutfy (mathieu@symbiotic.coop)  
https://www.symbiotic.coop/en

Copyright (C) 2020 Coop Symbiotic (info@symbiotic.coop)  
https://www.symbiotic.coop/en
