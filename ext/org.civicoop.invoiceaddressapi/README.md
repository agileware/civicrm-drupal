# invoiceaddressapi

Use case that led to this extension: our members log in on the website and can renew their membership, buy products and training, buy job adverts and register for events. When it comes to paying they need to be able to select an invoice address for this specific occassion. That could be their own private address, but also the address of their employer or the address of the co-operative group they are part of.

The extension provides an API: **InvoiceAddress Get** (with parameter _contact_id_) that will return all the available invoice addresses for the contact.

In the background the API will do the following:
* it will retrieve the billing addresses of the contact_id passed as parameter.
* it will retrieve all the relationships of the contact and check if the relationship type has been allowed as an invoice address relationship type (check the Settings section below).
* if the relationship is allowed, the billing addresses of the related contacts will be retrieved
* it will return all the found addresses
* the fields that will be returned depend on the settings (check the Settings section below)

The extension also introduces a specific relationship type **Invoice Address From/For** as you can see in this screenshot: 

![Screenshot](/images/relationship_type.png)

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Settings ##

The are a few settings for the Invoice Address API. You can access them with **Administer>Invoice Address API Settings**.
All settings can be seen on this screenshot: 

![Screenshot](/images/invoice_address_api_settings.png).

**Relationship Type(s) that Invoice Addresses will be presented from**
This setting shows all the relationship types that will be checked for billing addresses. So for example if the Employer/Employee relationship type is in the list, the API will look for the billing addresses of the _other_ contact in the relationship and present them as possible invoice addresses.

**Address Fields to return for Invoice Adresses**
In this setting you can select what address fields from CiviCRM will be returned with the Invoice Address API.

**Show Addresses of Location Type Billing?**
This setting controls if the addresses with the location type Billing (the _name_ of the location type has to be _Billing_) are collected with the InvoiceAddress API.
An example of an address with location type Billing can be seen in the screenshot:

![Screenshot](/images/billing.png)

Note: CiviCRM only allows one address with a location type Billing for a contact.

**Show Addresses checked as Billing Location for Contact?**
This setting controls if the addresses which have been ticked as **Billing location for this contact**. 
An example of this is in the screen shot:

![Screenshot](/images/billing_tick.png)

Note: CiviCRM allows more addresses per contact to be ticked as billing location in this way. 

### Techie talk ###
The settings are stored in the _civicrm_setting_ table and can be retrieved with
``` php
unserialize(Civi::settings()->get('invAdrApiRelTypes'))
```
In the screenshot below you can see all the relevant settings in the _civicrm_setting_ table:

![Screenshot](/images/setting_records.png)

## Requirements

* PHP v5.4+
* CiviCRM (*FIXME: Version number*)

## Installation (Web UI)

This extension has not yet been published for installation via the web UI.

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl invoiceaddressapi@https://github.com/FIXME/invoiceaddressapi/archive/master.zip
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://github.com/FIXME/invoiceaddressapi.git
cv en invoiceaddressapi
```
