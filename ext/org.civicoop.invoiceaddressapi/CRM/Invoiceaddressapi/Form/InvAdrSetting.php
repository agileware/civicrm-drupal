<?php
use CRM_Invoiceaddressapi_ExtensionUtil as E;
/**
 * Settings form for Invoice Addresses based on relationship type
 *
 * @author Erik Hommel (CiviCooP) <erik.hommel@civicoop.org>
 * @date 21 May 2018
 * @license AGPL-3.0
 */

class CRM_Invoiceaddressapi_Form_InvAdrSetting extends CRM_Core_Form {

  /**
   * Method to build the form
   */
  public function buildQuickForm() {
    // set user context if empty to avoid going back to home screen when leaving form
    $userContext = CRM_Core_Session::singleton()->readUserContext();
    if (empty($userContext)) {
      CRM_Core_Session::singleton()->pushUserContext(CRM_Utils_System::url('civicrm', ['reset => 1']));
    }
    CRM_Utils_System::setTitle(E::ts('Invoice Address API Settings'));
    // add form elements
    //$this->addEntityRef('inv_adr_relationship_type', E::ts("Relationship Type(s) that Invoice Addresses will be presented from"), [
      //'entity' => 'relationshipType',
      //'placeholder' => E::ts('- select relationship type(s) -'),
      //'multiple' => TRUE,
    //], TRUE);
    $this->add('select', 'inv_adr_relationship_type', E::ts("Relationship Type(s) that Invoice Addresses will be presented from"),
      $this->getRelationshipTypeList(), TRUE, ['multiple' => TRUE, 'class' => 'crm-select2 huge']);
    $this->add('select', 'inv_adr_columns', E::ts('Address Fields to return for Invoice Adresses'),
      $this->getColumnList(), TRUE, ['multiple' => TRUE, 'class' => 'crm-select2 huge']);
    // add buttons
    $this->addYesNo('inv_adr_billing_loc_type', E::ts('Show Addresses of Location Type Billing?'), FALSE, TRUE);
    $this->addYesNo('inv_adr_billing_check', E::ts('Show Addresses checked as Billing Location for Contact?'), FALSE, TRUE);
    $this->addButtons([
      ['type' => 'next', 'name' => E::ts('Save'), 'isDefault' => TRUE],
      ['type' => 'cancel', 'name' => E::ts('Cancel')],
    ]);
    parent::buildQuickForm();
  }

  /**
   * Method to get list of relationship types
   * @return array
   */
  public function getRelationshipTypeList():array {
    $list = [];
    try {
      $relationshipTypes = \Civi\Api4\RelationshipType::get(FALSE)
        ->addWhere('is_active', '=', TRUE)
        ->execute();
      foreach ($relationshipTypes as $relationshipType) {
        $label = $relationshipType['label_a_b'];
        if (isset($relationshipType['contact_type_a'])) {
          $label .= ' (' . $relationshipType['contact_type_a'] . ')';
        }
        $label .= ' / ' . $relationshipType['label_b_a'];
        if (isset($relationshipType['contact_type_b'])) {
          $label .= ' (' . $relationshipType['contact_type_b'] . ')';
        }
        $list[$relationshipType['id']] = $label;
      }
      asort($list);
    }
    catch (\API_Exception $ex) {
    }
    return $list;
  }

  /**
   * Method to get all address columns
   *
   * @return array
   */
  private function getColumnList(): array {
    $list = [];
    try {
      $fields = \Civi\Api4\Address::getFields(FALSE)
        ->addSelect('name', 'title')
        ->execute();
      foreach ($fields as $field) {
        if ($field['name']) {
          $list[$field['name']] = $field['title'];
        }
      }
    }
    catch (\API_Exception $ex) {

    }
    return $list;
  }

  /**
   * Method to set the default values
   */
  public function setDefaultValues() {
    $defaults['inv_adr_relationship_type'] = Civi::settings()->get('invAdrApiRelTypes');
    $defaults['inv_adr_columns'] = Civi::settings()->get('invAdrApiColumns');
    $defaults['inv_adr_billing_loc_type'] = Civi::settings()->get('invAdrApiBillingLocType');
    $defaults['inv_adr_billing_check'] = Civi::settings()->get('invAdrApiBillingCheck');
    return $defaults;
  }

  /**
   * Overridden parent method to process form once submitted
   */
  public function postProcess() {
    if (isset($this->_submitValues['inv_adr_relationship_type'])) {
      Civi::settings()->set('invAdrApiRelTypes', $this->_submitValues['inv_adr_relationship_type']);
    }
    if (isset($this->_submitValues['inv_adr_columns'])) {
      Civi::settings()->set('invAdrApiColumns', $this->_submitValues['inv_adr_columns']);
    }
    if (isset($this->_submitValues['inv_adr_billing_loc_type'])) {
      Civi::settings()->set('invAdrApiBillingLocType', $this->_submitValues['inv_adr_billing_loc_type']);
    }
    if (isset($this->_submitValues['inv_adr_billing_check'])) {
      Civi::settings()->set('invAdrApiBillingCheck', $this->_submitValues['inv_adr_billing_check']);
    }
    CRM_Core_Session::setStatus(E::ts('Invoice Address API Settings saved'), E::ts('Saved'), 'success');
  }

}
