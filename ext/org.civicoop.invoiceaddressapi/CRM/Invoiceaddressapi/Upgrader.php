<?php
use CRM_Invoiceaddressapi_ExtensionUtil as E;

/**
 * Collection of upgrade steps.
 */
class CRM_Invoiceaddressapi_Upgrader extends CRM_Extension_Upgrader_Base {

  // By convention, functions that look like "function upgrade_NNNN()" are
  // upgrade tasks. They are executed in order (like Drupal's hook_update_N).
  /**
   * Create settings for:
   *  - relationship types that can be used for invoice addresses
   *  - address columns to return
   *  - include address of location type Billing
   *  - include addresses with 'billing location for contact'
   * Create relation ship type 'invoice address for/from'
   */
  public function install() {
    Civi::settings()->set('invAdrApiRelTypes', '');
    $defaultColumns = ['street_address', 'supplemental_address_1', 'postal_code', 'city', 'country_id'];
    Civi::settings()->set('invAdrApiColumns', $defaultColumns);
    Civi::settings()->set('invAdrApiBillingLocType', TRUE);
    Civi::settings()->set('invAdrApiBillingCheck', TRUE);
    $this->createRelationshipType();
  }

  /**
   * Remove settings for relationship types that can be used for invoice addresses
   * Remove settings for invoice address columns
   */
  public function uninstall() {
    $params = [1 => ['invAdrApiRelTypes', 'String']];
    $count = CRM_Core_DAO::singleValueQuery('SELECT COUNT(*) FROM civicrm_setting WHERE name = %1', $params);
    if ($count > 0) {
      CRM_Core_DAO::executeQuery('DELETE FROM civicrm_setting WHERE name = %1', $params);
    }
    $params = [1 => ['invAdrApiColumns', 'String']];
    $count = CRM_Core_DAO::singleValueQuery('SELECT COUNT(*) FROM civicrm_setting WHERE name = %1', $params);
    if ($count > 0) {
      CRM_Core_DAO::executeQuery('DELETE FROM civicrm_setting WHERE name = %1', $params);
    }
    $params = [1 => ['invAdrApiBillingLocType', 'String']];
    $count = CRM_Core_DAO::singleValueQuery('SELECT COUNT(*) FROM civicrm_setting WHERE name = %1', $params);
    if ($count > 0) {
      CRM_Core_DAO::executeQuery('DELETE FROM civicrm_setting WHERE name = %1', $params);
    }
    $params = [1 => ['invAdrApiBillingCheck', 'String']];
    $count = CRM_Core_DAO::singleValueQuery('SELECT COUNT(*) FROM civicrm_setting WHERE name = %1', $params);
    if ($count > 0) {
      CRM_Core_DAO::executeQuery('DELETE FROM civicrm_setting WHERE name = %1', $params);
    }
  }

  /**
   * Method to create the required relationship type if it does not exist yet
   * @return void
   */
  private function createRelationshipType(): void {
    try {
      $count = \Civi\Api4\RelationshipType::get(FALSE)
        ->addWhere('name_a_b', '=', 'inv_adr_api_a_b')
        ->addWhere('name_b_a', '=', 'inv_adr_api_b_a')
        ->execute()->count();
      if ($count == 0) {
        \Civi\Api4\RelationshipType::create(FALSE)
          ->addValue('name_a_b', 'inv_adr_api_a_b')
          ->addValue('name_b_a', 'inv_adr_api_b_a')
          ->addValue('label_a_b', 'Invoice Address From')
          ->addValue('label_b_a', 'Invoice Address For')
          ->addValue('is_active', TRUE)
          ->addValue('is_reserved', TRUE)
          ->execute();
      }
    }
    catch (\API_Exception $ex) {
      Civi::log()->error(E::ts('Could not create relationship type for invoice addresses in ') . __METHOD__
        . E::ts(', error message from API4 Relationship get or create: ') . $ex->getMessage());
    }
  }

  /**
   * Set settings for relationship types that can be used for invoicing if required
   * Set settings for invoice address columns
   * Create relationship type for invoice addresses if required
   */
  public function enable() {
    $params = [1 => ['invAdrApiRelTypes', 'String']];
    $count = CRM_Core_DAO::singleValueQuery('SELECT COUNT(*) FROM civicrm_setting WHERE name = %1', $params);
    if ($count == 0) {
      Civi::settings()->set('invAdrApiRelTypes', '');
    }
    $params = [1 => ['invAdrApiColumns', 'String']];
    $count = CRM_Core_DAO::singleValueQuery('SELECT COUNT(*) FROM civicrm_setting WHERE name = %1', $params);
    if ($count == 0) {
      $defaultColumns = ['city', 'country_id', 'postal_code', 'street_address', 'supplemental_address_1'];
      Civi::settings()->set('invAdrApiColumns', serialize($defaultColumns));
    }
    $params = [1 => ['invAdrApiBillingLocType', 'String']];
    $count = CRM_Core_DAO::singleValueQuery('SELECT COUNT(*) FROM civicrm_setting WHERE name = %1', $params);
    if ($count == 0) {
      Civi::settings()->set('invAdrApiBillingLocType', TRUE);
    }
    $params = [1 => ['invAdrApiBillingCheck', 'String']];
    $count = CRM_Core_DAO::singleValueQuery('SELECT COUNT(*) FROM civicrm_setting WHERE name = %1', $params);
    if ($count == 0) {
      Civi::settings()->set('invAdrApiBillingCheck', TRUE);
    }
    $this->createRelationshipType();
  }

}
