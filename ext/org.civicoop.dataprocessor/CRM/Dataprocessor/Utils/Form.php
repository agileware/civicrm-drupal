<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

/**
 * Helper function for form elements.
 *
 * This is introduced so are sure the addDataPickerRange function is compatible with both
 * civicrm before 5.25.0 and after 5.25.0
 *
 * Class CRM_Dataprocessor_Utils_Form
 */
class CRM_Dataprocessor_Utils_Form {

  /**
   * Add a search for a range using date picker fields.
   *
   * @param CRM_Core_Form $form
   * @param string $fieldName
   * @param string $label
   * @param bool $isDateTime
   *   Is this a date-time field (not just date).
   * @param bool $required
   * @param string $fromLabel
   * @param string $toLabel
   * @param array $additionalOptions
   * @param string $to string to append to the to field.
   * @param string $from string to append to the from field.
   * @param string $additionalClass
   */
  public static function addDatePickerRange($form, $fieldName, $label, $isDateTime = FALSE, $required = FALSE, $fromLabel = 'From', $toLabel = 'To', $additionalOptions = [], $to = '_high', $from = '_low', $additionalClass='') {
    $options = [
        '' => ts('- any -'),
        0 => ts('Choose Date Range'),
      ] + CRM_Core_OptionGroup::values('relative_date_filters');

    if ($additionalOptions) {
      foreach ($additionalOptions as $key => $optionLabel) {
        $options[$key] = $optionLabel;
      }
    }
    $form->add('select',
      "{$fieldName}_relative",
      $label,
      $options,
      $required,
      ['class' => 'crm-select2 ' . $additionalClass]
    );
    $attributes = ['formatType' => 'searchDate'];
    $extra = ['time' => $isDateTime];
    $form->add('datepicker', $fieldName . $from, ts($fromLabel), $attributes, false, $extra);
    $form->add('datepicker', $fieldName . $to, ts($toLabel), $attributes, false, $extra);
  }

  /**
   * Create a single or multiple entity ref field.
   * @param string $name
   * @param string $label
   * @param array $props
   *   Mix of html and widget properties, including:.
   *   - select - params to give to select2 widget
   *   - entity - defaults to Contact
   *   - create - can the user create a new entity on-the-fly?
   *             Set to TRUE if entity is contact and you want the default profiles,
   *             or pass in your own set of links. @see CRM_Campaign_BAO_Campaign::getEntityRefCreateLinks for format
   *             note that permissions are checked automatically
   *   - api - array of settings for the getlist api wrapper
   *          note that it accepts a 'params' setting which will be passed to the underlying api
   *   - placeholder - string
   *   - multiple - bool
   *   - class, etc. - other html properties
   * @param bool $required
   *
   * @return HTML_QuickForm_Element
   */
  public static function addInExcludeEntityRef($form, $name, $label = '', $props = [], $required = FALSE) {
    // Default properties
    $props['api'] = CRM_Utils_Array::value('api', $props, []);
    $props['entity'] = CRM_Core_DAO_AllCoreTables::convertEntityNameToCamel($props['entity'] ?? 'Contact');
    $props['class'] = ltrim(($props['class'] ?? '') . ' crm-dataprocessor-filter-entityref-include-exclude');

    if (array_key_exists('create', $props) && empty($props['create'])) {
      unset($props['create']);
    }

    $defaults = [];
    if (!empty($props['multiple'])) {
      $defaults['multiple'] = TRUE;
    }
    $props['select'] = CRM_Utils_Array::value('select', $props, []) + $defaults;

    CRM_Utils_Hook::alterEntityRefParams($props, get_class($form));
    $props['data-select-params'] = json_encode($props['select']);
    $props['data-api-params'] = $props['api'] ? json_encode($props['api']) : NULL;
    $props['data-api-entity'] = $props['entity'];
    if (!empty($props['create'])) {
      $props['data-create-links'] = json_encode($props['create']);
    }
    CRM_Utils_Array::remove($props, 'multiple', 'select', 'api', 'entity', 'create');
    return $form->add('text', $name, $label, $props, $required);
  }

}
