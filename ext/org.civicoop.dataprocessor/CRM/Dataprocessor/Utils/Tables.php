<?php
/**
 * Copyright (C) 2024  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

class CRM_Dataprocessor_Utils_Tables {

  /**
   * Backwards compatibility function for retrieving all core tables.
   *
   * @return array
   */
  public function tables(): array {
    if (self::useNewAllCoreTableFunctions()) {
      return CRM_Core_DAO_AllCoreTables::tables();
    }
    return CRM_Core_DAO_AllCoreTables::getCoreTables();
  }

  /**
   * Backward compatibility function for get the full entity name.
   *
   * @param string $entity
   * @return string
   */
  public static function getDAONameForEntity(string $entity): string {
    if (self::useNewAllCoreTableFunctions()) {
      return CRM_Core_DAO_AllCoreTables::getDAONameForEntity($entity);
    }
    return CRM_Core_DAO_AllCoreTables::getFullName($entity);
  }

  /**
   * Backwards comaptibiluty function to get the entity name for a class.
   * @param string $class
   * @return string
   */
  public static function getEntityNameForClass(string $class): string {
    if (self::useNewAllCoreTableFunctions()) {
      return CRM_Core_DAO_AllCoreTables::getEntityNameForClass($class);
    }
    return CRM_Core_DAO_AllCoreTables::getBriefName($class);
  }

  private static function useNewAllCoreTableFunctions(): bool {
    static $_use;
    if ($_use === null) {
      $version = \CRM_Core_BAO_Domain::version();
      $_use = version_compare($version, '5.72', '>=');
    }
    return $_use;
  }

}
