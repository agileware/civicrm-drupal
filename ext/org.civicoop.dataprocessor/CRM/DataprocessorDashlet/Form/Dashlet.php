<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

class CRM_DataprocessorDashlet_Form_Dashlet extends CRM_DataprocessorSearch_Form_AbstractSearch {

  public function buildQuickform() {
    parent::buildQuickform();
    $this->add('hidden', 'data_processor');
    $this->setDefaults(array('data_processor' => $this->getDataProcessorName()));
  }

  /**
   * Returns the default row limit.
   *
   * @param array|null $output
   * @return int
   */
  protected static function getDefaultLimit(array $output = null): int {
    $defaultLimit = 10;
    if (!empty($output['configuration']['default_limit'])) {
      $defaultLimit = $output['configuration']['default_limit'];
    }
    return $defaultLimit;
  }


  /**
   * Returns the name of the ID field in the dataset.
   *
   * @return string|false
   */
  public function getIdFieldName(): string {
    return '';
  }

  /**
   * @return false|string
   */
  protected function getEntityTable():? string {
    return false;
  }

  /**
   * Returns the url for view of the record action
   *
   * @param $row
   *
   * @return false|string
   */
  protected function link($row):? string {
    return false;
  }

  /**
   * Returns the link text for view of the record action
   *
   * @param $row
   *
   * @return false|string
   */
  protected function linkText($row):? string {
    return false;
  }

  /**
   * Return the data processor ID
   *
   * @return String
   */
  protected function getDataProcessorName(): string {
    try {
      $dataProcessorName = CRM_Utils_Request::retrieve('data_processor', 'String');
    } catch (CRM_Core_Exception $e) {
      return '';
    }
    if (empty($dataProcessorName)) {
      $dataProcessorName = $this->controller->exportValue($this->_name, 'data_processor');
    }
    return $dataProcessorName;
  }

  /**
   * Returns the name of the output for this search
   *
   * @return string
   */
  protected function getOutputName(): string {
    return 'dashlet';
  }

  /**
   * Checks whether the output has a valid configuration
   *
   * @return bool
   */
  protected function isConfigurationValid(): bool {
    return TRUE;
  }

  /**
   * Add buttons for other outputs of this data processor
   */
  protected function addExportOutputs() {
    // Don't add exports
  }

  /**
   * Returns the size of the crireria form element.
   * There are two sizes full and compact.
   *
   * @return string
   */
  protected function getCriteriaElementSize(): string {
    return 'compact';
  }

  public function preProcess() {
    parent::preProcess();
    $this->assign('sticky_header', FALSE);
  }


}
