<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

/**
 * This class is used by the Search functionality.
 *
 *  - the search controller is used for building/processing multiform
 *    searches.
 *
 * Typically the first form will display the search criteria and it's results
 *
 * The second form is used to process search results with the associated actions.
 */
class CRM_DataprocessorSearch_Controller_CaseSearch extends CRM_DataprocessorSearch_Controller_Search {

  public function getSearchName(): string {
    return 'Search';
  }

  public function getSearchClass(): string {
    return 'CRM_DataprocessorSearch_Form_CaseSearch';
  }

  public function getComponent(): string {
    return 'Case';
  }

  public function getUrlForSearchRedirectReplacement():? string {
    return '/civicrm/case/search';
  }

  /**
   * @param string $selectedTask
   *
   * @return string|array|null
   */
  public function getTaskClass(string $selectedTask) {
    [$task] = CRM_Case_Task::getTask($selectedTask);
    if ($task) {
      return $task;
    }
    return null;
  }

}
