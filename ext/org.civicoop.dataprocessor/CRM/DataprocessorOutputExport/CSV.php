<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

use Civi\DataProcessor\FileFormat\Fileformat;

class CRM_DataprocessorOutputExport_CSV extends CRM_DataprocessorOutputExport_AbstractSpreadsheet {

  /** @var Fileformat */
  private $fileFormatClass;

  /**
   * Returns the File Format class for this spreadsheet format.
   *
   * @param array $configuration
   * @return \Civi\DataProcessor\FileFormat\Fileformat
   */
  protected function getFileFormatClass(array $configuration=[]): Fileformat {
    if (empty($this->fileFormatClass)) {
      $factory = dataprocessor_get_factory();
      $this->fileFormatClass = $factory->getFileFormatByName('csv');
    }
    return $this->fileFormatClass;
  }
  /**
   * Returns the alternate file name.
   *
   * @param array $configuration
   * @return string
   */
  protected function getAlternateFileName(array $configuration=[]):? string {
    if (isset($configuration['altfilename']) && ''!==$configuration['altfilename']) {
      return $configuration['altfilename'];
    } elseif (isset($configuration['altcsvfilename']) && ''!==$configuration['altcsvfilename']) {
      return $configuration['altcsvfilename'];
    }
    return null;
  }

}
