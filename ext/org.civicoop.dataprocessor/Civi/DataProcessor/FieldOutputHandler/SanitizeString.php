<?php

namespace Civi\DataProcessor\FieldOutputHandler;

use \Civi\DataProcessor\FieldOutputHandler\AbstractSimpleFieldOutputHandler;
use \Civi\DataProcessor\FieldOutputHandler\FieldOutput;
use CRM_Dataprocessor_ExtensionUtil as E;

class SanitizeString extends AbstractSimpleFieldOutputHandler {

  /**
   * @return string
   */
  protected function getFieldTitle() {
    return E::ts('Input field');
  }

  protected function getType() {
    return 'String';
  }


  /**
   * Returns the formatted value
   *
   * @param $rawRecord
   * @param $formattedRecord
   *
   * @return \Civi\DataProcessor\FieldOutputHandler\FieldOutput
   */
  public function formatField($rawRecord, $formattedRecord) {
    $inputField = $rawRecord[$this->inputFieldSpec->alias];

    $result = htmlspecialchars(preg_replace("/\R/", '\n', $inputField));

    return new FieldOutput($result);
  }
}