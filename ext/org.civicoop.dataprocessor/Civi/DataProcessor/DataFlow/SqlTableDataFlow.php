<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessor\DataFlow;

use Civi\DataProcessor\DataSpecification\DataSpecification;
use Civi\DataProcessor\DataSpecification\FieldSpecification;
use Civi\DataProcessor\DataSpecification\SqlFieldSpecification;

class SqlTableDataFlow extends SqlDataFlow {

  /**
   * @var string
   *   The name of the database table
   */
  protected $table;

  /**
   * @var string
   *   The alias of the database table
   */
  protected $table_alias;

  /**
   * @var string
   */
  protected $indexStatement;


  public function __construct($table, $table_alias) {
    parent::__construct();
    $this->table = $table;
    $this->table_alias = $table_alias;
  }

  public function getName() {
    return $this->table_alias;
  }

  /**
   * @param $table_alias
   *
   * @return SqlTableDataFlow
   */
  public function setTableAlias($table_alias) {
    $this->table_alias = $table_alias;
    return $this;
  }

  /**
   * @param string $indexStatement
   *
   * @return $this
   */
  public function setIndexStatement(string $indexStatement): SqlTableDataFlow {
    $this->indexStatement = $indexStatement;
    return $this;
  }

  /**
   * Returns the Table part in the from statement.
   *
   * @return string
   */
  public function getTableStatement(): string {
    return trim("`$this->table` `$this->table_alias` $this->indexStatement");
  }

  /**
   * Returns an array with the fields for in the select statement in the sql query.
   *
   * @param bool $isCountQuery
   * @return string[]
   * @throws \Civi\DataProcessor\DataSpecification\FieldExistsException
   */
  public function getFieldsForSelectStatement(bool $isCountQuery = false): array {
    $fields = array();
    foreach($this->getDataSpecification()->getFields() as $field) {
      if ($field instanceof SqlFieldSpecification) {
        $fieldStatement = $field->getSqlSelectStatement($this->table_alias, $isCountQuery);
        if (!empty($fieldStatement)) {
          $fields[] = $fieldStatement;
        }
      } else {
        $fields[] = "`{$this->table_alias}`.`{$field->name}` AS `{$field->alias}`";
      }
    }
    return $fields;
  }

  /**
   * @return string
   */
  public function getTable() {
    return $this->table;
  }

  /**
   * @return string
   */
  public function getTableAlias() {
    return $this->table_alias;
  }

}
