<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\DataProcessor\DataFlow\InMemoryDataFlow;

class AndFilter implements FilterInterface {

  /**
   * @var \Civi\DataProcessor\DataFlow\InMemoryDataFlow\FilterInterface[]
   */
  protected $filters = [];

  public function __construct(array $filters=[]) {
    $this->filters = $filters;
  }

  public function addFilter(FilterInterface $filter) {
    $this->filters[] = $filter;
  }

  public function removeFilter(FilterInterface $filter) {
    foreach($this->filters as $i => $f) {
      if ($f == $filter) {
        unset($this->filters[$i]);
      }
    }
  }

  /**
   * @param $record
   *
   * @return bool
   */
  public function filterRecord($record) {
    foreach($this->filters as $filter) {
      if (!$filter->filterRecord($record)) {
        return false;
      }
    }
    return true;
  }


}
