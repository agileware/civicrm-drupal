<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessor\DataFlow\MultipleDataFlows;

class SimpleNonRequiredJoin  extends  SimpleJoin {

  public function __construct($left_prefix = null, $left_field = null, $right_prefix = null, $right_field = null, $type = "LEFT") {
    parent::__construct($left_prefix, $left_field, $right_prefix, $right_field, $type);
  }

  /**
   * @param array $configuration
   *
   * @return \Civi\DataProcessor\DataFlow\MultipleDataFlows\JoinInterface
   */
  public function setConfiguration($configuration): JoinInterface {
    return parent::setConfiguration($configuration);
  }


}
