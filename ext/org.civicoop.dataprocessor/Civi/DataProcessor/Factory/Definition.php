<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessor\Factory;

use ReflectionClass;

class Definition {

  protected $class;

  protected $arguments = null;

  /**
   * Definition constructor.
   *
   * @param string $class
   * @param array|null $arguments
   *  Optional
   */
  public function __construct(string $class, array $arguments=null) {
    $this->class = $class;
    if (is_array($arguments)) {
      $this->arguments = $arguments;
    }
  }

  /**
   * @return object
   * @throws \ReflectionException
   */
  public function get(): object {
    $reflectionClass = new ReflectionClass($this->class);
    if ($this->arguments && $reflectionClass->getConstructor()) {
      return $reflectionClass->newInstanceArgs($this->arguments);
    } elseif ($reflectionClass->getConstructor()) {
      return $reflectionClass->newInstance();
    } else {
      return $reflectionClass->newInstanceWithoutConstructor();
    }
  }



}
