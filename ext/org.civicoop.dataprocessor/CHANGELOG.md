# Version 1.117

* Fixed issue with clicking export button

# Version 1.116

* Fixed issue with certain multi value fields on the filter configuration of a data source.
* Fixed issue with navigation menu.
* Make the 'Display the groups of a contact' output format resilient against empty inputs.

# Version 1.115

* Fixed issue with export and temporary storage.

# Version 1.114

* Minor refactor of generate PDF function. So that child classes could override the actual behaviour.

# Version 1.113

* Fixed issue with search output where all records are selected by default
* Fixed issue with exporting records not filtering before export.

# Version 1.112

* Fixed issue with API permissions.

# Version 1.111

* Fix for issue #143 error during CiviCRM upgrade. Make sure you update data processor extension before you upgrade CiviCRM.

# Version 1.110

* Added configuration for search outputs to limit and sort the task list.
* Fixed issue with Edit Participant link in participant searches. See !128 and thanks to @david124136468769
* Added two new Field output handlers for date ranges. See !127 and thanks to @gngn

# Version 1.109

* Fixed issue with running a search query twice after pressing Search
* Fixed issue with redirect after a search task.

# Version 1.108

* Fixed issue with API permissions and performance improvement.

# Version 1.107

* Fixed issue with API permissions and performance improvement.

# Version 1.106

* Fixed issue with API output and getsingle action.

# Version 1.105

* Fixed issue with in memory data flows. Such as the duplicate contacts.

# Version 1.104

* Fixed issue with in memory data flows. Such as the duplicate contacts.
* Added field for bounded calculations. Bounded calculations are calculations where you specify a minimum bound and maximum bound. If the value is outside the bounds the minimum value or maximum value is shown.

# Version 1.103

* Added contact types to contact tab output.

# Version 1.102

* Fixed issue with export output and php 8.
* Fix for deprecated function calls. See #142
* A data processor output now works in Drupal 10 after saving (without clearing the caches).

# Version 1.101

* Performance improvement.

# Version 1.100

* Fixed issue with in memory data flows. Such as the duplicate contacts.

# Version 1.99

* Fixed issue with in memory data flows. Such as the duplicate contacts.

# Version 1.98

* Fixed issue with in memory data flows. Such as the duplicate contacts.
* Fixed permission issue with Contact: Count number of cases.

# Version 1.97

* Fixed issue when a non-sql data source is used in the data processor and adding filters.
* Added a datasource for Country: Now you can sort on the country name.

# Version 1.96

* Prevent fatal PHP 8.1 error with CompareFieldFilter.
* Added output handler: Contact: Count number of cases.

# Version 1.95

* Added mixin for smarty templates. So it works with CiviCRM 5.69 and newer.
* Added error handling for the contact summary tab output. Prevent a hard failure.
* Added field for event availability information.
* Added campaign filter

# Version 1.94

* Added new field output handler for Show active memberships of a contact.

# Version 1.93

* Fixed issue with download spreadsheet export and selection of all records.

# Version 1.92

* Fixed issue #136: getSort also NULL as return
* Fixed issue with aggregated contribution data source.

# Version 1.91

* Fixed issue with aggregated contribution data source.

# Version 1.90

* Fixed issue with download spreadsheet export and selection of all records.

# Version 1.89

* Fixed TypeError in PDF Output
* Fixed issue with Timestamp fields in where clause.

# Version 1.88

* Removed PHP 8.2 warnings
* Fixed issue with exporting selected records.

# Version 1.87

* Added aggregation by receive date and id to the contribution aggrated data source.

# Version 1.86

* Added aggregation by receive date and id to the contribution aggrated data source.

# Version 1.85

* added output handlers to count and sum recurring contributions
* Fix issue for the participant role field that prevented showing multiple values.
* Added Data Processor Source for Entity FinancialTrxn.

# Version 1.84

* Reverted back the upgrader class so that it still works on Civi 5.37

# Version 1.83

* Reverted back the upgrader class so that it still works on Civi 5.37

# Version 1.82

* Fixed issue with date filter with operator not between and null values in the field.

# Version 1.81

* Fixed regression issue from !120 on CiviCRM version 5.37

# Version 1.80

* Fixed regression issue from !120 on CiviCRM version 5.37

# Version 1.79

* Fixed issue with contact search output and select all and following tasks.

# Version 1.78

* Fix for issue when filters are not set correctly.
* Fix Type Error when a filter has an empty description.
* Add option for the Custom Link with two fields to open in the same tab.

# Version 1.77

* Fix issue with Contact Data Sources in a join.
* PHP 8 and Drupal 10 compatibility. See !119 and !120

# Version 1.76

* Fix for #103: contribution search redirect.
* Added permission 'access dataprocessor definitions' that facilitates reading the DataProcessor Output Definitions without granting access CiviCRM. Created for the [Drupal CMRF Reference module](https://www.drupal.org/project/cmrf_reference).
* Fix for aggregate on case for activities.

# Version 1.75

* Added output handler: Sanitize string.
* Added output handler: Conditionally map strings.
* Fixed issue with PDF export and repeating headers.
* Fixed an error with settings the defaults in ConditionallyMapStrings. See !117

# Version 1.74

* Fixed issue with downloading the export file again after pressing the back button in the browser.

# Version 1.73

* Fixed bug with empty filter in search and then applying the defaults instead.
* Added filter: Contact has number of activities in period
* Added filter: Most recent event date

# Version 1.72

* Fixed regression bug during import.

# Version 1.71

* Possible to have certain output types multiple times at a data processor.

# Version 1.70

* Regression fix dashlets not working anymore #122
* Fixed contact Summary tab not loading due to fatal error. See !115 and #122
* Performance improvements.
* Fixed issue with Contact Search and Hard limit and paging.

# Version 1.69

* Fixed issue with expanded search form when hard limit is enabled.
* Export of selection is first stored in a temporary table. Performance improvement and fixes issue with export not containing all records.

# Version 1.68

* Fix for issue with get count api calls.

# Version 1.67

* Added filter Total Contribution Amount in Period.
* Fixed regression bug in contact search.
* Fixed regression bug with adding filters.
* Added aggregated contribution source
* Added date filter to data sources.
* Fixed issue in In- and Exclude filter contact has contributions.
* Fix for permission view contact filter.
* Added field output handler: Arthmetic Operations.
* Added field output handler for date range segments based on months.
* Fixed issue with permission to view contact. Also works now without a user record
* Added functionality to have a hard limit on a selection
* Added age filter
* Added possibility to provide index hints in a sql table data flow.

# Version 1.66

* Regression fix for issue #125.

# Version 1.65

* Regression fix for issue #125 after refactoring for filter collections.
* Added In- and Exclude to filter contact has contributions for the campaign field.
* Added multiplier to aggregate function field.
* Added found to nearest whole number to aggregate function field.

# Version 1.64

* Fixed issue with loading all records when opening a search screen or when doing an export.
* PHP 8 Compatibility: Don't pass NULL to functions that take string inputs by !114
* Added filter for contact has active recurring contributions.
* Added filter for contact has activity in period.
* PHP 8 Compatibility: Declarations of functions must be the same.
* Notices: Preventing several notices from calculated fields.
* Refactored filters (removed duplicate code)
* Added Filter Collections to group filter into a collection. Which could lead to an OR statement in the query.

# Version 1.63

* Fix for regression issue #123
* Preparation for PHP 8 compatibility
* Improved count query by removing group by and functions (such as AVG and SUM).

# Version 1.62

* Optimization of the contact data source and contact type filter.
* Added SQL Query Data Flow
* Added passing of extra data from the api or url to the filter.

# Version 1.61

* Improved on !98 filter parameter for SQL Table sources.
* Added data source Contribution (withouth Soft Contribution)

# Version 1.60.2

* Fixed regression bugs.

# Version 1.60.1

* Fixed regression bugs.

# Version 1.60

* Major refactor to search functionality. Simplified the code base and made it possible to have follow-up screens when exporting data.
* Add filter_sql parameter to sql tables. (See !98)
* Add filter for contact has contribution in period (See #120 and !112)
* Addedf filter for contact has relationship (See #121 and !113)

# Version 1.59

* Added week day filter.

# Version 1.58

* Fix error in Case Reports caused by removal of print permissions (by another extension).
* Fixed issue in Case Search. The hidden fields configuration was not respected.
* !110 PHP 8.1 compatibility - don't pull NULL to stripos
* !11 Update hook_civicrm_tabset for forward-compat with 5.57
* Make stripAlternavites (Removing the ALTERNATE text from activity detail field) also work for the API.

# Version 1.57

* When a DAO field does not have a title use the name instead.
* Fix for Legacy Custom Searches on CiviCRM older than 5.41, see #114

# Version 1.56

* Fixed regression with issue with joins.

# Version 1.55

* Fixed regression with issue with joins.

# Version 1.54

* Add stripAlternatives to MarkupFieldOutputHandler to hide the email ALTERNATE notation
* Fixed issue with Joins

# Version 1.53

* Fix for #109 sticky headers and dashlet.

# Version 1.52

* Fixed issue with event participant count field (do not count deleted contacts).

# Version 1.51

* Fixed regression with sticky row header.

# Version 1.50

* When using a search output the row header will stick upon scroll.
* Fixed when using the CSV export, the web browser can cache the downloaded CSV file with MR !103
* Provides ability to set the file name for the CSV export download, instead of using randomly generated file name  with MR !104

# Version 1.49

* Added filter for Contact Has Event of Type
* Added filter for contact has contributions with status
* Added field output handler for listing event registrations of a contact

# Version 1.48

* Fixed permission issue with Contact Type Filter
* Fixed export from Smart Groups, by manage groups --> contacts --> export CSV

# Version 1.47

* Added Field Output Handler for Text from Template.


# Version 1.46

* The TotalFieldOutputHandler now sorts numeric instead of alphabetic (so first 9 and then 10)
* Added Field Outputhandler for Custom Link with three fields by !74
* Don't emit a PHP warning on a null string with 'use label as value' by !99
* Fix CiviCRM Case, adding Case as source, the default values cause an Error, see #102

# Version 1.45

* Fixed error with Aggregated Fields (#105)
* Fixed error with CalculationFields

# Version 1.44

* Introduce AbstractFormattedNumberOutputHandler with !95

# Version 1.43

* Enable upgrade to CiviCRM 5.43 and beyond without legacycustomsearches module enabled !94.
* Prevent non-numeric value warning in the TotalFieldOutputHandler.
* DateFieldOutput DateTime Option by !93

# Version 1.42

* !101 Fix upgrade crash when upgrading to CiviCRM 5.41.0
* Documented adding dataprocessors to your extension.
* Ability to return multi-valued field as an array.

# Version 1.41

* Fixed regression bug with joins and filters on a data source.

# Version 1.40

* Fixed issue with joins and filters on a data source.

# Version 1.39

* Improvement of the layout of the relationship output handler.
* Add link from the participant count field.
* Layout of date filter.

# Version 1.38

* Fixed #96: use INNER JOIN on custom fields when a filter is set.
* Fixes issue with Relationship output field handler.

# Version 1.37.1

* Fixed regression bug with operator options in the filter.

# Version 1.37

* Added filter to compare two fields.
* Fixed #94. Renamed the calculation Percentage to Percentage change (to avoid confusion).
* Make the totals (calculations that add up) and percentages sortable.
* Fixed #91. Duplicate column name 'participant_payment_participant_id' when a Data Processor is configured to use two or more Pariticipant Sources by !85
* Fixed issue with Participant and Contribution Searches by !87
* Fixed issue with sorting and limit. #95.

# Version 1.36

* Inconsistent use of "e-mail" and "email", use the CiviCRM standard "email" by !79
* Default delimiter for CSV export is semi-colon, not comma by !81
* Fixed issue with participant payment
* Ability to return contact as array in relashionship type by !84

# Version 1.35

* Fixed #87 Dataprocessors with required contact id filters throws "One of parameters (value: ) is not of the type Int" error for anonymous users by !77
* Compatibility fix for Symfony 2.8, 3.4 and 4.0
* Fixed issue with non required join and filtering on case type.

# Version 1.34

* Fixed issue with filtering on state/province custom fields.
* Fixed issue with filtering on multi value fields (fields which holds multiple values separated by the VALUE SEPARATOR symbol).
* Fix for #85 DB Error: no such field when using "Tab on contact summary" with contributions

# Version 1.33

* Added weight to search/reports outputs.
* Fix for #71 deprecation warnings.
* Compatibility fix for Symfony ^3.4 ^4.0.

# Version 1.32

* Added Field Output Handler for clean file names.  E.g. change `Expense/2/receipt_802fd5cd009e0a39cf2202f4bfb9c0b4.pdf`
  into `receipt.pdf`.
* Fixed #77 Contribution dataprocessor with current user contact filter shows no results by !75

# Version 1.31

* Fixed backwards compatibility with joins on the edit screen.
* Add custom link handler referencing two fields. (!73 & #79)
* Fixed issue with CSV and PDF Download output on wordpress.

# Version 1.30

* Fixed issue with relative date and required filters.
* Compatibility with php 7.0
* Activity search with empty id.
* Fixed issue with aggregation on date fields #78
* Fixed issue with CSV and PDF Download output.

# Version 1.29

* Fixed issue when same field existed twice but with different names.

# Version 1.28

* Fixed #73: Aggregation on SQL Source does not always work
* Fixed #40: SQL Table data source error and #45: Allow MySQL Views as Sources. See !71
* Fixed regression bug with custom fields on activity data sources.

# Version 1.27

* Added Contribution Count Field Output Handler.
* Fixed issue with importing/exporting of data processor.
* Fixed issue with participant data source.
* Fixed issue with loading menu items.

# Version 1.26

* Fixed issue with formatting of money fields and 0 value in the formatted number field output handler.

# Version 1.25

* Fix for #68: contact search actions did not work after upgrading to 5.32.2
* Fix for #67: error when a custom field or custom groups contains an @ or % sign in the help texts.
* Fixed issue with join on contribution data source.
* Added better error handling.
* Fixed issue with aggregation fields and a subquery data flow.
* Fixed issue with backwards compatibility and field drop down.
* Fixed issue with date filter and civicrm version before 5.25
* Join when aggregation is enabled on a data source should be of type LEFT and not INNER.
* Fix for regression bug #69: Multiple Field Filter

# Version 1.24.1

* Fixed regression bug when only one activity data source is present.

# Version 1.24

* Fixed issue with sorting the data processor and case insenstivity.

# Version 1.23.8

* Fixed regression bug in Checksum filter.

# Version 1.23.7

* Fixed another regression bug with filter on certain fields which are secondary to the data source. Such as the activity record type id.

# Version 1.23.6

* Fixed regression issue with file data source and with contribution source.
* Fixed #64 aggregation on a custom field of a contribution data source works.

# Version 1.23.5

* Fixed regression bug with filter on certain fields which are secondary to the data source. Such as the activity record type id.
* Fixed regression bug #66: Dataprocessor breaks API3 explorer actions list

# Version 1.23.4

* Fixed regression bug with case role filter (and other filters).

# Version 1.23.3

* Fixed issue with data sources for file, contributions and cases.

# Version 1.23.2

* Made smart group implementation compatible with civicrm version 5.14.1

# Version 1.23.1

* Fixed issue with config.

# Version 1.23

* Refactor of how the navigation menu is build/stored. It now makes use of hook_civicrm_navigationMenu instead of storing the items in the database directly.
* Refactor of config classes.
* Fixed regression bug in getting where statements and join statements.

# Version 1.22.1

* Fixed regression bug with API output.

# Version 1.22

* Fix issue filtering for multiple tags !66
* Added aggregation for membership data source so you can aggregate on the most recent, the least recent memberships.
* Added aggregation for activity data source so you can aggregate on the most recent, the least recent activity.
* Fixed issue with Field filter and operator is not one of on a multi value field.
* Contact Search Output is now also able to create smart groups and send bulk mails (with a hidden smarty group). See #32.
* Fixed issue with Field Filter on relationship type and also made relationship type a non required filter on the data source.
* Improved performance by caching the API entities and actions configured by a data processor output. (Previously during an api call all actions and entities where retrieved from the database)
* Fixed issue when a filter is required and no default value is set and you want to set the default value.

# Version 1.21

* Added contribution ID to the participant data source so that you can link a contribution and an event registration.
* Added a field for contact tags.
* Added a data source for entity tag and tag.

# Version 1.20

* ContactInGroup filter only supports IN/NOT IN so map =/!= !65
* Fixed issue with multiple Custom Group as data source and additional filters.

# Version 1.19.1

* Fixed issue with install sql and innodb.

# Version 1.19

* Added File Data Source

# Version 1.18

* Fix broken CSV (etc) download buttons (!62)
* Fix unable to select relative date value in filter #56
* Fix issue with tasks after an Participant Search.

# Version 1.17

* Added participant count field
* Fixed custom link field so that the raw value also contains the html link (#55).
* Fix for Field filter. It did not work on a custom field that has the type country.
* Added image field output handler (!61)
* Fixed issue with View and Edit an activity in Activity Search.

# Version 1.16

* Fixed blank worldregion in CVS output #52
* Fixed issue with not empty case role filter.

# Version 1.15.1

* Fixed backwards compatibility issue.

# Version 1.15

* Added percentage calculation field to calculate the difference in percentage between two fields.
* Fixed issue with saving default filter values.
* Fixed issue with filter for multiple select fields.
* Added filter for month.

# Version 1.14.2

* Fixed issue with broken filter #51
* Added all contact types customf fields to contact data source.

# Version 1.14.1

* Fixed issue with shoreditch theme #49
* Fixed issue with custom fields with the same name as an entity field.

# Version 1.14.0

* Fixed filter options for money fields. See issue #50
* Added Field Output handler for Custom Links. !57
* Fixed issue with shoreditch theme #49

# Version 1.13.0

* Added Checksum filter.

# Version 1.12.0

* Allow to specify defaults for a search through the URL.(!46)
* Fixed several notices (!47, !48, !49, !53)
* Fixed URL of dashlet on non drupal installations (!50)
* Show title of data processor when asking for a confirmation upon deleting.
* Add support for filter defaults to be set using the URL.
* Added Filter for World Region
* Added Output for World Region
* Fixed integration with search action designer extension for the Search/Report output.
* Fixed bug with aggregation fields (#44)
* Added option to expose the hide fields setting to the user at Search/Report output (#34)

# Version 1.11.0

* Added Field Output Handler for outputting a text when a contact has a certain relationship.
* Refactored Field Output Handlers and simplified duplicate code for initializing a field.
* Date filter now also works with Date/Time fields.
* Added Is not empty as an operator to the contact filter.
* Added field to edit activity.
* Renamed Case Role filter to Contact has role on case and added option to search for case without a role set and to search for current user has the role.

# Version 1.10.0

* Added option to return URL to File Download Link Field.

# Version 1.9.1

* Fixed regression issue with MySQL function such as Year on the Date Field.

# Version 1.9.0

* Fixed issue with filtering on contact subtype.
* Fixed issue with returning after a participant task.
* Added data source for membership payments.
* Added Contact Checksum Field.
* Added header fields to the PDF export output
* Fixed bug in File Download Link Field.

# Version 1.8.0

* Added Manage Case Link field.
* Added checkbox to show Manage Case on the Case Search output.
* Fixed issue with dashlet opening in full screen.

# Version 1.7.1

* Fixed issue with cloning data processors.

# Version 1.7.0

* Fixed #35: Custom Fields on a tab are also available as field now.
* Changed Age field so aggeragation is working correctly.
* Changed Field Specification to allow more advanced mysql functions.
* Added Event Filter.
* Added Formatted Address field.
* Added data source for note
* Refactored API Output to an Abstract Class so that it is easy for extension developers to develop their own implementation.
* Added Markup/Html Field Value output field handler.
* Improved In Memory Dataflow so that joins and filters would work.
* Improved Contact Summary Tab output so it includes a count.
* Fixed caching issues on the contact data source #31.
* Fixed bugs with ContactInGroup filter #33

# Version 1.6.0

* Update to avoid using a system function that is being deprecated. (See !37)
* Fixed issue with case role field.

# Version 1.5.0

* Added relationship type order by Relationship Field Type.
* Added smart group contact data source.

# Version 1.4.0

* Search tasks (eg. Export) work with Member,Contribute,Participant,Case...
* Added source to retrieve the owner membership, when owner membership is not set (meaning it is already the primary) then it will return itself.
* Added date filter to filter date with the PHP Date Format.
* Added filtering on Contact (sub) type on the contact filter.
* Added PDF Export Output
* Added Union Query Data Flow.
* Added a field specification for a fixed value
* Fixed #24
* Improved export/import functionality.
* Added documentation generator to the API output.
* Added default sort configuration for a data processor (#26).
* Added Age field.
* Added current user to contact filter.
* Added data source for permissioned contact (#25).
* Fixed issue with configuration contact source sub type filter.
* Added a no result text to the outputs.

# Version 1.3.0

* Fixed the dashlets.
* Fixed caching issues.
* Add Recurring Contribution as datasource
* Added Field Output Handler for Is Active fields based on dates (start date and end date).
* Refactored the factory (the factory is used by developers to add data source, field outputs, outputs, filters etc.).
* Added data sources for custom groups which are a multiple data set.

# Version 1.2.0

* Made CSV Export download available for anonymous users.
* Change Group Filter so that it also works with smart groups
* Fixed bug with date filter
* Added date group by function to date output field handler.
* Added exposure of Aggregation on the Search/Report output.

**Remark for extension developers**

If you have an extension which implements an `OutputHandlerAggregate` in your _Field Output Handlers_ then you
have to implement to additional methods: `enableAggregation` and `disableAggregation`.

# Version 1.1.0

* Respect selected permissions for outputs
* Allow to specify "Is Empty" for various filters.
* Allow to limit ContactFilter to only show contacts from specific groups.
* Output a data processor as a dashboard.
* Output a data processor as a tab on the contact summary screen.
* Output a data processor as a contribution search.
* Output a data processor as a membership search.
* Added field outputs for simple calculations (substract and total).
* Added escaped output to search screens.
* Replaced the value separator in the raw field with a comma.
* Added filter to search text in multiple fields.
* Added filter for searching contacts with a certain tag.
* Added filter for searching contacts with a certain type.
* Added filter for contact has membership.
* Added filter to respect the ACL. So that a user only sees the contacts he is allowed to see.
* Removed the title attribute from the outputs as those don't make sense.
* Refactored aggregation functionality and added aggregation function field.
* Fixed issue with updating navigation after editing an output.
* Added option to expand criteria forms on search forms.
* Added a Date field.
* Added function to clone a data processor.
* Added Case ID field on the activity source.
* Added field to display relationships.
* Added is not empty as a filter operator.
* Added hidden fields option to search outputs, dashboard output and contact summary tab output.
* Added formatted number output field handler
* Added SQL Table Data Source
* Export from a search only exports the selected rows.

# Version 1.0.7

* Changed Event Participants Field Output Handler to return a string.
* Build a cache clear when a data processor configuration is changed.

# Version 1.0.6

* Performance improvement by caching the data processor and the api calls.

# Version 1.0.5

* Added error handling to importer
* Added sort in Manage data processor screen

# Version 1.0.4

* Fixed issue with activity search and actions after the search when the actions are run on all records.

# Version 1.0.3

* Fixed issue with date filters.

# Version 1.0.2

* Fixed bug #11 (Fatal error clone on non object)

# Version 1.0.1

Initial release.
