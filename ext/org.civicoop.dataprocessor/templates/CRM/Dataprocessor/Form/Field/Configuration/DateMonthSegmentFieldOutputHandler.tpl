{crmScope extensionKey='dataprocessor'}
<div class="crm-section">
    <div class="label">{$form.field.label}</div>
    <div class="content">{$form.field.html}</div>
    <div class="clear"></div>
</div>
<div class="crm-section">
  <div class="label">{$form.date_range_start.label}</div>
  <div class="content">{$form.date_range_start.html}</div>
  <div class="clear"></div>
</div>
<div class="crm-section">
  <div class="label">{$form.date_range_end.label}</div>
  <div class="content">{$form.date_range_end.html}</div>
  <div class="clear"></div>
</div>
<div class="crm-section">
  <div class="label">{$form.text_before_range.label}</div>
  <div class="content">{$form.text_before_range.html}</div>
  <div class="clear"></div>
</div>
<div class="crm-section">
  <div class="label">{$form.text_after_range.label}</div>
  <div class="content">{$form.text_after_range.html}</div>
  <div class="clear"></div>
</div>
<div class="crm-section">
  <div class="label">{$form.text_within_range.label}</div>
  <div class="content">{$form.text_within_range.html}</div>
  <div class="clear"></div>
</div>
{/crmScope}
