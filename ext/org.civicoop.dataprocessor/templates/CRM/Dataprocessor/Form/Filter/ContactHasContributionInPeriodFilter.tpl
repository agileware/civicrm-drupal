{crmScope extensionKey='dataprocessor'}
{assign var=fieldOp     value=$filter.alias|cat:"_op"}
{assign var=filterStatusIds   value=$filter.alias|cat:"_status_ids"}
{assign var=filterFinancialTypeIds   value=$filter.alias|cat:"_financial_type_ids"}
{assign var=filterCampaignIds   value=$filter.alias|cat:"_campaign_ids"}

<tr>
    <td class="label">{$filter.title}</td>
    <td>
      {if $form.$fieldOp.html}
      <span class="filter-processor-element filter-{$filter.alias}">{$form.$fieldOp.html}</span>
      <span class="filter-processor-show-close filter-{$filter.alias}">&nbsp;</span>
      {/if}
    </td>
    <td>
      {include file="CRM/Dataprocessor/Form/Filter/DateRange.tpl" fieldName=$filter.alias from='_low' to='_high'}
      <p>
      {ts}With status: {/ts}<br /><span id="{$filterStatusIds}_cell">{$form.$filterStatusIds.html}</span> <br />
      {ts}With financial type: {/ts}<br /><span id="{$filterFinancialTypeIds}_cell">{$form.$filterFinancialTypeIds.html}</span> <br />
      {ts}With campaign: {/ts}<br /><span id="{$filterCampaignIds}_cell">{$form.$filterCampaignIds.html}</span>
      </p>
      {include file="CRM/Dataprocessor/Form/Filter/InExcludeEntityRef.js.tpl"  inExcludeFieldId=$filterCampaignIds}
    </td>
</tr>
{/crmScope}
