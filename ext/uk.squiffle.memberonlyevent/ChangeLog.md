# ChangeLog

## Version 2.0

- v2.0 is a major rewrite.  It should migrate your old configuration
  but you are advised to review your settings and configuration and test thoroughly.

- The storage of settings has changed and now uses separate settings rather
  than a single, complex setting.  The upgrader should take care of this...
  
- v1 had a setting to determine whether or not a message was displayed, and a
  separate setting for the message.  This is now combined: the message will
  always be shown, so if you do not want a message then leave it blank.

- New feature: You can now specify the Membership Types to consider.  If
  configured, only Memberships of the specified types are eligible.  If left
  blank, any Membership Type (of the specified Statuses) is eligible.

## Version 1.x

- Initially developed by vakeesan@millertech.co.uk as
  https://github.com/MillerTech-CRM/uk.co.nfpservices.module.memberonlyevent