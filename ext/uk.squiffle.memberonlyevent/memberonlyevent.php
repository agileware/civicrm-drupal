<?php

require_once 'memberonlyevent.civix.php';
use CRM_Memberonlyevent_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config
 */
function memberonlyevent_civicrm_config(&$config) {
  _memberonlyevent_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function memberonlyevent_civicrm_enable() {
  _memberonlyevent_civix_civicrm_enable();
}

/**
 * check whether member only event or not
 * Implements hook_civicrm_buildForm().
 *
 * Redirect attempts to reach the registration page to the info page if restricted.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_buildForm
 */
function memberonlyevent_civicrm_buildForm($formName, &$form) {
  if ($formName == 'CRM_Event_Form_Registration_Register') {
    $eventID = $form->getEventID();
    if (!CRM_Memberonlyevent::allowedToRegister($form->getContactID(), $eventID)) {
      CRM_Utils_System::redirect(CRM_Utils_System::url('civicrm/event/info', "id=$eventID&reset=1"));
    }
  }
}

/**
 *
 * Implements hook_civicrm_pageRun().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_pageRun
 */
function memberonlyevent_civicrm_pageRun(&$page) {
  if (get_class($page) == 'CRM_Event_Page_EventInfo') {
    if (!CRM_Memberonlyevent::allowedToRegister($page->get('_contactId'), $page->getEventID())) {
      $page->assign('allowRegistration', FALSE);

      $msg = \Civi::settings()->get('memberonlyevent_message');
      if ($msg) {
        $page->assign('memberonlyevent_message', $msg);
        \CRM_Core_Region::instance('page-footer')->add([
          'template' => E::path("templates/CRM/Event/Page/EventInfoCustom.tpl"),
        ]);
      }
    }
  }
}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
function memberonlyevent_civicrm_navigationMenu(&$menu) {
  _memberonlyevent_civix_insert_navigation_menu($menu, 'Administer/CiviEvent', [
    'label' => E::ts('Member-Only Event Settings'),
    'name' => 'memberonlyevent_settings',
    'url' => 'civicrm/admin/setting/memberonlyevent',
    'permission' => 'administer CiviCRM',
  ]);
  _memberonlyevent_civix_navigationMenu($menu);
}
