<?php

class CRM_Memberonlyevent {

  /**
   * Gets list of Membership Statuses for settings form.
   *
   * @return array
   *   status_id => status_label
   */
  public static function getMembershipStatuses() : array {
    return \Civi\Api4\MembershipStatus::get(FALSE)
      ->addSelect('label')
      ->addWhere('is_active', '=', TRUE)
      ->execute()
      ->column('label', 'id');
  }

  /**
   * Gets list of Membership Types for settings form.
   *
   * @return array
   *   type_id => type_label
   */
  public static function getMembershipTypes() : array {
    return \Civi\Api4\MembershipType::get(FALSE)
      ->addSelect('name')
      ->addWhere('is_active', '=', TRUE)
      ->execute()
      ->column('name', 'id');
  }

  /**
   * Checks whether the specified contactID is allowed to register for eventID
   *
   * @param $contactID  contact id or NULL if not logged in
   * @param $eventID    event id
   *
   * @return bool
   */
  public static function allowedToRegister(?int $contactID, int $eventID) : bool {
    $isMemberOnly = \Civi\Api4\Event::get(FALSE)
      ->addSelect('member_only_event.is_member_only_event')
      ->addWhere('id', '=', $eventID)
      ->execute()
      ->first();
    // Not a members-only event - everyone allowed to register
    if (!(bool) $isMemberOnly['member_only_event.is_member_only_event'][0]) {
      return TRUE;
    }
    // Restricted & user not logged in
    if (!$contactID) {
      return FALSE;
    }
    // Get permitted statuses and types, then check for memberships,
    $permittedStatuses = \Civi::settings()->get('memberonlyevent_statuses') ?? [];
    $permittedTypes = \Civi::settings()->get('memberonlyevent_types') ?? [];
    $memberships = \Civi\Api4\Membership::get(FALSE)
      ->addWhere('contact_id', '=', $contactID)
      ->addWhere('status_id', 'IN', $permittedStatuses);
    // Permit any type if none specified
    if ($permittedTypes) {
      $memberships = $memberships->addWhere('membership_type_id', 'IN', $permittedTypes);
    }
    $memberships = $memberships->execute()
      ->countFetched();
    return (bool) $memberships;
  }

}
