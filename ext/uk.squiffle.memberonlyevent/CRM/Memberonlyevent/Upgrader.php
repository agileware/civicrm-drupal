<?php

class CRM_Memberonlyevent_Upgrader extends CRM_Extension_Upgrader_Base {

  /**
   * Convert from old settings
   */
  public function upgrade_1000() {
    $this->ctx->log->info('Starting update 1000: Convert settings');

    $old_settings = \Civi::settings()->get('custom_memberonlyevent_settings');

    if ($old_settings) {
      $old_settings = unserialize($old_settings);
      $settings = [
        'memberonlyevent_statuses' => $old_settings['memberonly_statuses'] ?? [],
        'memberonlyevent_message' => $old_settings['memberonly_message'] ?? '',
      ];
      \Civi::settings()->add($settings)->revert('custom_memberonlyevent_settings');
    }

    $this->ctx->log->info('Finishing update 1000');
    return TRUE;
  }

}
