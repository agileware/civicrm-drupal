### Changelog

#### 1.9 (2024-03-13)
* Fix broken upgrader.
* New & Improved user interface; finally looks and works the same regardless of CMS/CiviCRM theme.

#### 1.8.2 (2024-02-07)
* Fix broken install.
* Civix upgrade.

#### 1.8 (2024-01-08)
* Fixes issue editing templates where the system default plaint-text is NULL.
* System default revisions.

#### 1.7.1 (2023-12-25)
* Fixes upgrade issue on MariaDB.

#### 1.7 (2023-12-24)
* Improved symbol management and variables list.
* Preview empty text using html2text.
* Translations and drafts.
* Various UI improvements.
* Visual Tour.

#### 1.6 (2023-09-19)
* Replaced standard textarea control in *Message* pane with the *Monaco* code editor.
  * Same code editor that powers VS Code.
  * Provides HTML syntax highlighting.
  * Custom actions to refresh current tool, and to show current position within *Outline* tool.
  * Personal preference to select wordwrap mode.

#### 1.5 (2023-08-27)
* Headers & Footers
  * Defines {header} and {footer} tokens for all active header/footer mailing components, that can be used wherever tokens are available.
  * Adds global settings to *Better Message Templates* to select a header and/or footer to automatically inject into system workflow messages only.
  * Inspired by an earlier extension from Fuzion, written by Jitendra Purohit. Used with permission.
* Improved *Outline* tool.
  * New smaller, faster, smarter template parser.
  * Style changes make it easier to read.
  * Various UX tweaks.
* Fixed issue creating revisions table in mariadb <10.5.2.

#### 1.4 (2023-07-24)
* Improved sample management.
  * All sample names are immediately available for selection. No more *10 at a time* entity lookup.
  * Shows how many samples are available as placeholder.
  * Easier to create, rename, and delete samples.
  * Now saves separate from message template.
  * Updated documentation on how to curate quality reference samples.
* *Compare* tool improvements.
  * Now line based instead of character based comparision, makes it much easier to understand how things changed and how they would be reverted.
  * Revision support added...
  * Initializes from database logging table if enabled, otherwise with current versions.
  * Revisions are saved no matter what saved them; e.g. *btrmsgtpl*, *message_admin*, *OG editor*.
  * Rename and delete revisions to help better manage history.
  * Revert entire revision, or individual changes, same as with *System Default*.
* Buttons in the *Sample*, *Message*, and *Tool* panes have been replaced with <code><i class="crm-i fa-bolt"></i>&nbsp;Action<i class="crm-i fa-caret-down" style="margin-left:2rem"></i></code> controls.
* Updated and improved documentation.
  * Requires <a href="https://civicrm.org/extensions/extension-readme" target="_blank">Extension README</a> extension (v1.1 or newer) to view in the CiviCRM message template administrative page.

---

#### 1.3 (2023-05-01)
* Injects, as well as can, into the new *Message Administration (aka message_admin)* core extension.
* Fixes issue parsing tokens inside Smarty tags with *Outline* tool.
* Smarty errors during *Preview* are displayed. Usually include a line number, so...
* Provides *Goto* line number functionality in the *Message* pane...
* And adds a personal preference *Goto error* to make it automatic.
* Improved *Message* pane scrolling.

---

#### 1.2 (2023-04-19)
* Added functional tab to the standard CiviCRM message template admin page.
  * Search all message templates, user-driven and system workflow, for specific text.
  * Personal preferences.
  * Outdated token use warnings.
  * Indication of modified status and number of samples available.
  * Built in documentation.
* No hassle maximized editor mode.
* Many other UI improvements/tweaks.

---

#### 1.1 (2022-08-02)
* Links added to standard interface in order to access *Better Message Templates* are now CMS friendly.
* Misc style tweaks.
* Maybe a little easier to configure pinned position.
* Better handling and updating of right hand pane.
* Compare/revert to default template.

---

#### 1.0 (2022-07-17)
* Initial release.

---

[Contents](toc.md)