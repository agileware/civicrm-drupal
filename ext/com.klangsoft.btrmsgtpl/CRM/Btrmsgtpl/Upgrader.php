<?php
use CRM_Btrmsgtpl_ExtensionUtil as E;

/**
 * Collection of upgrade steps.
 */
class CRM_Btrmsgtpl_Upgrader extends CRM_Extension_Upgrader_Base {

  /**
   * Add revision support for system workflow message templates.
   *
   * @return TRUE on success
   */
  public function upgrade_1400() {
    $this->ctx->log->info(E::ts('btrmsgtpl: adding revision support for system workflow message templates.'));
    return btrmsgtpl_add_revision_support();
  }

  public function upgrade_1700() {
    $this->ctx->log->info(E::ts('btrmsgtpl: adding language support for system workflow message templates revisions.'));
    
    CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_msg_template_revisions`
      ADD COLUMN `language` varchar(5) NOT NULL DEFAULT '_std_' COMMENT 'Locale of the revision',
      ADD INDEX (`language`)
    ");

    CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_msg_template_revisions`
      CHANGE COLUMN `msg_tpl_id` `msg_template_id` int unsigned NOT NULL COMMENT 'Message Template ID',
      RENAME INDEX `msg_tpl_id` TO `msg_template_id`
    ");

    $version = CRM_Utils_SQL::getDatabaseVersion();
    if (stripos($version, 'mariadb') === FALSE) {
      CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_msg_template_revisions`  
        ALTER INDEX `msg_template_id` VISIBLE
      ");
    } else {
      CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_msg_template_revisions`  
        ALTER INDEX `msg_template_id` NOT IGNORED
      ");
    }

    return TRUE;
  }

  public function upgrade_1800() {
    $this->ctx->log->info(E::ts('btrmsgtpl: adding revision support for default system workflow message templates.'));  

    CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_msg_template_revisions`
      ADD COLUMN `git_hash` varchar(40) DEFAULT NULL COMMENT 'Commit ID default template changed',
      ADD COLUMN `notes` varchar(2048) DEFAULT NULL COMMENT 'Notes/commit message for the revision'
    ");

    return TRUE;
  }
  
  public function upgrade_1801() {
    $this->ctx->log->info(E::ts('btrmsgtpl: remove errant system default revisions.'));

    CRM_Core_DAO::executeQuery("DELETE FROM `civicrm_msg_template_revisions` WHERE `git_hash` IS NOT NULL");

    return TRUE;
  }

  public function upgrade_1802() {
    //$this->ctx->log->info(E::ts('btrmsgtpl: checking/fixing broken revisions table.'));

    /* $dao = CRM_Core_DAO::executeQuery("SHOW COLUMNS FROM `civicrm_msg_template_revisions` LIKE 'language'");

    if (!$dao->fetch()) {
      CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_msg_template_revisions`
        ADD COLUMN `language` varchar(5) NOT NULL DEFAULT '_std_' COMMENT 'Locale of the revision',
        ADD COLUMN `git_hash` varchar(40) DEFAULT NULL COMMENT 'Commit ID default template changed',
        ADD COLUMN `notes` varchar(2048) DEFAULT NULL COMMENT 'Notes/commit message for the revision',
        ADD INDEX (`language`)
      ");
    } */

    return TRUE;
  }

  public function upgrade_1900() {
    $this->ctx->log->info(E::ts('btrmsgtpl: checking/fixing broken revisions table.'));

    $dao = CRM_Core_DAO::executeQuery("SHOW COLUMNS FROM `civicrm_msg_template_revisions` LIKE 'language'");

    if (!$dao->fetch() || !$dao->Field) {
      CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_msg_template_revisions`
        ADD COLUMN `language` varchar(5) NOT NULL DEFAULT '_std_' COMMENT 'Locale of the revision',
        ADD INDEX (`language`)
      ");
    }

    $dao = CRM_Core_DAO::executeQuery("SHOW COLUMNS FROM `civicrm_msg_template_revisions` LIKE 'git_hash'");

    if (!$dao->fetch() || !$dao->Field) {
      CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_msg_template_revisions`
        ADD COLUMN `git_hash` varchar(40) DEFAULT NULL COMMENT 'Commit ID default template changed',
        ADD COLUMN `notes` varchar(2048) DEFAULT NULL COMMENT 'Notes/commit message for the revision'
      ");
    }

    return TRUE;
  }

}
