<?php
use CRM_Btrmsgtpl_ExtensionUtil as E;

class CRM_Btrmsgtpl_BAO_MessageTemplateData extends CRM_Btrmsgtpl_DAO_MessageTemplateData {

  /**
   * Create a new MessageTemplateData based on array-data
   *
   * @param array $params key-value pairs
   * @return CRM_Btrmsgtpl_DAO_MessageTemplateData|NULL
   */
  public static function create($params) {
    $className = 'CRM_Btrmsgtpl_DAO_MessageTemplateData';
    $entityName = 'MessageTemplateData';
    $hook = empty($params['id']) ? 'create' : 'edit';

    CRM_Utils_Hook::pre($hook, $entityName, CRM_Utils_Array::value('id', $params), $params);
    $instance = new $className();
    $instance->copyValues($params);
    $instance->save();
    CRM_Utils_Hook::post($hook, $entityName, $instance->id, $instance);

    return $instance;
  }

}
