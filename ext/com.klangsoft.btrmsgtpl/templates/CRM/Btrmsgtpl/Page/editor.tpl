<nav class="navbar navbar-expand">
  <ul class="navbar-nav">
    <!-- language -->
    <li class="nav-item ms-2" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-title="{ts}Template Variation{/ts}">
      <select name="l" id="language">
        <option value="_std_">{ts}Standard{/ts}</option>
        <option value="-" disabled>-</option>
      </select>
    </li>
    <!-- format -->
    <li class="nav-item" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-title="{ts}Message Format{/ts}">
      <select name="f" id="format">
        <option value="msg_html">{ts}HTML{/ts}</option>
        <option value="msg_text">{ts}Plain-Text{/ts}</option>
        <option value="msg_subject">{ts}Subject{/ts}</option>
      </select>
    </li>
    <!-- template actions-->
    <li class="nav-item ms-2" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-title="{ts}Template Actions{/ts}">
      <select name="ma" id="template-actions" class="actions">
        <option value="language_add" data-icon="fa-solid fa-plus">{ts}Add Translation...{/ts}</option>
        <option value="draft_create" data-icon="fa-solid fa-plus">{ts}Create Draft{/ts}</option>
        <option value="draft_activate" data-icon="fa-solid fa-rocket">{ts}Activate Draft{/ts}</option>
        <option value="template_save" data-icon="fa-solid fa-floppy-disk">{ts}Save Template{/ts}</option>
        <option value="template_delete" data-icon="fa-solid fa-trash">{ts}Delete Template...{/ts}</option>
      </select>
    </li>
    <div class="vr mx-2"></div>

    <!-- tool -->
    <li class="nav-item ms-2" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-title="{ts}Tool{/ts}">
      <select id="tool">
        <option value="outline" data-icon="fa-solid fa-bars-staggered">{ts}Outline{/ts}</option>
        <option value="preview" data-icon="fa-solid fa-eye">{ts}Preview{/ts}</option>
        <option value="compare" data-icon="fa-solid fa-code-compare">{ts}Compare{/ts}</option>
      </select>
    </li>

    <!-- outline -->
      <!-- actions -->
      <li data-tool="outline" class="nav-item ms-2" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-title="{ts}Outline Actions{/ts}">
        <select name="oa" id="outline-actions" class="actions">
          <option value="outline_expand" data-icon="fa-solid fa-expand">{ts}Expand All{/ts}</option>
          <option value="outline_collapse" data-icon="fa-solid fa-compress">{ts}Collapse All{/ts}</option>
          <option value="-">-</option>
          <option value="variable_refresh" data-icon="fa-solid fa-refresh">{ts}Refresh Symbols{/ts}</option>
        </select>
      </li>

    <div data-tool="outline" class="vr mx-2"></div>

    <li data-tool="outline" class="nav-item me-2" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-title="{ts}Symbols{/ts}">
      <select name="s" id="symbols"><option></option></select>
    </li>

    <li data-tool="outline" class="nav-item mx-1" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-title="{ts}Previous Reference{/ts}">
      <button id="symbol-prev" class="btn btn-sm btn-primary" disabled><i class="fa-solid fa-chevron-up"></i></button>
    </li>
    <li data-tool="outline" class="nav-item mx-1" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-title="{ts}Next Reference{/ts}">
      <button id="symbol-next" class="btn btn-sm btn-primary" disabled><i class="fa-solid fa-chevron-down"></i></button>
    </li>
    <li data-tool="outline" class="nav-item mx-1">
      <span id="symbol-count" class="align-middle"></span>
    </li>
    <li class="nav-item mx-1">
      <div id="you-are-here" class="d-none"></div>
    </li>

    <!-- preview -->
      <!-- actions -->
      <li data-tool="preview" class="nav-item" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-title="{ts}Preview Actions{/ts}">
        <select name="pa" id="preview-actions" class="actions">
          <option value="sample_save" data-icon="fa-solid fa-floppy-disk">{ts}Save Sample{/ts}</option>
          <option value="sample_rename" data-icon="fa-solid fa-floppy-disk">{ts}Save Sample As...{/ts}</option>
          <option value="sample_delete" data-icon="fa-solid fa-trash">{ts}Delete Sample...{/ts}</option>
          <option value="sample_refresh" data-icon="fa-solid fa-refresh">{ts}Refresh Samples{/ts}</option>
          <option value="-">-</option>
          <option value="email_send" data-icon="fa-solid fa-paper-plane">{ts}Send Email...{/ts}</option>
        </select>
      </li>

    <div data-tool="preview" class="vr mx-2"></div>

    <li data-tool="preview" class="nav-item me-2" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-title="{ts}Sample{/ts}">
      <select id="sample"><option></option></select>
    </li>


    <!-- needed to keep toolbar same height for all tools -->
    <li data-tool="preview" class="nav-item">
      <button class="btn btn-sm btn-secondary" style="visibility:hidden"><i class="fa-solid fa-chevron-up"></i></button>
    </li>

    <!-- compare -->
      <!-- actions-->
      <li data-tool="compare" class="nav-item" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-title="{ts}Compare Actions{/ts}">
        <select name="ca" id="compare-actions" class="actions">
          <option value="revision_revert" data-icon="fa-solid fa-undo">{ts}Revert Revision{/ts}</option>
          <option value="revision_rename" data-icon="fa-solid fa-pencil">{ts}Rename Revision...{/ts}</option>
          <option value="revision_delete" data-icon="fa-solid fa-trash">{ts}Delete Revision...{/ts}</option>
          <option value="revision_refresh" data-icon="fa-solid fa-refresh">{ts}Refresh Revisions{/ts}</option>
        </select>
      </li>
    <div data-tool="compare" class="vr mx-2"></div>

    <li data-tool="compare" class="nav-item me-2" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-title="{ts}Revision{/ts}">
      <select id="revision"></select>
      <div id="default-info" class="text-bg-secondary p-2 z-3 rounded"></div>
    </li>

    <li data-tool="compare" class="nav-item mx-1" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-title="{ts}Side-By-Side{/ts}">
      <div class="form-check form-switch">
        <input type="checkbox" class="form-check-input" role="switch" id="side-by-side">
      </div>
    </li>
    <li data-tool="compare" class="nav-item mx-1" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-title="{ts}Previous Difference{/ts}">
      <button id="diff-prev" class="btn btn-sm btn-primary"><i class="fa-solid fa-chevron-up"></i></button>
    </li>
    <li data-tool="compare" class="nav-item mx-1" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-title="{ts}Next Difference{/ts}">
      <button id="diff-next" class="btn btn-sm btn-primary"><i class="fa-solid fa-chevron-down"></i></button>
    </li>
    <li data-tool="compare" class="nav-item mx-1">
      <span id="diff-count" class="align-middle"></span>
    </li>
  </ul>
</nav>

<hr class="separator" />

<div class="row" id="tools">
  <!-- monaco -->
  <div class="col">
    <div id="monaco" class="m-2 me-0"></div>
  </div>
  <!-- tool -->
  <div class="col">
    <div id="other-tool" class="m-2 ms-0">
      <!-- outline -->
      <div data-tool="outline" id="outline" class="mt-2 btrmsgtpl-outline-container"></div>
      <!-- preview -->
      <div data-tool="preview" id="sampler" class="mt-2"></div>
      <iframe data-tool="preview" id="preview" class="mt-2"></iframe>
      <!-- compare -->
      <div data-tool="compare" id="compare" class="ms-2 mt-2"></div>
    </div>
  </div>
</div>
