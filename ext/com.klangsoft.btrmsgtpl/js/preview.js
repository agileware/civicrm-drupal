(function(_event, _language, _cymbal) {

  // debounce
  let __lang = null;
  let __format = null;

  // import needed vars
  const { tpl } = btrmsgtpl.vars;

  // preview actions
  const PA = btrmsgtpl.actions('preview-actions');

  PA._disable('sample_save', true)
    ._disable('sample_rename', true)
    ._disable('sample_delete', true)
    ._disable('email_send', true);

  const $preview = $('#preview');
  const $sampler = $('#sampler');

  let _sampler = null;
  let _sample = null;
  let _didModifyThisTime = false;

  const $sample = $('#sample');

  $sample.select2({
    allowClear: true,
    dropdownAutoWidth: true,
    width: 'unset',
    placeholder: '', // gets replaced by...
    templateSelection: function(state) {
      if (state.id === '') {
        return ts('%1 available', { 1: $sample.find('option:not(:disabled)').length - 1 });
      }
      if (!PA.sample_save.hasClass('modified')) {
        return state.text;
      }
      return $(`<span class="modified">${state.text}</span>`);
    }
  });

  /***********
   * HELPERS *
   ***********/

  function getSample() {
    if (!_sample) {
      return false;
    }
    const symbols = _cymbal.list();
    const sample = {};

    symbols.forEach((sym) => {
      const name = sym.substr(1);
      if (_sample.msg_variables.hasOwnProperty(name)) {
        sample[name] = _sample.msg_variables[name];
      }
    });
    sample.tokenContext = _sample.msg_variables.tokenContext;
    
    return sample;
  }

  function updateSample() {
    const before = getSample();

    const symbols = _cymbal.list();
    try {
      const pos = _sampler.getPosition();
      const edit = JSON.parse(_sampler.getValue());
      const update = {};
  
      symbols.forEach((sym) => {
        const name = sym.substr(1);
        if (edit.hasOwnProperty(name)) {
          _sample.msg_variables[name] = update[name] = edit[name];
        }
      });
      _sample.msg_variables.tokenContext = update.tokenContext = (edit.hasOwnProperty('tokenContext') ? edit.tokenContext : {});
      _sampler.setValue(JSON.stringify(update, null, 2));
      _sampler.setPosition(pos);
  
      if (JSON.stringify(before) !== JSON.stringify(update)) {
        PA._modify('sample_save', true)
          ._disable('sample_save', false);
        $sample.trigger('change.select2');
      }
      preview(update);
    }
    catch (err) {
      preview(ts('Sample data is not valid JSON.'));
    }
  }

  function preview(tplParams) {
    $preview.css('border-color', 'silver').data('modified', false);
    _didModifyThisTime = false;

    if (typeof tplParams === 'string') {
      $preview.attr('srcdoc', tplParams);
      return;
    }
    if (!tplParams) {
      tplParams = getSample();
    }
    if (!tplParams) {
      $preview.attr('srcdoc', ts('Select a sample for preview.'));
      return;
    }

    $preview.attr('srcdoc', ts('Loading preview...'));

    const { content } = _language.val();
    const format = btrmsgtpl.format();

    btrmsgtpl.api3('MessageTemplateData', 'preview', {
      id: tpl.id,
      source: content[format].getValue(),
      source_id: format,
      tplParams
    })
      .then((result) => {
        if (result[format]) {
          if (format === 'msg_text') {
            result[format] = result[format].replace(/\n/g, '<br />');
          }
          $preview.attr('srcdoc', result[format]);
        }
      });
  }

  /******************
   * EVENT HANDLERS *
   ******************/
  
  /**
   * action_email_send
   */
  _event.on('action_email_send', (evt, actions) => {
    if (!_sample) {
      return;
    }

    btrmsgtpl.prompt(ts('Send Email'), ts('Enter recipient email address'), '')
      .then((email) => {
        if (email) {
          const $busy = PA._busy('Sending email...');
  
          const { content } = _language.val();
          const format = btrmsgtpl.format();
      
          btrmsgtpl.api3('MessageTemplateData', 'send', {
            email,
            tplParams: getSample(),
            msg_html: content.msg_html.getValue(),
            msg_text: content.msg_text.getValue(),
            msg_subject: content.msg_subject.getValue(),
          })
            .then((result) => {
              PA._busy($busy);
  
              if (result.is_error === 0) {
                btrmsgtpl.success(ts('Email has been sent.'));
              }
              else {
                btrmsgtpl.error(result.error_message);
              }
            });
        }
      });
  });
  /**
   * action_sample_delete
   */
  _event.on('action_sample_delete', (evt, actions) => {
    if (!_sample) {
      return;
    }

    btrmsgtpl.confirm(ts('Delete Sample?'), `<code>${_sample.sample_name}</code>`, {
      primary: ts('Yes'),
      secondary: ts('No')
    })
      .then((confirm) => {
        if (confirm) {
          const $busy = PA._busy(ts('Deleting sample...'));
  
          btrmsgtpl.api3('MessageTemplateData', 'delete', { id: _sample.id })
            .then((result) => {
              PA._busy($busy);
  
              if (result.is_error === 0) {
                $sample.find(':selected').remove();
                $sample.val(null).trigger('change');
  
                btrmsgtpl.success(ts('Sample has been deleted.'));
              }
              else {
                btrmsgtpl.error(result.error_message);
              }
            });
        }
      });
  });
  /**
   * action_sample_refresh
   */
  _event.on('action_sample_refresh', (evt, actions) => {

    const $busy = PA._busy(ts('%1 samples...', {
      1: (actions ? ts('Refreshing') : ts('Fetching'))
    }));

    btrmsgtpl.api3('MessageTemplateData', 'get', {
      sequential: 1,
      msg_template_id: tpl.id,
      return: ['id', 'sample_name', 'is_capture'],
      options: {
        sort: 'created_date DESC',
        limit: 0
      }
    })
      .then((result) => {
        if (result.is_error === 0) {
          PA._busy($busy);

          const id = $sample.val();
          $sample.empty();
          $('<option>').appendTo($sample); // placeholder

          const captures = result.values.filter((sample) => sample.is_capture === '1');
          if (captures.length > 0) {
            $('<option>')
              .text(ts('Recent Captures'))
              .addClass('capture')
              .prop('disabled', true)
              .appendTo($sample);

            captures.forEach((sample) => {
              $('<option>')
                .text(sample.sample_name)
                .val(sample.id)
                .addClass('capture')
                .appendTo($sample);
            });
          }
          const refs = result.values
            .filter((sample) => sample.is_capture !== '1')
            .sort((a, b) => a.sample_name.localeCompare(b.sample_name));

          if (refs.length > 0) {
            $('<option>')
              .text(ts('Reference Samples'))
              .addClass('sample')
              .prop('disabled', true)
              .appendTo($sample);

            refs.forEach((sample) => {
              $('<option>')
                .text(sample.sample_name)
                .val(sample.id)
                .addClass('sample')
                .appendTo($sample);
            });
          }
          $sample.val(id);
          setTimeout(() => $sample.trigger('change.select2'), 10); // stupid we have to do this

          if (actions) {
            btrmsgtpl.success(ts('Samples have been refreshed.'));``
          }
        }
        else {
          btrmsgtpl.error(result.error_message);
        }
      });
  });
  /**
   * action_sample_rename
   */
  _event.on('action_sample_rename', (evt, actions) => {
    if (!_sample) {
      return;
    }

    btrmsgtpl.prompt(ts('Save Sample As'), ts('Enter a new name for the sample'), _sample.sample_name)
      .then((alias) => {
        if (alias && alias.localeCompare(_sample.sample_name) !== 0) {
          const $busy = PA._busy(ts('Saving sample...'));

          btrmsgtpl.api3('MessageTemplateData', 'create', {
            id: _sample.id,
            sample_name: alias,
            msg_variables: JSON.stringify(_sample.msg_variables),
            is_capture: 0
          })
            .then((result) => {
              PA._busy($busy);

              if (result.is_error === 0) {
                $sample.find(':selected').remove();
                $renamed = $('<option>')
                  .val(_sample.id)
                  .text(alias)
                  .addClass('sample')
                  .appendTo($sample);
              
                $sample.find('.sample').each(function() {
                  const $opt = $(this);
                  if ($opt.prop('disabled') === false && $opt.text().localeCompare(alias) > 0) {
                    $renamed.insertBefore($opt).addClass('sample');
                    return false;
                  }
                });
                PA._modify('sample_save', false)
                  ._disable('sample_save', true);
                $sample.val(_sample.id).trigger('change.select2');
            
                btrmsgtpl.success(ts('Sample has been saved.'));
              }
              else {
                btrmsgtpl.error(result.error_message);
              }
            });
        }
      });
  });
  /**
   * action_sample_save
   */
  _event.on('action_sample_save', (evt, actions) => {
    if (!_sample) {
      return;
    }
    const $busy = PA._busy(ts('Saving sample...'));

    btrmsgtpl.api3('MessageTemplateData', 'create', {
      id: _sample.id,
      msg_variables: JSON.stringify(_sample.msg_variables)
    })
      .then((result) => {
        PA._busy($busy);

        if (result.is_error === 0) {
          PA._modify('sample_save', false)
            ._disable('sample_save', true);
          $sample.trigger('change.select2');

          btrmsgtpl.success(ts('Sample has been saved.'));
        }
        else {
          btrmsgtpl.error(result.error_message);
        }
      });
  });
  /**
   * btrmsgtpl_resize
   */
  _event.on('btrmsgtpl_resize', (evt, height) => {
    $preview.height(height - $preview.position().top - 16);
    if (_sampler) {
      _sampler.layout();
    }
  });
  /**
   * format_select
   */
  _event.on('format_select', (evt, format, model) => {
    if (format === __format) {
      return;
    }
    __format = format;

    preview();
  });
  /**
   * language_select
   */
  _event.on('language_select', (evt, lang, content) => {
    if (lang === __lang) {
      return;
    }
    __lang = lang;
    __format = null;
  });
  /**
   * monaco_modified
   */
  _event.on('monaco_modified', () => {
    $preview.css('border-color', 'firebrick').data('modified', true);
  });
  /**
   * monaco_refresh
   */
  _event.on('monaco_refresh', (evt, editor) => {
    __format = null;
    btrmsgtpl.$format.trigger('change');
  });
  /**
   * $sample.change
   */
  $sample.on('change', () => {
    const id = $sample.val();

    PA._disable('sample_save', true)
      ._modify('sample_save', false)
      ._disable('sample_rename', !id)
      ._disable('sample_delete', !id)
      ._disable('email_send', !id);

    if (id) {
      const $busy = PA._busy(ts('Loading sample...'));
  
      btrmsgtpl.api3('MessageTemplateData', 'getsingle', { id })
        .then((result) => {
          PA._busy($busy);
  
          _sample = result;
          _sample.msg_variables = JSON.parse(_sample.msg_variables);
          
          const tplParams = getSample();
          _sampler.setValue(JSON.stringify(tplParams, null, 2));
          _sampler.updateOptions({ readOnly: false });
          preview(tplParams);
        });
    }
    else {
      bootstrap.Tooltip.getInstance($sample.parent()).hide();

      _sample = null;
      _sampler.setValue('');
      _sampler.updateOptions({ readOnly : true });
      preview();
    }
    $sample.trigger('change.select2');
  });
  /**
   * $sample.change.select2
   */
  $sample.on('change.select2', (evt) => {
    $('#select2-sample-container').removeAttr('title');
  });
  /**
   * $sample.select2:clearing
   */
  $sample.on('select2:clearing', (evt) => {
    if (PA.sample_save.hasClass('modified')) {
      evt.preventDefault();
      $sample.select2('close');

      btrmsgtpl.confirm(ts('Close Sample?'), ts('<code>%1</code> has been modified.', {
        1: evt.params.args.data[0].text
      }), {
        primary: ts('Yes'),
        secondary: ts('No')
      })
        .then((confirm) => {
          if (confirm) {
            $sample.val(null).trigger('change');
          }
        });
    }
  });
  /**
   * $sample.select2:selecting
   */
  $sample.on('select2:selecting', (evt) => {
    if (PA.sample_save.hasClass('modified')) {
      evt.preventDefault();
      $sample.select2('close');

      btrmsgtpl.confirm(ts('Switch Sample?'), ts('<code>%1</code> has been modified.<br /><br />Switch to <code>%2</code>?', {
        1: $sample.find(':selected').text(),
        2: evt.params.args.data.text
      }), {
        primary: ts('Yes'),
        secondary: ts('No')
      })
        .then((confirm) => {
          if (confirm) {
            $sample.val(evt.params.args.data.id).trigger('change');
          }
        });
    }
  });
  /**
   * tool_select
   */
  _event.on('tool_select', (evt ,tool) => {
    if (tool === 'preview') {
      if (!_sampler) {
        _sampler = monaco.editor.create($sampler[0], {
          fontSize: btrmsgtpl.pref('font-size', 12),
          language: 'json',
          lineNumbers: 'off',
          minimap: {
            enabled: false
          },
          readOnly: true
        });

        _sampler.addAction({
          contextMenuGroupId: 'btrmsgtpl',
          contextMenuOrder: 0,
          id: 'btrmsgtpl-header',
          label: 'Better Message Templates...',
          run: () => {
          }
        });
            _sampler.addAction({
          contextMenuGroupId: 'btrmsgtpl',
          contextMenuOrder: 1,
          id: 'btrmsgtpl-update-preview',
          label: ts('Update Preview'),
          keybindings: [monaco.KeyMod.chord(
            monaco.KeyMod.CtrlCmd | monaco.KeyCode.KeyK,
            monaco.KeyMod.CtrlCmd | monaco.KeyCode.Space,
          )],
          run: () => {
            updateSample();
          }
        });

        _sampler.onDidFocusEditorWidget(() => {
          _didModifyThisTime = false;
        });
        _sampler.onDidBlurEditorWidget(() => {
          if (_didModifyThisTime) {
            updateSample();
          }
        });
        _sampler.onDidChangeModelContent(() => {
          if (!_didModifyThisTime) {
            _didModifyThisTime = true;
            $preview.css('border-color', 'firebrick').data('modified', true);
          }
        });

        _event.trigger('action_sample_refresh'); // lazy load
      }
    }
  });
  
}(btrmsgtpl._event, btrmsgtpl._language, btrmsgtpl._cymbal));