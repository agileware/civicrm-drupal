(function(_event, _language, _cymbal) {

  // debounce
  let __lang = null;
  let __format = null;

  // import needed vars
  const { reserved } = btrmsgtpl.vars;

  // outline actions
  const OA = btrmsgtpl.actions('outline-actions');

  const $outline = $('#outline').css('font-size', btrmsgtpl.pref('font-size', 12) + 'px')
    .on('mouseleave', () => $youAreHere.addClass('d-none'));

  const $symbolCount = $('#symbol-count').addClass('d-none');

  const $symbolPrev = $('#symbol-prev').on('click', () => prevNext('prev'));
  const $symbolNext = $('#symbol-next').on('click', () => prevNext('next'));

  const $youAreHere = $('#you-are-here');

  let outlines = {
    lang: 'klingon'
  };

  const smarty = {
    closers: ['capture', 'foreach', 'if', 'literal', 'php', 'section', 'strip', 'textformat'],
    blockify: ['capture', 'foreach', 'if', 'literal', 'php', 'section', 'strip', 'textformat'],
    alt: {
      'else': 'if',
      'elseif' : 'if',
      'foreachelse': 'foreach',
      'sectionelse': 'section'
    }
  };

  /***********
   * HELPERS *
   ***********/

  /**
   * Calculate a block ID.
   * 
   * @link https://www.geeksforgeeks.org/string-hashing-using-polynomial-rolling-hash-function/
   * @private
   * @param {string} str The string to calculate a hash for.
   * @returns {string{} The generated hash to be used as an ID.
   */
  function calcHash(str) {
    const p1 = 31, p2 = 37;
    const m1 = 99999989, m2 = 91999981;
    let h1 = h2 = 0;
    let p_pow1 = p_pow2 = 1;

    for (let i = 0; i < str.length; i++) {
      h1 = (h1 + str.charCodeAt(i) * p_pow1) % m1;
      p_pow1 = (p_pow1 * p1) % m1;
      h2 = (h2 + str.charCodeAt(i) * p_pow2) % m2;
      p_pow2 = (p_pow2 * p2) % m2;
    }
    return (Math.abs(h1) + 100000000).toString().substr(1).concat((Math.abs(h2) + 100000000).toString().substr(1));
  }

  /**
   * HTML entities that need escaping so they can be output as text.
   */
  const entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
  };
  /**
   * Escape HTML content for direct display.
   * @param {string} str
   * @returns {string}
   */
  function escapeHtml(str) {
    return String(str).replace(/[&<>"'`=\/]/g, (s) => {
      return entityMap[s];
    });
  }

  /**
   * Expand all smarty blocks containing a node.
   * 
   * @param {jquery} $node The node being revealed.
   */
  function expandAllParents($node) {
    do {
      if ($node.length === 0 || $node.hasClass('btrmsgtpl-outline-container')) {
        break;
      }
      $node = $node.parent();
      if ($node.hasClass('btrmsgtpl-outline-header')) {
        const caret = $node.find('.fa-solid:first-child');
        if (caret && $(caret[0]).hasClass('fa-caret-right')) {
          $node.trigger('click');
        }
      }
    } while (1 === 1);
  }

  function parseContent(outline, symbolsOnly) {
    if (outline.hasOwnProperty('html')) {
      return;
    }
    _cymbal.parsing(true);

    const fix = new RegExp('\\[{3}(.*?):(.*?):(.*?)\\]{3}', 'm');
    let s = outline.model.getValue(),
      i = 0,
      c = inner = '',
      tags = [],
      tag = sup = literal = false,
      token = ti = vars = matches = null;

    // PHASE 1 - find and initial process {things}

    while (i < s.length) {
      c = s[i];

      if (c === '{') {
        if (tag !== false) {
          tags.push(tag);
        }
        tag = {
          i,
          len: 0,
          tag: '',
          opens: false,
          closes: false,
          sub: tag !== false
        }
      }
      if (tag) {
        tag.tag += c;
      }
      if (tag && c === '}') {
        inner = tag.tag.slice(1, -1).trim();
        token = inner.match(/([\w]+)\.([\w:\.]+)(?:\|(\w+(?::[\w": %\-_()\[\]\+\/#@!,\.\?]*)?))?/);

        if (token && token.index === 0) {
          ti = _cymbal.addSymbol(token[0]);
          tag.tag = tag.tag.replace(ti.name, `[[[token:${ti.id}:${ti.name}]]]`);
        }
        else if (inner[0] === '*' && inner[inner.length - 1] === '*') {
          tag.tag = `[[[comment::${tag.tag}]]]`;
        }
        else if (inner[0] === '/') {
          tag.closes = inner.substr(1);
          if (smarty.closers.indexOf(tag.closes) === -1) {
            smarty.closers.push(tag.closes);
          }
        }
        else { // var extrapolation or smarty tag
          if (inner[0] !== '$') {
            tag.opens = inner.match(/\w+/)[0];
          }
          vars = parseSmarty(tag.tag);
          matches = inner.match(/\$[a-z0-9_]+/gi);
          if (matches) {
            vars = vars.concat(matches);
          }
          vars = vars.sort().reverse().reduce(function(unique, val) {
            if (unique.indexOf(val) === -1) {
              unique.push(val);
            }
            return unique;
          }, []);

          vars.forEach((v) => {
            const vi = _cymbal.addSymbol(v.replace('=', '$'));

            if (v[0] === '=') {
              v = v.substr(1);
              const re = new RegExp('(\\w+)=(\'|")*' + v + '(\'|")*', 'm');
              tag.tag = tag.tag.replace(re, '$1=$2[[[var:' + vi.id + ':' + v + ']]]$2');
            }
            else {
              tag.tag = tag.tag.replaceAll(v, `[[[var:${vi.id}:${vi.name}]]]`);
            }
          });

          if (inner[0] === '$') {
          }
        }

        if (tag.sub) {
          sup = tags.pop();
          sup.tag += tag.tag;
          tag = sup;
        }
        else {
          delete tag.sub;
          tag.len = i - tag.i + 1;

          tag.tag = escapeHtml(tag.tag);
          while (tag.tag.match(fix)) {
            tag.tag = tag.tag.replace(fix, '<span class="btrmsgtpl-$1" data-btrmsgtpl-id="$2">$3</span>');
          }

          tags.push(tag);
          tag = false;
        }
      }
      i++;
    }

    if (symbolsOnly) {
      return;
    }

    // PHASE 2 - organize and check
    
    const blocks = [];
    const depth = [];
    newBlock = function(tag) {
      if (blocks.length > 0 && blocks[blocks.length - 1].len === 0) {
        blocks.pop();
      }
      const blk = {
        i: tag ? tag.i : i,
        len: tag ? tag.len : 0,
        content: tag ? tag.tag : '',
        opened: false,
        closed: false
      };
      blocks.push(blk);
      return blk;
    };
    primaryBlock = function(opener) {
      for (let p = blocks.length - 1; p >= 0; p--) {
        if (blocks[p].opened === false || blocks[p].closed === true) {
          continue;
        }
        return blocks[p].opened === opener ? blocks[p] : false;
      }
      return false;
    };

    let block = primary = null, n = 0;
    i = 0;

    while (tag = tags.shift()) {
      block = block || newBlock();
      
      // non-smarty content
      if (i !== tag.i) {
        n = tag.i - i;
        block.content += escapeHtml(s.substr(i, n));
        block.len += n;
        i += n;
      }
      // variable/token
      if (!tag.opens && !tag.closes) {
        block.content += tag.tag;
        block.len += tag.len;
      }
      // *** OPENING ***
      else if (tag.opens) {
        // alternate smarty
        if (smarty.alt.hasOwnProperty(tag.opens)) {
          primary = primaryBlock(smarty.alt[tag.opens]);
          if (primary) {
            primary.content += ' :: ' + tag.tag;
            block.content += `<div class="btrmsgtpl-sub">${tag.tag}</div>`;
          }
          else {
            block.content += `<span class="btrmsgtpl-unexpected" data-unexpected="Not supported by current block">${tag.tag}</span>`;
          }
          block.len += tag.len;
        }
        // inline smarty
        else if (smarty.blockify.indexOf(tag.opens) === -1) {
          block.content += `<span class="btrmsgtpl-inline btrmsgtpl-${tag.opens}">${tag.tag}`;
          if (smarty.closers.indexOf(tag.opens) === -1) {
            block.content += '</span>';
          }
          else {
            depth.push(tag.opens);
          }
          block.len += tag.len;
         }
        // smarty block
        else {
          block = newBlock(tag);
          block.opened = tag.opens;
          depth.push(tag.opens);
          block = null;
        }
      }
      // *** CLOSING ***
      else if (tag.closes) {
        const opened = depth.pop();
        if (opened === tag.closes) {
          // inline smarty
          if (smarty.blockify.indexOf(tag.closes) === -1) {
            block.content += tag.tag + '</span>';
            block.len += tag.len;
          }
          // smarty block
          else {
            block = newBlock(tag);
            block.closed = tag.closes;
            primary = primaryBlock(tag.closes);
            if (primary) {
              primary.closed = true;
              primary.inout = [primary.i, primary.i + primary.len, tag.i, tag.i + tag.len];
            }
            block = null;
          }
        }
        else {
          block.content += `<span class="btrmsgtpl-unexpected" data-unexpected="Unexpected closing tag">${tag.tag}</span>`;
          block.len += tag.len;
          if (opened) {
            depth.push(opened);
          }
        }
      }
      i += tag.len;
    }
    block = block || newBlock();
    if (i < s.length) {
      n = s.length - i;
      block.content += escapeHtml(s.substr(i, n));
      block.len += n;
    }
    while (opened = depth.pop()) {
      block.content += `<span class="btrmsgtpl-unexpected" data-unexpected="Missing tag">{/${opened}}</span>`;
    }

    // PHASE 3 - generate HTML

    outline.html = '';
    outline.map = [];
    let beg = end = 0;
    let cur = caret = hide = null;
    const full = s;

    while (block = blocks.shift()) {
      if (!block.hasOwnProperty('inout')) {
        block.inout = [block.i, block.i + block.len, 0, 0];
      }
      beg = block.inout[0];
      end = block.inout[3] > 0 ? block.inout[3] : block.inout[1];
      block.inout = block.inout.join(',');
      block.id = calcHash(full.slice(beg, end));

      outline.map.push({
        beg, end,
        range: end - beg,
        block: block.inout
      });

      if (!block.opened && !block.closed) {
        s = block.content.trimEnd();
        i = s.search(/\S+/);
        if (i !== -1) {
          i = s.substr(0, i).lastIndexOf('\n');
          if (i !== -1) {
            s = s.substr(i + 1);
          }
        }
        if (s.length > 0) {
          outline.html += `<div class="btrmsgtpl-outline-content" data-btrmsgtpl-block="${block.inout}">${s}</div>`;
        }
      }
      if (block.opened) {
        cur = $outline.find(`.fa-caret-down[data-btrmsgtpl-id="${block.id}"]:not(.btrmsgtpl-used)`);
        if (cur.length === 0) {
          caret = 'right';
          hide = ' d-none';
          $(cur[0]).addClass('btrmsgtpl-used');
        }
        else {
          caret = 'down';
          hide = '';
        }

        outline.html += `<div class="btrmsgtpl-outline-header" data-btrmsgtpl-block="${block.inout}"><i class="fa-solid fa-caret-${caret} mx-2" data-btrmsgtpl-id="${block.id}"></i><span class="btrmsgtpl-header-title">${block.content}</span><div class="btrmsgtpl-outline-blocks${hide}">`;
      }
      else if (block.closed) {
        outline.html += '</div></div>';
      }
    }

    outline.map.sort((a, b) => {
      let seq = a.range - b.range;
      if (seq === 0) {
        seq = a.beg - b.beg;
      }
      return seq;
    });

    _cymbal.parsing(false);

    return outline;
  }

  function parseSmarty(tag) {
    const added = [];
    tag = tag.slice(1, -1).trim();

    let match = tag.match(/^(\w+)\s(.*)$/);
    if (match) {
      const tagName = match[1].toLowerCase();
      const attrs = {};
      let vars = [];

      if (match.length > 2) {
        match = match[2].match(/(\S)+/gim);
        match.forEach((m) => {
          m = m.trim().split('=');
          if (m.length === 2) {
            attrs[m[0]] = m[1];
          }
        })
      }

      switch (tagName) {
        case 'assign':
          vars.push('var');
          break;

        case 'capture':
        case 'counter':
        case 'cycle':
        case 'eval':
        case 'fetch':
        case 'include':
        case 'include_php':
        case 'insert':
        case 'math':
        case 'textformat':
          vars.push('assign');
          break;

        case 'foreach':
          vars.push('item');
          vars.push('key');
          break;
      }

      vars.forEach((attr) => {
        if (attrs.hasOwnProperty(attr)) {
          let name = attrs[attr];
          if ((name[0] === '"' || name[0] === "'") && name[0] === name[name.length - 1]) {
            name = name.slice(1, -1);
          }
          _cymbal.addSymbol('$' + name, true);
          added.push('=' + name);
        }
      });
    }
    return added;
  }

  function scrollSource(beg, end) {
    _event.trigger('source_click', [beg, end, 'outline']);
  }

  /******************
   * EVENT HANDLERS *
   ******************/

  /**
   * action_outline_collapse
   */
  _event.on('action_outline_collapse', (evt, actions) => {
    $outline.find('.btrmsgtpl-outline-blocks')
      .addClass('d-none');
    $outline.find('.fa-solid.fa-caret-down')
      .removeClass('fa-caret-down')
      .addClass('fa-caret-right');
  });
  /**
   * action_outline_expand
   */
  _event.on('action_outline_expand', (evt, actions) => {
    $outline.find('.btrmsgtpl-outline-blocks')
      .removeClass('d-none');
    $outline.find('.fa-solid.fa-caret-right')
      .removeClass('fa-caret-right')
      .addClass('fa-caret-down');
  });
  /**
   * action_variable_refresh
   */
  _event.on('action_variable_refresh', (evt, actions) => {
    _cymbal.symbols.filter((sym) => !sym.isDef).forEach((sym) => _cymbal.remove(sym));

    const { content } = _language.val();
    parseContent({ model: content.msg_html }, true);
    parseContent({ model: content.msg_text }, true);
    parseContent({ model: content.msg_subject }, true);
  });
  /**
   * btrmsgtpl_resize
   */
  _event.on('btrmsgtpl_resize', (evt, height) => {
    $outline.height(height - $outline.position().top - 16);
  });
  /**
   * clickBlock
   */
  function clickBlock(evt) {
    btrmsgtpl.stopPrevent(evt);

    let $node = $(evt.target);
    let block = $node.attr('data-btrmsgtpl-block');

    if (!block) {
      return;
    }

    if (!evt.shiftKey && !evt.ctrlKey) {
      const [beg, end] = block.split(',');
      scrollSource(beg, end);
    }
    else {
      do {
        $node = $node.parent();
        if ($node.hasClass('btrmsgtpl-outline-container')) {
          break;
        }
      } while (!$node.hasClass('btrmsgtpl-outline-header'));
      
      block = $node.attr('data-btrmsgtpl-block');
      if (block) {
        const [beg_out, beg_in, end_in, end_out] = block.split(',');
        
        if (evt.ctrlKey) {
          scrollSource(beg_out, end_out);
        }
        else if (evt.shiftKey) {
          scrollSource(beg_in, end_in);
        }
      }
    }
  }
  /**
   * clickHeader
   */
  function clickHeader(evt) {
    btrmsgtpl.stopPrevent(evt);

    const target = $(evt.currentTarget);
    if (evt.ctrlKey) {
      target.find('.btrmsgtpl-outline-blocks').toggleClass('d-none');
      target.find('.fa-solid').toggleClass('fa-caret-right').toggleClass('fa-caret-down');
    }
    else {
      $(target.find('.btrmsgtpl-outline-blocks')[0]).toggleClass('d-none');
      $(target.find('.fa-solid')[0]).toggleClass('fa-caret-right').toggleClass('fa-caret-down');
    }
  }
  /**
   * clickVar
   */
  let $selection = $('');

  function clickVar(evt) {
    if (!evt.ctrlKey) {
      btrmsgtpl.stopPrevent(evt);
    }
    const node = $(evt.target);
    const id = node.addClass('btrmsgtpl-clicking').data('btrmsgtpl-id');
    _cymbal.val(id, true);
  }
  /**
   * format_select
   */
  _event.on('format_select', (evt, format, model) => {
    if (btrmsgtpl.tool() !== 'outline' || format === __format) {
      return;
    }
    __format = format;

    $outline.html(outlines[format].html);

    $outline.find('.btrmsgtpl-outline-header')
      .on('click', clickHeader)
      .on('mouseenter', youAreHere);

    $outline.find('.btrmsgtpl-outline-content')
      .on('click', clickBlock)
      .on('mouseenter', (evt) => {
        $(evt.target).removeClass('selected');
        youAreHere(evt);
      });

    $outline.find('.btrmsgtpl-var').on('click', clickVar);
    $outline.find('.btrmsgtpl-token').on('click', clickVar);

    $outline.find('.btrmsgtpl-header-title').each(function() {
      const $title = $(this);
      $title.parent().attr('data-you-are-here', $title.text());
    });

    _event.trigger('symbol_select', [ _cymbal.val() ]);
  });
  /**
   * language_select
   */
  _event.on('language_select', (evt, lang, content) => {
    if (btrmsgtpl.tool() !== 'outline' || lang === __lang) {
      return;
    }
    __lang = lang;
    __format = null;

    _cymbal.empty();
    for (let o in outlines) {
      delete outlines[o];
    }
    if (!lang) {
      return;
    }
    _cymbal.defaulting(true);
    parseContent({ model: reserved.msg_html });
    parseContent({ model: reserved.msg_text });
    parseContent({ model: reserved.msg_subject });
    _cymbal.defaulting(false);

    outlines = {
      msg_html: parseContent({ model: content.msg_html }),
      msg_text: parseContent({ model: content.msg_text }),
      msg_subject: parseContent({ model: content.msg_subject })
    };
  });
  /**
   * monaco_modified
   */
  _event.on('monaco_modified', () => {
    $outline.css('border-color', 'firebrick').data('modified', true);
  });
  /**
   * monaco_refresh
   */
  _event.on('monaco_refresh', (evt, editor) => {
    outlines[btrmsgtpl.format()] = parseContent({ model: editor.getModel() });
    $outline.css('border-color', 'silver').data('modified', false);
    
    __format = null;
    btrmsgtpl.$format.trigger('change');
  });
  /**
   * prevNext
   */
  function prevNext(dir) {
    if ($selection.length === 0) {
      return;
    }
    let i = $selection.filter('.selected').removeClass('selected').data('btrmsgtpl-index');

    if (dir === 'next') {
      i++;
      if (i === $selection.length) {
        i = 0;
      }
    }
    else {
      i--;
      if (i < 0) {
        i = $selection.length - 1;
      }
    }
    const $sel = $selection.eq(i).addClass('selected');
    expandAllParents($sel);

    $sel[0].scrollIntoView({ behavior: 'smooth', block: 'center' });

    $symbolCount.text(`${i + 1} of ${$selection.length}`);
  }
  /**
   * source_click
   */
  _event.on('source_click', (evt, i, select, context) => {
    if (context === 'editor') {
      const outline = outlines[btrmsgtpl.format()];
      const block = outline.map.find((m) => i >= m.beg && i <= m.end);

      if (block) {
        if (!block.hasOwnProperty('node')) {
          block.node = $outline.find(`[data-btrmsgtpl-block="${block.block}"]`);
        }
        if (block.node.length === 0) {
          return;
        }
        expandAllParents(block.node);
        block.node.addClass('selected')[0].scrollIntoView({ behavior: 'smooth', block: 'center' });
        
        if (select) {
          block.node.click();
        }
      }
    }
  });
  /**
   * symbol_select
   */
  _event.on('symbol_select', (evt, sym) => {
    $outline.find('.selected').removeClass('selected');

    if (sym) {
      let n = 0;
      $selection = $outline.find(`[data-btrmsgtpl-id="${sym.id}"]`)
        .each(function(i) {
          const $sel = $(this);
          $sel.data('btrmsgtpl-index', i);
          if ($sel.hasClass('btrmsgtpl-clicking')) {
            $sel.removeClass('btrmsgtpl-clicking');
            n = i;
          }
        });

      if ($selection.length > 0) {
        $sel = $selection.eq(n).addClass('selected');
        expandAllParents($sel);
        $sel[0].scrollIntoView({ behavior: 'smooth', block: 'center' });
      }
      else {
        n--;
      }
      $symbolCount.text(`${n + 1} of ${$selection.length}`).removeClass('d-none');
      $symbolPrev.prop('disabled', $selection.length === 0);
      $symbolNext.prop('disabled', $selection.length === 0);
    }
    else {
      $symbolCount.addClass('d-none');
      $symbolPrev.prop('disabled', true);
      $symbolNext.prop('disabled', true);
    }
  });
  /**
   * youAreHere
   */
  function youAreHere(evt) {
    btrmsgtpl.stopPrevent(evt);

    let $node = $(evt.target);
    while ($node && $node.length > 0 && !$node.hasClass('btrmsgtpl-outline-header')) {
      $node = $node.parent();
    }
    const yah = $node && $node.length > 0 ? $node.attr('data-you-are-here') : '';
    
    if (yah.length > 0) {
      $youAreHere.text(yah).removeClass('d-none');
    }
    else {
      $youAreHere.addClass('d-none');
    }
  }
}(btrmsgtpl._event, btrmsgtpl._language, btrmsgtpl._cymbal));