(function(_event) {

  const opts = {
    value: '',
    language: 'html',
    minimap: {
      enabled: false
    },
    lineNumbersMinsChars: 3,
    fontSize: btrmsgtpl.pref('font-size', 12)
  };
  const ww = btrmsgtpl.pref('wordwrap', false);
  if (ww) {
    opts.wordWrap = 'on';
    opts.wrappingIndent = ww;
  }

  const $monaco = $('#monaco');
  let _editor = null;

  const $tokens = $('#tokens')
    .on('blur', () => {
      _editor.removeContentWidget(_widget);

      const val = $tokens.val();
      if (val) {
        const pos = _editor.getPosition();
        const edit = {
          forceMoveMarkers: true,
          range: {
            startLineNumber: pos.lineNumber,
            startColumn: pos.column,
            endLineNumber: pos.lineNumber,
            endColumn: pos.column
          },
          text: val,
        };
        _editor.executeEdits('btrmsgtpl', [edit]);
      }
      _editor.focus();
    })
    .on('keydown', (evt) => {
      if (evt.keyCode === 13 || evt.keyCode === 27) {
        btrmsgtpl.stopPrevent(evt);

        if (evt.keyCode === 27) {
          $tokens.val('');
        }
        _editor.focus();
      }
    });

  const _widget = {
    allowEditorOverflow: true,
    getDomNode: () => {
      return $tokens.val('').removeClass('d-none')[0];
    },
    getId: () => {
      return 'btrmsgtpl-insert-token';
    },
    getPosition: () => {
      return {
        position: btrmsgtpl._editor.getPosition(),
        preference: [1, 2]
      }
    }
  }

  const _int = setInterval(() => {
    if (typeof monaco === 'undefined' || !monaco.hasOwnProperty('editor')) {
      return;
    }
    clearInterval(_int);

    btrmsgtpl._editor = _editor = monaco.editor.create($monaco[0], opts);
    _editor.addAction({
      contextMenuGroupId: 'btrmsgtpl',
      contextMenuOrder: 0,
      id: 'btrmsgtpl-header',
      label: 'Better Message Templates...',
      run: () => {
      }
    });
    _editor.addAction({
      contextMenuGroupId: 'btrmsgtpl',
      contextMenuOrder: 1,
      id: 'btrmsgtpl-insert-token',
      label: ts('Insert Token'),
      enabled: false,
      keybindings: [monaco.KeyMod.chord(
        monaco.KeyMod.CtrlCmd | monaco.KeyCode.KeyK,
        monaco.KeyMod.CtrlCmd | monaco.KeyCode.KeyK
      )],
      run: () => {
        btrmsgtpl._editor.addContentWidget(_widget);
        setTimeout(() => {
          $tokens.focus();
        }, 10);
      }
    });
    _editor.addAction({
      contextMenuGroupId: 'btrmsgtpl',
      contextMenuOrder: 2,
      id: 'btrmsgtpl-refresh-tool',
      label: ts('Refresh Current Tool'),
      keybindings: [monaco.KeyMod.chord(
        monaco.KeyMod.CtrlCmd | monaco.KeyCode.KeyK,
        monaco.KeyMod.CtrlCmd | monaco.KeyCode.Space,
      )],
      run: () => {
        _event.trigger('monaco_refresh', [ _editor ]);
        _didModifyThisTime = false;
      }
    });
    _editor.addAction({
      contextMenuGroupId: 'btrmsgtpl',
      contextMenuOrder: 10,
      id: 'btrmsgtpl-reveal-in-outline',
      label: ts('Reveal In Outline'),
      keybindings: [monaco.KeyMod.chord(
        monaco.KeyMod.CtrlCmd | monaco.KeyCode.KeyK,
        monaco.KeyMod.CtrlCmd | monaco.KeyCode.KeyV,
      )],
      run: () => _event.trigger('source_click', [ lc2i(), false, 'editor' ])
    });
    _editor.addAction({
      contextMenuGroupId: 'btrmsgtpl',
      contextMenuOrder: 11,
      id: 'btrmsgtpl-reveal-in-outline-select',
      label: ts('Reveal In Outline & Select'),
      keybindings: [monaco.KeyMod.chord(
        monaco.KeyMod.CtrlCmd | monaco.KeyCode.KeyK,
        monaco.KeyMod.CtrlCmd | monaco.KeyCode.KeyS,
      )],
      run: () => _event.trigger('source_click', [ lc2i(), true, 'editor' ])
    });
    _editor.setModel(null);

    let _didModifyThisTime = false;

    _editor.onDidFocusEditorWidget(() => {
      _didModifyThisTime = false;
    });
    _editor.onDidBlurEditorWidget(() => {
      if (_didModifyThisTime) {
        _event.trigger('monaco_refresh', [ _editor ]);
      }
    });
    _editor.onDidChangeModelContent(() => {
      if (!_didModifyThisTime) {
        _didModifyThisTime = true;
      }
      _event.trigger('monaco_modified', [ _editor ]);
    });
  }, 100);

  /**
   * Convert index into a monaco position.
   * 
   * @param {int} i Index into source.
   * @returns {Position} Line number and column.
   */
  function i2lc(i) {
    const source = _editor.getValue();
    const lc = {
      lineNumber: 1,
      column: 1
    }
    for (let n = 0; n < i; n++) {
      if (source[n] === '\n') {
        lc.lineNumber += 1;
        lc.column = 1;
      }
      else {
        lc.column += 1;
      }
    }
    return lc;
  }

  function lc2i() {
    let i = 0;
    const src = _editor.getValue();    
    const pos = _editor.getPosition().clone();
    while (pos.lineNumber > 1) {
      if (src[i] === '\n') {
        pos.lineNumber -= 1;
      }
      i++;
    }
    i += pos.column - 1;

    return i;
  }

  /******************
   * EVENT HANDLERS *
   ******************/

  /**
   * btrmsgtpl_resize
   */
  _event.on('btrmsgtpl_resize', (evt, height) => {
    $monaco.height(height - $monaco.position().top - 16);
    _editor.layout()
  });
  /**
   * format_select
   */
  _event.on('format_select', (evt, format, model) => {
    _editor.setModel(model);
  });
  /**
   * source_click
   */
  _event.on('source_click', (evt, beg, end, context) => {
    if (context === 'outline') {
      beg = i2lc(beg);
      end = i2lc(end);
      const range = {
        startLineNumber: beg.lineNumber,
        startColumn: beg.column,
        endLineNumber: end.lineNumber,
        endColumn: end.column
      };
      _editor.setSelection(range);
      _editor.revealRangeInCenterIfOutsideViewport(range, monaco.editor.ScrollType.Smooth);
      _editor.focus(); // ??? not sure if should
    }
  });
  /**
   * tool_deselect
   */
  _event.on('tool_deselect', (evt, tool) => {
    if (tool === 'monaco') {
      $monaco.parent().addClass('d-none');
    }
  });
  /**
   * tool_select
   */
  _event.on('tool_select', (evt, tool) => {
    if (tool === 'monaco') {
      $monaco.parent().removeClass('d-none');
      setTimeout(() => btrmsgtpl._resize(), 100);
    }
  });

}(btrmsgtpl._event));