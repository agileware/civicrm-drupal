(function(_event, _language) {

  const { reserved } = btrmsgtpl.vars;

  /******************
   * EVENT HANDLERS * 
   ******************/

  /**
   * monaco_modified
   */
  _event.on('monaco_modified', (evt, monaco) => {
    _language.modify(true);
  });

  /**************
   * INITIALIZE *
   **************/

  if (btrmsgtpl.pref('tool-side', 'right') === 'left') {
    $('#tools .col:first-of-type').appendTo('#tools');
    $('#monaco').removeClass('me-0').addClass('ms-0');
    $('#other-tool').removeClass('ms-0').addClass('me-0');
    $('#compare').removeClass('ms-2').addClass('me-2');
  }

  // even tough Monaco loads first, some times it isn't ready yet
  const int = setInterval(() => {
    if (typeof monaco === 'undefined' || !monaco.hasOwnProperty('editor')) {
      return;
    }
    clearInterval(int);

    btrmsgtpl._resize();

    $('[data-bs-toggle="tooltip"]').each(function() {
      new bootstrap.Tooltip(this);
    });

    reserved.msg_html = monaco.editor.createModel(reserved.msg_html, 'html');
    reserved.msg_text = monaco.editor.createModel(reserved.msg_text, 'text');
    reserved.msg_subject = monaco.editor.createModel(reserved.msg_subject, 'text');

    btrmsgtpl.$tool.trigger('change');
  }, 100);
    
}(btrmsgtpl._event, btrmsgtpl._language));