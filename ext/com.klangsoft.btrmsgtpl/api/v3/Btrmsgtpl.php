<?php

use CRM_Btrmsgtpl_ExtensionUtil as E;

/**
 * delete a translation
 *
 * @param array $params
 * @return array
 */
function civicrm_api3_btrmsgtpl_delete($params) {
  try {
    $api = \Civi\Api4\Translation::delete(TRUE)
      ->addWhere('entity_table', '=', 'civicrm_msg_template')
      ->addWhere('entity_id', '=', $params['id'])
      ->addWhere('language', '=', substr($params['lang'], 0, 5));
      
    if (substr($params['lang'], -5) == 'draft') {
      $api->addWhere('status_id:name', '=', 'draft');
    }
    else {
      $api->addWhere('status_id:name', '=', 'active');
    }
    $api->execute();
  }
  catch (\Exception $e) {
    return [
      'is_error' => 1,
      'error_message' => E::ts('btrmsgtpl: Error deleting translation/draft: ') . $e->getMessage()
    ];
  }
  return [ 'is_error' => 0 ];
}

/**
 * get a revision
 *
 * @param array $params
 * @return array
 */
function civicrm_api3_btrmsgtpl_revision($params) {
  $action = $params['action'] ?? 'get';

  try {
    switch ($action) {
      case 'get':
        $dao = CRM_Core_DAO::executeQuery("SELECT msg_template_id AS id, id AS rid, msg_html, msg_subject, msg_text, git_hash, 0 AS is_error
          FROM civicrm_msg_template_revisions
          WHERE id = %1
            AND msg_template_id = %2
        ", [
          1 => [$params['revision_id'], 'Integer'],
          2 => [$params['msg_template_id'], 'Integer']
        ]);
        if ($dao->fetch()) {
          return $dao->toArray();
        }
        throw new \Exception(E::ts('Revision not found. '));
        break;
    
      case 'rename':
        CRM_Core_DAO::executeQuery('UPDATE civicrm_msg_template_revisions
          SET rev_alias = %1
          WHERE id = %2
        ', [
          1 => [$params['alias'], 'String'],
          2 => [$params['revision_id'], 'Integer']
        ]);
        return ['is_error' => 0];
        break;

      case 'delete':
        CRM_Core_DAO::executeQuery('DELETE FROM civicrm_msg_template_revisions
          WHERE id = %1
        ', [
          1 => [$params['revision_id'], 'Integer']
        ]);
        return ['is_error' => 0];
        break;

      case 'refresh':
        return [
          'is_error' => 0,
          'revisions' => btrmsgtpl_getRevisions($params['msg_template_id'], $params['lang'] ?? '')
        ];
        break;

      case 'defaults':
        $defaults = _civicrm_api3_revision_defaults($params);
        return $defaults;
        break;
    
      default:
        throw new \Exception(E::ts('Unknown revision action. '));
        break;
    }
  }
  catch (\Exception $e) {
    return [
      'is_error' => 1,
      'error_message' => 'btrmsgtpl: ' . $e->getMessage()
    ];
  }
}

/**
 * save a template/translation
 *
 * @param array $params
 * @return array
 */
function civicrm_api3_btrmsgtpl_save($params) {
  $lang = $params['lang'];
  $ids = $params['ids'];
  unset($params['lang'], $params['ids']);

  // primary message template
  if ($lang == '_std_') {
    $result = civicrm_api3('MessageTemplate', 'create', $params);
  }
  else {
    // translation
    $draft = strlen($lang) > 5 && substr($lang, 5) == '_draft';
    if ($draft) {
      $lang = substr($lang, 0, 5);
    }
    try {
      $translations = \Civi\Api4\Translation::save()
        ->setRecords([
          [
            'id' => $ids['msg_subject'],
            'entity_field' => 'msg_subject',
            'string' => $params['msg_subject']
          ],
          [
            'id' => $ids['msg_html'],
            'entity_field' => 'msg_html',
            'string' => $params['msg_html']
          ],
          [
            'id' => $ids['msg_text'],
            'entity_field' => 'msg_text',
            'string' => $params['msg_text']
          ],
        ])
        ->setDefaults([
          'entity_table' => 'civicrm_msg_template',
          'entity_id' => $params['id'],
          'status_id:name' => $draft ? 'draft' : 'active',
          'language' => $lang,
        ])
        ->execute();
    
      $result = [
        'is_error' => 0,
        'ids' => []
      ];
      foreach ($translations as $translation) {
        $result['ids'][$translation['entity_field']] = $translation['id'];
      }
    }
    catch (\Exception $e) {
      return [
        'is_error' => 1,
        'error_message' => E::ts('btrmsgtpl: Error saving translation: ') . $e->getMessage()
      ];
    }
  }
  $result['revisions'] = btrmsgtpl_getRevisions($params['id'], $lang);

  return $result;
}

function civicrm_api3_btrmsgtpl_search($params) {
  $params['needle'] = "%{$params['needle']}%";

  $result = \Civi\Api4\MessageTemplate::get(TRUE)
    ->addSelect('id')
    ->addClause('OR',
      ['msg_html', 'LIKE', $params['needle']],
      ['msg_text', 'LIKE', $params['needle']],
      ['msg_subject', 'LIKE', $params['needle']]
    )
    ->addWhere('is_reserved', '=', FALSE)
    ->execute();

  $ids = $result->column('id');

  $result = \Civi\Api4\Translation::get(TRUE)
    ->addSelect('entity_id')
    ->addWhere('entity_table', '=', 'civicrm_msg_template')
    ->addWhere('status_id:label', '=', 'Active')
    ->addWhere('string', 'LIKE', $params['needle'])
    ->execute();

  $ids = array_unique(array_merge($ids, $result->column('entity_id')));

  return [
    'is_error' => 0,
    'ids' => $ids
  ];

}

function _btrmsgtpl_github($url) {
  $headers = [];

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_USERAGENT, 'btrmsgtpl');
  curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Accept: application/vnd.github+json',
    'X-GitHub-Api-Version: 2022-11-28'
  ]);
  curl_setopt($ch, CURLOPT_HEADERFUNCTION, function($curl, $header) use (&$headers) {
    $len = strlen($header);
    $header = explode(':', $header, 2);
    if (count($header) == 2) {
      $headers[strtolower(trim($header[0]))] = trim($header[1]);
    }
    return $len;
  });

  $resp = curl_exec($ch);
  $code = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

  curl_close($ch);

  return [$code, $headers, json_decode($resp)];
}

function _civicrm_api3_revision_defaults($params) {
  $result = [
    'is_error' => 0,
    'is_info' => 0,
    'defaults' => []
  ];
  $error = function($msg) use (&$result) {
    $result['is_error'] = 1;
    $result['error_message'] = "btrmsgtpl: $msg";
  };
  $info = function($msg) use (&$result) {
    $result['is_info'] = 1;
    $result['info_message'] = $msg;
  };

  $msg_template_id = $params['msg_template_id'] ?? FALSE;

  if ($msg_template_id) {
    $result['defaults'] = btrmsgtpl_getDefaults($msg_template_id);

    [$code, $headers, $api] = _btrmsgtpl_github("https://api.github.com/rate_limit");
    
    $now = time();
    $minutes = round(($api->rate->reset - $now) / 60);

    if ($code == 200 && $api->rate->remaining > 3) {
      $need = [];
      $added = [];
      
      try {
        $fmt = Civi::settings()->get('dateformatDatetime');
        $formats = ['html', 'text', 'subject'];
        $workflow = civicrm_api3('MessageTemplate', 'getvalue', [
          'id' => $msg_template_id,
          'return' => 'workflow_name'
        ]);
        $prominence = strtotime(CRM_Core_DAO::singleValueQuery('SELECT MIN(created_date) FROM civicrm_contact'));

        foreach ($formats as $format) {
          $url = "https://api.github.com/repos/civicrm/civicrm-core/commits?per_page=50&path=xml%2Ftemplates%2Fmessage_templates%2F{$workflow}_{$format}.tpl";

          [$code, $headers, $commits] = _btrmsgtpl_github($url);

          if ($code == 200) {
            foreach ($commits as $commit) {
              if (empty($need[$commit->sha]) && empty($result['defaults'][$commit->sha]))  {
                $ts = strtotime($commit->commit->committer->date);
                $need[$commit->sha] = [
                  'id' => 0,
                  'name' => E::ts('System Default ') . date('Y-m-d', $ts),
                  'alias' => CRM_Utils_Date::customFormat(date('Y-m-d H:i:s', $ts), $fmt),
                  'ts' => $ts,
                  'hash' => $commit->sha,
                  'notes' => $commit->commit->message,
                  'url' => $commit->url
                ];
                if ($ts < $prominence) {
                  break;
                }
              }
            }
          }
          else {
            throw new \Exception("Code $code reading $url");
          }
        }
        uasort($need, function($a, $b) {
          return $b['ts'] - $a['ts'];
        });

        if (!empty($need)) {
          if (count($need) <= ($api->rate->remaining - 3)) {
            $regex = "#{$workflow}_(html|text|subject)\.tpl#";
            $contact_id = CRM_Core_BAO_Domain::getDomain()->contact_id;

            foreach ($need as &$default) {
              [$code, $headers, $commit] = _btrmsgtpl_github($default['url']);

              if ($code == 200) {
                
                if (!empty($commit->files)) {
                  foreach ($commit->files as $file) {
                    if (preg_match($regex, $file->filename)) {
                      $files = [];
                      $base = substr($file->raw_url, 0, strpos($file->raw_url, $workflow));

                      foreach ($formats as $format) {
                        $files[$format] = file_get_contents("{$base}{$workflow}_{$format}.tpl");
                      }

                      $dao = CRM_Core_DAO::executeQuery('INSERT INTO civicrm_msg_template_revisions
                        (msg_template_id, msg_html, msg_subject, msg_text, rev_date, rev_user_id, git_hash, notes)
                        VALUES (%1, %2, %3, %4, %5, %6, %7, %8)
                      ', [
                        1 => [$msg_template_id, 'Integer'],
                        2 => [$files['html'], 'String'],
                        3 => [$files['subject'], 'String'],
                        4 => [$files['text'], 'String'],
                        5 => [date('Y-m-d H:i:s', $default['ts']), 'String'],
                        6 => [$contact_id, 'Integer'],
                        7 => [$default['hash'], 'String'],
                        8 => [$default['notes'], 'String']
                      ]);

                      $default['id'] = CRM_Core_DAO::singleValueQuery('SELECT id
                        FROM civicrm_msg_template_revisions
                        WHERE msg_template_id = %1
                          AND git_hash = %7
                      ', [
                        1 => [$msg_template_id, 'Integer'],
                        7 => [$default['hash'], 'String']
                      ]);
                      unset($default['url']);
                      
                      $added[$default['hash']] = $default;

                      break;
                    }
                  }
                }
              }
              else {
                throw new \Exception("Code $code reading {$default['url']}");
              }
            }
          }
          else {
            $info(E::ts('Access to system default history currently restricted. Try again in %1 minutes(s).', [
              1 => $minutes
            ]));
          }
        }
    
        if (!empty($added)) {
          $result['defaults'] = array_merge($added, $result['defaults']);
          uasort($result['defaults'], function($a, $b) {
            return $b['ts'] - $a['ts'];
          });
        }
      }
      catch (\Exception $e) {
        $error($e->getMessage());
      }
    }
    else {
      if ($code != 200) {
        $error(E::ts('Error %1 while reading /rate_limit.', [ 1 => $code ]));
      }
      else {
        $info(E::ts('Access to system default history currently restricted. Try again in %1 minutes(s).', [
          1 => $minutes
        ]));
      }
    }
  }
  else {
    $error(E::ts('Template ID (msg_template_id) is required.'));
  }

  $result['defaults'] = array_values($result['defaults']);
  
  return $result;
}
