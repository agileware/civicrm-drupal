<?php
use CRM_Btrmsgtpl_ExtensionUtil as E;

/**
 * Determine if the value supplied is safe to evaluate.
 *
 * @param string $val The value to be checked.
 * @return bool
 */
function btrmsgtpl_isEvalSafe(&$val) {
  $val = str_replace('&gt;', '>', rtrim(trim($val), ';')) . ';';

  // strip out string literals
  for ($i = 0, $sans = '', $in = FALSE; $i < strlen($val); $i++) {
    $c = $val[$i];
    if ($in) {
      if ($c == $in && ord($val[$i - 1]) != 134) {
        $in = false;
      }
    }
    else {
      if ($c == '"' || $c == "'") {
        $in = $c;
      }
      else {
        $sans .= $c;
      }
    }
  }
  $sans = strtolower($sans);

  // disallow more than one statement
  if (count(explode(';', $sans)) > 2) {
    return FALSE;
  }

  // allowed with parenthensis
  $matches = [];
  $allowed = ['array'];
	if (preg_match_all('/(\w+)(\s*)\(/', $sans, $matches) > 0) {
    if (count(array_diff($matches[1], $allowed)) > 0) {
      return FALSE;
    }

  }
  // banned "words"
  $matches = [];
  $banned = ['die', 'exit', 'include', 'include_once', 'require', 'require_once'];
	if (preg_match_all('/(\w+)/', $sans, $matches) > 0) {
    if (count(array_intersect($banned, $matches[1])) > 0) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Process variables and set up token context.
 *
 * @param array $msg_variables
 * @return array
 */
function btrmsgtpl_processVars($msg_variables) {
  $vars = [];
  $tokenContext = $msg_variables ? array_pop($msg_variables) : [];

  foreach($msg_variables as $var) {
    if ($var['type'] == 'structured') {
      if (btrmsgtpl_isEvalSafe($var['value'])) {
        eval('$value = ' . $var['value']);
      }
      else {
        \Civi::log()->warning("btrmsgtpl: Unsafe structured variable detected/skipped: {$var['name']}\n{$var['value']}");
        $value = [];
      }
    }
    elseif ($var['type'] == 'known') {
      $vars['config'] = CRM_Core_Config::singleton();
    }
    else {
      $value = $var['value'];
    }
    if ($var['name'][0] == '$') {
      $vars[substr($var['name'], 1)] = $value;
    }
    else {
      @[$entity, $field] = explode('.', $var['name'], 2);
      if (!empty($field)) {
        $entityId = "{$entity}Id";
        if (empty($tokenContext[$entityId])) {
          $tokenContext[$entityId]= $value;
        }
      }
    }
  }
  return [$vars, $tokenContext];
}

/**
 * Render a preview of html, text, or subject.
 *
 * @param array $params
 * @return array
 */
function civicrm_api3_message_template_data_preview($params) {

  $old = set_error_handler(function($no, $str, $file, $line) {
    \Civi::log()->error("btrmsgtpl: $str, $file, line $line");
    throw new Exception($str);
  }, E_USER_ERROR);

  try {
    $tpl = civicrm_api3('MessageTemplate', 'getsingle', [ 'id' => $params['id']]);

    if ($convert = $params['source_id'] == 'msg_text' && empty($params['source'])) {
      $params['source'] = $tpl['msg_html'];
    }
    $tpl['msg_subject'] = $tpl['msg_text'] = $tpl['msg_html'] = '';
    $tpl[$params['source_id']] = $params['source'];

    if (!empty($params['tplParams'])) {
      $vars = $params['tplParams'];
      $tokenContext = $vars['tokenContext'];
      unset($vars['tokenContext']);
    }
    // todo - remove on next release
    else {
      [$vars, $tokenContext] = btrmsgtpl_processVars($params['msg_variables']);
    }
    $vars['_btrmsgtpl_preview_'] = TRUE;

    $render = [
      'workflow' => $tpl['workflow_name'],
      'tplParams' => $vars,
      'tokenContext' => $tokenContext,
      'messageTemplate' => $tpl
    ];

    $result[$params['source_id']] = CRM_Core_BAO_MessageTemplate::renderTemplate($render)[substr($params['source_id'], 4)];

    if ($convert) {
      $result[$params['source_id']] = CRM_Utils_String::htmlToText($result[$params['source_id']]);
    }
  }
  catch (Exception $e) {
    $result = [$params['source_id'] => $e->getMessage()];
  }

  set_error_handler($old);

  return $result;
}

/**
 * Send template to a specified email address.
 *
 * @param [array] $params
 * @return array
 */
function civicrm_api3_message_template_data_send($params) {
  $vars = $params['tplParams'];
  $tokenContext = $vars['tokenContext'];
  unset($vars['tokenContext']);

  $messages = [
    'msg_html' => $params['msg_html'],
    'msg_text' => $params['msg_text'],
    'msg_subject' => $params['msg_subject']
  ];

  try {
    $result = CRM_Core_TokenSmarty::render($messages, $tokenContext, $vars);

    $sendParams = [
      'from' => CRM_Core_BAO_Domain::getFromEmail(),
      'toEmail' => $params['email'],
      'subject' => $result['msg_subject'],
      'html' => $result['msg_html'],
      'text' => $result['msg_text']
    ];
    $result = CRM_Utils_Mail::send($sendParams);
  }
  catch (Exception $e) {
    \Civi::log()->error($e->getMessage());
  }
  return $result;
}

/**
 * MessageTemplateData.create API specification (optional).
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 *
 * @see https://docs.civicrm.org/dev/en/latest/framework/api-architecture/
 */
function _civicrm_api3_message_template_data_create_spec(&$spec) {
  // $spec['some_parameter']['api.required'] = 1;
}

/**
 * MessageTemplateData.create API.
 *
 * @param array $params
 *
 * @return array
 *   API result descriptor
 *
 * @throws API_Exception
 */
function civicrm_api3_message_template_data_create($params) {
  return _civicrm_api3_basic_create(_civicrm_api3_get_BAO(__FUNCTION__), $params, 'MessageTemplateData');
}

/**
 * MessageTemplateData.delete API.
 *
 * @param array $params
 *
 * @return array
 *   API result descriptor
 *
 * @throws API_Exception
 */
function civicrm_api3_message_template_data_delete($params) {
  return _civicrm_api3_basic_delete(_civicrm_api3_get_BAO(__FUNCTION__), $params);
}

/**
 * MessageTemplateData.get API.
 *
 * @param array $params
 *
 * @return array
 *   API result descriptor
 *
 * @throws API_Exception
 */
function civicrm_api3_message_template_data_get($params) {
  $get = _civicrm_api3_basic_get(_civicrm_api3_get_BAO(__FUNCTION__), $params, TRUE, 'MessageTemplateData');

  foreach ($get['values'] as &$vals) {
    // convert old format sample into new, if needed
    if ($vals['msg_variables'][0] === '[') {
      [$vars, $tokenContext] = btrmsgtpl_processVars(json_decode($vals['msg_variables'], TRUE));
      unset($tokenContext['name']);

      $vars['tokenContext'] = $tokenContext;
      $vals['msg_variables'] = json_encode($vars); // null becomes '' if not encoded, and breaks things
    }
  }

  return $get;
}
