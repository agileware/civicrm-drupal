# CiviCRM Turfcutter

![An image of the turfcutter](images/turfcutter.png)

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v5.4+
* CiviCRM (*FIXME: Version number*)

## Installation (Web UI)

This extension has not yet been published for installation via the web UI.

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl https://gitlab.com/asludds/civicrm-turfcutter/archive/master.zip
cd turfcutter
npm install
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).
make sure to run npm install inside of the project directory to download the required javascript dependencies

```bash
git clone https://gitlab.com/asludds/civicrm-turfcutter
cv en turfcutter
cd turfcutter
npm install
```

## Usage

Currently one of the most time consuming tasks for a political campaign is to be able to "cut-turf". This is the process by which a staffer or campaign manager assigns volunteers to a subsection of voters for canvassing. Similar practices are used in other fields such as door-to-door fundraising and census data collection.

### User Interface

An image of the user interface can be seen above

#### Technologies 

For the creation of the user interface several different technologies will be used in addition to CiviCRM.
Leaflet, a javascript interactive maps libary, will be heavily utilized for displaying and interacting with maps. "https://leafletjs.com/"

Turf.js is probably the largest part of the application in terms of hard drive real-estate. It allows for logic to be performed on the mapping data. In particular, it is useful for figuring out who is inside of a polygon. "http://turfjs.org/"

Leaflet.markercluster is used in order to improve the performance whenever there is a large amount of markers being loaded. "https://github.com/Leaflet/Leaflet.markercluster"

leaflet-sidebar-v2 is used in order to create the left and rightmost sidebars. "https://github.com/nickpeihl/leaflet-sidebar-v2"

Though technically not an extension, it is worth noting that leaflet has a layers feature which will be used for implementing the voter view and volunteer view. "https://leafletjs.com/examples/layers-control/"

Leaflet draw is used to draw polygons. "https://github.com/Leaflet/Leaflet.draw"

Leaflet fullscreen is an extension which allows for fullscreen usage of the turfcutter. "https://github.com/Leaflet/Leaflet.fullscreen"


#### User Interface Descrption

There are several components to this user interface.

### Center Map
This center maps contains information about geographical locations. While in voter view, this will show markers for voter locations. While in volunteer view, previous turfs of the volunteer will be shown to give context to the end-user. While in heat-map view the markers will change color to correspond to voter information. What type of information this will be can be set in an options menu. For example, you could query the data and ask "what voters have not been canvassed in the last two weeks?" and the color of markers will change accordingly.

### Group Select Button
This button, which can be found on the right side of the page with a blue outline, allows for the user to choose which group they want to cut turf from. In addition to choose a group to cut turf from the user can use the + and - buttons to create longer boolean queries. For example, suppose I want to turf cut from people who are under 30 years old and living within Brooklyn. One way to do this would be to create two groups within Civi, the group of people under 30 and the group of people within Brooklyn, and then create the expression "group a AND group b". Another option for how to do this is by using smart groups. A description of how to use smart groups can be found in the right slide menu under "Filter".

### Left Slide In Menu
This menu will contain meta information about the project. For example, a tutorial which describes how different parts of the project can be utilized. An about-us menu for information on contacting the developers and reporting bugs.

### Right Slide in Menu
This menu contains information about how to filter using smart groups as well as information about the volunteers and which turf that have.

## Future Work
This turfcutter, though working for the features it has currently, is by no means complete. In the future hopefully an autocutting feature will be added as well as google-maps intergration allowing for visualization of turf-routes and walking time estimates. 
