<?php
use CRM_Turfcutter_ExtensionUtil as E;

/**
 * Survey.Selectbyvolunteername API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC/API+Architecture+Standards
 */
function _civicrm_api3_survey_Selectbyvolunteername_spec(&$spec) {
  $spec['volunteer_id']['api.required'] = 1;
}

/**
 * Survey.Selectbyvolunteername API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_survey_Selectbyvolunteername($params) {
  if (array_key_exists('volunteer_id', $params)) {
    // Query civicrm-turfcutter table to see if volunteer_name is present
    $volunteer_id = $params['volunteer_id'];
    //We make a call to activity to find all activities that have the
    //assignee_contact_id the same as this contacts name
    //From these activities we want the voter IDs


    $result = civicrm_api3('Activity', 'get', [
                'sequential' => 1,
                'return' => ["target_contact_id"],
                'assignee_contact_id' => $volunteer_id,
                'options' => ['limit' => 1000000],
                'activity_type_id' => ['IN' => ["Survey", "WalkList", "Canvass", "Phone Call", "Petition"]],
                'is_current_revision' => 1,
    ]);
    $target_id_array = array();
    foreach($result["values"] as $contains_target){
      $result_contact = civicrm_api3('Contact', 'get', [
                  'sequential' => 1,
                  'return' => ["first_name", "last_name", "id", "address_id","geo_code_1","geo_code_2","street_address","city"],
                  'id' => $contains_target["target_contact_id"][0],
                ]);
      $result_contact_value = $result_contact["values"][0];
      $returnableArray = array(
        "first_name" => $result_contact_value["first_name"],
        "last_name" => $result_contact_value["last_name"],
        "id" => $result_contact_value["id"],
        "street_address" => $result_contact_value["street_address"],
        "city" => $result_contact_value["city"],
        "geo_code_1" => $result_contact_value["geo_code_1"],
        "geo_code_2" => $result_contact_value["geo_code_2"]);
      array_push($target_id_array, $returnableArray);
    }
    $returnValues = $target_id_array;
    return civicrm_api3_create_success($returnValues, $params, 'NewEntity', 'NewAction');
  }
  else {
    throw new API_Exception(/*errorMessage*/ 'Everyone knows that the magicword is "sesame"', /*errorCode*/ 1234);
  }
}
