<?php

/**
 * Job.Removeactionlog API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC/API+Architecture+Standards
 */
function _civicrm_api3_job_Removeactionlog_spec(&$spec) {
}

/**
 * Job.Removeactionlog API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_job_Removeactionlog($params) {
  // Initialize the ActionLog table
  $action_log = new CRM_Core_DAO_ActionLog();
  // Only delete entries older than 6 months ago
  $action_log->whereAdd('action_date_time < DATE_SUB(NOW(), INTERVAL 6 MONTH)');
  // Only delete rows that are from the Membership table
  $action_log->whereAdd("entity_table = '" . CRM_Member_DAO_Membership::getTableName() . "'");

  // Execute the DELETE query using "where" conditions.
  $action_log->delete(true);

  $returnValues = [];

  // Always assume success.
  return civicrm_api3_create_success($returnValues, $params, 'Job', 'Removeactionlog');
}
