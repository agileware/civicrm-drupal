# Options Importer

Options Importer Extension allows you to view import automatically custom field's options within a CSV file, also you can delete all options in a custom field (Use with care!!)

![Screenshot](/images/screenshot.png)

## Changelog
`1.x`   - Older version, not supported anymore  
`2.0.x` - Compatible with CiviCRM 5.x  
`2.1.x` - Using CiviCRM APIv4  


## Installation
1. Move the downloaded extension to your extensions folder.
2. Browse site's url `/civicrm/admin/extensions&reset=1` and install the extension
  
More info about installing extensions [here](https://docs.civicrm.org/sysadmin/en/latest/customize/extensions/#enabling-extensions)

## Usage

1. Go to the Custom Field Section by the top menu in Administer / Customiza Data and Screens / Custom Fields
2. Select the custom fiueld you want to import / delete the options
3. Click on Edit Multiple Choice Optionsmore link
4. Options list will displayed (if any already created). Below the list you will see two buttons. "Import Options" and "Delete Options"
5. "Delete Options" will delete alloptions in the custom field. USE IT WITH CARE!!
6. "Import Options" will go to the Import Form. There select:
  * The csv file to import containing the options
  * Check if first row contains column headers
  * Select the csv field separator (by default is comma)
  * Select the text field enclosure (if necesary, depending on the csv format)
  * Select the order of the columns in the file:
    2 columns (value, label)
    2 columns (label, value)
    only 1 column (label will be same as value)
  * Click on Import, and save loads of typing time !!!

## Support and Maintenance
This extension is supported and maintained by:  

[![iXiam Global Solutions](images/ixiam-logo.png)](https://www.ixiam.com)  
  
Distributed under the terms of the GNU Affero General public license (AGPL).  
See [LICENSE.txt](LICENSE.txt) for details.
