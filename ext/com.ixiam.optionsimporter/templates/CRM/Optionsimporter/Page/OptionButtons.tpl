<div class="action-link">
  {crmButton p="civicrm/admin/custom/group/field/import" q="reset=1&fid=$fid&gid=$gid" class="action-item open-inline-noreturn" icon="plus-circle"}{ts}Import Options{/ts}{/crmButton}
  {crmButton id="optionsimporterDeleteButton" class="action-item open-inline-noreturn" icon="times"}{ts}Delete ALL Options{/ts}{/crmButton}
</div>

{literal}
<script>
(function($, _, ts) {
  $("#optionsimporterDeleteButton").on('click', function(e) {
    e.preventDefault();
    CRM.confirm({
      title: ts('Delete ALL Options'),
      message: ts('This action will delete ALL options for this field. Are you sure to continue?'),
    })
      .on('crmConfirm:yes', function() {
        var url = '{/literal}{crmURL p="civicrm/admin/custom/group/field/delete-options" q="reset=1&fid=$fid&gid=$gid"}{literal}';
        location.replace(url.replace(/\&amp;/g,'&'));
        return true;
      });
  });
})(CRM.$, CRM._, CRM.ts('optionsimporter'));
</script>
{/literal}
