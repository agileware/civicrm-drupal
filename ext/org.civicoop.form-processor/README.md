# form-processor

The form processor extension gives you the ability to define business logic for handling form submissions.
A form submission could come from the CiviMRF, external system or just a direct submission.

[See **docs** for more information](https://docs.civicrm.org/formprocessor/en/latest/).

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Contributing

Feel free to contribute to this extension. Create a Merge Request and also add a little description to CHANGELOG.md of what you have changed

## Releasing Process

When one of the admins think it is time to relase a new version they might do so. You can also ask them to release a new version.
Usually this when the release is being tested and put in production.

For admins creating a release those are the steps to follow:

1. Update info.xml to reflect the new version number and release date
2. Remove the 'not yet released' from CHANGELOG.md
3. Copy the changes
4. Go to tags, create a new tag and paste the changes
5. After that update info.xml to add a new version number ending with _-dev_
6. Update changelog add a heading for the new version with the text _not yet released_

## Requirements

* PHP v7.4+
* CiviCRM v5.68+
* a compatible theme (e.g. seven) when using Drupal 8

## Installation (Web UI)

This extension has not yet been published for installation via the web UI.

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl form-processor@https://lab.civicrm.org/jaapjansma/form-procesor/repository/master/archive.zip
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://lab.civicrm.org/jaapjansma/form-procesor.git
cv en form_processor
```

## Usage

(* FIXME: Where would a new user navigate to get started? What changes would they see? *)

## Known Issues

(* FIXME *)
