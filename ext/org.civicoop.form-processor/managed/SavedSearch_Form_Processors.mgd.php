<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
use CRM_FormProcessor_ExtensionUtil as E;
return [
  [
    'name' => 'SavedSearch_Form_Processors',
    'entity' => 'SavedSearch',
    'cleanup' => 'always',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Form_Processors',
        'label' => E::ts('Form Processors'),
        'api_entity' => 'FormProcessorInstance',
        'api_params' => [
          'version' => 4,
          'select' => [
            'id',
            'name',
            'title',
            'description',
            'is_active',
            'status:label',
          ],
          'orderBy' => [],
          'where' => [],
          'groupBy' => [],
          'join' => [],
          'having' => [],
        ],
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'SavedSearch_Form_Processors_SearchDisplay_Form_Processors',
    'entity' => 'SearchDisplay',
    'cleanup' => 'always',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Form_Processors',
        'label' => E::ts('Form Processors'),
        'saved_search_id.name' => 'Form_Processors',
        'type' => 'table',
        'settings' => [
          'description' => '',
          'sort' => [
            [
              'title',
              'ASC',
            ],
          ],
          'limit' => 50,
          'pager' => [],
          'placeholder' => 5,
          'columns' => [
            [
              'type' => 'field',
              'key' => 'title',
              'dataType' => 'String',
              'label' => E::ts('Title'),
              'sortable' => TRUE,
            ],
            [
              'type' => 'field',
              'key' => 'description',
              'dataType' => 'Text',
              'label' => E::ts('Description'),
              'sortable' => TRUE,
            ],
            [
              'type' => 'field',
              'key' => 'is_active',
              'dataType' => 'Boolean',
              'label' => E::ts('Is active'),
              'sortable' => TRUE,
            ],
            [
              'type' => 'field',
              'key' => 'status:label',
              'dataType' => 'Integer',
              'label' => E::ts('Status'),
              'sortable' => TRUE,
            ],
            [
              'text' => '',
              'style' => 'default',
              'size' => 'btn-xs',
              'icon' => 'fa-bars',
              'links' => [
                [
                  'path' => 'civicrm/admin/automation/formprocessor/configuration?reset=1&action=update&id=[id]',
                  'icon' => 'fa-external-link',
                  'text' => E::ts('Edit Form Processor'),
                  'style' => 'default',
                  'condition' => [],
                  'task' => '',
                  'entity' => '',
                  'action' => '',
                  'join' => '',
                  'target' => '',
                ],
                [
                  'task' => 'delete',
                  'entity' => 'FormProcessorInstance',
                  'join' => '',
                  'target' => 'crm-popup',
                  'icon' => 'fa-trash',
                  'text' => E::ts('Delete Form Processor'),
                  'style' => 'danger',
                  'path' => '',
                  'action' => '',
                  'condition' => [],
                ],
                [
                  'task' => 'enable',
                  'entity' => 'FormProcessorInstance',
                  'join' => '',
                  'target' => 'crm-popup',
                  'icon' => 'fa-toggle-on',
                  'text' => E::ts('Enable Form Processor'),
                  'style' => 'default',
                  'path' => '',
                  'action' => '',
                  'condition' => [
                    'is_active',
                    '=',
                    FALSE,
                  ],
                ],
                [
                  'task' => 'disable',
                  'entity' => 'FormProcessorInstance',
                  'join' => '',
                  'target' => 'crm-popup',
                  'icon' => 'fa-toggle-off',
                  'text' => E::ts('Disable Form Processor'),
                  'style' => 'default',
                  'path' => '',
                  'action' => '',
                  'condition' => [
                    'is_active',
                    '=',
                    TRUE,
                  ],
                ],
                [
                  'path' => 'civicrm/admin/automation/formprocessor/run?_qf_formProcessorName=[name]',
                  'icon' => 'fa-play-circle-o',
                  'text' => E::ts('Try out'),
                  'style' => 'success',
                  'condition' => [],
                  'task' => '',
                  'entity' => '',
                  'action' => '',
                  'join' => '',
                  'target' => '',
                ],
                [
                  'path' => 'civicrm/admin/automation/formprocessor/export?reset=1&id=[id]',
                  'icon' => 'fa-download',
                  'text' => E::ts('Export'),
                  'style' => 'default',
                  'condition' => [],
                  'task' => '',
                  'entity' => '',
                  'action' => '',
                  'join' => '',
                  'target' => '',
                ],
                [
                  'path' => 'civicrm/admin/automation/formprocessor/configuration?reset=1&action=revert&id=[id]',
                  'icon' => 'fa-undo',
                  'text' => E::ts('Back to imported version'),
                  'style' => 'danger',
                  'condition' => [
                    'status:name',
                    '=',
                    '3',
                  ],
                  'task' => '',
                  'entity' => '',
                  'action' => '',
                  'join' => '',
                  'target' => '',
                ],
              ],
              'type' => 'menu',
              'alignment' => 'text-right',
            ],
          ],
          'actions' => [
            'delete',
            'disable',
            'enable',
          ],
          'classes' => [
            'table',
            'table-striped',
          ],
          'toolbar' => [
            [
              'path' => 'civicrm/admin/automation/formprocessor/configuration/reset=1&action=add',
              'icon' => 'fa-plus',
              'text' => E::ts('Add form processor'),
              'style' => 'success',
              'condition' => [],
              'task' => '',
              'entity' => '',
              'action' => '',
              'join' => '',
              'target' => '',
            ],
            [
              'path' => 'civicrm/admin/automation/formprocessor/import',
              'icon' => 'fa-upload',
              'text' => E::ts('Import'),
              'style' => 'info',
              'condition' => [],
              'task' => '',
              'entity' => '',
              'action' => '',
              'join' => '',
              'target' => '',
            ],
          ],
        ],
      ],
      'match' => [
        'saved_search_id',
        'name',
      ],
    ],
  ],
];
