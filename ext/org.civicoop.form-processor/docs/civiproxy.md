# Configuration of CiviProxy

When using the form processor behind a CiviProxy you need to white list the following api calls:

* `FormProcessorInstance.list`
* `FormProcessorInstance.get_output` to get information about the fields returned from a form processor
* `FormProcessor`.`get_fields` to retrieve the inputs of a form processor.
* `FormProcessor`.`_your_form_processor_name_` to run the form processor
* `FormProcessorDefaults`.`get_fields` to retrieve the fields for retrieving default data a form processor
* `FormProcessorDefaults`.`_your_form_processor_name_` the actual retrieval of default data
* `FormProcessorValidation`.`get_fields` to retrieve the inputs able to validate
* `FormprocessorValidation`.`_your_form_processor_name_` the actual validation of your form processor
* `FormProcessorCalculation`.`get_fields` to retrieve the inputs able to do calculations
* `FormProcessorCalculation`.`_your_form_processor_name_` the actual calculation of your form processor
