# Example of a basic form to sign up for a newsletter

This example details how to create a form on my public website where a visitor can enter *first name, last name and email* and click to sign up for our monhtly newsletter.
The **Form Processor** extension will make sure this is processed in the CiviCRM backend.

The public website is on another server than CiviCRM and I am using Drupal 7 on my public website example.

## Defining the form processor

The general information for my form processor looks like this:

![Form Processor General](img/newsletter-general-fp.png)

On my form processor I will accept 3 inputs: first name, last name and email.

![Form Processor Inputs](img/newsletter-inputs-fp.png)

Once the form is sent from the public website I would like the following to happen:

* check if we already have a contact with the email entered and if so, use that contact
* if there is no contact with the email yet, create a new contact with the email, first and last name from the form
* add the found or created contact to our newsletter group if the visitor wants to receive the newsletter.

So here is what the actions look like:

![Form Processor Actions](img/newsletter-actions-fp.png)

The action to find or create the contact has been specified so:

![Action to Find or Create Contact](img/action-find-contact.png)

So this action tries to find a contact with the email provided. If no contact with the email can be found, it uses the email and the first/last name to create a contact.

Finally in the action to add the contact to the group I have selected the group the contact should be added to and specified that the contact ID found in the find action should be used.

![Action to Add to Group](img/action-add-to-group.png)

## Defining the form in Drupal 7

I now need to design a form on my public website that communicates with my form processor. In this example I use Drupal 7.

### CiviMRF profile

To be able to communicate with a CiviCRM installation on another server the **CMRF core** module is installed, and I have also enabled the **CRMF Webform** module and the **CMRF Form Processor** module (see the [Requirements](./requirements.md) section):

![CiviMRF modules](img/newsletter-cmrf-modules.png)

Once I have installed the CiviMRF modules I can specify a so called CMRF Profile from the Drupal Configuration menu:

![CiviMRF Profile Menu](img/newsletter-cmrf-menu.png).

From this menu I can edit or add a CiviMRF profile. In this profile I specify if my connection with CiviCRM is remote (on another server) or local (the same server).
In this example it is remote, I have specified the URL to the REST interface of CiviCRM and added the site and API keys.

!!! Note "Site and API keys"
    (for background information check [System Administrator Guide on Site Key][sitekey] and [StackExchange on API key][apikey]).

![CiviMRF Profile](img/newsletter-cmrf-profile.png)

Once I have done this I have configured how this profile connects to my CiviCRM installation.

!!! Note
    Expect to have a profile for each CiviCRM installation you want to connect to. This is probably only one in most cases.

### CiviMRF for the form

I am now going to create my form in Drupal by clicking *Add content*, then select *Webform* and finally add a name for my webform and hit the *Save* button.
Once I have done this I will see my new form like this:

![New empty form](img/newsletter-empty-form.png)

To specify that I am going to use the CiviMRF profile with this form so it can communicate with the **Form Processor** I will now click on the **CiviMRF** tab and select *Submit to CiviCRM Form Processor*.
In the form I can select what *CiviMRF profile* I want to use and what *Form Processor* I want to use:

![CiviMRF part of the form](img/newsletter-form-cmrf-part.png)

### Adding the fields on the form

Once I have hit *Save* I will go back to the *Form components* tab but I will now see a message warning me that I have not yet included fields from the Form Processor:

![Form components message](img/newsletter-form-fields-message.png)

Next step is to add all the fields I want to be on my form. In my example I have only included the ones from my form processor but you could add whatever you want additionally, just remember this will not be sent to CiviCRM.

![Form components](img/newsletter-form-components.png)

### The end result of the form

I have also updated the text for my submit button, and the resulting form will look like this (I am not a styling guru :-):

![Resulting form](img/newsletter-form.png)

## Result!

If I now enter data in my form as you can see here:

![Form for John Doe](img/newsletter-jd-form.png)

The result in CiviCRM will be that a contact is created and he is added to the newsletter group:

![CiviCRM John Doe](img/newsletter-jd-civi1.png)

![CiviCRM John Doe](img/newsletter-jd-civi2.png)

[sitekey]:https://docs.civicrm.org/sysadmin/en/latest/setup/site-key/
[apikey]:https://civicrm.stackexchange.com/questions/31092/where-is-the-api-key
