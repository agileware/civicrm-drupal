# Developer: format submitted data

As a developer you can add formats for formatting submitted data. This extension comes with two formats `plain` and `html table`.
The formatted data can be used to set a description of an activity. Or to send out an e-mail when data has been submitted.

## Create Event Subscriber

Add a file in your extension under `Civi\MyExt\EventListener` and call it `FormProcessorFormatSubscriber`

Below an example of how the file looks for the defaults formats

```php

namespace Civi\FormProcessor\EventListener;

use Civi\FormProcessor\Event\FormatDatabagEvent;
use Civi\FormProcessor\Event\FormatTypeEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use CRM_FormProcessor_ExtensionUtil as E;

class FormatSubscriber implements EventSubscriberInterface {

  /**
   * Returns an array of event names this subscriber wants to listen to.
   *
   * The array keys are event names and the value can be:
   *
   *  * The method name to call (priority defaults to 0)
   *  * An array composed of the method name to call and the priority
   *  * An array of arrays composed of the method names to call and respective
   *    priorities, or 0 if unset
   *
   * For instance:
   *
   *  * ['eventName' => 'methodName']
   *  * ['eventName' => ['methodName', $priority]]
   *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
   *
   * @return array The event names to listen to
   */
  public static function getSubscribedEvents() {
    return [
      FormatTypeEvent::NAME => 'onFormatType',
      FormatDatabagEvent::NAME => 'onFormatDatabag',
    ];
  }

  /**
   * @param \Civi\FormProcessor\Event\FormatTypeEvent $event
   *
   * @return void
   */
  public function onFormatType(FormatTypeEvent $event) {
    $event->formats = [
      'plain' => E::ts('Plain Text with submitted data'),
      'html_table' => E::ts('HTML Table with submitted data'),
    ];
  }

  public function onFormatDatabag(FormatDatabagEvent $event) {
    $htmlTable = '<table>';
    $plain = '';
    foreach ($event->databag->getAllInputs() as $input) {
      $htmlTable .= '<tr><th>' . $input->title . '</th><td>' . $event->databag->getInputData($input) . '</td></tr>';
      $plain .=  $input->title . ': ' . $event->databag->getInputData($input) . "\r\n";
    }
    $htmlTable .= '</table>';
    $event->databag->setFormattedDatabag('plain', $plain);
    $event->databag->setFormattedDatabag('html_table', $htmlTable);
  }

}

```

The function `getSubscribedEvents` gives meta information on to what events this class is listening.

The function `onFormatType` makes the format types known to the form processor, in this example it is Plain Text and HTML Table.

The function `onFormatDatabag` formats the data and sets it in the databag.

## Register the event subscriber

In the hook_civicrm_container you have to add this subscriber.

Below an example:

```php

use \Symfony\Component\DependencyInjection\ContainerBuilder;
use \Symfony\Component\DependencyInjection\Definition;

/**
 * Implements hook_civicrm_container()
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_container/
 */
function hook_civicrm_container(ContainerBuilder $container) {
  $formatSubscriberEventDefinition = new Definition('Civi\FormProcessor\EventListener\FormatSubscriber')
  $container->findDefinition('dispatcher')
    ->addMethodCall('addSubscriber', [$formatSubscriberEventDefinition]);
}

```
