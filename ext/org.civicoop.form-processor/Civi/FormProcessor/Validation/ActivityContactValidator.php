<?php

/**
 * @author  Justin Freeman (Agileware) <support@agileware.com.au>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Validation;

use Civi\FormProcessor\Config\Specification;
use Civi\FormProcessor\Config\SpecificationBag;

use CRM_FormProcessor_ExtensionUtil as E;

class ActivityContactValidator extends AbstractValidator {

  public function getLabel() {
    return E::ts('User is a Contact for the Activity');
  }

  /**
   * Returns the name of the validator.
   *
   * @return string
   */
  public function getName() {
    return 'activity_contact';
  }

  /** Returns the invalid message.
   *
   * @return string
   */
  public function getInvalidMessage() {
    return E::ts('User is not an Activity Contact Type (%1).', [1 => $this->configuration->get('activity_contact_type')]);
  }


  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag(
      [
        new Specification(
          'activity_contact_type',
          'String',
          E::ts('Compare logged-in user with'),
          TRUE,
          'any',
          NULL,
          [
            'any'      => E::ts('Any Activity Contact'),
            'target'   => E::ts('Activity Targets'),
            'source'   => E::ts('Activity Source'),
            'assignee' => E::ts('Activity Assignees'),
          ]
        ),
      ]
    );
  }

  public function validateConfiguration() {
    $valid = parent::validateConfiguration();
    return $valid;
  }

  /**
   * Validate the input.
   *
   * @param   mixed                                  $input
   * @param   \Civi\FormProcessor\Type\AbstractType  $inputType
   *
   * @return bool
   */
  public function validate($input, $inputType) {
    $activityContacts      = \CRM_Activity_BAO_ActivityContact::buildOptions('record_type_id', 'validate');
    $loggedInContactId     = \CRM_Core_Session::getLoggedInContactID();
    $activity_contact_type = $this->configuration->get('activity_contact_type');

    $targetID   = \CRM_Utils_Array::key('Activity Targets', $activityContacts);
    $sourceID   = \CRM_Utils_Array::key('Activity Source', $activityContacts);
    $assigneeID = \CRM_Utils_Array::key('Activity Assignees', $activityContacts);

    switch ($activity_contact_type) {
      case  'target':
        $activityContacts = \CRM_Activity_BAO_ActivityContact::retrieveContactIdsByActivityId($input, $targetID);
        break;
      case 'source':
        $activityContacts = \CRM_Activity_BAO_ActivityContact::retrieveContactIdsByActivityId($input, $sourceID);
        break;
      case 'assignee':
        $activityContacts = \CRM_Activity_BAO_ActivityContact::retrieveContactIdsByActivityId($input, $assigneeID);
        break;
      default:
        // Activity Contact Type, Any - check all
        $activityTargetContacts   = \CRM_Activity_BAO_ActivityContact::retrieveContactIdsByActivityId($input, $targetID);
        $activitySourceContacts   = \CRM_Activity_BAO_ActivityContact::retrieveContactIdsByActivityId($input, $sourceID);
        $activityAssigneeContacts = \CRM_Activity_BAO_ActivityContact::retrieveContactIdsByActivityId($input, $assigneeID);

        $activityContacts = array_merge($activityTargetContacts, $activitySourceContacts, $activityAssigneeContacts);
    }

    $result = FALSE;
    // Check if the logged-in Contact ID is in the Activity Contacts
    foreach ($activityContacts as $activityContactID) {
      if ($loggedInContactId == $activityContactID) {
        $result = TRUE;
      }
    }
    return $result;
  }

}
