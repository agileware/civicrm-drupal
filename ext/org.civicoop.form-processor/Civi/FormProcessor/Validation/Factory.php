<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

 namespace Civi\FormProcessor\Validation;

 class Factory {

  /**
   * @var array<AbstractValidator>
   */
  protected array $validators = array();

   /**
    * @var array
    */
  protected array $validatorLabels = array();

  /**
   * @var Factory
   */
  private static $singleton;

  private function __construct() {
    $this->addValidator(new EmailValidator());
    $this->addValidator(new MinLengthValidator());
    $this->addValidator(new MinValidator());
    $this->addValidator(new MaxValidator());
    $this->addValidator(new DateValidator());
    $this->addValidator(new ActivityContactValidator());
  }

  /**
   * @return Factory
   */
  public static function singleton(): Factory {
    if (!self::$singleton) {
      self::$singleton = new Factory();
    }
    return self::$singleton;
  }

  /**
   * Adds a validator
   *
   * @param \Civi\FormProcessor\Validation\AbstractValidator $validator
   * @return Factory
   */
  public function addValidator(AbstractValidator $validator): Factory {
    $this->validators[$validator->getName()] = $validator;
    $this->validatorLabels[$validator->getName()] = $validator->getLabel();
    return $this;
  }

  /**
   * Returns a validator
   *
   * @param string $name
   *   The name of the validator.
   * @return AbstractValidator
   */
  public function getValidatorByName(string $name): AbstractValidator {
    return $this->validators[$name];
  }

  /**
   * @return array<AbstractValidator>
   */
  public function getValidators(): array {
    return $this->validators;
  }

  public function getValidatorLabels(): array {
    return $this->validatorLabels;
  }
 }
