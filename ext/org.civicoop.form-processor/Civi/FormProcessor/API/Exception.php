<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\FormProcessor\API;

use Throwable;

class Exception extends \Exception {

  public function __construct($actionName, Throwable $previous = NULL) {
    $message = "Action ".$actionName." failed.";
    if ($previous) {
      $message .= " Caused by ".$previous->getMessage();
    }
    parent::__construct($message, null, $previous);
  }

}