<?php
/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

 namespace Civi\FormProcessor\API;

 use Civi\ActionProvider\Action\AbstractAction;
 use Civi\ActionProvider\Parameter\ParameterBag;
 use Civi\API\Event\ResolveEvent;
 use Civi\API\Event\RespondEvent;
 use Civi\API\Events;
 use Civi\API\Provider\ProviderInterface as API_ProviderInterface;
 use Civi\FormProcessor\DataBag;
 use Civi\FormProcessor\Runner;
 use Civi\FormProcessor\Type\OptionListInterface;
 use Civi\FormProcessor\Utils\Cache;
 use Symfony\Component\EventDispatcher\EventSubscriberInterface;

 use CRM_FormProcessor_ExtensionUtil as E;

/**
 * This class connects the enabled form processors to
 * the the api interface of CiviCRM. That way the form processors
 * could be executed over the api.
 */
 class FormProcessorCalculation implements API_ProviderInterface, EventSubscriberInterface {

   const VALIDATION_ERROR_CODE = 2;

  public function __construct() {
  }

  /**
   * @return array
   */
  public static function getSubscribedEvents() {
    // Some remarks on the solution to implement the getFields method.
    // We would like to have implemented it as a normal api call. Meaning
    // it would been processed in the invoke method.
    //
    // However the reflection provider has a stop propegation so we cannot use
    // getfields here unles we are earlier then the reflection provider.
    // We should then use a weight lower than Events::W_EARLY and do a
    // stop propegation in our event. But setting an early flag is not a neat way to do stuff.
    // So instead we use the Respond event and check in the respond event whether the action is getFields and
    // if so do our getfields stuff there.
    return array(
      Events::RESOLVE => array(
        array('onApiResolve', Events::W_EARLY),
      ),
    );
  }

  public function onApiResolve(ResolveEvent $event) {
    $apiRequest = $event->getApiRequest();
    if (strtolower($apiRequest['entity']) == 'formprocessorcalculation') {
      $event->setApiProvider($this);
      if (strtolower($apiRequest['action']) == 'getfields' || strtolower($apiRequest['action']) == 'getoptions') {
        $event->stopPropagation();
      }
    }
  }

  /**
   * Event listener on the ResponddEvent to handle the getfields actions.
   * So the fields defined by the user are availble in the api explorer for example.
   */
  public function invokeGetFields($apiRequest) {
    $params = $apiRequest['params'];
    $result = array();

    // Now check whether the action param is set. With the action param we can find the form processor.
    if (isset($params['action'])) {
      $result['values'] = $this->getFields($params['action'], $params);
      if (!is_array($result['values'])) {
        $result['values'] = array();
      }
      $result['count'] = count($result['values']);
    }
    return $result;
  }

  protected function getFields($formProcessorName, $apiParams, $fieldName = null, $useCache=true) {
    $fields = array();
    $hash = md5(json_encode($apiParams));
    $cacheKey = 'FormProcessorCalculation.getfields.'.strtolower($formProcessorName).'.'.$hash;
    if ($useCache && ($cache = Cache::get($cacheKey))) {
      return $cache;
    }

    try {
      $formProcessor = Runner::getFormProcessor($formProcessorName);
    } catch (\Exception $e) {
      return [];
    }

    $usedInputs = \CRM_FormProcessor_BAO_FormProcessorInstance::getCalculationTriggers($formProcessorName);
    $usedOutputs = \CRM_FormProcessor_BAO_FormProcessorInstance::getCalculationOutputs($formProcessorName);

    // Process all inputs of the formprocessor.
    foreach($formProcessor['inputs'] as $input) {
      if ((!in_array($input['name'], $usedInputs) && !in_array($input['name'], $usedOutputs)) || ($fieldName && $fieldName != $input['name'])) {
        continue;
      }
      $isOutput = false;
      if (in_array($input['name'], $usedOutputs)) {
        $isOutput = true;
      }
      $input['type']->setParameters(Runner::inputParameterMapping($input['parameter_mapping'], $apiParams));
      $field = array(
        'name' => $input['name'],
        'title' => $input['title'],
        'description' => '',
        'type' => $input['type']->getCrmType(),
        'api.required' => $input['is_required'],
        'api.return' => $isOutput,
        'api.aliases' => array(),
        'entity' => 'FormProcessor',
      );
      if ($input['type'] instanceof OptionListInterface) {
        $field['options'] = $input['type']->getOptions($apiParams);
        $field['formprocessor.is_multiple'] = $input['type']->isMultiple(); // We add our own spec here, as an option for is multiple does not exists.
      }
      // Set a default value
      if (isset($input['default_value']) && $input['default_value'] != '') {
        $field['api.default'] = $input['type']->getDefaultValue($input['default_value']);
      }
      if ($input['type']->getCrmType()) {
        $fields[$input['name']] = $field;
      }
    }

    Cache::setForFormProcessor($formProcessorName, $cacheKey, $fields);
    return $fields;
  }

   /**
    * Invoke the GetOptions api call
    *
    * @param $apiRequest
    * @return array
    */
   protected function invokeGetOptions($apiRequest) {
     $params = $apiRequest['params'];
     $result = array();
     $result['values'] = array();
     // Now check whether the action param is set. With the action param we can find the data processor.
     if (isset($params['field'])) {
       try {
         $fieldName = $params['field'];
         $fields = $this->getFields($params['api_action'], $params, $fieldName, false);
         if (isset($fields[$fieldName]) && isset($fields[$fieldName]['options'])) {
           $result['values'] = $fields[$fieldName]['options'];
         }
       } catch(\Exception $e) {
         // Do nothing.
       }

       $result['count'] = count($result['values']);
     }
     return $result;
   }

   /**
    * @param array $apiRequest
    *   The full description of the API request.
    * @return array
    *   structured response data (per civicrm_api3_create_success)
    * @see civicrm_api3_create_success
    * @throws \Exception
    */
   public function invoke($apiRequest) {
     switch (strtolower($apiRequest['action'])) {
       case 'getfields':
         // Do get fields
         return $this->invokeGetFields($apiRequest);
         break;
       case 'getoptions':
         // Do get options
         return $this->invokeGetOptions($apiRequest);
         break;
       default:
         return $this->invokeFormProcessor($apiRequest);
         break;
     }
   }

  /**
   * @param array $apiRequest
   *   The full description of the API request.
   * @return array
   *   structured response data (per civicrm_api3_create_success)
   * @see civicrm_api3_create_success
   * @throws \API_Exception
   */
  public function invokeFormProcessor($apiRequest) {
    $params = $apiRequest['params'];

    $dataBag = Runner::createDatabag();
    $validationErrors = Runner::runValidation($dataBag, $apiRequest['action'], $params);
    if (count($validationErrors)) {
      $return['is_error'] = '1';
      $return['error_message'] = 'Validation Error';
      $return['error_code'] = self::VALIDATION_ERROR_CODE;
      $return['validation_errors'] = $validationErrors;
      return $return;
    }

    return Runner::runCalculations($dataBag, $apiRequest['action'], $params, FALSE);
  }

  /**
   * @param int $version
   *   API version.
   * @return array<string>
   */
  public function getEntityNames($version) {
    return array(
      'FormProcessorCalculation'
    );
  }

  /**
   * @param int $version
   *   API version.
   * @param string $entity
   *   API entity.
   * @return array<string>
   */
  public function getActionNames($version, $entity) {
    if (strtolower($entity) != 'formprocessorcalculation') {
      return array();
    }

    $cachekey = 'formprocessorcalculation.getactions';
    if ($cache = Cache::get($cachekey)) {
      return $cache;
    }

    $params['is_active'] = 1;
    $form_processors = \CRM_FormProcessor_BAO_FormProcessorInstance::getValues($params);
    $actions[] = 'getfields';
    $actions[] = 'getoptions';
    foreach($form_processors as $form_processor) {
      $actions[] = $form_processor['name'];
    }
    Cache::set($cachekey, $actions);
    return $actions;
  }

 }
