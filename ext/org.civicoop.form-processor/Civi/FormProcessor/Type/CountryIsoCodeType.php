<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use Civi\FormProcessor\Type\AbstractType;
use Civi\FormProcessor\Type\OptionListInterface;
use Civi\FormProcessor\Config\Specification;
use Civi\FormProcessor\Config\SpecificationBag;

use CRM_FormProcessor_ExtensionUtil as E;

class CountryIsoCodeType extends AbstractType implements OptionListInterface {

  /**
   * @var array|null
   */
  protected $options = null;

  /**
   * Get the configuration specification
   *
   * @return \Civi\FormProcessor\Config\SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag([
      new Specification('limit_to_enabled', 'Boolean', E::ts('Limit to enabled countries'), TRUE, 1, NULL, [
        0 => E::ts('No'),
        1 => E::ts('Yes'),
      ], FALSE, E::ts('Limit countries in Administer » Localization » Languages, Currency, Locations.
<br /><br /><strong>Note</strong> that this input type translates the ISO code into a CiviCRM country ID.
<br />So if the initial value is NL, the Form Processor action using the input receives 1152')),
    ]);
  }

  /**
   * Returns true when this field is a multiple field.
   *
   * @return bool
   */
  public function isMultiple() {
    return FALSE;
  }

  public function validateValue($value, $allValues = []) {
    if (\CRM_Utils_Type::validate($value, 'String', FALSE) === NULL) {
      return FALSE;
    }

    $countries = $this->getOptions($allValues);
    return isset($countries[$value]);
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return \CRM_Utils_Type::T_INT;
  }

  public function getOptions($params) {
    if ($this->options === null) {
      $limitToEnabled = $this->configuration->get('limit_to_enabled') ? TRUE : FALSE;
      $countries = \CRM_Core_PseudoConstant::country(FALSE, $limitToEnabled);
      $iso_codes = \CRM_Core_PseudoConstant::countryIsoCode();
      $this->options = [];
      foreach ($countries as $id => $country) {
        if (isset($iso_codes[$id]) && $iso_codes[$id]) {
          $this->options[$iso_codes[$id]] = $country;
        }
      }
    }
    return $this->options;
  }

  /**
   * Normalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function normalizeValue($value) {
    $iso_codes = \CRM_Core_PseudoConstant::countryIsoCode();
    foreach ($iso_codes as $id => $iso_code) {
      if ($iso_code == $value) {
        return $id;
      }
    }
    return NULL;
  }

  /**
   * Denormalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function denormalizeValue($value) {
    $iso_codes = \CRM_Core_PseudoConstant::countryIsoCode();
    if (isset($iso_codes[$value])) {
      return $iso_codes[$value];
    }
    return NULL;
  }

}
