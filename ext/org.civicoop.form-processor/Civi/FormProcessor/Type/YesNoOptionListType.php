<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use \Civi\FormProcessor\Config\Specification;
use \Civi\FormProcessor\Config\SpecificationBag;
use \Civi\FormProcessor\Type\AbstractType;
use \Civi\FormProcessor\Type\OptionListInterface;

use \CRM_FormProcessor_ExtensionUtil as E;

class YesNoOptionListType extends AbstractType implements OptionListInterface {

  private $options;

  private $optionsLoaded = FALSE;

  /**
   * Returns true when this field is a multiple field.
   *
   * @return bool
   */
  public function isMultiple() {
    return FALSE;
  }

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag([
      new Specification('yes_label', 'String', E::ts('Yes label'), TRUE, E::ts('Yes'), NULL, NULL, FALSE),
      new Specification('no_label', 'String', E::ts('No label'), TRUE, E::ts('No'), NULL, NULL, FALSE),
    ]);
  }

  public function validateValue($value, $allValues = []) {
    return TRUE;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return \CRM_Utils_Type::T_BOOLEAN;
  }

  public function getOptions($params) {
    $this->loadOptions();
    return $this->options;
  }

  /**
   * Normalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function normalizeValue($value) {
    return $value ? '1' : '0';
  }

  /**
   * Denormalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function denormalizeValue($value) {
    return $value ? '1' : '0';
  }

  private function loadOptions() {
    if ($this->optionsLoaded) {
      return;
    }

    $yes_label = strlen($this->configuration->get('yes_label')) ? $this->configuration->get('yes_label') : E::ts('Yes');
    $no_label = strlen($this->configuration->get('no_label')) ? $this->configuration->get('no_label') : E::ts('No');
    $this->options = [
      '1' => $yes_label,
      '0' => $no_label,
    ];
    $this->optionsLoaded = TRUE;

    return $this->options;
  }

  /**
   * Sets the default values of this action
   */
  public function setDefaults() {
    parent::setDefaults();
    $this->options = NULL;
    $this->optionsLoaded = FALSE;
  }

}
