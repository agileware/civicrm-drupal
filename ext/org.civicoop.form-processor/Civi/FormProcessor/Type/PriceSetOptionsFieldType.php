<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\FormProcessor\Type;

use Civi\FormProcessor\Config\ConfigurationBag;
use Civi\FormProcessor\Config\Specification;
use Civi\FormProcessor\Config\SpecificationBag;
use CRM_Core_BAO_Discount;
use CRM_Core_Config;
use CRM_Core_DAO;
use CRM_Core_Exception;
use CRM_FormProcessor_ExtensionUtil as E;
use CRM_Price_BAO_PriceField;
use CRM_Price_BAO_PriceSet;
use CRM_Utils_Array;
use CRM_Utils_Money;
use CRM_Utils_Type;

class PriceSetOptionsFieldType extends AbstractType implements OptionListInterface {

  protected $options;

  protected $normalizedOptions;

  protected $denormalizedOptions;

  protected $optionsLoaded = FALSE;

  public function __construct($label) {
    parent::__construct('PriceSetOptionsField', $label);
  }

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    static $priceFields;
    if (!$priceFields) {
      $priceFields = [];
      $sql = "SELECT
        `civicrm_price_field`.`id`, `civicrm_price_field`.`label` as `price_field_label`, `civicrm_price_set`.`title` as `price_set_title`
        FROM `civicrm_price_field`
        INNER JOIN `civicrm_price_set` ON `civicrm_price_field`.`price_set_id` = `civicrm_price_set`.`id`
        WHERE `civicrm_price_set`.`is_quick_config` = '0'
        AND (`civicrm_price_field`.`html_type` = 'Radio' OR `civicrm_price_field`.`html_type` = 'Select' OR `civicrm_price_field`.`html_type` = 'CheckBox')";
      $dao = \CRM_Core_DAO::executeQuery($sql);
      while($dao->fetch()) {
        $priceFields[$dao->id] = $dao->price_set_title . ': ' . $dao->price_field_label;
      }
    }

    return new SpecificationBag([
      new Specification('price_fields', 'Integer', E::ts('Price set option fields'), true, null, null, $priceFields, TRUE),
      new Specification('inlcude_field_label', 'Boolean', E::ts('Include field label'), false),
      new Specification('multiple', 'Boolean', E::ts('Multiple'), FALSE, 0, NULL, [
        0 => E::ts('Single value'),
        1 => E::ts('Multiple values'),
      ]),
    ]);
  }

  public function setParameters(array $parameters) {
    parent::setParameters($parameters);
    $this->optionsLoaded = FALSE;
    $this->options = null;
    $this->denormalizedOptions = null;
    $this->normalizedOptions = null;
  }

  /**
   * Returns true when this field is a multiple field.
   *
   * @return bool
   */
  public function isMultiple() {
    return $this->configuration->get('multiple') ? TRUE : FALSE;
  }

  public function validateValue($value, $allValues = []) {
    $multiple = $this->configuration->get('multiple') ? TRUE : FALSE;
    $options = $this->getOptions($allValues);

    // Correct array values when field is not multiple.
    // this could be caused for example by a drupal webform with
    // a checkbox field. The value is submitted as an array, altough it contains
    // only one value.
    if (!$multiple && is_array($value) && count($value) == 1) {
      $value = reset($value);
    }

    if ($multiple && is_array($value)) {
      $value = $this->removeNullValues($value);
      foreach ($value as $valueItem) {
        if (\CRM_Utils_Type::validate($valueItem, 'String', FALSE) === NULL) {
          return FALSE;
        }
        if (!isset($options[$valueItem])) {
          return FALSE;
        }
      }
    }
    elseif (!is_array($value) && $value) {
      if (\CRM_Utils_Type::validate($value, 'String', FALSE) === NULL) {
        return FALSE;
      }

      if (!isset($options[$value])) {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return CRM_Utils_Type::T_MONEY;
  }

  public function getOptions($params) {
    $this->loadOptions();
    return $this->options;
  }

  /**
   * Normalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function normalizeValue($value) {
    $this->loadOptions();
    $value = $this->removeNullValues($value);
    $multiple = $this->configuration->get('multiple') ? TRUE : FALSE;

    // Correct array values when field is not multiple.
    // this could be caused for example by a drupal webform with
    // a checkbox field. The value is submitted as an array, altough it contains
    // only one value.
    if (!$multiple && is_array($value) && count($value) == 1) {
      $value = reset($value);
    }

    if ($multiple && is_array($value)) {
      $return = [];
      foreach ($value as $item) {
        $return[] = $this->normalizedOptions[$item];
      }
      return $return;
    }
    elseif (!is_array($value) && $value !== null && strlen($value) && isset($this->normalizedOptions[$value])) {
      return $this->normalizedOptions[$value];
    }
    else {
      return NULL;
    }
  }

  /**
   * Denormalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function denormalizeValue($value) {
    $this->loadOptions();
    $multiple = $this->configuration->get('multiple') ? TRUE : FALSE;
    if ($multiple && is_array($value)) {
      $return = [];
      foreach ($value as $item) {
        $return[] = $this->denormalizedOptions[$item];
      }
      return $return;
    }
    elseif (!is_array($value) && $value !== null && strlen($value) && isset($this->denormalizedOptions[$value])) {
      return $this->denormalizedOptions[$value];
    }
    else {
      return NULL;
    }
  }

  protected function loadOptions() {
    if ($this->optionsLoaded) {
      return;
    }

    $this->options = [];
    $this->normalizedOptions = [];
    $this->denormalizedOptions = [];
    $this->loadPriceset();
    $this->optionsLoaded = TRUE;
  }

  public function setConfiguration(ConfigurationBag $configuration) {
    parent::setConfiguration($configuration);
    $this->optionsLoaded = FALSE;
    return $this;
  }

  /**
   * Sets the default values of this action
   */
  public function setDefaults() {
    parent::setDefaults();
    $this->optionsLoaded = FALSE;
  }

  /**
   * Load a specific price set.
   *
   * @return void
   */
  private function loadPriceset() {
    $includePriceFieldLabel = $this->configuration->get('inlcude_field_label');
    $fieldIds = $this->configuration->get('price_fields');
    $adminVisibilityID = CRM_Price_BAO_PriceField::getVisibilityOptionID('admin');
    $sql = "
      SELECT
        `civicrm_price_field_value`. `id`,
        `civicrm_price_field_value`.`label`,
        `civicrm_price_field`.`label` as `price_field_label`,
        `civicrm_price_field`.`is_display_amounts`,
        `civicrm_price_field_value`.`amount`
      FROM `civicrm_price_field_value`
      INNER JOIN `civicrm_price_field` ON `civicrm_price_field`.`id` = `civicrm_price_field_value`.`price_field_id`
      INNER JOIN `civicrm_price_set` ON `civicrm_price_set`.`id` = `civicrm_price_field`.`price_set_id`
      WHERE `civicrm_price_field_value`.`is_active` = '1'
      AND `civicrm_price_field`.`is_active` = '1'
      AND `civicrm_price_set`.`is_active` = '1'
      AND `civicrm_price_field_value`.`visibility_id` != %1
      AND `civicrm_price_field`.`visibility_id` != %1
      AND `civicrm_price_set`.`is_quick_config` = '0'
      AND (`civicrm_price_field`.`active_on` IS NULL OR `civicrm_price_field`.`active_on` <= NOW())
      AND (`civicrm_price_field`.`expire_on` IS NULL OR `civicrm_price_field`.`expire_on` >= NOW())
      AND `civicrm_price_field`.`id` IN (" . implode(',', $fieldIds) . ")
      ORDER BY `civicrm_price_field_value`.`weight`, `civicrm_price_field`.`weight`
    ";
    $sqlParams[1] = [$adminVisibilityID, 'Integer'];
    $dao = CRM_Core_DAO::executeQuery($sql, $sqlParams);
    while($dao->fetch()) {
      $amount = '';
      try {
        $amount = ' ' . CRM_Utils_Money::format($dao->amount);
      }
      catch (CRM_Core_Exception $e) {
      }
      if ($includePriceFieldLabel) {
        $this->options[$dao->id] = $dao->price_field_label . ' - ' . $dao->label;
      } else {
        $this->options[$dao->id] = $dao->label;
      }
      if ($dao->is_display_amounts) {
        $this->options[$dao->id] .= $amount;
      }
      $this->normalizedOptions[$dao->id] = $dao->id;
      $this->denormalizedOptions[$dao->id] = $dao->id;
    }
  }

}
