<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\FormProcessor\Type;

use CRM_Core_Exception;
use CRM_Utils_Type;

abstract class AbstractEntityType extends GenericType {

  /**
   * Returns the entity name.
   * @return string
   */
  abstract protected function getFkEntity(): string;

  public function alterApi4FieldSpec($field) {
    $field = parent::alterApi4FieldSpec($field);
    $field['fk_entity'] = $this->getFkEntity();
    $field['input_type'] = 'EntityRef';
    return $field;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return CRM_Utils_Type::T_INT;
  }

  public function validateValue($value, $allValues = []) {
    try {
      if (CRM_Utils_Type::validate($value, 'Integer', FALSE) === NULL) {
        return FALSE;
      }
    }
    catch (CRM_Core_Exception $e) {
      return FALSE;
    }
    return TRUE;
  }

}
