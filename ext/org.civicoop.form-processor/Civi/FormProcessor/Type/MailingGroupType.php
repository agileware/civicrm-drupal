<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use Civi\FormProcessor\Config\Specification;
use Civi\FormProcessor\Config\SpecificationBag;
use Civi\FormProcessor\Type\AbstractType;
use Civi\FormProcessor\Type\OptionListInterface;

use CRM_FormProcessor_ExtensionUtil as E;

class MailingGroupType extends AbstractType implements OptionListInterface {

  /**
   * Returns true when this field is a multiple field.
   *
   * @return bool
   */
  public function isMultiple() {
    return $this->configuration->get('multiple') == 1 ? TRUE : FALSE;
  }

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag([
      new Specification('multiple', 'Boolean', E::ts('Multiple'), FALSE, 0, NULL, [
        0 => E::ts('Single value'),
        1 => E::ts('Multiple values'),
      ]),
      new Specification('visibility', 'Integer', E::ts('Restrict to'), FALSE, 0, NULL, [
        'Public Pages' => E::ts('Public Pages'),
        'User and User Admin Only' => E::ts('User and User Admin Only'),
      ]),
    ]);
  }

  public function validateValue($value, $allValues = []) {
    $multiple = $this->configuration->get('multiple') ? TRUE : FALSE;
    $groups = $this->getOptions($allValues);
    if ($multiple && is_array($value)) {
      foreach ($value as $valueItem) {
        if ($valueItem && \CRM_Utils_Type::validate($valueItem, 'Integer', FALSE) === NULL) {
          return FALSE;
        }
        if ($valueItem && !isset($groups[$valueItem])) {
          return FALSE;
        }
      }
    }
    elseif (!is_array($value) && $value) {
      if (\CRM_Utils_Type::validate($value, 'Integer', FALSE) === NULL) {
        return FALSE;
      }

      if (!isset($groups[$value])) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return \CRM_Utils_Type::T_INT;
  }

  /**
   * Normalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function normalizeValue($value) {
    $multiple = $this->configuration->get('multiple') ? TRUE : FALSE;

    // Correct array values when field is not multiple.
    // this could be caused for example by a drupal webform with
    // a checkbox field. The value is submitted as an array, altough it contains
    // only one value.
    if (!$multiple && is_array($value) && count($value) == 1) {
      $value = reset($value);
    } elseif ($multiple && !is_array($value) && !strlen($value)) {
      return [];
    }

    return $value;
  }


  public function getOptions($params) {
    static $return;
    if ($return) {
      return $return;
    }
    $groupsParams['is_active'] = 1;
    $groupsParams['options']['limit'] = 0;
    if ($this->configuration->get('visibility')) {
      $groupsParams['visibility'] = $this->configuration->get('visibility');
    }
    $groups = civicrm_api3('Group', 'get', $groupsParams);
    $return = [];
    foreach ($groups['values'] as $group) {
      if (is_array($group) && key_exists('group_type',$group)) {
        if (!is_array($group['group_type'])) {
          $group['group_type'] = explode(",", $group['group_type']);
        }
        if (in_array('2', $group['group_type'])) {
          $return[$group['id']] = $group['title'];
        }
      }
    }
    return $return;
  }

}
