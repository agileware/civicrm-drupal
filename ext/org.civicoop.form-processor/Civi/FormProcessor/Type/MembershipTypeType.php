<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use \Civi\FormProcessor\Type\AbstractType;
use \Civi\FormProcessor\Type\OptionListInterface;

class MembershipTypeType extends AbstractType implements OptionListInterface {

  protected $options;

  /**
   * Returns true when this field is a multiple field.
   *
   * @return bool
   */
  public function isMultiple() {
    return FALSE;
  }

  public function validateValue($value, $allValues = []) {
    if (\CRM_Utils_Type::validate($value, 'Integer', FALSE) === NULL) {
      return FALSE;
    }

    $membershipTypes = $this->getOptions($allValues);
    if (!isset($membershipTypes[$value])) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return \CRM_Utils_Type::T_INT;
  }

  public function getOptions($params) {
    if (!$this->options) {
      $this->options = [];
      $membershipTypeApi = civicrm_api3('MembershipType', 'get', [
        'is_active' => 1,
        'options' => ['limit' => 0],
      ]);
      foreach ($membershipTypeApi['values'] as $membershipType) {
        $this->options[$membershipType['id']] = $membershipType['name'];
      }
    }
    return $this->options;
  }

  /**
   * Sets the default values of this action
   */
  public function setDefaults() {
    parent::setDefaults();
    $this->options = NULL;
  }

}
