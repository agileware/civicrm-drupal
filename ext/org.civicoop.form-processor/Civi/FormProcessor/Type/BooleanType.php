<?php

/**
 * @author Jon Goldberg <jon@megaphonetech.com>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use \Civi\FormProcessor\Config\Specification;
use \Civi\FormProcessor\Config\SpecificationBag;

use \CRM_FormProcessor_ExtensionUtil as E;

class BooleanType extends AbstractType {

  /**
   * Get the configuration specification
   *
   * @return \Civi\FormProcessor\Config\SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag([
      new Specification('return_as', 'String', E::ts('Represent data as:'), TRUE, 'boolean', NULL, [
        'boolean' => E::ts('TRUE/FALSE'),
        'integer' => E::ts('1/0'),
      ]),
    ]);
  }

  public function validateValue($value, $allValues = []) {
    return \CRM_Utils_Rule::boolean($value);
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    $returnAs = $this->configuration->get('return_as') ?? 'boolean';
    if ($returnAs === 'integer') {
      return \CRM_Utils_Type::T_INT;
    }
    return \CRM_Utils_Type::T_BOOLEAN;
  }

  /**
   * Normalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function normalizeValue($value) {
    $returnAs = $this->configuration->get('return_as');
    // switch-case does loose comparison, so:
    if (gettype($value) === 'boolean') {
      $result = $value;
    }
    else {
      switch (strtolower((string) $value)) {
        case '1';
        case 'true':
        case 'yes':
        case 'y':
        case 't':
          $result = TRUE;
          break;

        default:
          // We've already validated this as a boolean so we don't need to enumerate the FALSE values.
          $result = FALSE;
      }
    }
    if ($returnAs === 'integer') {
      return (int) $result;
    }
    return $result;
  }

  /**
   * Denormalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function denormalizeValue($value) {
    return $this->normalizeValue($value);
  }

}
