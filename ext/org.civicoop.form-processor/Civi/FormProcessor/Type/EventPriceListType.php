<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\FormProcessor\Type;

use Civi\FormProcessor\Config\ConfigurationBag;
use Civi\FormProcessor\Config\Specification;
use Civi\FormProcessor\Config\SpecificationBag;
use CRM_Core_BAO_Discount;
use CRM_Core_Config;
use CRM_Core_DAO;
use CRM_Core_Exception;
use CRM_FormProcessor_ExtensionUtil as E;
use CRM_Price_BAO_PriceField;
use CRM_Price_BAO_PriceSet;
use CRM_Utils_Array;
use CRM_Utils_Money;
use CRM_Utils_Type;

class EventPriceListType extends AbstractType implements OptionListInterface {

  protected $options;

  protected $amountOptions;

  protected $optionsLoaded = FALSE;

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    $returnOptions = [
      'id' => E::ts('ID (Default)'),
      'amount' => E::ts('Amount'),
      'label' => E::ts('Label'),
    ];
    $return = new Specification('return', 'String', E::ts('Use as'), true, 'id', null, $returnOptions);
    $return->setDescription(E::ts('If unsure chose ID. The value is used in the form processor processing.'));
    return new SpecificationBag([$return]);
  }

  /**
   * Override this function in a child class to when you expect
   * to build a. option list based on the default values.
   *
   * @return \Civi\FormProcessor\Config\SpecificationBag
   */
  public function getDefaultsParameterSpecification() {
    return new SpecificationBag([new Specification('event_id', 'Integer', E::ts('Event ID'), true)]);
  }

  public function setParameters(array $parameters) {
    parent::setParameters($parameters);
    $this->optionsLoaded = FALSE;
  }

  /**
   * Returns true when this field is a multiple field.
   *
   * @return bool
   */
  public function isMultiple() {
    return FALSE;
  }

  public function validateValue($value, $allValues = []) {
    $options = $this->getOptions($allValues);
    if (!is_array($value) && !empty($value) && isset($options[$value])) {
     try {
       if (($value = CRM_Utils_Type::validate($value, 'String', FALSE)) !== NULL) {
         if (isset($options[$value])) {
           return TRUE;
         }
       }
     }
     catch (CRM_Core_Exception $e) {
       return FALSE;
     }
    }

    return FALSE;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return CRM_Utils_Type::T_MONEY;
  }

  public function getOptions($params) {
    $this->loadOptions();
    return $this->options;
  }

  /**
   * Normalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function normalizeValue($value) {
    $this->loadOptions();
    switch ($this->configuration->get('return')) {
      case 'amount':
        if (!is_array($value) && !empty($value) && isset($this->amountOptions[$value])) {
          return $this->amountOptions[$value];
        }
        break;
      case 'label':
        if (!is_array($value) && !empty($value) && isset($this->options[$value])) {
          return $this->options[$value];
        }
        break;
      default:
        if (!is_array($value) && !empty($value) && isset($this->options[$value])) {
          return $value;
        }
        break;
    }
    return null;
  }

  /**
   * Denormalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function denormalizeValue($value) {
    $this->loadOptions();

    switch ($this->configuration->get('return')) {
      case 'amount':
        foreach($this->amountOptions as $id => $amount) {
          if ($amount == $value) {
            return $id;
          }
        }
        break;
      case 'label':
        foreach($this->options as $id => $label) {
          if ($label == $value) {
            return $id;
          }
        }
        break;
      default:
        if (!is_array($value) && !empty($value) && isset($this->options[$value])) {
          return $value;
        }
        break;
    }
    return null;
  }

  protected function loadOptions() {
    if ($this->optionsLoaded) {
      return;
    }

    $this->options = [];
    $this->amountOptions = [];
    $event_id = $this->parameters['event_id'] ?? null;
    if ($event_id) {
      $price_set_id = null;
      try {
        $discount_id = CRM_Core_BAO_Discount::findSet($event_id, 'civicrm_event');
        if (!empty($discount_id)) {
          $price_set_id = CRM_Core_DAO::singleValueQuery("SELECT `price_set_id` FROM `civicrm_discount` WHERE `id` = %1", [1=>[$discount_id, 'Integer']]);
        }
      }
      catch (CRM_Core_Exception $e) {
      }
      if (empty($price_set_id)) {
        $price_set_id = CRM_Price_BAO_PriceSet::getFor('civicrm_event', $event_id);
      }
      if ($price_set_id) {
        $this->loadPriceset($price_set_id);
      }
    } else {
      $this->loadAllPricesets();
    }
    $this->optionsLoaded = TRUE;
  }

  public function setConfiguration(ConfigurationBag $configuration) {
    parent::setConfiguration($configuration);
    $this->optionsLoaded = FALSE;
    return $this;
  }

  /**
   * Sets the default values of this action
   */
  public function setDefaults() {
    parent::setDefaults();
    $this->optionsLoaded = FALSE;
  }

  /**
   * Load a specific price set.
   *
   * @param int $priceSetId
   *
   * @return void
   */
  private function loadPriceset(int $priceSetId) {
    $adminVisibilityID = CRM_Price_BAO_PriceField::getVisibilityOptionID('admin');
    $sql = "
      SELECT
        `civicrm_price_field_value`. `id`,
        `civicrm_price_field_value`.`label`,
        `civicrm_price_field_value`.`amount`
      FROM `civicrm_price_field_value`
      INNER JOIN `civicrm_price_field` ON `civicrm_price_field`.`id` = `civicrm_price_field_value`.`price_field_id`
      INNER JOIN `civicrm_price_set` ON `civicrm_price_set`.`id` = `civicrm_price_field`.`price_set_id`
      WHERE `civicrm_price_field_value`.`is_active` = '1'
      AND `civicrm_price_field`.`is_active` = '1'
      AND `civicrm_price_set`.`is_active` = '1'
      AND `civicrm_price_field_value`.`visibility_id` != %1
      AND `civicrm_price_field`.`visibility_id` != %1
      AND `civicrm_price_set`.`is_quick_config` = '1'
      AND `civicrm_price_set`.`id` = %2
    ";
    $sqlParams[1] = [$adminVisibilityID, 'Integer'];
    $sqlParams[2] = [$priceSetId, 'Integer'];
    $dao = CRM_Core_DAO::executeQuery($sql, $sqlParams);
    while($dao->fetch()) {
      $amount = '';
      try {
        $amount = ' ' . CRM_Utils_Money::format($dao->amount);
      }
      catch (CRM_Core_Exception $e) {
      }
      $this->options[$dao->id] = $dao->label . $amount;
      $this->amountOptions[$dao->id] = $dao->amount;
    }
  }

  /**
   * Load a specific price set.
   *
   * @param int $priceSetId
   *
   * @return void
   */
  private function loadAllPricesets() {
    $adminVisibilityID = CRM_Price_BAO_PriceField::getVisibilityOptionID('admin');
    $domainID = CRM_Core_Config::domainID();
    $sql = "
      SELECT
        `civicrm_price_field_value`. `id`,
        `civicrm_price_field_value`.`label`,
        `civicrm_price_set`.`title`,
        `civicrm_price_field_value`.`amount`
      FROM `civicrm_price_field_value`
      INNER JOIN `civicrm_price_field` ON `civicrm_price_field`.`id` = `civicrm_price_field_value`.`price_field_id`
      INNER JOIN `civicrm_price_set` ON `civicrm_price_set`.`id` = `civicrm_price_field`.`price_set_id`
      LEFT JOIN `civicrm_price_set_entity` ON `civicrm_price_set_entity`.`price_set_id` = `civicrm_price_set`.`id` AND `civicrm_price_set_entity`.`entity_table` = 'civicrm_event'
      LEFT JOIN `civicrm_discount` ON `civicrm_discount`.`price_set_id` = `civicrm_price_set`.`id` AND `civicrm_discount`.`entity_table` = 'civicrm_event'
        AND (`civicrm_discount`.`start_date` IS NULL OR `civicrm_discount`.`start_date` <= CURRENT_DATE())
        AND (`civicrm_discount`.`end_date` IS NULL OR `civicrm_discount`.`end_date` >= CURRENT_DATE())
      WHERE `civicrm_price_field_value`.`is_active` = '1'
      AND `civicrm_price_field`.`is_active` = '1'
      AND `civicrm_price_set`.`is_active` = '1'
      AND `civicrm_price_field_value`.`visibility_id` != %1
      AND `civicrm_price_field`.`visibility_id` != %1
      AND `civicrm_price_set`.`is_quick_config` = '1'
      AND (`civicrm_price_set`.`domain_id` IS NULL OR `civicrm_price_set`.`domain_id` = %2)
      AND ((`civicrm_price_set_entity`.`id` IS NOT NULL AND `civicrm_discount`.`id` IS NULL) OR (`civicrm_price_set_entity`.`id` IS NULL AND `civicrm_discount`.`id` IS NOT NULL))
    ";
    $sqlParams[1] = [$adminVisibilityID, 'Integer'];
    $sqlParams[2] = [$domainID, 'Integer'];
    $dao = CRM_Core_DAO::executeQuery($sql, $sqlParams);
    while($dao->fetch()) {
      $amount = '';
      try {
        $amount = ' ' . CRM_Utils_Money::format($dao->amount);
      }
      catch (CRM_Core_Exception $e) {
      }
      $this->options[$dao->id] = $dao->title . ': ' . $dao->label . $amount;
      $this->amountOptions[$dao->id] = $dao->amount;
    }
  }

}
