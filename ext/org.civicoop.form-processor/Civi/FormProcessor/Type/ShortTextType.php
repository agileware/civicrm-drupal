<?php
/**
 * Copyright (C) 2024  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\FormProcessor\Type;

use Civi\FormProcessor\Config\Specification;
use Civi\FormProcessor\Config\SpecificationBag;
use CRM_FormProcessor_ExtensionUtil as E;

class ShortTextType extends GenericType {

  public function __construct($label) {
    parent::__construct('String', $label);
  }

  /**
   * Get the configuration specification
   *
   * @return \Civi\FormProcessor\Config\SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag([
      new Specification('trim', 'Boolean', E::ts('Remove leading and trailing spaces')),
    ]);
  }

  /**
   * Normalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function normalizeValue($value) {
    if ($this->configuration->get('trim')) {
      $value = trim($value);
    }
    return parent::normalizeValue($value);
  }

}
