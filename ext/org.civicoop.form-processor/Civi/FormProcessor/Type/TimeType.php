<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use \Civi\FormProcessor\Type\GenericType;
use \Civi\FormProcessor\Config\Specification;
use \Civi\FormProcessor\Config\SpecificationBag;

use \CRM_FormProcessor_ExtensionUtil as E;

class TimeType extends GenericType {

  public function __construct($label) {
    parent::__construct('Time', $label);
    $this->default_value_description = 'If you want to provide a default date set it here. See <a href="https://secure.php.net/manual/en/datetime.formats.php">Date and Time Formats on php.net</a> for possible formats';
  }

  /**
   * Get the configuration specification.
   *
   * The configuration is
   *   format: a string containing the input format.
   *           see for valid formats
   * http://nl1.php.net/manual/en/datetime.createfromformat.php#refsect1-datetime.createfromformat-parameters
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag([
      new Specification(
        'format', 'String', E::ts('Format'), TRUE, 'H:i', '', NULL, FALSE,
        E::ts("The format is the input format of the date. The following modifiers are availble: <br />
          H: hours in 24 format with a preceding 0 (eg 08) <br />
          i: minute with a preceding 0 (eg 06) <br />
          <br />
          <a href=\"http://nl1.php.net/manual/en/datetime.createfromformat.php#refsect1-datetime.createfromformat-parameters\">More information at php.net</a>
          ")
      ),
    ]);
  }

  /**
   * Returns the default value
   *
   * @param $defaultValue
   *
   * @return mixed
   */
  public function getDefaultValue($defaultValue) {
    return $defaultValue;
  }

  /**
   * Normalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function normalizeValue($value) {
    return $value;
  }

  /**
   * Denormalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function denormalizeValue($value) {
    if ($value) {
      $date = new \DateTime($value);
      return $date->format($this->configuration->get('format'));
    }
    return $value;
  }

  /**
   * Validate the value
   */
  public function validateValue($value, $allValues = []) {
    if (!$this->validateConfiguration()) {
      return FALSE;
    }
    if (!is_string($value)) {
      return FALSE;
    }
    $date = \DateTime::createFromFormat($this->configuration->get('format'), $value);
    if (!$date) {
      return FALSE;
    }
    return TRUE;
  }

}
