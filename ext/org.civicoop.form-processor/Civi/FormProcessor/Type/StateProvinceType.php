<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use \Civi\FormProcessor\Config\Specification;
use \Civi\FormProcessor\Config\SpecificationBag;

use \CRM_FormProcessor_ExtensionUtil as E;

class StateProvinceType extends AbstractType implements OptionListInterface {

  private $options = [];
  private $normalizedOptions = [];
  private $denormalizedOptions = [];
  private $optionsLoaded = FALSE;

  /**
   * Get the configuration specification
   *
   * @return \Civi\FormProcessor\Config\SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag([
      new Specification('use_name_as_value', 'Boolean', E::ts('Use state/province name as value'), TRUE, 0, NULL, [
        0 => E::ts('No'),
        1 => E::ts('Yes'),
      ], FALSE, E::ts('Select "Yes" to pass a state/province name, "No" to pass a numeric ID.')),
      new Specification('limit_to_enabled', 'Boolean', E::ts('Limit to enabled states/provinces'), TRUE, 1, NULL, [
        0 => E::ts('No'),
        1 => E::ts('Yes'),
      ], FALSE, E::ts('Limit states/provinces in Administer » Localization » Languages, Currency, Locations.')),
    ]);
  }

  /**
   * Returns true when this field is a multiple field.
   *
   * @return bool
   */
  public function isMultiple() {
    return FALSE;
  }

  public function validateValue($value, $allValues = []) {
    if (\CRM_Utils_Type::validate($value, 'String', FALSE) === NULL) {
      return FALSE;
    }

    $states = $this->getOptions($allValues);
    if (!isset($states[$value])) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return \CRM_Utils_Type::T_STRING;
  }

  /**
   * Normalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function normalizeValue($value) {
    $this->loadOptions();

    // Correct array values
    // this could be caused for example by a drupal webform with
    // a checkbox field. The value is submitted as an array, altough it contains
    // only one value.
    if (is_array($value) && count($value) == 1) {
      $value = reset($value);
    }
    return $this->normalizedOptions[$value];
  }

  /**
   * Denormalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function denormalizeValue($value) {
    $this->loadOptions();
    if (!is_array($value) && strlen($value)) {
      return $this->denormalizedOptions[$value];
    }
  }

  public function getOptions($params) {
    $this->loadOptions();
    return $this->options;
  }

  public function loadOptions() {
    if ($this->optionsLoaded) {
      return;
    }
    $use_name = $this->configuration->get('use_name_as_value') ? TRUE : FALSE;
    $limitToEnabled = $this->configuration->get('limit_to_enabled') ? TRUE : FALSE;
    $states = \CRM_Core_PseudoConstant::stateProvince(FALSE, $limitToEnabled);
    foreach ($states as $key => $state) {
      $value = $key;
      if ($use_name) {
        $value = $state;
      }
      $this->options[$value] = $state;
      $this->normalizedOptions[$value] = $key;
      $this->denormalizedOptions[$key] = $value;
    }
    $this->optionsLoaded = TRUE;
  }

}
