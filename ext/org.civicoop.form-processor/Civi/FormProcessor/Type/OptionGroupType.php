<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use Civi\FormProcessor\Config\ConfigurationBag;
use Civi\FormProcessor\Config\Specification;
use Civi\FormProcessor\Config\SpecificationBag;
use CRM_FormProcessor_ExtensionUtil as E;

class OptionGroupType extends AbstractType implements OptionListInterface {

  protected $options;

  protected $normalizedOptions;

  protected $denormalizedOptions;

  protected $optionsLoaded = FALSE;

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    $optionsGroups = [];
    $optionGroupsApi = civicrm_api3('OptionGroup', 'get', [
      'is_active' => 1,
      'options' => ['limit' => 0],
    ]);
    foreach ($optionGroupsApi['values'] as $optionGroup) {
      $optionsGroups[$optionGroup['name']] = $optionGroup['title'];
    }
    return new SpecificationBag([
      new Specification('use_label_as_value', 'Boolean', E::ts('Use label as value'), TRUE, 0, NULL, [
        0 => E::ts('No'),
        1 => E::ts('Yes'),
      ]),
      new Specification('option_group_name', 'Integer', E::ts('Option group'), TRUE, NULL, NULL, $optionsGroups, FALSE),
      new Specification('multiple', 'Boolean', E::ts('Multiple'), FALSE, 0, NULL, [
        0 => E::ts('Single value'),
        1 => E::ts('Multiple values'),
      ]),
    ]);
  }

  public function setParameters(array $parameters) {
    parent::setParameters($parameters);
    $this->optionsLoaded = FALSE;
    $this->options = null;
    $this->denormalizedOptions = null;
    $this->normalizedOptions = null;
  }

  /**
   * Returns true when this field is a multiple field.
   *
   * @return bool
   */
  public function isMultiple() {
    return $this->configuration->get('multiple') ? TRUE : FALSE;
  }

  public function validateValue($value, $allValues = []) {
    $multiple = $this->configuration->get('multiple') ? TRUE : FALSE;
    $options = $this->getOptions($allValues);

    // Correct array values when field is not multiple.
    // this could be caused for example by a drupal webform with
    // a checkbox field. The value is submitted as an array, altough it contains
    // only one value.
    if (!$multiple && is_array($value) && count($value) == 1) {
      $value = reset($value);
    }

    if ($multiple && is_array($value)) {
      $value = $this->removeNullValues($value);
      foreach ($value as $valueItem) {
        if (\CRM_Utils_Type::validate($valueItem, 'String', FALSE) === NULL) {
          return FALSE;
        }
        if (!isset($options[$valueItem])) {
          return FALSE;
        }
      }
    }
    elseif (!is_array($value) && $value) {
      if (\CRM_Utils_Type::validate($value, 'String', FALSE) === NULL) {
        return FALSE;
      }

      if (!isset($options[$value])) {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return \CRM_Utils_Type::T_STRING;
  }

  public function getOptions($params) {
    $this->loadOptions();
    return $this->options;
  }

  /**
   * Normalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function normalizeValue($value) {
    $this->loadOptions();
    $value = $this->removeNullValues($value);
    $multiple = $this->configuration->get('multiple') ? TRUE : FALSE;

    // Correct array values when field is not multiple.
    // this could be caused for example by a drupal webform with
    // a checkbox field. The value is submitted as an array, altough it contains
    // only one value.
    if (!$multiple && is_array($value) && count($value) == 1) {
      $value = reset($value);
    }

    if ($multiple && is_array($value)) {
      $return = [];
      foreach ($value as $item) {
        $return[] = $this->normalizedOptions[$item];
      }
      return $return;
    }
    elseif (!is_array($value) && $value !== null && strlen($value) && isset($this->normalizedOptions[$value])) {
      return $this->normalizedOptions[$value];
    }
    else {
      return NULL;
    }
  }

  /**
   * Denormalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function denormalizeValue($value) {
    $this->loadOptions();
    $multiple = $this->configuration->get('multiple') ? TRUE : FALSE;
    if ($multiple && is_array($value)) {
      $return = [];
      foreach ($value as $item) {
        $return[] = $this->denormalizedOptions[$item];
      }
      return $return;
    }
    elseif (!is_array($value) && $value !== null && strlen($value) && isset($this->denormalizedOptions[$value])) {
      return $this->denormalizedOptions[$value];
    }
    else {
      return NULL;
    }
  }

  protected function loadOptions() {
    if ($this->optionsLoaded) {
      return;
    }

    $this->options = [];
    $this->normalizedOptions = [];
    $this->denormalizedOptions = [];

    $use_label = $this->configuration->get('use_label_as_value') ? TRUE : FALSE;
    $option_group_name = $this->configuration->get('option_group_name');
    if (!$option_group_name) {
      return;
    }
    $optionsApi = civicrm_api3('OptionValue', 'get', [
      'option_group_id' => $option_group_name,
      'is_active' => 1,
      'options' => ['sort' => 'weight', 'limit' => 0],
    ]);
    foreach ($optionsApi['values'] as $optionValue) {
      $value = $optionValue['value'];
      if ($use_label) {
        $value = $optionValue['label'];
      }
      $this->options[$value] = $optionValue['label'];
      $this->normalizedOptions[$value] = $optionValue['value'];
      $this->denormalizedOptions[$optionValue['value']] = $value;
    }

    $this->optionsLoaded = TRUE;
  }

  public function setConfiguration(ConfigurationBag $configuration) {
    parent::setConfiguration($configuration);
    $this->normalizedOptions = NULL;
    $this->denormalizedOptions = NULL;
    $this->options = NULL;
    return $this;
  }

  /**
   * Sets the default values of this action
   */
  public function setDefaults() {
    parent::setDefaults();
    $this->normalizedOptions = NULL;
    $this->denormalizedOptions = NULL;
    $this->options = NULL;
    $this->optionsLoaded = FALSE;
  }

}
