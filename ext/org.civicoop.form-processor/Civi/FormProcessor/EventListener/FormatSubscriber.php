<?php
/**
 * Copyright (C) 2022  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\FormProcessor\EventListener;

use Civi\FormProcessor\Event\FormatDatabagEvent;
use Civi\FormProcessor\Event\FormatTypeEvent;
use Civi\FormProcessor\Type\BooleanType;
use Civi\FormProcessor\Type\CountryIsoCodeType;
use Civi\FormProcessor\Type\OptionListInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use CRM_FormProcessor_ExtensionUtil as E;

class FormatSubscriber implements EventSubscriberInterface {

  /**
   * Returns an array of event names this subscriber wants to listen to.
   *
   * The array keys are event names and the value can be:
   *
   *  * The method name to call (priority defaults to 0)
   *  * An array composed of the method name to call and the priority
   *  * An array of arrays composed of the method names to call and respective
   *    priorities, or 0 if unset
   *
   * For instance:
   *
   *  * ['eventName' => 'methodName']
   *  * ['eventName' => ['methodName', $priority]]
   *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
   *
   * @return array The event names to listen to
   */
  public static function getSubscribedEvents() {
    return [
      FormatTypeEvent::NAME => 'onFormatType',
      FormatDatabagEvent::NAME => 'onFormatDatabag',
    ];
  }

  /**
   * @param \Civi\FormProcessor\Event\FormatTypeEvent $event
   *
   * @return void
   */
  public function onFormatType(FormatTypeEvent $event) {
    $event->formats = [
      'plain' => E::ts('Plain Text with submitted data'),
      'html_table' => E::ts('HTML Table with submitted data'),
    ];
  }

  public function onFormatDatabag(FormatDatabagEvent $event) {
    $htmlTable = '<table>';
    $plain = '';
    foreach ($event->databag->getAllInputs() as $input) {
      if (!$input->include_formatted_params) {
        continue;
      }
      $value = $event->databag->getInputData($input);
      $inputType = $event->databag->getInputType($input->name);
      if ($inputType instanceof BooleanType) {
        if ($value) {
          $value = E::ts('Yes');
        } else {
          $value = E::ts('No');
        }
      } elseif ($inputType instanceof OptionListInterface) {
        $options = $inputType->getOptions([]);
        $rawValue = $value;
        if (is_array($rawValue)) {
          $value = [];
          foreach($rawValue as $v) {
            $value[] = $options[$inputType->denormalizeValue($v)];
          }
        } else {
          $value = $options[$inputType->denormalizeValue($rawValue)];
        }
      }
      if (is_array($value)) {
        $value = implode(", ", $value);
      }
      $htmlTable .= '<tr><th>' . $input->title . '</th><td>' . $value . '</td></tr>';
      $plain .=  $input->title . ': ' . $value . "\r\n";
    }
    $htmlTable .= '</table>';
    $event->databag->setFormattedDatabag('plain', $plain);
    $event->databag->setFormattedDatabag('html_table', $htmlTable);
  }

}
