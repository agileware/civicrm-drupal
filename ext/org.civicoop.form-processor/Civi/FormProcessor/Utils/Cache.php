<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\FormProcessor\Utils;

use Civi;

class Cache {

  /**
   * @param $key
   * @param null|mixed $default
   *
   * @return mixed
   */
  public static function get($key, $default=null) {
    try {
      $cache = self::getCacheClass();
      return $cache->get($key, $default);
    }
    catch (\Throwable $e) {

    }
    return $default;
  }

  /**
   * Set the value in the cache.
   *
   * @param string $key
   * @param mixed $value
   * @param null|int|\DateInterval $ttl
   * @return bool
   */
  public static function set($key, $value, $ttl=null): bool {
    try {
      $cache = self::getCacheClass();
      return $cache->set($key, $value, $ttl);
    } catch (\Throwable $e) {

    }
    return false;
  }

  /**
   * Set the value in the cache.
   *
   * @param string $formProcessorName
   * @param string $key
   * @param mixed $value
   * @param null|int|\DateInterval $ttl
   * @return bool
   */
  public static function setForFormProcessor(string $formProcessorName, $key, $value, $ttl=null): bool {
    $keys = Civi::cache('metadata')->get('FormProcessor.cachekeys');
    if (!is_array($keys)) {
      $keys = [];
    }
    if (!isset($keys[$formProcessorName])) {
      $keys[$formProcessorName] = [];
    }
    if (!in_array($key, $keys[$formProcessorName])) {
      $keys[$formProcessorName][] = $key;
    }
    Civi::cache('metadata')->set('FormProcessor.cachekeys', $keys);
    return self::set($key, $value, $ttl);
  }

  /**
   * Delete all values from the cache.
   *
   * NOTE: flush() and clear() should be aliases. flush() is specified by
   * Civi's traditional interface, and clear() is specified by PSR-16.
   *
   * @return bool
   * @see flush
   */
  public static function clear(): bool {
    try {
      // Also clear metadata cache for APIv4 entities created from FormProcessors
      Civi::cache('metadata')->delete('api4.entities.info');
      Civi::cache('metadata')->delete('api4.schema.map');
      $cache = self::getCacheClass();
      $cache->delete('FormProcessor.run.lists');
      $cache->delete('formprocessor.getactions');
      $cache->delete('formprocessorcalculation.getactions');
      $cache->delete('formprocessordefaults.getactions');
      $cache->delete('formprocessorvalidation.getactions');
      $formProcessors = Civi::cache('metadata')->get('FormProcessor.cachekeys');
      if (!is_array($formProcessors)) {
        $formProcessors = [];
      }
      foreach ($formProcessors as $formProcessorName => $keys) {
        foreach ($keys as $key) {
          $cache->delete($key);
        }
      }
      Civi::cache('metadata')->delete('FormProcessor.cachekeys');
    } catch (\CRM_Core_Exception $e) {
    }
    return true;
  }

  public static function cacheClearForFormProcessorByName(string $formProcessorName): void {
    try {
      $formProcessors = Civi::cache('metadata')->get('FormProcessor.cachekeys');
      if (!is_array($formProcessors)) {
        $formProcessors = [];
      }
      if (isset($formProcessors[$formProcessorName]) && is_array($formProcessors[$formProcessorName])) {
        $cache = self::getCacheClass();
        foreach ($formProcessors[$formProcessorName] as $key) {
          $cache->delete($key);
        }
        unset($formProcessors[$formProcessorName]);
        Civi::cache('metadata')->set('FormProcessor.cachekeys', $formProcessors);
      }
    } catch (\CRM_Core_Exception $e) {
    }
  }

  public static function cacheClearForFormProcessorById(int $formProcessorId): void {
    $name = \CRM_FormProcessor_BAO_FormProcessorInstance::getName($formProcessorId);
    if (strlen($name)) {
      self::cacheClearForFormProcessorByName($name);
    }
  }

  /**
   * @return \CRM_Utils_Cache_Interface
   * @throws \CRM_Core_Exception
   */
  public static function getCacheClass() {
    $cache = \CRM_Utils_Cache::create([
      'name' => 'formprocessor',
      'type' => ['*memory*', 'SqlGroup', 'ArrayCache'],
      'prefetch' => true,
    ]);
    return $cache;
  }

}
