<?php

namespace Civi\FormProcessor\Config;

use Civi\FormProcessor\Config\ConfigurationBag;
use Civi\FormProcessor\Config\SpecificationBag;
use Civi\FormProcessor\Config\SpecificationInterface;

class SpecificationCollection implements SpecificationInterface {

  /**
   * @var SpecificationBag
   */
  protected $specificationBag;

  /**
   * @var false|int
   */
  protected $min = false;

  /**
   * @var false|int
   */
  protected $max = false;

  /**
   * @var string
   */
  protected $name;
  /**
   * @var string
   */
  protected $title;
  /**
   * @var string
   */
  protected $description;

    /**
   * @param $name
   * @param $dataType
   */
  public function __construct($name, $title,  SpecificationBag $specificationBag, $min=false, $max=false, $description='') {
    $this->setName($name);
    $this->setTitle($title);
    $this->setMin($min);
    $this->setMax($max);
    $this->specificationBag = $specificationBag;
    $this->setDescription($description);
  }

  /**
   * Returns the type of specifcation
   *
   * @return string
   */
  public function getType() {
    return 'collection';
  }

  /**
   * Validates the given value
   *
   * @param mixed $value
   * @return bool
   */
  public function validateValue($collection) {
    if (!is_array($collection)) {
      return false;
    }
    $count = count($collection);
    if ($this->min !== false && $count < $this->min) {
      return false;
    }
    if ($this->max !== false && $count > $this->max) {
      return false;
    }
    foreach($collection as $value) {
      if ($value instanceof ConfigurationBag) {
        $configuration = $value;
      } elseif (is_array($value)) {
        $configuration = new ConfigurationBag();
        foreach($value as $k => $v) {
          $configuration->set($k, $v);
        }
      } else {
        return false; // invalid value
      }
      if (!SpecificationBag::validate($configuration, $this->specificationBag)) {
        return false;
      }
    }
  }

  /**
   * Returns the default value
   *
   * @return mixed
   */
  public function getDefaultValue() {
    $defaultValues = array();
    if ($this->min > 0) {
      $defaultValue = new ConfigurationBag;
      foreach($this->specificationBag as $spec) {
        $defaultValue->set($spec->getName(), $spec->getDefaultValue());
      }
      for($i=0; $i < $this->min; $i++) {
        $defaultValues[] = $defaultValue;
      }
    }
    return $defaultValues;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $name
   *
   * @return $this
   */
  public function setName($name) {
    $this->name = $name;
    return $this;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @param string $title
   *
   * @return $this
   */
  public function setTitle($title) {
    $this->title = $title;
    return $this;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param string $description
   *
   * @return $this
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * Returns the minimum count for the collection. False if not set.
   *
   * @return false|int
   */
  public function getMin() {
    return $this->min;
  }

  /**
   * Sets the minimum count for the collection. False if not set.
   *
   * @param false|int
   * @return SpecificationCollection
   */
  public function setMin($min) {
    $this->min = $min;
    return $this;
  }

  /**
   * Returns the maximum count for the collection. False if not set.
   *
   * @return false|int
   */
  public function getMax() {
    return $this->max;
  }

  /**
   * Sets the maximum count for the collection. False if not set.
   *
   * @param false|int
   * @return SpecificationCollection
   */
  public function setMax($max) {
    $this->max = $max;
    return $this;
  }

  public function getSpecificationBag(): SpecificationBag {
    return $this->specificationBag;
  }

  /**
   * Converts the object to an array.
   *
   * @return array
   */
  public function toArray() {
    $defaultValues = array();
    foreach($this->getDefaultValue() as $idx => $value) {
      $defaultValues[$idx] = $value->toArray();
    }
    return array(
      'specification_bag' => $this->specificationBag->toArray(),
      'type' => $this->getType(),
      'name' => $this->getName(),
      'title' => $this->getTitle(),
      'description' => $this->getDescription(),
      'min' => $this->getMin(),
      'max' => $this->getMax(),
      'default_value' => $defaultValues,
    );
  }

}
