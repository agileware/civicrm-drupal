<?php

namespace Civi\FormProcessor\Config;

class ConvertFromActionProvider {

  public static function convertSpecification(\Civi\ActionProvider\Parameter\Specification $actionProviderSpec): Specification {
    return new Specification($actionProviderSpec->getName(), $actionProviderSpec->getDataType(), $actionProviderSpec->getTitle());
  }

}
