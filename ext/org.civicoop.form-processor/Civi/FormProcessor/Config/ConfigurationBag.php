<?php

namespace Civi\FormProcessor\Config;

class ConfigurationBag implements \IteratorAggregate, \JsonSerializable {

  protected $parameters = array();

  /**
   * Get the parameter.
   */
  public function get($name) {
    if (isset($this->parameters[$name])) {
      return $this->parameters[$name];
    }
    return null;
  }
  /**
   * Tests whether the parameter with the name exists.
   */
  public function doesConfigExists($name) {
    if (isset($this->parameters[$name])) {
      return true;
    }
    return false;
  }

  /**
   * Sets parameter.
   */
  public function set($name, $value) {
    $this->parameters[$name] = $value;
  }

  public function getIterator(): \Traversable {
    return new \ArrayIterator($this->parameters);
  }

  /**
   * Converts the object to an array.
   *
   * @return array
   */
  public function toArray() {
    $return = array();
    foreach($this->parameters as $parameter => $value) {
      if (is_array($value)) {
        foreach ($value as $i => $v) {
          if ($v instanceof ConfigurationBag) {
            $value[$i] = $v->toArray();
          }
        }
      }
      $return[$parameter] = $value;
    }
    return $return;
  }

  /**
   * Specify data which should be serialized to JSON
   *
   * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
   * @return mixed data which can be serialized by <b>json_encode</b>,
   * which is a value of any type other than a resource.
   * @since 5.4.0
   */
  public function jsonSerialize(): array {
    return $this->toArray();
  }

}
