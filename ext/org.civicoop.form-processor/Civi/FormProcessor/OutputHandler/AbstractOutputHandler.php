<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

 namespace Civi\FormProcessor\OutputHandler;

 use \Civi\FormProcessor\Config\ConfigurationBag;
 use \Civi\FormProcessor\Config\Specification;
 use \Civi\FormProcessor\Config\SpecificationBag;
 use \Civi\FormProcessor\OutputHandler\OutputHandlerInterface;
 use \Civi\FormProcessor\DataBag;

 abstract class AbstractOutputHandler implements OutputHandlerInterface {

  /**
   * @var ConfigurationBag
   */
  protected $configuration;

  /**
   * @var ConfigurationBag
   */
  protected $defaultConfiguration;

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  abstract public function getConfigurationSpecification();

  /**
   * Convert the action data to output data.
   *
   * @param DataBag $dataBag
   * @param string $formProcessorName
   * @return array
   */
  abstract public function handle(DataBag $dataBag, string $formProcessorName): array;

   /**
    * Returns the output of a form processor.
    *
    * @param string $formProcessorName
    * @return \Civi\FormProcessor\Config\SpecificationBag
    */
  abstract public function getOutputSpecification(string $formProcessorName): SpecificationBag;

  /**
   * Returns the label of the output handler.
   *
   * @return string
   */
  abstract public function getLabel();

  public function __construct() {
    $this->configuration = null;
    $this->defaultConfiguration = null;
  }

  /**
   * @return ConfigurationBag
   */
  public function getConfiguration() {
    if (!$this->configuration) {
      $this->configuration = clone $this->getDefaultConfiguration();
    }
    return $this->configuration;
  }

  /**
   * @return ConfigurationBag
   */
  public function getDefaultConfiguration() {
    if (!$this->defaultConfiguration) {
      $this->defaultConfiguration = new ConfigurationBag();
      foreach($this->getConfigurationSpecification() as $spec) {
        $this->defaultConfiguration->set($spec->getName(), $spec->getDefaultValue());
      }
    }
    return $this->defaultConfiguration;
  }

  /**
   * @param ConfigurationBag $configuration
   */
  public function setConfiguration(ConfigurationBag $configuration) {
    $this->configuration = $configuration;
    return $this;
  }

  /**
   * Converts the handler to an array
   *
   * @return array
   */
  public function toArray() {
    return array(
      'name' => $this->getName(),
      'label' => $this->getLabel(),
      'configuration_spec' => $this->getConfigurationSpecification()->toArray(),
      'configuration' => $this->getConfiguration()->toArray(),
      'default_configuration' => $this->getDefaultConfiguration()->toArray(),
    );
  }

  /**
   * Returns the name of the output handler.
   *
   * @return string
   */
  public function getName() {
    $reflect = new \ReflectionClass($this);
    $className = $reflect->getShortName();
    return $className;
  }

 }
