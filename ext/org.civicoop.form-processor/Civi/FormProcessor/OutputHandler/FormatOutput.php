<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

 namespace Civi\FormProcessor\OutputHandler;

 use Civi\FormProcessor\Config\Specification;
 use Civi\FormProcessor\Config\SpecificationBag;
 use Civi\FormProcessor\Config\SpecificationCollection;
 use Civi\FormProcessor\Config\SpecificationFields;
  use Civi\FormProcessor\DataBag;

 use CRM_FormProcessor_ExtensionUtil as E;

 class FormatOutput extends AbstractOutputHandler {

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification(): SpecificationBag {
    return new SpecificationBag(array(
      new SpecificationCollection(
        'fields',
        E::ts('Output fields'),
        new SpecificationBag(array(
          new SpecificationFields('field', E::ts('Field'), true),
          new Specification('output_name', 'String', E::ts('Output name'), true, null, null, null),
        ))
      )
    ));
  }

  /**
   * Convert the action data to output data.
   *
   * @param DataBag $dataBag
   * @param string $formProcessorName
   * @return array
   */
  public function handle(DataBag $dataBag, string $formProcessorName): array {
    $output = array();
    $fields = $this->configuration->get('fields');
    foreach($fields as $field) {
      $output[$field['output_name']] = $dataBag->getDataByAlias($field['field']);
    }
    return $output;
  }

   public function getOutputSpecification(string $formProcessorName): SpecificationBag {
     $fields = $this->configuration->get('fields');
     $spec = new SpecificationBag();
     foreach($fields as $field) {
       $spec->addSpecification(new Specification($field['output_name'], 'String', $field['output_name']));
     }
     return $spec;
   }

   /**
   * Returns the label of the output handler.
   *
   * @return string
   */
  public function getLabel(): string {
    return E::ts('Decide what to send');
  }

 }
