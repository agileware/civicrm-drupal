<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

 namespace Civi\FormProcessor\OutputHandler;

 use \Civi\FormProcessor\Config\ConfigurationBag;
 use \Civi\FormProcessor\Config\Specification;
 use \Civi\FormProcessor\Config\SpecificationBag;
 use \Civi\FormProcessor\DataBag;

 interface OutputHandlerInterface {

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification();

  /**
   * @return ConfigurationBag
   */
  public function getConfiguration();

  /**
   * @return ConfigurationBag
   */
  public function getDefaultConfiguration();

  /**
   * @param ConfigurationBag $configuration
   */
  public function setConfiguration(ConfigurationBag $configuration);

   /**
    * Returns the output of a form processor.
    *
    * @param string $formProcessorName
    * @return \Civi\FormProcessor\Config\SpecificationBag
    */
   public function getOutputSpecification(string $formProcessorName): SpecificationBag;

  /**
   * Convert the action data to output data.
   *
   * @param DataBag $dataBag
   * @param string $formProcessorName
   * @return array
   */
  public function handle(DataBag $dataBag, string $formProcessorName): array;

  /**
   * Converts the handler to an array
   *
   * @return array
   */
  public function toArray();

  /**
   * Returns the name of the output handler.
   *
   * @return string
   */
  public function getName();

  /**
   * Returns the label of the output handler.
   *
   * @return string
   */
  public function getLabel();

 }
