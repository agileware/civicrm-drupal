<?php
namespace Civi\Api4;

/**
 * FormProcessorValidateAction entity.
 *
 * Provided by the Form Processor extension.
 *
 * @package Civi\Api4
 */
class FormProcessorValidateAction extends Generic\DAOEntity {

}
