<?php
/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\Api4\Action\FormProcessorEntity;

use Civi\Api4\Generic\AbstractSaveAction;
use Civi\Api4\Generic\Result;
use Civi\FormProcessor\Runner;

class FormProcessorSaveAction extends AbstractSaveAction {

  /**
   * @param \Civi\Api4\Generic\Result $result
   */
  public function _run(Result $result) {
    $formProcessorName = substr($this->getEntityName(), strlen('FormProcessor_'));
    foreach ($this->getRecords() as $record) {
      $result[] = Runner::run($formProcessorName, $record);
    }
  }

  public function getPermissions() {
    $formProcessorName = substr($this->getEntityName(), strlen('FormProcessor_'));
    $processor = Runner::getFormProcessor($formProcessorName);
    return (array) ($processor['permission'] ?: NULL);
  }

}
