{if $legacyTags}
  {literal}
  <div ng-app="crmApp">
    <div ng-view></div>
  </div>
  {/literal}
{else}
  {literal}
    <crm-angular-js modules="crmApp">
      <div ng-view></div>
    </crm-angular-js>
  {/literal}
{/if}

