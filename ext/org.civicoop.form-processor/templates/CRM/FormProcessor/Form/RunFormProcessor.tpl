{crmScope extensionKey='form-processor'}
{include file="CRM/FormProcessor/Form/Tab.tpl"}
{if ($enableDefault)}
<div class="crm-form-block">
  <div class="crm-accordion-wrapper crm-advanced_search_form-accordion">
    <div class="crm-accordion-header crm-master-accordion-header">{ts}Retrieve default data{/ts}</div>
    <div class="crm-accordion-body">
      <p class="help">
        {ts}With the retrieval of default data you can lookup existing data and then change it.{/ts}
      </p>
      {foreach from=$defaultFieldNames item=fieldName}
        <div class="crm-section">
          <div class="label">{$form.$fieldName.label}</div>
          <div class="content">{$form.$fieldName.html}</div>
          <div class="clear"></div>
        </div>
      {/foreach}
      <div class="crm-submit-buttons">

      <span class="crm-button crm-button-type-refresh crm-button">
        {$form.buttons._qf_RunFormProcessor_refresh.html}
      </span>

      </div>
    </div>
  </div>
</div>
{/if}

<div class="crm-block crm-form-block crm-form-processor-tryout-block">
<p class="help">
  <i class="crm-i fa-exclamation-triangle" aria-hidden="true">&nbsp;</i>{ts}<strong>Danger!</strong> Using the form below might change actual data. Use it with care.{/ts}
</p>
{foreach from=$fieldNames item=fieldName}
<div class="crm-section">
  <div class="label">{$form.$fieldName.label}</div>
  <div class="content">{$form.$fieldName.html}</div>
  <div class="clear"></div>
</div>
{/foreach}

<div class="crm-section">
  <div class="label">&nbsp;</div>
  <div class="content">
    <span class="crm-button crm-button-type-submit crm-button">
      {$form.buttons._qf_RunFormProcessor_submit.html}
    </span>
  </div>
  <div class="clear"></div>

  {if !empty($result_json)}
    <h3>{ts}Result{/ts}</h3>
    <pre>{$result_json}</pre>
  {/if}
</div>
</div>
{/crmScope}
