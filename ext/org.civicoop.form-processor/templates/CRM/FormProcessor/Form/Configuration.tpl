{crmScope extensionKey='form-processor'}
{include file="CRM/FormProcessor/Form/Tab.tpl"}
{if $action eq 8}
    {* Are you sure to delete form *}
  <h3>{ts}Delete Form Processor{/ts}</h3>
  <div class="crm-block crm-form-block crm-form-processor-block">
    <div class="crm-section">{ts 1=$form_processor_instance.title}Are you sure to delete form processor '%1'?{/ts}</div>
  </div>

  <div class="crm-submit-buttons">
      {include file="CRM/common/formButtons.tpl" location="bottom"}
  </div>
{elseif $action eq 131072}
    {* Are you sure to revert form *}
  <h3>{ts}Restore Form Processor to imported version{/ts}</h3>
  <div class="crm-block crm-form-block crm-form-processor-block">
    <div class="crm-section">{ts 1=$form_processor_instance.title}Are you sure to restore form processor '%1' to its imported version?{/ts}</div>
  </div>

  <div class="crm-submit-buttons">
      {include file="CRM/common/formButtons.tpl" location="bottom"}
  </div>
{elseif (!$show_only_output_handler_config)}
<h3>{ts}Define Form Processor{/ts}</h3>
<div class="crm-block crm-form-block crm-form-processor-configuration-block">
  <div class="crm-section">
    <div class="label">{$form.title.label}</div>
    <div class="content">{$form.title.html}
      <span class="">
        {ts}System name:{/ts}&nbsp;
        <span id="systemName" style="font-style: italic;">{if (!empty($form_processor_instance))}{$form_processor_instance.name}{/if}</span>
        <a href="javascript:void(0);" onclick="CRM.$('#nameSection').removeClass('hiddenElement'); CRM.$(this).parent().addClass('hiddenElement'); return false;">
          {ts}Change{/ts}
        </a>
        </span>
    </div>
    <div class="clear"></div>
  </div>
  <div id="nameSection" class="crm-section hiddenElement">
    <div class="label">{$form.name.label}</div>
    <div class="content">{$form.name.html}</div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.description.label}</div>
    <div class="content">{$form.description.html}</div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.is_active.label}</div>
    <div class="content">{$form.is_active.html}</div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.permission.label}</div>
    <div class="content">{$form.permission.html}</div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.output_handler.label}</div>
    <div class="content">{$form.output_handler.html}</div>
    <div class="clear"></div>
  </div>
  {if $form_processor_id}
    <h3>{ts}Inputs{/ts}</h3>
    <div class="crm-block crm-form-block crm-form-processor-inputs-block">
        {include file="CRM/FormProcessor/Form/Blocks/Inputs.tpl"}
      <div class="crm-submit-buttons">
        <a class="add button" title="{ts}Add Input{/ts}" href="{crmURL p=$input_base_url q="reset=1&action=add&form_processor_id=`$form_processor_id`"}">
          <span><div class="icon add-icon ui-icon-circle-plus"></div>{ts}Add input{/ts}</span></a>
      </div>
    </div>

    <h3>{ts}Actions{/ts}</h3>
    <div class="crm-block crm-form-block crm-form-processor-actions-block">
      {include file="CRM/FormProcessor/Form/Blocks/Actions.tpl"}
      <div class="crm-submit-buttons">
        <a class="add button" title="{ts}Add Action{/ts}" href="{crmURL p=$action_base_url q="reset=1&action=add&form_processor_id=`$form_processor_id`"}">
          <span><div class="icon add-icon ui-icon-circle-plus"></div>{ts}Add action{/ts}</span></a>
      </div>
    </div>

    <div class="crm-block crm-form-block crm-form-processor-output_handler-block" id="output_handler_configuration">
        {include file="CRM/FormProcessor/Form/Blocks/Configuration.tpl" prefix='output_handler' title=$outputConfigurationTitle}
    </div>
    <script type="text/javascript">
    {literal}
    CRM.$(function($) {
      var current_output_handler = {/literal}{if (!empty($outputHandler))}'{$outputHandler}'{else}false{/if}{literal};
      var id = {/literal}{if ($form_processor_instance)}{$form_processor_instance.id}{else}false{/if}{literal};
      $('#output_handler').on('change', function() {
        var output_handler = $('#output_handler').val();
        if (current_output_handler != output_handler) {
          var dataUrl = CRM.url('{/literal}{$base_url}{literal}', {'output_handler': output_handler, 'id': id});
          CRM.loadPage(dataUrl, {'target': '#output_handler_configuration'});
        }
        current_output_handler = output_handler;
      });
    });
    {/literal}
    </script>
  {/if}

  <div class="crm-submit-buttons">
      {include file="CRM/common/formButtons.tpl" location="bottom"}
  </div>
  <script type="text/javascript">
    {literal}
    CRM.$(function($) {
      var id = {/literal}{if ($form_processor_instance)}{$form_processor_instance.id}{else}false{/if}{literal};
      $('#title').on('blur', function() {
        var title = $('#title').val();
        if (($('#nameSection').hasClass('hiddenElement') && !id) || $('#name').val().length === 0) {
          CRM.api3('FormProcessorInstance', 'check_name', {
            'title': title,
            'id': id
          }).done(function (result) {
            $('#systemName').html(result.name);
            $('#name').val(result.name);
          });
        }
      });
    });
    {/literal}
  </script>
  </div>
{else}
  <div class="crm-block crm-form-block crm-form-processor-output_handler-block" id="output_handler_configuration">
      {include file="CRM/FormProcessor/Form/Blocks/Configuration.tpl" prefix='output_handler'}
  </div>
{/if}
{/crmScope}
