{crmScope extensionKey='form-processor'}

{if $action eq 8}
    {* Are you sure to delete form *}
  <h3>{ts}Delete Validator{/ts}</h3>
  <div class="crm-block crm-form-block crm-form-processor-validator-block">
    <div class="crm-section">{ts 1=$validator.title}Are you sure to delete validator '%1'?{/ts}</div>
  </div>

  <div class="crm-submit-buttons">
      {include file="CRM/common/formButtons.tpl" location="bottom"}
  </div>
{elseif (!$snippet)}
    {if $action eq 1}
      <h3>{ts}Add Validator{/ts}</h3>
    {else}
      <h3>{ts}Edit Validator{/ts}</h3>
    {/if}
  <div class="crm-block crm-form-block crm-form-processor_title-block">
    <div class="crm-section">
      <div class="label">{$form.type.label}</div>
      <div class="content">{$form.type.html}</div>
      <div class="clear"></div>
    </div>
    <div class="crm-section">
      <div class="label">{$form.title.label}</div>
      <div class="content">
          {$form.title.html}
        <span class="">
        {ts}System name:{/ts}&nbsp;
        <span id="systemName" style="font-style: italic;">{if isset($validatorObject)}{$validatorObject.name}{/if}</span>
        <a href="javascript:void(0);" onclick="CRM.$('#nameSection').removeClass('hiddenElement'); CRM.$(this).parent().addClass('hiddenElement'); return false;">
          {ts}Change{/ts}
        </a>
        </span>
      </div>
      <div class="clear">
      </div>
    </div>
    <div id="nameSection" class="crm-section hiddenElement">
      <div class="label">{$form.name.label}</div>
      <div class="content">{$form.name.html}</div>
      <div class="clear"></div>
    </div>
    <div class="crm-section">
      <div class="label">{$form.inputs.label}</div>
      <div class="content">{$form.inputs.html}</div>
      <div class="clear"></div>
    </div>

    <div id="type_configuration">
        {if ($validatorClass && $validatorClass->getHelpText())}
          <div class="crm-section">
            <div class="label">&nbsp;</div>
            <div class="content">
              <div class="help">
                  {$validatorClass->getHelpText()}
              </div>
            </div>
            <div class="clear"></div>
          </div>
        {/if}
        {include file="CRM/FormProcessor/Form/Blocks/ValidatorConfiguration.tpl"}
    </div>
  </div>

  <div class="crm-submit-buttons">
      {include file="CRM/common/formButtons.tpl" location="bottom"}
  </div>

  <script type="text/javascript">
      {literal}
      CRM.$(function($) {
        var form_processor_id = {/literal}{$form_processor_id}{literal};
        var id = {/literal}{if isset($validatorObject)}{$validatorObject.id}{else}false{/if}{literal};

        $('#type').on('change', function() {
          var type = $('#type').val();
          if (type) {
            var dataUrl = CRM.url('civicrm/admin/automation/formprocessor/validatorconfiguration/validator', {type: type, 'form_processor_id': form_processor_id, 'id': id});
            CRM.loadPage(dataUrl, {'target': '#type_configuration'});
          }
        });

        $('#title').on('blur', function() {
          var title = $('#title').val();
          if ($('#nameSection').hasClass('hiddenElement') && !id) {
            CRM.api3('FormProcessorValidateValidator', 'check_name', {
              'title': title,
              'form_processor_id': form_processor_id
            }).done(function (result) {
              $('#systemName').html(result.name);
              $('#name').val(result.name);
            });
          }
        });

        $('#type').change();
      });
      {/literal}
  </script>
{else}
  <div id="type_configuration">
      {if ($validatorClass && $validatorClass->getHelpText())}
        <div class="crm-section">
          <div class="label">&nbsp;</div>
          <div class="content">
            <div class="help">
                {$validatorClass->getHelpText()}
            </div>
          </div>
          <div class="clear"></div>
        </div>
      {/if}
      {include file="CRM/FormProcessor/Form/Blocks/ValidatorConfiguration.tpl"}
  </div>
{/if}
{/crmScope}
