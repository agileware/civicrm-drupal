{crmScope extensionKey='form-processor'}
  <h3>{ts}Validators{/ts}</h3>

  <div class="crm-block crm-form-block crm-form-processor-validator-validators-block">
    <table>
      <tr>
        <th>{ts}Title{/ts}</th>
        <th>{ts}System name{/ts}</th>
        <th>{ts}Type{/ts}</th>
        <th></th>
        <th></th>
      </tr>
        {foreach from=$validators item=validator}
            {assign var="validator_type" value=$validator.type}
          <tr>
            <td>
                {$validator.title}
            </td>
            <td>
              <span class="description">{$validator.name}</span>
            </td>
            <td>{$validator_types.$validator_type}</td>
            <td>{if ($validator.weight && !is_numeric($validator.weight))}{$validator.weight}{/if}</td>
            <td>
              <a href="{crmURL p="civicrm/admin/automation/formprocessor/validatorconfiguration/validator" q="reset=1&action=update&form_processor_id=`$validator.form_processor_id`&id=`$validator.id`"}">{ts}Edit{/ts}</a>
              <a href="{crmURL p="civicrm/admin/automation/formprocessor/validatorconfiguration/validator" q="reset=1&action=delete&form_processor_id=`$validator.form_processor_id`&id=`$validator.id`"}">{ts}Remove{/ts}</a>
            </td>
          </tr>
        {/foreach}
    </table>

    <div class="crm-submit-buttons">
      <a class="add button" title="{ts}Add Validator{/ts}" href="{crmURL p="civicrm/admin/automation/formprocessor/validatorconfiguration/validator" q="reset=1&action=add&form_processor_id=`$form_processor_id`"}">
        <span><div class="icon add-icon ui-icon-circle-plus"></div>{ts}Add Validator{/ts}</span></a>
    </div>
  </div>
{/crmScope}
