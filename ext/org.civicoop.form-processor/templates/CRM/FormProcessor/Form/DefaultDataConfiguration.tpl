{crmScope extensionKey='form-processor'}
{include file="CRM/FormProcessor/Form/Tab.tpl"}
<h3>{ts}Retrieval of defaults{/ts}</h3>
<div class="crm-block crm-form-block crm-form-processor-default-data-configuration-block">
  <div class="crm-section">
    <div class="label">{$form.enable_default_data.label}</div>
    <div class="content">{$form.enable_default_data.html}
      <p class="description">{ts}You can use retrieval of default data to create a form which already filled with existing data. For example if you have a form for updating mailing preference you want to the form to display the current preferences. To achieve this use the retrieval of default data.{/ts}</p>
    </div>
    <div class="clear"></div>
  </div>
  <div id="defaul_data_container" class="hiddenElement">
    <div class="crm-block crm-form-block crm-form-processor-inputs-block">
    <h3>{ts}Retrieval criteria for default data{/ts}</h3>
        {include file="CRM/FormProcessor/Form/Blocks/Inputs.tpl"}
      <div class="crm-submit-buttons">
        <a class="add button" title="{ts}Add Retrieval Criteria{/ts}" href="{crmURL p=$input_base_url q="reset=1&action=add&form_processor_id=`$form_processor_id`"}">
          <span><div class="icon add-icon ui-icon-circle-plus"></div>{ts}Add retrieval criteria{/ts}</span></a>
      </div>
    </div>

    <div class="crm-block crm-form-block crm-form-processor-actions-block">
    <h3>{ts}Retrieval methods of default data{/ts}</h3>
      {include file="CRM/FormProcessor/Form/Blocks/Actions.tpl"}
        <a class="add button" title="{ts}Add Action{/ts}" href="{crmURL p=$action_base_url q="reset=1&action=add&form_processor_id=`$form_processor_id`"}">
          <span><div class="icon add-icon ui-icon-circle-plus"></div>{ts}Add retrieval method{/ts}</span>
        </a>
    </div>

    <div class="clear"></div>

    <div class="crm-block crm-form-block crm-form-processor-defaults-block">
      <h3>{ts}Set defaults{/ts}</h3>
      <table>
        <tr>
          <th>{ts}Input{/ts}</th>
          <th>{ts}Value{/ts}</th>
        </tr>

          {foreach from=$default_data_output_configuration item=field}
            <tr class="{cycle values="odd-row,even-row"}">
              <td>{$form.$field.label}</td>
              <td>{$form.$field.html}</td>
            </tr>
          {/foreach}
      </table>
    </div>
  </div>
  <div class="crm-submit-buttons">
      {include file="CRM/common/formButtons.tpl" location="bottom"}
  </div>
  </div>
  <script type="text/javascript">
      {literal}
      CRM.$(function($) {
        $('#enable_default_data').on('change', function() {
          $('form').submit();
        });

        $('#defaul_data_container').addClass('hiddenElement');
        if (document.getElementById('enable_default_data').checked) {
          $('#defaul_data_container').removeClass('hiddenElement');
        }

      });
      {/literal}
  </script>
{/crmScope}
