{crmScope extensionKey='form-processor'}
{include file="CRM/FormProcessor/Form/Tab.tpl"}
<h3>{ts}Form Processor Calculation{/ts}</h3>
<div class="crm-block crm-form-block crm-form-processor-calculation-configuration-block">
  <h3>{ts}Actions{/ts}</h3>
  <div class="crm-block crm-form-block crm-form-processor-actions-block">
    {include file="CRM/FormProcessor/Form/Blocks/Actions.tpl"}
    <div class="crm-submit-buttons">
      <a class="add button" title="{ts}Add Action{/ts}" href="{crmURL p=$action_base_url q="reset=1&action=add&form_processor_id=`$form_processor_id`"}">
        <span><div class="icon add-icon ui-icon-circle-plus"></div>{ts}Add Action{/ts}</span></a>
    </div>
  </div>
  <h3>{ts}Set input to a calculated value(s){/ts}</h3>

  <table>
    <tr>
      <th>{ts}Input{/ts}</th>
      <th>{ts}Action Output Parameter{/ts}</th>
    </tr>

    {foreach from=$calculation_output_configuration item=field}
      <tr class="{cycle values="odd-row,even-row"}">
        <td>{$form.$field.label}</td>
        <td>{$form.$field.html}</td>
      </tr>
    {/foreach}
  </table>
  <div class="crm-submit-buttons">
      {include file="CRM/common/formButtons.tpl" location="bottom"}
  </div>
</div>

{/crmScope}
