<?php
// This file declares a new entity type. For more details, see "hook_civicrm_entityTypes" at:
// https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
return [
  [
    'name' => 'FormProcessorValidateValidator',
    'class' => 'CRM_FormProcessor_DAO_FormProcessorValidateValidator',
    'table' => 'civicrm_form_processor_validate_validator',
  ],
];
