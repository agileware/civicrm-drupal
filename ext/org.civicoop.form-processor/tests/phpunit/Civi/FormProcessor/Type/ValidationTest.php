<?php

use CRM_FormProcessor_ExtensionUtil as E;
use Civi\Test\HeadlessInterface;
use Civi\Test\HookInterface;
use Civi\Test\TransactionalInterface;

/**
 * This test tests all the provided types and whether their validation works
 * as expected.
 *
 * Tips:
 *  - With HookInterface, you may implement CiviCRM hooks directly in the test class.
 *    Simply create corresponding functions (e.g. "hook_civicrm_post(...)" or similar).
 *  - With TransactionalInterface, any data changes made by setUp() or test****() functions will
 *    rollback automatically -- as long as you don't manipulate schema or truncate tables.
 *    If this test needs to manipulate schema or truncate tables, then either:
 *       a. Do all that using setupHeadless() and Civi\Test.
 *       b. Disable TransactionalInterface, and handle all setup/teardown yourself.
 *
 * @group headless
 */
class Civi_FormProcessor_Type_ValidationTest extends \CivixPhar\PHPUnit\Framework\TestCase implements HeadlessInterface, HookInterface, TransactionalInterface {

  public function setUpHeadless() {
    // Civi\Test has many helpers, like install(), uninstall(), sql(), and sqlFile().
    // See: https://github.com/civicrm/org.civicrm.testapalooza/blob/master/civi-test.md
    return \Civi\Test::headless()
      ->installMe(__DIR__)
      ->apply();
  }

  public function setUp() {
    parent::setUp();
  }

  public function tearDown() {
    parent::tearDown();
  }

  /**
   * Test a type
   *
   * @param string
   * @param array
   * @param array
   * @dataProvider typeProvider
   */
  public function testType($type_name, $validValues, $invalidValues) {
    \Civi::reset(); // Reset the civi container so its rebuild again
    $civi_container = \Civi::container();
    $factory = $civi_container->get('form_processor_type_factory');
    $this->assertInstanceOf('Civi\FormProcessor\Type\Factory', $factory, 'Not a valid form_processor_type_factory has been defined');

    $type = $factory->getTypeByName($type_name);
    $this->assertNotNull($type, $type_name.' is not valid');
    $this->assertInstanceOf('Civi\FormProcessor\Type\AbstractType', $type, $type_name.' is not valid');

    $defaultConfig = $type->getDefaultConfiguration();

    foreach($validValues as $value) {
      $configuration = clone $type->getDefaultConfiguration();
      $validValue = $value;

      // When $value is an array it also contains the configuration
      // and the value as elements in the array.
      if (is_array($value)) {
        $validValue = $value['value'];
        foreach($value['config'] as $config_name => $config_value) {
          $configuration->set($config_name, $config_value);
        }
      }

      $type->setConfiguration($configuration);
      $isValid = $type->validateValue($validValue, $validValues);
      $this->assertTrue($isValid, 'Expected the value ('.$validValue.') to be valid.');
    }

    foreach($invalidValues as $key => $value) {
      $configuration = clone $type->getDefaultConfiguration();
      $invalidValue = $value;

      // When $value is an array it also contains the configuration
      // and the value as elements in the array.
      if (is_array($value)) {
        $invalidValue = $value['value'];
        foreach($value['config'] as $config_name => $config_value) {
          $configuration->set($config_name, $config_value);
        }
      }

      $type->setConfiguration($configuration);

      $isValid = $type->validateValue($invalidValue, $invalidValues);
      $this->assertFalse($isValid, 'Expected the value ('.$invalidValue.') to be invalid.');
    }
  }

  public function typeProvider() {
    return array(
      'Integer' => array(
        'Integer',
        array(1, 10, -10, '100', '-100', 0, 0.00),
        array('text', 1.12, -1.12, '12-12'),
      ),
      'Float' => array(
        'Float',
        array(0.02, -0.02, 1.12, '1.12', '-1.12', 1, 10, -10, '100', '-100', 0, 0.00),
        array('text'),
      ),
      'String' => array(
        'String',
        array('text',  '1.12', '-1.12','100', '-100', 0.02, -0.02, 1.12, 1, 10, -10),
        array(),
      ),
      'Text' => array(
        'Text',
        array('text',  '1.12', '-1.12','100', '-100', "Multiline\ntext", 0.02, -0.02, 1.12, 1, 10, -10),
        array(),
      ),
      'Date' => array(
        'Date',
        array(
          "2018-02-18", // is valid with default configuration
          array(
            'value' => "10-02-2018",
            'config' => array('format' => 'd-m-Y'),
          ),
          array(
            'value' => "2018/02/19",
            'config' => array('format' => 'Y/m/d'),
          ),
          array(
            'value' => "18-02-2018 16:54",
            'config' => array('format' => 'd-m-Y H:i'),
          ),
        ),
        array(
          "18-02-2018", // is invalid with default configuration
          array(
            'value' => "2018-02-18",
            'config' => array('format' => 'd-m-Y'),
          ),
          array(
            'value' => "2018/02/19",
            'config' => array('format' => 'Y-m-d'),
          ),
          array(
            'value' => "18-02-2018 4:54",
            'config' => array('format' => 'Y-m-d H:i'),
          ),
          array(
            'value' => "18-02-2018 4:54",
            'config' => array('format2' => 'Y-m-d H:i'), // invalid configuration
          ),
          'text',
          '46.47',
          0.02,
          -0.02,
          42.46,
          1,
          10,
          -10,
          '-1.12',
          '100',
          '-100',
          "Multiline\ntext"
        ),
      ),
      'Boolean' => array(
        'Boolean',
        array('true', 'false', '0', '1', 0, 1, 'Y', 'N', 'Yes', 'No'),
        array(0.02, -0.02, 1.12, 10, -10, 'text',  '1.12', '-1.12','100', '-100', "Multiline\ntext", "2018-02-18", "18-02-2018", "2018/02/18"),
      ),
    );
  }

}
