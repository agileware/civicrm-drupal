<?php

use CRM_FormProcessor_ExtensionUtil as E;

/**
 * FormProcessorValidateValidator.Create API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRM/API+Architecture+Standards
 */
function _civicrm_api3_form_processor_validate_validator_create_spec(&$spec) {
  $spec['id'] = array(
    'title' => E::ts('ID'),
    'type' => CRM_Utils_Type::T_INT,
    'api.required' => false
  );
  $spec['form_processor_id'] = array(
    'title' => E::ts('Form Processor Instance ID'),
    'type' => CRM_Utils_Type::T_INT,
    'api.required' => true,
    'FKApiName' => 'FormProcessorInstance',
  );
  $spec['weight'] = array(
    'title' => E::ts('Weight'),
    'type' => CRM_Utils_Type::T_INT,
    'api.required' => false,
  );
  $spec['name'] = array(
    'title' => E::ts('Name'),
    'type' => CRM_Utils_Type::T_STRING,
    'api.required' => true
  );
  $spec['title'] = array(
    'title' => E::ts('Title'),
    'type' => CRM_Utils_Type::T_STRING,
    'api.required' => true
  );
  $spec['type'] = array(
    'title' => E::ts('Type'),
    'type' => CRM_Utils_Type::T_STRING,
    'api.required' => true
  );
  $spec['configuration'] = array(
    'title' => E::ts('Configuration'),
    'type' => CRM_Utils_Type::T_TEXT,
    'api.required' => false
  );
  $spec['mapping'] = array(
    'title' => E::ts('Mapping'),
    'type' => CRM_Utils_Type::T_TEXT,
    'api.required' => false
  );
  $spec['inputs'] = array(
    'title' => E::ts('Inputs'),
    'type' => CRM_Utils_Type::T_TEXT,
    'api.required' => false
  );
}

/**
 * FormProcessorValidateValidator.Create API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 *
 *
 */
function civicrm_api3_form_processor_validate_validator_create($params) {
  if (!isset($params['weight']) && !isset($params['id'])) {
    $params['weight'] = CRM_Utils_Weight::getDefaultWeight('CRM_FormProcessor_DAO_FormProcessorValidateValidator', array('form_processor_id' => $params['form_processor_id']));
  }
  $returnValue = CRM_FormProcessor_BAO_FormProcessorValidateValidator::add($params);
  CRM_FormProcessor_BAO_FormProcessorInstance::updateAndChekStatus($returnValue['form_processor_id']);
  // Clear the cache
  \Civi\FormProcessor\Utils\Cache::cacheClearForFormProcessorById($returnValue['form_processor_id']);
  $returnValues[$returnValue['id']] = $returnValue;
  return civicrm_api3_create_success($returnValues, $params, 'FormProcessorValidateValidator', 'Create');
}

