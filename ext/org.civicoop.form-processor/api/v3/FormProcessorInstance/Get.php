<?php
/**
 * FormProcessorInstance.Get API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_form_processor_instance_get($params) {
  $returnValues = CRM_FormProcessor_BAO_FormProcessorInstance::getValues($params);
  foreach($returnValues as $index => $formProcessor) {
    // Convert inputs to arrays
    foreach($formProcessor['inputs'] as $key => $input) {
      if (isset($input['type']) && is_object($input['type'])) {
        $returnValues[$index]['inputs'][$key]['type'] = $input['type']->toArray();
      }
      foreach($input['validators'] as $validator_key => $validator) {
        if (isset($validator['validator']) && is_object($validator['validator'])) {
          $returnValues[$index]['inputs'][$key]['validators'][$validator_key]['validator'] = $validator['validator']->toArray();
        }
      }
    }

    // Convert inputs to arrays
    foreach($formProcessor['default_data_inputs'] as $key => $input) {
      if (isset($input['type']) && is_object($input['type'])) {
        $returnValues[$index]['default_data_inputs'][$key]['type'] = $input['type']->toArray();
      }
      foreach($input['validators'] as $validator_key => $validator) {
        if (isset($validator['validator']) && is_object($validator['validator'])) {
          $returnValues[$index]['default_data_inputs'][$key]['validators'][$validator_key]['validator'] = $validator['validator']->toArray();
        }
      }
    }

    // Convert output handler object to array
    if (is_object($returnValues[$index]['output_handler'])) {
      $returnValues[$index]['output_handler'] = $returnValues[$index]['output_handler']->toArray();
    }
    if (!empty($returnValues[$index]['source_file'])) {
      $returnValues[$index]['uploaded_source_file'] = \Civi\FormProcessor\Exporter\ExportToJson::isUploadedFile($returnValues[$index]['source_file']);
    }
    $returnValues[$index]['try_out_url'] = \CRM_Utils_System::url('civicrm/admin/automation/formprocessor/run', ['_qf_formProcessorName' => $formProcessor['name']], TRUE, NULL, FALSE);
    $returnValues[$index]['validator_configuration_url'] = \CRM_Utils_System::url('civicrm/admin/automation/formprocessor/validatorconfiguration', ['action' => 'update', 'reset' => 1, 'id' => $formProcessor['id']], TRUE, NULL, FALSE);
    $returnValues[$index]['calculations_configuration_url'] = \CRM_Utils_System::url('civicrm/admin/automation/formprocessor/calculationconfiguration', ['action' => 'update', 'reset' => 1, 'id' => $formProcessor['id']], TRUE, NULL, FALSE);
    $returnValues[$index]['export_url'] = \CRM_Utils_System::url('civicrm/admin/automation/formprocessor/export', ['reset' => 1, "id" =>$formProcessor['id'], "action" => "export"], TRUE, NULL, FALSE);
  }
  return civicrm_api3_create_success($returnValues, $params, 'FormProcessorInstance', 'Get');
}

/**
 * FormProcessorInstance.Get API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRM/API+Architecture+Standards
 */
function _civicrm_api3_form_processor_instance_get_spec(&$spec) {
  $fields = CRM_FormProcessor_BAO_FormProcessorInstance::fields();
  foreach($fields as $fieldname => $field) {
    $spec[$fieldname] = $field;
  }
}

