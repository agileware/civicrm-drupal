<?php
use CRM_FormProcessor_ExtensionUtil as E;

/**
 * FormProcessorInstance.Import API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC/API+Architecture+Standards
 */
function _civicrm_api3_form_processor_instance_Import_spec(&$spec) {
  $spec['file'] = array(
    'title' => E::ts('Specification File'),
    'type' => CRM_Utils_Type::T_STRING,
    'description' => E::ts('If a file path is given, the JSON configuration will be imported from there'),
    'api.required' => false
  );
  $spec['import_locally'] = array(
    'title' => E::ts('Import Locally'),
    'type' => CRM_Utils_Type::T_BOOLEAN,
    'api.required' => false
  );
}

/**
 * FormProcessorInstance.Import API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_form_processor_instance_Import($params) {
  $exporter = new \Civi\FormProcessor\Exporter\ExportToJson();
  $returnValues = array();

  if (empty($params['file'])) {
    // default behaviour
    $returnValues['import'] = $exporter->importFromExtensions();
    $returnValues['is_error'] = 0;

  } else {
    $importLocally = !empty($params['import_locally']) ? true : false;
    $return = $exporter->importFromFile($params['file'], $importLocally);
    if (!isset($return['error'])) {
      $returnValues['import'] = $return;
    } else {
      $returnValues = civicrm_api3_create_error("The file '{$params['file']}' doesn't contain a form processor specification.");
    }
  }
  return $returnValues;
}
