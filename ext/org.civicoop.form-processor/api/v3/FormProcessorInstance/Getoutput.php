<?php

use Civi\FormProcessor\Runner;
use CRM_FormProcessor_ExtensionUtil as E;

/**
 * FormProcessorInstance.Getoutput API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_form_processor_instance_getoutput($params) {
  $returnValues = Runner::getFormProcessorOutputs($params['form_processor_name']);
  return civicrm_api3_create_success($returnValues, $params, 'FormProcessorInstance', 'Getoutput');
}

/**
 * FormProcessorInstance.Get API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRM/API+Architecture+Standards
 */
function _civicrm_api3_form_processor_instance_getoutput_spec(&$spec) {
  $options = [];
  foreach(Runner::listFormProcessors() as $option) {
    $options[$option['name']] = $option['title'];
  }

  $spec['form_processor_name'] = [
    'type' => 'String',
    'name' => 'form_processor_name',
    'title' => E::ts('Form Processor'),
    'options' => $options,
    'api.required' => true,
  ];
}

