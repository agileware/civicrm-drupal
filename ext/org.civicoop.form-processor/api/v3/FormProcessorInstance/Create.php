<?php

use CRM_FormProcessor_ExtensionUtil as E;

/**
 * FormProcessor.Create API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRM/API+Architecture+Standards
 */
function _civicrm_api3_form_processor_instance_create_spec(&$spec) {
  $spec['id'] = array(
    'title' => E::ts('ID'),
    'type' => CRM_Utils_Type::T_INT,
    'api.required' => false
  );
  $spec['name'] = array(
    'title' => E::ts('Name'),
    'type' => CRM_Utils_Type::T_STRING,
    'api.required' => true
  );
  $spec['title'] = array(
    'title' => E::ts('Title'),
    'type' => CRM_Utils_Type::T_STRING,
    'api.required' => true
  );
  $spec['is_active'] = array(
    'title' => E::ts('Is active'),
    'type' => CRM_Utils_Type::T_BOOLEAN,
    'api.required' => true,
    'api.default' => true,
  );
  $spec['description'] = array(
    'title' => E::ts('Description'),
    'type' => CRM_Utils_Type::T_TEXT,
    'api.required' => false,
  );
  $spec['output_handler'] = array(
    'title' => E::ts('Output handler'),
    'type' => CRM_Utils_Type::T_STRING,
    'api.required' => true
  );
  $spec['output_handler_configuration'] = array(
    'title' => E::ts('Output handler configuration'),
    'type' => CRM_Utils_Type::T_TEXT,
    'api.required' => false
  );
  $spec['enable_default_data'] = array(
    'title' => E::ts('Enable default data'),
    'type' => CRM_Utils_Type::T_BOOLEAN,
    'api.required' => false,
    'api.default' => false,
  );
  $spec['default_data_output_configuration'] = array(
    'title' => E::ts('Default data output configuration'),
    'type' => CRM_Utils_Type::T_TEXT,
    'api.required' => false
  );
  $spec['calculation_output_configuration'] = array(
    'title' => E::ts('Calculation output configuration'),
    'type' => CRM_Utils_Type::T_TEXT,
    'api.required' => false
  );
  $spec['permission'] = array(
    'title' => E::ts('Permission'),
    'type' => CRM_Utils_Type::T_STRING,
    'api.required' => false
  );
}

/**
 * FormProcessor.Create API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 *
 *
 */
function civicrm_api3_form_processor_instance_create($params) {
  if (!isset($params['id']) && empty($params['title'])) {
    return civicrm_api3_create_error('Title can not be empty when adding a new FormProcessorInstance');
  }

  /*
   * set created or modified date and user_id
   */
  $session = CRM_Core_Session::singleton();
  $userId = $session->get('userID');
  if (isset($params['id'])) {
    $params['modified_date'] = date('Ymd');
    $params['modified_user_id'] = $userId;
  } else {
    $params['created_date'] = date('Ymd');
    $params['created_user_id'] = $userId;
  }

  if (isset($params['name'])) {
    $current_status = CRM_FormProcessor_BAO_FormProcessorInstance::getStatus($params['name']);
    if ($current_status == CRM_FormProcessor_Status::IN_CODE) {
      $params['status'] = CRM_FormProcessor_Status::OVERRIDDEN;
    }
  }

  $returnValue = CRM_FormProcessor_BAO_FormProcessorInstance::add($params);
  CRM_FormProcessor_BAO_FormProcessorInstance::updateAndChekStatus($returnValue['id']);
  // Clear the cache
  \Civi\FormProcessor\Utils\Cache::clear();
  $returnValues[$returnValue['id']] = $returnValue;
  return civicrm_api3_create_success($returnValues, $params, 'FormProcessorInstance', 'Create');
}

