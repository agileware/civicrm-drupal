<?php

use CRM_FormProcessor_ExtensionUtil as E;

/**
 * FormProcessorInstance.Export API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_form_processor_instance_export($params) {
  $exporter = new \Civi\FormProcessor\Exporter\ExportToJson();
  $returnValues = array();
  $returnValues['export'] = (array) $exporter->export($params['id']);
  if (!empty($params['to_file'])) {
    // set export date
    $data = $returnValues['export'];
    $data['export_timestamp'] = date('Y-m-d H:i:s');
    $data['export_system_url'] = CIVICRM_UF_BASEURL;

    // if the path is a directory, use the name
    if (is_dir($params['to_file'])) {
      $params['to_file'] = $params['to_file'] . DIRECTORY_SEPARATOR . $data['name'] . '.json';
    }
    file_put_contents($params['to_file'], json_encode($data, JSON_PRETTY_PRINT));
  }
  return $returnValues;
}

/**
 * FormProcessorInstance.Export API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRM/API+Architecture+Standards
 */
function _civicrm_api3_form_processor_instance_export_spec(&$spec) {
  $spec['id'] = array(
    'title' => E::ts('ID'),
    'type' => CRM_Utils_Type::T_INT,
    'api.required' => true
  );
  $spec['to_file'] = array(
      'title' => E::ts('To File'),
      'type' => CRM_Utils_Type::T_STRING,
      'description' => E::ts('If a file path is given, the JSON configuration will be written there.'),
      'api.required' => false
  );
}