<?php

use CRM_FormProcessor_ExtensionUtil as E;

/**
 * FormProcessorDefaultDataAction.check_name API specification
 *
 * @param $params
 */
function _civicrm_api3_form_processor_default_data_action_check_name_spec($params) {
  $params['id'] = array(
    'name' => 'id',
    'title' => E::ts('ID'),
  );
  $params['title'] = array(
    'name' => 'title',
    'title' => E::ts('Title'),
    'api.required' => true,
  );
  $params['form_processor_id'] = array(
    'name' => 'form_processor_id',
    'title' => E::ts('Form Processor Instance Id'),
    'api.required' => true,
  );
  $params['name'] = array(
    'name' => 'name',
    'title' => E::ts('Name'),
  );
}

/**
 * FormProcessorDefaultDataAction.check_name API
 *
 * @param $params
 */
function civicrm_api3_form_processor_default_data_action_check_name($params) {
  $name = CRM_FormProcessor_BAO_FormProcessorDefaultDataAction::checkName($params['title'], $params['form_processor_id'], $params['id'], $params['name']);
  return array(
    'name' => $name,
  );
}
