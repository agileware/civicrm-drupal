<?php

use CRM_FormProcessor_ExtensionUtil as E;

/**
 * FormProcessorValidation.Create API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRM/API+Architecture+Standards
 */
function _civicrm_api3_form_processor_validation_create_spec(&$spec) {
  $spec['id'] = array(
    'title' => E::ts('ID'),
    'type' => CRM_Utils_Type::T_INT,
    'api.required' => false
  );
  $spec['entity'] = array(
    'title' => E::ts('Entity'),
    'type' => CRM_Utils_Type::T_STRING,
    'api.required' => true,
  );
  $spec['entity_id'] = array(
    'title' => E::ts('Entity ID'),
    'type' => CRM_Utils_Type::T_INT,
    'api.required' => true,
  );
  $spec['validator'] = array(
    'title' => E::ts('Validator'),
    'type' => CRM_Utils_Type::T_STRING,
    'api.required' => true
  );
  $spec['configuration'] = array(
    'title' => E::ts('Configuration'),
    'type' => CRM_Utils_Type::T_TEXT,
    'api.required' => false
  );
}

/**
 * FormProcessorValidation.Create API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 *
 *
 */
function civicrm_api3_form_processor_validation_create($params) {
  $returnValue = CRM_FormProcessor_BAO_FormProcessorValidation::add($params);

  $formProcessorId = civicrm_api3($params['entity'], 'getvalue', array('id' => $params['entity_id'], 'return' => 'form_processor_id'));
  CRM_FormProcessor_BAO_FormProcessorInstance::updateAndChekStatus($formProcessorId);

  // Clear the cache
  \Civi\FormProcessor\Utils\Cache::cacheClearForFormProcessorById($formProcessorId);

  $returnValues[$returnValue['id']] = $returnValue;
  return civicrm_api3_create_success($returnValues, $params, 'FormProcessorValidation', 'Create');
}

