<?php
use CRM_FormProcessor_ExtensionUtil as E;

class CRM_Admin_Page_AutomationFormProcessor extends CRM_Core_Page {

  public function run() {
    $civiVersion = CRM_Core_BAO_Domain::version();

    $this->assign('legacyTags',FALSE);
    if(version_compare($civiVersion,'5.39.0','>=')) {
      Civi::service('angularjs.loader')
        ->addModules('form_processor')
        ->useApp(['defaultRoute' => 'formprocessors']);
    } else {
      $loader = new \Civi\Angular\AngularLoader();
      $loader->setModules(array('form_processor'));
      $loader->setPageName('civicrm/admin/formprocessor');
      \Civi::resources()->addSetting(array(
        'crmApp' => array(
          'defaultRoute' => 'formprocessors',
        ),
      ));
      $loader->load();
      $this->assign('legacyTags',TRUE);
    }
    parent::run();
  }

}
