<?php
use CRM_FormProcessor_ExtensionUtil as E;

/**
 * Collection of upgrade steps.
 */
class CRM_FormProcessor_Upgrader extends CRM_Extension_Upgrader_Base {

  public function install() {
    // Do nothing
  }

  public function upgrade_1001() {
    if (!CRM_Core_BAO_SchemaHandler::checkIfFieldExists('civicrm_form_processor_action', 'condition_configuration')) {
      CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_action` ADD COLUMN `condition_configuration` TEXT NULL AFTER `mapping`");
    }
    if (!CRM_Core_BAO_SchemaHandler::checkIfFieldExists('civicrm_form_processor_default_data_action', 'condition_configuration')) {
      CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_default_data_action` ADD COLUMN `condition_configuration` TEXT NULL AFTER `mapping`");
    }
    return TRUE;
  }

  public function upgrade_1002() {
    if (!CRM_Core_BAO_SchemaHandler::checkIfFieldExists('civicrm_form_processor_input', 'title')) {
      CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_input` ADD COLUMN `title` VARCHAR(128) NULL AFTER `form_processor_instance_id`");
    }
    if (!CRM_Core_BAO_SchemaHandler::checkIfFieldExists('civicrm_form_processor_input', 'condition_configuration')) {
      CRM_Core_DAO::executeQuery("UPDATE `civicrm_form_processor_input` SET `title` = `name`");
    }
    return true;
  }

  public function upgrade_1003() {
    if (!CRM_Core_BAO_SchemaHandler::checkIfFieldExists('civicrm_form_processor_action', 'delay')) {
      CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_action`
        ADD COLUMN `delay` VARCHAR(255) NULL,
        ADD COLUMN `delay_configuration` TEXT NULL");
    }
    return TRUE;
  }

  public function upgrade_1004() {
    if (!CRM_Core_BAO_SchemaHandler::checkIfFieldExists('civicrm_form_processor_default_data_input', 'title')) {
      CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_default_data_input` ADD COLUMN `title` VARCHAR(128) NULL AFTER `form_processor_instance_id`");
    }
    return TRUE;
  }

  public function upgrade_1005() {
    if (!CRM_Core_BAO_SchemaHandler::checkIfFieldExists('civicrm_form_processor_input', 'weight')) {
      CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_input` ADD COLUMN `weight` INT UNSIGNED NOT NULL DEFAULT '0' AFTER `form_processor_instance_id`");
    }
    if (!CRM_Core_BAO_SchemaHandler::checkIfFieldExists('civicrm_form_processor_default_data_input', 'weight')) {
      CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_default_data_input` ADD COLUMN `weight` INT UNSIGNED NOT NULL DEFAULT '0' AFTER `form_processor_instance_id`");
    }
    CRM_Core_DAO::executeQuery("UPDATE `civicrm_form_processor_input` SET `weight` = `id`");
    CRM_Core_DAO::executeQuery("UPDATE `civicrm_form_processor_default_data_input` SET `weight` = `id`");
    if (!CRM_Core_BAO_SchemaHandler::checkIfFieldExists('civicrm_form_processor_input', 'include_formatted_params')) {
      CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_input` ADD COLUMN `include_formatted_params` TINYINT NULL DEFAULT 1 AFTER `weight`");
    }
    if (!CRM_Core_BAO_SchemaHandler::checkIfFieldExists('civicrm_form_processor_default_data_input', 'include_formatted_params')) {
      CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_default_data_input` ADD COLUMN `include_formatted_params` TINYINT NULL DEFAULT 1 AFTER `weight`");
    }
    return TRUE;
  }

  public function upgrade_1006() {
    CRM_Core_DAO::executeQuery("
      CREATE TABLE IF NOT EXISTS `civicrm_form_processor_validate_action` (
        `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
        `form_processor_instance_id` INT UNSIGNED NOT NULL,
        `weight` INT UNSIGNED NOT NULL,
        `name` VARCHAR(80) NOT NULL,
        `title` VARCHAR(80) NOT NULL,
        `type` VARCHAR(80) NOT NULL,
        `configuration` TEXT NULL,
        `mapping` TEXT NULL,
        `condition_configuration` TEXT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE = InnoDB;
    ");
    CRM_Core_DAO::executeQuery("
      CREATE TABLE IF NOT EXISTS `civicrm_form_processor_validate_validator` (
        `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
        `form_processor_instance_id` INT UNSIGNED NOT NULL,
        `weight` INT UNSIGNED NOT NULL,
        `name` VARCHAR(80) NOT NULL,
        `title` VARCHAR(80) NOT NULL,
        `type` VARCHAR(80) NOT NULL,
        `configuration` TEXT NULL,
        `mapping` TEXT NULL,
        `inputs` TEXT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE = InnoDB;
    ");
    return TRUE;
  }
  public function upgrade_1007() {
    CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_instance` ADD COLUMN `calculation_output_configuration` TEXT NULL AFTER `default_data_output_configuration`");
    CRM_Core_DAO::executeQuery("
    CREATE TABLE IF NOT EXISTS `civicrm_form_processor_calculate_action` (
      `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
      `form_processor_instance_id` INT UNSIGNED NOT NULL,
      `weight` INT UNSIGNED NOT NULL,
      `name` VARCHAR(80) NOT NULL,
      `title` VARCHAR(80) NOT NULL,
      `type` VARCHAR(80) NOT NULL,
      `configuration` TEXT NULL,
      `mapping` TEXT NULL,
      `condition_configuration` TEXT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE = InnoDB;
    ");
    CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_input`
        ADD COLUMN `parameter_mapping` TEXT NULL,
        ADD COLUMN `default_data_parameter_mapping` TEXT NULL");
    CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_default_data_input`
        ADD COLUMN `parameter_mapping` TEXT NULL,
        ADD COLUMN `default_data_parameter_mapping` TEXT NULL");
    return TRUE;
  }

  public function upgrade_1008() {
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_instance`
      MODIFY `permission` varchar(255) NULL,
      MODIFY `source_file` varchar(255) NULL,
      MODIFY `created_user_id` int unsigned COMMENT 'FK to Contact',
      MODIFY `modified_user_id` int unsigned COMMENT 'FK to Contact',
      ADD CONSTRAINT FK_civicrm_form_processor_instance_created_user_id FOREIGN KEY (`created_user_id`) REFERENCES `civicrm_contact`(`id`) ON DELETE SET NULL,
      ADD CONSTRAINT FK_civicrm_form_processor_instance_modified_user_id FOREIGN KEY (`modified_user_id`) REFERENCES `civicrm_contact`(`id`) ON DELETE SET NULL
    ");
    return TRUE;
  }
  public function upgrade_1009() {
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_input`
      MODIFY `weight` int NOT NULL DEFAULT 0,
      MODIFY `configuration` longtext NULL,
      MODIFY `parameter_mapping` longtext NULL,
      MODIFY `default_data_parameter_mapping` longtext NULL,
      ADD COLUMN `form_processor_id` int unsigned NOT NULL DEFAULT '0' COMMENT 'FK to Form Processor'
    ");
    CRM_Core_DAO::executeQuery("UPDATE `civicrm_form_processor_input` SET `form_processor_id` = `form_processor_instance_id`");
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_input`
      MODIFY `form_processor_id` int unsigned NOT NULL COMMENT 'FK to Form Processor',
      ADD CONSTRAINT FK_civicrm_form_processor_input_form_processor_id FOREIGN KEY (`form_processor_id`) REFERENCES `civicrm_form_processor_instance`(`id`) ON DELETE CASCADE;
    ");
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_input`
      DROP INDEX name_id_UNIQUE
    ");
    CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_input` DROP COLUMN `form_processor_instance_id`");
    CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_input` ADD UNIQUE INDEX `name_id_UNIQUE`(form_processor_id, name)");
    return TRUE;
  }
  public function upgrade_1010() {
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_default_data_input`
      MODIFY `weight` int NOT NULL DEFAULT 0,
      MODIFY `configuration` longtext NULL,
      MODIFY `parameter_mapping` longtext NULL,
      MODIFY `default_data_parameter_mapping` longtext NULL,
      ADD COLUMN `form_processor_id` int unsigned NOT NULL DEFAULT '0' COMMENT 'FK to Form Processor';
    ");
    CRM_Core_DAO::executeQuery("UPDATE `civicrm_form_processor_default_data_input` SET `form_processor_id` = `form_processor_instance_id`");
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_default_data_input`
      MODIFY `form_processor_id` int unsigned NOT NULL COMMENT 'FK to Form Processor',
      ADD CONSTRAINT FK_civicrm_form_processor_default_data_input_form_processor_id FOREIGN KEY (`form_processor_id`) REFERENCES `civicrm_form_processor_instance`(`id`) ON DELETE CASCADE;
    ");
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_default_data_input`
      DROP INDEX name_id_UNIQUE
    ");
    CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_default_data_input` DROP COLUMN `form_processor_instance_id`");
    CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_default_data_input` ADD UNIQUE INDEX `name_id_UNIQUE`(form_processor_id, name)");
    return TRUE;
  }
  public function upgrade_1011() {
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_validation` MODIFY `configuration` longtext NULL;
    ");
    return TRUE;
  }
  public function upgrade_1012() {
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_action`
      MODIFY `weight` int NOT NULL DEFAULT 0,
      MODIFY `title` varchar(128) NOT NULL,
      MODIFY `configuration` longtext NULL,
      MODIFY `mapping` longtext NULL,
      MODIFY `condition_configuration` longtext NULL,
      MODIFY `delay_configuration` longtext NULL,
      CHANGE `form_processor_instance_id` `form_processor_id` int unsigned NOT NULL COMMENT 'FK to Form Processor'
    ");
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_action`
      ADD CONSTRAINT FK_civicrm_form_processor_action_form_processor_id FOREIGN KEY (`form_processor_id`) REFERENCES `civicrm_form_processor_instance`(`id`) ON DELETE CASCADE
    ");
    return TRUE;
  }
  public function upgrade_1013() {
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_default_data_action`
      MODIFY `weight` int NOT NULL DEFAULT 0,
      MODIFY `title` varchar(128) NOT NULL,
      MODIFY `configuration` longtext NULL,
      MODIFY `mapping` longtext NULL,
      MODIFY `condition_configuration` longtext NULL,
      CHANGE `form_processor_instance_id` `form_processor_id` int unsigned NOT NULL COMMENT 'FK to Form Processor'
    ");
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_default_data_action`
      ADD CONSTRAINT FK_civicrm_form_processor_default_data_action_form_processor_id FOREIGN KEY (`form_processor_id`) REFERENCES `civicrm_form_processor_instance`(`id`) ON DELETE CASCADE
    ");
    return TRUE;
  }
  public function upgrade_1014() {
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_validate_action`
      MODIFY `weight` int NOT NULL DEFAULT 0,
      MODIFY `title` varchar(128) NOT NULL,
      MODIFY `configuration` longtext NULL,
      MODIFY `mapping` longtext NULL,
      MODIFY `condition_configuration` longtext NULL,
      CHANGE `form_processor_instance_id` `form_processor_id` int unsigned NOT NULL COMMENT 'FK to Form Processor'
    ");
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_validate_action`
      ADD CONSTRAINT FK_civicrm_form_processor_validate_action_form_processor_id FOREIGN KEY (`form_processor_id`) REFERENCES `civicrm_form_processor_instance`(`id`) ON DELETE CASCADE
    ");
    return TRUE;
  }
  public function upgrade_1015() {
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_calculate_action`
      MODIFY `weight` int NOT NULL DEFAULT 0,
      MODIFY `title` varchar(128) NOT NULL,
      MODIFY `configuration` longtext NULL,
      MODIFY `mapping` longtext NULL,
      MODIFY `condition_configuration` longtext NULL,
      CHANGE `form_processor_instance_id` `form_processor_id` int unsigned NOT NULL COMMENT 'FK to Form Processor'
    ");
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_calculate_action`
      ADD CONSTRAINT FK_civicrm_form_processor_calculate_action_form_processor_id FOREIGN KEY (`form_processor_id`) REFERENCES `civicrm_form_processor_instance`(`id`) ON DELETE CASCADE
    ");
    return TRUE;
  }
  public function upgrade_1016() {
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_validate_validator`
      MODIFY `weight` int NOT NULL DEFAULT 0,
      MODIFY `title` varchar(128) NOT NULL,
      MODIFY `configuration` longtext NULL,
      MODIFY `mapping` longtext NULL,
      MODIFY `inputs` longtext NULL,
      CHANGE `form_processor_instance_id` `form_processor_id` int unsigned NOT NULL COMMENT 'FK to Form Processor'
    ");
    CRM_Core_DAO::executeQuery("
      ALTER TABLE `civicrm_form_processor_validate_validator`
      ADD CONSTRAINT FK_civicrm_form_processor_validate_validator_form_processor_id FOREIGN KEY (`form_processor_id`) REFERENCES `civicrm_form_processor_instance`(`id`) ON DELETE CASCADE
    ");
    return TRUE;
  }

  public function upgrade_2001() {
    CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_default_data_action` ADD COLUMN `delay` varchar(255) NULL, ADD COLUMN `delay_configuration` longtext NULL");
    CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_validate_action` ADD COLUMN `delay` varchar(255) NULL, ADD COLUMN `delay_configuration` longtext NULL");
    CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_form_processor_calculate_action` ADD COLUMN `delay` varchar(255) NULL, ADD COLUMN `delay_configuration` longtext NULL");
    return TRUE;
  }

  /**
   * Example: Work with entities usually not available during the install step.
   *
   * This method can be used for any post-install tasks. For example, if a step
   * of your installation depends on accessing an entity that is itself
   * created during the installation (e.g., a setting or a managed entity), do
   * so here to avoid order of operation problems.
   *
  public function postInstall() {
    $customFieldId = civicrm_api3('CustomField', 'getvalue', array(
      'return' => array("id"),
      'name' => "customFieldCreatedViaManagedHook",
    ));
    civicrm_api3('Setting', 'create', array(
      'myWeirdFieldSetting' => array('id' => $customFieldId, 'weirdness' => 1),
    ));
  }*/

  public function uninstall() {
    $config = \CRM_Core_Config::singleton();
    $directoryName = $config->customFileUploadDir . 'FormProcessor';
    \CRM_Utils_File::cleanDir($directoryName, TRUE);
  }

  /**
   * Look up extension dependency error messages and display as Core Session Status
   *
   * @param array $unmet
   */
  public static function displayDependencyErrors(array $unmet){
    foreach ($unmet as $ext) {
      $message = self::getUnmetDependencyErrorMessage($ext);
      CRM_Core_Session::setStatus($message, E::ts('Prerequisite check failed.'), 'error');
    }
  }

  /**
   * Mapping of extensions names to localized dependency error messages
   *
   * @param string $unmet an extension name
   */
  public static function getUnmetDependencyErrorMessage($unmet) {
    switch ($unmet[0]) {
      case 'action-provider':
        return ts('Form Processor was installed successfully, but you must also install and enable the <a href="%1">action-provider Extension</a> version %2 or newer.', array(1 => 'https://lab.civicrm.org/extensions/action-provider', 2=>$unmet[1]));
    }

    CRM_Core_Error::fatal(ts('Unknown error key: %1', array(1 => $unmet)));
  }

  /**
   * Extension Dependency Check
   *
   * @return Array of names of unmet extension dependencies; NOTE: returns an
   *         empty array when all dependencies are met.
   */
  public static function checkExtensionDependencies() {
    $manager = CRM_Extension_System::singleton()->getManager();

    $dependencies = array(
      ['action-provider', '1.130']
    );

    $unmet = array();
    foreach($dependencies as $ext) {
      if (!self::checkExtensionVersion($ext[0], $ext[1])) {
        array_push($unmet, $ext);
      }
    }
    return $unmet;
  }

  public static function checkExtensionVersion($extension, $version) {
    try {
      static $extensions = null;
      if (!$extensions) {
        $extensions = civicrm_api3('Extension', 'get', array('options' => array('limit' => 0)));
      }
      foreach($extensions['values'] as $ext) {
        if ($ext['key'] == $extension && $ext['status'] == 'installed') {
          if (version_compare($ext['version'], $version, '>=')) {
            return true;
          }
        }
      }
    }
    catch (Exception $e) {
      return FALSE;
    }
    return FALSE;
  }

}
