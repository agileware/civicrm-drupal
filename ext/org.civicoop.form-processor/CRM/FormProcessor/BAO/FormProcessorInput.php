<?php

use CRM_FormProcessor_ExtensionUtil as E;

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */
class CRM_FormProcessor_BAO_FormProcessorInput extends CRM_FormProcessor_DAO_FormProcessorInput {

   /**
   * Function to get values
   *
   * @return array $result found rows with data
   * @access public
   * @static
   */
  public static function getValues($params) {
    $result = array();
    $input = new CRM_FormProcessor_BAO_FormProcessorInput();
    if (!empty($params)) {
      $fields = self::fields();
      foreach ($params as $key => $value) {
        if (isset($fields[$key])) {
          $input->$key = $value;
        }
      }
    }
    $input->orderBy('weight');
    $input->find();
    while ($input->fetch()) {
      $row = array();
      self::storeValues($input, $row);
      if (!empty($row['form_processor_id'])) {
        $type = \Civi::service('form_processor_type_factory')->getTypeByName($row['type']);
        if ($type) {
          $configuration = $type->getDefaultConfiguration();
          if (isset($row['configuration']) && is_string($row['configuration'])) {
            $row['configuration'] = json_decode($row['configuration'], true);
          }
          if (empty($row['configuration']) || !is_array($row['configuration'])) {
            $row['configuration'] = array();
          }
          foreach($row['configuration'] as $name => $value) {
            $configuration->set($name, $value);
          }
          $type->setConfiguration($configuration);

          $row['type'] = $type;
        }

        if (isset($row['parameter_mapping']) && is_string($row['parameter_mapping'])) {
          $row['parameter_mapping'] = json_decode($row['parameter_mapping'], true);
        }
        if (empty($row['parameter_mapping']) || !is_array($row['parameter_mapping'])) {
          $row['parameter_mapping'] = array();
        }
        if (isset($row['default_data_parameter_mapping']) && is_string($row['default_data_parameter_mapping'])) {
          $row['default_data_parameter_mapping'] = json_decode($row['default_data_parameter_mapping'], true);
        }
        if (empty($row['default_data_parameter_mapping']) || !is_array($row['default_data_parameter_mapping'])) {
          $row['default_data_parameter_mapping'] = array();
        }

        $row['validators'] = array_values(CRM_FormProcessor_BAO_FormProcessorValidation::getValues(array('entity_id' => $input->id, 'entity' => 'FormProcessorInput')));

        $result[$row['id']] = $row;
      } else {
        //invalid input because no there is no form processor
        CRM_FormProcessor_BAO_FormProcessorInput::deleteWithId($row['id']);
      }
    }
    return $result;
  }

  /**
   * Function to add or update form processor input
   *
   * @param array $params
   * @return array $result
   * @access public
   * @throws Exception when params is empty
   * @static
   */
  public static function add($params) {
    $result = array();
    if (empty($params)) {
      throw new Exception('Params can not be empty when adding or updating a form processor input');
    }

    if (!empty($params['id'])) {
      CRM_Utils_Hook::pre('edit', 'FormProcessorInput', $params['id'], $params);
    }
    else {
      CRM_Utils_Hook::pre('create', 'FormProcessorInput', NULL, $params);
    }

    $input = new CRM_FormProcessor_BAO_FormProcessorInput();
    $fields = self::fields();
    foreach ($params as $key => $value) {
      if (isset($fields[$key])) {
        $input->$key = $value;
      }
    }
    if (is_array($input->configuration)) {
      $input->configuration = json_encode($input->configuration);
    }
    if (is_array($input->parameter_mapping)) {
      $input->parameter_mapping = json_encode($input->parameter_mapping);
    }
    if (is_array($input->default_data_parameter_mapping)) {
      $input->default_data_parameter_mapping = json_encode($input->default_data_parameter_mapping);
    }
    if (empty($params['id']) && empty($params['weight'])) {
      $input->weight = \CRM_Core_DAO::singleValueQuery("SELECT COUNT(*) + 1 FROM `".self::getTableName()."` WHERE `form_processor_id` = %1", [1=>[$params['form_processor_id'], 'Integer']]);
    }
    $input->save();
    self::storeValues($input, $result);

    if (!empty($params['id'])) {
      CRM_Utils_Hook::post('edit', 'FormProcessorInput', $input->id, $input);
    }
    else {
      CRM_Utils_Hook::post('create', 'FormProcessorInput', $input->id, $input);
    }

    return $result;
  }

  /**
   * Function to delete a form processor input with id
   *
   * @param int $id
   * @throws Exception when $id is empty
   * @access public
   * @static
   */
  public static function deleteWithId($id) {
    if (empty($id)) {
      throw new Exception('id can not be empty when attempting to delete a form processor input');
    }

    CRM_Utils_Hook::pre('delete', 'FormProcessorInput', $id, CRM_Core_DAO::$_nullArray);

    $input = new CRM_FormProcessor_BAO_FormProcessorInput();
    $input->id = $id;
    if ($input->find(true)) {
      CRM_FormProcessor_BAO_FormProcessorValidation::deleteWithEntityId('FormProcessorInput', $input->id);
      $input->delete();
    }

    CRM_Utils_Hook::post('delete', 'FormProcessorInput', $id, CRM_Core_DAO::$_nullArray);

    return;
  }

  /**
   * Function to delete all inputs with a form processor instance id
   *
   * @param int $formProcessorId
   * @access public
   * @static
   */
  public static function deleteWithFormProcessorInstanceId($formProcessorId) {
    $input = new CRM_FormProcessor_BAO_FormProcessorInput();
    $input->form_processor_id = $formProcessorId;
    $input->find(FALSE);
    while ($input->fetch()) {
      self::deleteWithId($input->id);
    }
  }

  public static function checkName($title, $form_processor_id, $id=null,$name=null): string {
    if (!$name) {
      $name = preg_replace('@[^a-z0-9_]+@','_',strtolower($title));
    }

    $name = preg_replace('@[^a-z0-9_]+@','_',strtolower($name));
    $name_part = $name;

    $sql = "SELECT COUNT(*) FROM `civicrm_form_processor_input` WHERE `name` = %1 AND `form_processor_id` = %2";
    $sqlParams[1] = array($name, 'String');
    $sqlParams[2] = array($form_processor_id, 'String');
    if (isset($id)) {
      $sql .= " AND `id` != %3";
      $sqlParams[3] = array($id, 'Integer');
    }

    $i = 1;
    while(CRM_Core_DAO::singleValueQuery($sql, $sqlParams) > 0) {
      $i++;
      $name = $name_part .'_'.$i;
      $sqlParams[1] = array($name, 'String');
    }
    return $name;
  }


}
