<?php

use Civi\ActionProvider\Parameter\SpecificationBag;
use Civi\ActionProvider\Parameter\SpecificationGroup;
use Civi\ActionProvider\Parameter\SpecificationInterface;
use CRM_FormProcessor_ExtensionUtil as E;

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */
class CRM_FormProcessor_BAO_FormProcessorDefaultDataAction extends CRM_FormProcessor_DAO_FormProcessorDefaultDataAction {

   /**
   * Function to get values
   *
   * @return array $result found rows with data
   * @access public
   * @static
   */
  public static function getValues($params) {
    $result = array();
    $action = new CRM_FormProcessor_BAO_FormProcessorDefaultDataAction();
    if (!empty($params)) {
      $fields = self::fields();
      foreach ($params as $key => $value) {
        if (isset($fields[$key])) {
          $action->$key = $value;
        }
      }
    }
    $action->orderBy('weight');
    $action->find();
    while ($action->fetch()) {
      $row = array();
      self::storeValues($action, $row);
      if (!empty($row['form_processor_id'])) {
        if (isset($row['configuration']) && is_string($row['configuration'])) {
          $row['configuration'] = json_decode($row['configuration'], TRUE);
        }
        if (empty($row['configuration']) || !is_array($row['configuration'])) {
          $row['configuration'] = array();
        }
        if (isset($row['mapping']) && is_string($row['mapping'])) {
          $row['mapping'] = json_decode($row['mapping'], true);
        }
        if (empty($row['mapping']) || !is_array($row['mapping'])) {
          $row['mapping'] = array();
        }

        if (isset($row['condition_configuration']) && is_string($row['condition_configuration'])) {
          $row['condition_configuration'] = json_decode($row['condition_configuration'], TRUE);
        }
        if (empty($row['condition_configuration']) || !is_array($row['condition_configuration'])) {
          $row['condition_configuration'] = null;
        }
        if (isset($row['delay_configuration']) && is_string($row['delay_configuration'])) {
          $row['delay_configuration'] = json_decode($row['delay_configuration'], TRUE);
        }

        $result[$row['id']] = $row;
      } else {
        //invalid input because no there is no form processor
        CRM_FormProcessor_BAO_FormProcessorDefaultDataAction::deleteWithId($row['id']);
      }
    }
    return $result;
  }

  /**
   * Function to add or update form processor action
   *
   * @param array $params
   * @return array $result
   * @access public
   * @throws Exception when params is empty
   * @static
   */
  public static function add($params) {
    $result = array();
    if (empty($params)) {
      throw new Exception('Params can not be empty when adding or updating a form processor action');
    }

    if (empty($params['id']) && empty($params['weight'])) {
      $params['weight'] = \CRM_Core_DAO::singleValueQuery("SELECT COUNT(*) + 1 FROM `".self::getTableName()."` WHERE `form_processor_id` = %1", [1=>[$params['form_processor_id'], 'Integer']]);
    }

    if (!empty($params['id'])) {
      CRM_Utils_Hook::pre('edit', 'FormProcessorDefaultDataAction', $params['id'], $params);
    }
    else {
      CRM_Utils_Hook::pre('create', 'FormProcessorDefaultDataAction', NULL, $params);
    }

    $action = new CRM_FormProcessor_BAO_FormProcessorDefaultDataAction();
    $fields = self::fields();
    foreach ($params as $key => $value) {
      if (isset($fields[$key])) {
        $action->$key = $value;
      }
    }

    if (isset($action->configuration) && is_array($action->configuration)) {
      $action->configuration = json_encode($action->configuration);
    }
    if (isset ($action->mapping) && is_array($action->mapping)) {
      $action->validateSavedMapping();
      $action->mapping = json_encode($action->mapping);
    }
    if (isset($action->condition_configuration) && is_array($action->condition_configuration)) {
      // Delete condition if set to "No condition".
      if ($action->condition_configuration['name']) {
        $action->condition_configuration = json_encode($action->condition_configuration);
      }
      else {
        $action->condition_configuration = '';
      }
    }
    if (isset($action->delay_configuration) && is_array($action->delay_configuration)) {
      $action->delay_configuration = json_encode($action->delay_configuration);
    }
    $action->save();
    self::storeValues($action, $result);

    if (!empty($params['id'])) {
      CRM_Utils_Hook::post('edit', 'FormProcessorDefaultDataAction', $action->id, $action);
    }
    else {
      CRM_Utils_Hook::post('create', 'FormProcessorDefaultDataAction', $action->id, $action);
    }

    return $result;
  }

  /**
   * Function to delete a form processor action with id
   *
   * @param int $id
   * @throws Exception when $id is empty
   * @access public
   * @static
   */
  public static function deleteWithId($id) {
    if (empty($id)) {
      throw new Exception('id can not be empty when attempting to delete a form processor action');
    }

    CRM_Utils_Hook::pre('delete', 'FormProcessorDefaultDataAction', $id, CRM_Core_DAO::$_nullArray);

    $action = new CRM_FormProcessor_BAO_FormProcessorDefaultDataAction();
    $action->id = $id;
    if ($action->find(true)) {
      $action->delete();
    }

    CRM_Utils_Hook::post('delete', 'FormProcessorDefaultDataAction', $id, CRM_Core_DAO::$_nullArray);

    return;
  }

  /**
   * Function to delete all actions with a form processor instance id
   *
   * @param int $formProcessorId
   * @access public
   * @static
   */
  public static function deleteWithFormProcessorInstanceId($formProcessorId) {
    $action = new CRM_FormProcessor_BAO_FormProcessorDefaultDataAction();
    $action->form_processor_id = $formProcessorId;
    $action->find(FALSE);
    while ($action->fetch()) {
      self::deleteWithId($action->id);
    }
  }

  public static function checkName($title, $form_processor_id, $id=null,$name=null): string {
    if (!$name) {
      $name = preg_replace('@[^a-z0-9_]+@','_',strtolower($title));
    }

    $name = preg_replace('@[^a-z0-9_]+@','_',strtolower($name));
    $name_part = $name;

    $sql = "SELECT COUNT(*) FROM `civicrm_form_processor_default_data_action` WHERE `name` = %1 AND `form_processor_id` = %2";
    $sqlParams[1] = array($name, 'String');
    $sqlParams[2] = array($form_processor_id, 'String');
    if (isset($id)) {
      $sql .= " AND `id` != %3";
      $sqlParams[3] = array($id, 'Integer');
    }

    $i = 1;
    while(CRM_Core_DAO::singleValueQuery($sql, $sqlParams) > 0) {
      $i++;
      $name = $name_part .'_'.$i;
      $sqlParams[1] = array($name, 'String');
    }
    return $name;
  }

  /**
   * Returns an array with all the fields available for parameter mapping
   *
   * @param $form_processor_id
   * @param int|null $id
   * @param bool $onlyInputs
   * @param bool $skipInputs
   * @return array
   */
  public static function getFieldsForMapping($actionProvider, int $form_processor_id, int $id=null, $onlyInputs = false, $skipInputs = false): array {
    $return = [];
    if (!$skipInputs) {
      $inputs = CRM_FormProcessor_BAO_FormProcessorDefaultDataInput::getValues(['form_processor_id' => $form_processor_id]);
      $return = \Civi\FormProcessor\Runner::getFormProcessorInputMapping('input', $inputs);
    }

    $return = array_merge($return, \Civi\FormProcessor\Runner::getFormattedMapping());

    if (!$onlyInputs) {
      $actions = CRM_FormProcessor_BAO_FormProcessorDefaultDataAction::getValues(['form_processor_id' => $form_processor_id]);
      $return = array_merge($return, \Civi\FormProcessor\Runner::getActionMapping('action', E::ts('Action'), $actions, $actionProvider, $id));
    }
    return $return;
  }

  /**
   * @param array $mapping
   * @param string $namePrefix
   * @param string $titlePrefix
   * @param \Civi\ActionProvider\Parameter\SpecificationInterface $specification
   *
   * @return array
   */
  public static function addFieldsFromSpecificationToMapping(array $mapping, string $namePrefix, string $titlePrefix, SpecificationInterface $specification): array {
    if ($specification instanceof SpecificationGroup) {
      foreach ($specification->getSpecificationBag() as $subSpec) {
        $mapping = self::addFieldsFromSpecificationToMapping($mapping, $namePrefix, $titlePrefix, $subSpec);
      }
    } else {
      $mapping[$namePrefix . $specification->getName()] = $titlePrefix . $specification->getTitle();
    }
    return $mapping;
  }



}
