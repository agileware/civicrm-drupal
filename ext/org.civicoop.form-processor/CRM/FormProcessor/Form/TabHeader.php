<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use CRM_FormProcessor_ExtensionUtil as E;

class CRM_FormProcessor_Form_TabHeader {

  /**
   * @param CRM_Core_Form $form
   *
   * @return array
   * @throws \CRM_Core_Exception
   */
  public static function build(&$form) {
    $tabs = $form->get('tabHeader');
    if (!$tabs || empty($_GET['reset'])) {
      $tabs = self::process($form);
      $form->set('tabHeader', $tabs);
    }
    $form->assign('tabHeader', $tabs);
    //CRM_Core_Resources::singleton()
      //->addScriptFile('civicrm', 'templates/CRM/common/TabHeader.js', 1, 'html-header')
      //->addSetting([
      //  'tabSettings' => [
      //    'active' => self::getCurrentTab($tabs),
      //  ],
      //]);
    return $tabs;
  }

  /**
   * @param \CRM_Core_Form $form
   *
   * @return array
   * @throws Exception
   */
  public static function process(\CRM_Core_Form &$form) {
    $qfKey = $form->get('qfKey');
    $qfKeyAttribute = null;
    if ($qfKey) {
      $qfKeyAttribute = "&qfKey={$qfKey}";
    }
    $formProcessorId = null;
    if ($form instanceof CRM_FormProcessor_Form_AbstractConfiguration) {
      $formProcessorId = $form->getFormProcessorInstanceId();
      $formProcessor = $form->getFormProcessor();
    } elseif ($form instanceof CRM_FormProcessor_Form_RunFormProcessor) {
      $formProcessor = $form->getFormProcessor();
      $formProcessorId = $formProcessor['id'];
    }

    $tabs = [];
    $tabs['configuration']['title'] = E::ts('Define form processor');
    $tabs['configuration']['class'] = '';
    $tabs['configuration']['active'] = TRUE;
    $tabs['configuration']['link'] = NULL;
    $tabs['configuration']['valid'] = TRUE;
    $tabs['configuration']['current'] = FALSE;
    $tabs['configuration']['qfKey'] = $qfKeyAttribute;

    if ($formProcessorId) {
      $tabs['defaultdataconfiguration']['title'] = E::ts('Retrieval of defaults');
      $tabs['defaultdataconfiguration']['class'] = '';
      $tabs['defaultdataconfiguration']['link'] = NULL;
      $tabs['defaultdataconfiguration']['valid'] = FALSE;
      $tabs['defaultdataconfiguration']['current'] = FALSE;
      $tabs['defaultdataconfiguration']['active'] = TRUE;
      $tabs['defaultdataconfiguration']['qfKey'] = $qfKeyAttribute;
      if (!empty($formProcessor['enable_default_data'])) {
        $tabs['defaultdataconfiguration']['valid'] = TRUE;
      }

      $tabs['validatorconfiguration']['title'] = E::ts('Validation');
      $tabs['validatorconfiguration']['class'] = '';
      $tabs['validatorconfiguration']['link'] = NULL;
      $tabs['validatorconfiguration']['valid'] = FALSE;
      $tabs['validatorconfiguration']['current'] = FALSE;
      $tabs['validatorconfiguration']['active'] = TRUE;
      $tabs['validatorconfiguration']['qfKey'] = $qfKeyAttribute;
      $validatorCount = civicrm_api3('FormProcessorValidateValidator', 'getcount', ['form_processor_id' => $formProcessorId]);
      if ($validatorCount) {
        $tabs['validatorconfiguration']['valid'] = TRUE;
      }

      $tabs['calculationconfiguration']['title'] = E::ts('Calculation');
      $tabs['calculationconfiguration']['class'] = '';
      $tabs['calculationconfiguration']['link'] = NULL;
      $tabs['calculationconfiguration']['valid'] = FALSE;
      $tabs['calculationconfiguration']['current'] = FALSE;
      $tabs['calculationconfiguration']['active'] = TRUE;
      $tabs['calculationconfiguration']['qfKey'] = $qfKeyAttribute;
      if (!empty($formProcessor['calculation_output_configuration'])) {
        $tabs['calculationconfiguration']['valid'] = TRUE;
      }

      $tabs['tryout']['title'] = E::ts('Try out');
      $tabs['tryout']['class'] = '';
      $tabs['tryout']['link'] = CRM_Utils_System::url('civicrm/admin/automation/formprocessor/run', ['_qf_formProcessorName' => $formProcessor['name']]);
      $tabs['tryout']['valid'] = TRUE;
      $tabs['tryout']['current'] = FALSE;
      $tabs['tryout']['active'] = TRUE;
      $tabs['tryout']['qfKey'] = $qfKeyAttribute;
    }

    // see if any other modules want to add any tabs
    // note: status of 'valid' flag of any injected tab, needs to be taken care in the hook implementation.
    CRM_Utils_Hook::tabset('civicrm/admin/automation/formprocessor', $tabs,  ['form_processor_id' => $formProcessorId]);

    $class = null;
    $fullName = $form->getVar('_name');
    $className = CRM_Utils_String::getClassName($fullName);

    // hack for special cases.
    switch ($className) {
      case 'DefaultDataConfiguration':
        $class = 'defaultdataconfiguration';
        break;
      case 'ValidationConfiguration':
        $class = 'validatorconfiguration';
        break;
      case 'CalculationConfiguration':
        $class = 'calculationconfiguration';
        break;
      case 'RunFormProcessor':
        $class = 'tryout';
        break;
      case 'Configuration':
        $class = 'configuration';
        break;
    }
    $selectedChild = CRM_Utils_Request::retrieve('selectedChild', 'String');
    if ($selectedChild) {
      $class = $selectedChild;
    }

    if ($class && array_key_exists($class, $tabs)) {
      $tabs[$class]['current'] = TRUE;
    }

    if ($formProcessorId) {
      $reset = !empty($_GET['reset']) ? 'reset=1&' : '';

      foreach ($tabs as $key => $value) {
        if (!isset($tabs[$key]['qfKey'])) {
          $tabs[$key]['qfKey'] = NULL;
        }

        if (empty($tabs[$key]['link'])) {
          $action = 'update';
          $link = "civicrm/admin/automation/formprocessor/{$key}";
          $query = "{$reset}action={$action}&id={$formProcessorId}";

          $tabs[$key]['link'] =  CRM_Utils_System::url($link, $query);
        }
      }
    }

    return $tabs;
  }

  /**
   * @param CRM_Event_Form_ManageEvent $form
   */
  public static function reset(&$form) {
    $tabs = self::process($form);
    $form->set('tabHeader', $tabs);
  }

  /**
   * @param $tabs
   *
   * @return int|string
   */
  public static function getCurrentTab($tabs) {
    static $current = FALSE;

    if ($current) {
      return $current;
    }

    if (is_array($tabs)) {
      foreach ($tabs as $subPage => $pageVal) {
        if (($pageVal['current'] ?? NULL) === TRUE) {
          $current = $subPage;
          break;
        }
      }
    }

    if (!$current) {
      $current = 'configuration';
    }
    return $current;
  }

}
