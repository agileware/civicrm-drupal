<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Civi\ActionProvider\Provider;
use CRM_FormProcessor_ExtensionUtil as E;

abstract class CRM_FormProcessor_Form_AbstractConfiguration extends CRM_Core_Form {

  /** @var int */
  protected $formProcessorId;

  protected $formProcessor = [];

  /**
   * Returns the current url
   * @return string
   */
  abstract protected function getCurrentCurl(): string;

  /**
   * Returns the action of this configuration
   *
   * @param int $formProcessorId
   *
   * @return array
   */
  abstract protected function getActions(int $formProcessorId): array;

  /**
   * Returns the action provider
   *
   * @return \Civi\ActionProvider\Provider
   */
  abstract protected function getActionProvider(): Provider;

  /**
   * Returns the url for editing a condition.
   *
   * @return string
   */
  abstract protected function getEditConditionUrl(): string;

  /**
   * Returns the url for editing a condition.
   *
   * @return string
   */
  abstract protected function getEditDelayUrl(): string;

  /**
   * Returns the url for editing an action.
   *
   * @return string
   */
  abstract protected function getEditActionUrl(): string;

  /**
   * Returns the name of the Dao Class of the action.
   * @return string
   */
  abstract protected function getActionDaoClass(): string;

  public function preProcess() {
    parent::preProcess();
    $this->formProcessorId = CRM_Utils_Request::retrieve('id', 'Integer', $this, $this->requireFormProcessorId());
    if ($this->formProcessorId) {
      $this->formProcessor = civicrm_api3('FormProcessorInstance', 'getsingle', ['id' => $this->formProcessorId]);
      if (empty($this->formProcessor['calculation_output_configuration']) || !is_array($this->formProcessor['calculation_output_configuration'])) {
        $this->formProcessor['calculation_output_configuration'] = [];
      }
    }
    $this->currentUrl = $this->getCurrentCurl();
    $this->assign('form_processor_id', $this->formProcessorId);
    $this->assign('form_processor_instance', $this->formProcessor);
    $this->addActions();
    $this->assign('condition_base_url', $this->getEditConditionUrl());
    $this->assign('delay_base_url', $this->getEditDelayUrl());
    $this->assign('action_base_url', $this->getEditActionUrl());
    CRM_FormProcessor_Form_TabHeader::build($this);
  }

  public function buildQuickForm() {
    $this->add('hidden', 'id');
    $this->addButtons(array(
      array('type' => 'submit', 'name' => E::ts('Save'), 'isDefault' => TRUE),
      array('type' => 'cancel', 'name' => E::ts('Cancel'))));
    parent::buildQuickForm();
  }

  /**
   * Function to set default values (overrides parent function)
   *
   * @return array $defaults
   * @access public
   */
  function setDefaultValues(): array {
    $defaults = [];
    $defaults['id'] = $this->formProcessorId;
    return $defaults;
  }

  /**
   * Function that can be defined in Form to override or.
   * perform specific action on cancel action
   */
  public function cancelAction() {
    $url = CRM_Utils_System::url('civicrm/admin/formprocessor/search', [], TRUE);
    CRM_Utils_System::redirect($url);
  }


  public function postProcess() {
    CRM_Core_Session::setStatus(E::ts('Saved form processor'), E::ts('Saved'), 'success');
    CRM_Utils_System::redirect($this->getCurrentCurl());
  }

  protected function requireFormProcessorId(): bool {
    return true;
  }

  protected function addActions() {
    $provider = $this->getActionProvider();
    $this->assign('action_types', $provider->getActionTitles());
    $this->assign('condition_types', $provider->getConditions());
    $actions = [];
    if ($this->formProcessorId) {
      $actions = $this->getActions($this->formProcessorId);
    }
    $actions = $this->correctWeight($actions);
    CRM_Utils_Weight::addOrder($actions, $this->getActionDaoClass(), 'id', $this->currentUrl, 'form_processor_id='.$this->formProcessorId);
    $this->assign('actions', $actions);
  }

  protected function correctWeight(array $items): array {
    $weight = 1;
    $newItems = [];
    foreach($items as $i => $item) {
      $item['weight'] = $weight;
      $newItems[$item['id']] = $item;
      $weight ++;
    }
    return $newItems;
  }

  public function getFormProcessorInstanceId():? int {
    return $this->formProcessorId;
  }

  /**
   * @return array
   */
  public function getFormProcessor(): array {
    return $this->formProcessor;
  }

}
