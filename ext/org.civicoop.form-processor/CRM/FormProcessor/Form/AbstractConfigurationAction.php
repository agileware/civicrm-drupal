<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Civi\ActionProvider\Provider;
use Civi\ActionProvider\Utils\UserInterface\AddConfigToQuickForm;
use Civi\ActionProvider\Utils\UserInterface\AddMappingToQuickForm;
use CRM_FormProcessor_ExtensionUtil as E;

abstract class CRM_FormProcessor_Form_AbstractConfigurationAction extends CRM_Core_Form {

  /** @var int */
  protected $formProcessorId;

  protected $actionId;

  protected $actionType;

  protected $action = array();

  protected $actionConfiguration = array();

  protected $actionMapping = array();

  protected $availableFields = array();

  /**
   * @var Civi\ActionProvider\Action\AbstractAction
   */
  protected $actionClass;

  protected $snippet;

  /**
   * Returns the action provider
   *
   * @return \Civi\ActionProvider\Provider
   */
  abstract public function getActionProvider(): Provider;

  /**
   * Returns the API 3 entity name.
   * E.g. FormProcessorValidateAction or FormProcessorDefaultDataAction
   * @return string
   */
  abstract protected function getApi3Entity(): string;

  /**
   * Returns an array of available fields for the mapping
   *
   * @return array
   */
  abstract protected function getAvailableFieldsForMapping(Provider $provider, int $formProcessorId, int $actionObjectId = null): array;

  /**
   * Returns the return url
   *
   * @return string
   */
  abstract protected function getReturnUrl(): string;

  /**
   * @return string
   */
  abstract protected function getBaseUrl(): string;

  public function preProcess() {
    parent::preProcess();
    $this->snippet = CRM_Utils_Request::retrieve('snippet', 'String');
    if ($this->snippet) {
      $this->assign('suppressForm', TRUE);
      $this->controller->_generateQFKey = FALSE;
    }

    $this->actionId = CRM_Utils_Request::retrieve('id', 'Integer');
    $this->formProcessorId = CRM_Utils_Request::retrieve('form_processor_id', 'Integer', $this, TRUE);
    $this->assign('form_processor_id', $this->formProcessorId);
    $provider = $this->getActionProvider();

    if ($this->actionId) {
      try {
        $this->action = civicrm_api3($this->getApi3Entity(), 'getsingle', ['id' => $this->actionId]);
        $this->assign('actionObject', $this->action);
        $this->actionClass = $provider->getActionByName($this->action['type']);
        $this->actionType = $this->action['type'];
        if (isset($this->action['configuration'])) {
          $this->actionConfiguration = $this->action['configuration'];
        }
        if (isset($this->action['mapping'])) {
          $this->actionMapping = $this->action['mapping'];
        }
      } catch (API_Exception $e) {
      }
    }

    $type = CRM_Utils_Request::retrieve('type', 'String');
    if ($type) {
      $this->actionType = $type;
      $this->actionClass = $provider->getActionByName($type);
    }
    $this->assign('actionClass', $this->actionClass);
    $this->availableFields = $this->getAvailableFieldsForMapping($provider, $this->formProcessorId, $this->actionId);
    $this->assign('base_url', $this->getBaseUrl());
    $this->assign('api3_entity_name', $this->getApi3Entity());
  }

  public function buildQuickForm() {
    $provider = $this->getActionProvider();
    if (!$this->snippet) {
      $this->add('hidden', 'id');
      $this->add('hidden', 'form_processor_id');
    }

    if ($this->_action == CRM_Core_Action::DELETE) {
      $this->addButtons(array(
        array('type' => 'next', 'name' => E::ts('Delete'), 'isDefault' => TRUE,),
        array('type' => 'cancel', 'name' => E::ts('Cancel'))
      ));
    } else {
      $this->add('select', 'type', E::ts('Type'), $provider->getActionTitles(), TRUE, [
        'style' => 'min-width:250px',
        'class' => 'crm-select2 huge',
        'placeholder' => E::ts('- select -'),
      ]);
      $this->add('text', 'title', E::ts('Title'), [
        'size' => 100,
        'maxlength' => 255
      ], TRUE);
      $this->add('text', 'name', E::ts('Name'), [
        'size' => 100,
        'maxlength' => 255
      ], FALSE);

      if ($this->actionClass) {
        AddConfigToQuickForm::buildForm($this, $this->actionClass, $this->actionType);
        $defaults = AddConfigToQuickForm::setDefaultValues($this->actionClass, $this->actionConfiguration, $this->actionType);
        $this->setDefaults($defaults);
        AddMappingToQuickForm::addMapping('parameter_', $this->actionClass->getParameterSpecification(), $this->actionMapping, $this, $this->availableFields);
      } else {
        $this->assign('actionProviderElementNames', []);
        $this->assign('actionProviderElementDescriptions', []);
        $this->assign('actionProviderElementPreHtml', []);
        $this->assign('actionProviderElementPostHtml', []);
        $this->assign('actionProviderMappingFields', ['parameter_' => []]);
        $this->assign('actionProviderGroupedMappingFields', ['parameter_' => []]);
        $this->assign('actionProviderCollectionMappingFields', ['parameter_' => []]);
        $this->assign('actionProviderMappingDescriptions', ['parameter_' => []]);
      }

      $this->addButtons([
        ['type' => 'next', 'name' => E::ts('Save'), 'isDefault' => TRUE,],
        ['type' => 'cancel', 'name' => E::ts('Cancel')]
      ]);
    }

    parent::buildQuickForm();
  }

  /**
   * Function to set default values (overrides parent function)
   *
   * @return array $defaults
   * @access public
   */
  function setDefaultValues(): array {
    $defaults = [];
    $defaults['form_processor_id'] = $this->formProcessorId;
    if ($this->actionId) {
      $defaults['id'] = $this->actionId;
      $defaults['type'] = $this->action['type'];
      $defaults['title'] = $this->action['title'];
      $defaults['name'] = $this->action['name'];
    }
    return $defaults;
  }

  /**
   * Function that can be defined in Form to override or.
   * perform specific action on cancel action
   */
  public function cancelAction() {
    $this->formProcessorId = CRM_Utils_Request::retrieve('form_processor_id', 'Integer', $this, TRUE);
    CRM_Utils_System::redirect($this->getReturnUrl());
  }

  public function postProcess() {
    $redirectUrl = $this->getReturnUrl();
    if ($this->_action == CRM_Core_Action::DELETE) {
      $session = CRM_Core_Session::singleton();
      civicrm_api3($this->getApi3Entity(), 'delete', array('id' => $this->actionId));
      $session->setStatus(E::ts('Action removed'), E::ts('Removed'), 'success');
      CRM_Utils_System::redirect($redirectUrl);
    }

    $values = $this->exportValues();
    $params['type'] = $values['type'];
    $params['title'] = $values['title'];
    if (empty($values['name'])) {
      $result = civicrm_api3($this->getApi3Entity(), 'check_name', ['title' => $values['title'], 'form_processor_id' => $this->formProcessorId, 'id' => $this->actionId]);
      if (!empty($result['name'])) {
        $values['name'] = $result['name'];
      }
    }
    $params['name'] = $values['name'];
    $params['form_processor_id'] = $this->formProcessorId;
    if ($this->actionId) {
      $params['id'] = $this->actionId;
    }
    if ($this->actionClass) {
      $configuration = AddConfigToQuickForm::getSubmittedConfiguration($this, $this->actionClass, $this->actionType);
      $params['configuration'] = $configuration;
      $params['mapping'] = AddMappingToQuickForm::processMapping($values,'parameter_', $this->actionClass->getParameterSpecification());
    }

    $result = civicrm_api3($this->getApi3Entity(), 'create', $params);

    CRM_Utils_System::redirect($redirectUrl);
  }

}
