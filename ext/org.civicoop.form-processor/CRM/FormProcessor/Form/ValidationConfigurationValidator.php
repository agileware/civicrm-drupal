<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Civi\ActionProvider\Utils\UserInterface\AddMappingToQuickForm;
use Civi\ActionProvider\Utils\UserInterface\AddValidatorConfigToQuickForm;
use CRM_FormProcessor_ExtensionUtil as E;

class CRM_FormProcessor_Form_ValidationConfigurationValidator extends CRM_Core_Form {

  /** @var int */
  private $formProcessorId;

  protected $validatorId;

  protected $validatorType;

  protected $validator = array();

  protected $validatorConfiguration = array();

  protected $validatorMapping = array();

  protected $availableFields = array();

  protected $availableInputs = array();

  /**
   * @var Civi\ActionProvider\Validation\AbstractValidator
   */
  protected $validatorClass;

  protected $snippet;

  public function preProcess() {
    parent::preProcess();
    $this->snippet = CRM_Utils_Request::retrieve('snippet', 'String');
    if ($this->snippet) {
      $this->assign('suppressForm', TRUE);
      $this->controller->_generateQFKey = FALSE;
    }

    $this->validatorId = CRM_Utils_Request::retrieve('id', 'Integer');
    $this->formProcessorId = CRM_Utils_Request::retrieve('form_processor_id', 'Integer', $this, TRUE);
    $this->assign('form_processor_id', $this->formProcessorId);
    /** @var Civi\ActionProvider\Provider $provider */
    $provider = form_processor_get_action_provider_for_validation();

    if ($this->validatorId) {
      try {
        $this->validator = civicrm_api3('FormProcessorValidateValidator', 'getsingle', ['id' => $this->validatorId]);
        $this->assign('validatorObject', $this->validator);
        $this->validatorClass = $provider->getValidatorByName($this->validator['type']);
        $this->validatorType = $this->validator['type'];
        if (isset($this->validator['configuration'])) {
          $this->validatorConfiguration = $this->validator['configuration'];
        }
        if (isset($this->validator['mapping'])) {
          $this->validatorMapping = $this->validator['mapping'];
        }
      } catch (API_Exception $e) {
      }
    }

    $type = CRM_Utils_Request::retrieve('type', 'String');
    if ($type) {
      $this->validatorType = $type;
      $this->validatorClass = $provider->getValidatorByName($type);
    }
    $this->assign('validatorClass', $this->validatorClass);
    $this->availableFields = CRM_FormProcessor_BAO_FormProcessorValidateAction::getFieldsForMapping($provider, $this->formProcessorId, null);
    $this->availableInputs = CRM_FormProcessor_BAO_FormProcessorValidateAction::getFieldsForMapping($provider, $this->formProcessorId, null, true);
  }

  public function buildQuickForm() {
    /** @var Civi\ActionProvider\Provider $provider */
    $provider = form_processor_get_action_provider_for_validation();
    if (!$this->snippet) {
      $this->add('hidden', 'id');
      $this->add('hidden', 'form_processor_id');
    }

    if ($this->_action == CRM_Core_Action::DELETE) {
      $this->addButtons(array(
        array('type' => 'next', 'name' => E::ts('Delete'), 'isDefault' => TRUE,),
        array('type' => 'cancel', 'name' => E::ts('Cancel'))
      ));
    } else {
      $this->add('select', 'inputs', E::ts('Show message for inputs'), $this->availableInputs, FALSE, [
        'style' => 'min-width:250px',
        'class' => 'crm-select2 huge',
        'placeholder' => E::ts('- all inputs -'),
        'multiple' => 'multiple'
      ]);
      $this->add('select', 'type', E::ts('Type'), $provider->getValidatorTitles(), TRUE, [
        'style' => 'min-width:250px',
        'class' => 'crm-select2 huge',
        'placeholder' => E::ts('- select -'),
      ]);
      $this->add('text', 'title', E::ts('Title'), [
        'size' => 100,
        'maxlength' => 255
      ], TRUE);
      $this->add('text', 'name', E::ts('Name'), [
        'size' => 100,
        'maxlength' => 255
      ], FALSE);

      if ($this->validatorClass) {
        AddValidatorConfigToQuickForm::buildForm($this, $this->validatorClass, $this->validatorType);
        $defaults = AddValidatorConfigToQuickForm::setDefaultValues($this->validatorClass, $this->validatorConfiguration, $this->validatorType);
        $this->setDefaults($defaults);
        AddMappingToQuickForm::addMapping('parameter_', $this->validatorClass->getParameterSpecification(), $this->validatorMapping, $this, $this->availableFields);
      } else {
        $this->assign('actionProviderElementNames', []);
        $this->assign('actionProviderMappingFields', []);
        $this->assign('actionProviderMappingFields', ['parameter_' => []]);
        $this->assign('actionProviderGroupedMappingFields', ['parameter_' => []]);
        $this->assign('actionProviderCollectionMappingFields', ['parameter_' => []]);
        $this->assign('actionProviderMappingDescriptions', ['parameter_' => []]);
      }

      $this->addButtons([
        ['type' => 'next', 'name' => E::ts('Save'), 'isDefault' => TRUE,],
        ['type' => 'cancel', 'name' => E::ts('Cancel')]
      ]);
    }

    parent::buildQuickForm();
  }

  /**
   * Function to set default values (overrides parent function)
   *
   * @return array $defaults
   * @access public
   */
  function setDefaultValues(): array {
    $defaults = [];
    $defaults['form_processor_id'] = $this->formProcessorId;
    if ($this->validatorId) {
      $defaults['id'] = $this->validatorId;
      $defaults['type'] = $this->validator['type'];
      $defaults['title'] = $this->validator['title'];
      $defaults['name'] = $this->validator['name'];
      $defaults['inputs'] = $this->validator['inputs'];
    }
    return $defaults;
  }

  /**
   * Function that can be defined in Form to override or.
   * perform specific action on cancel action
   */
  public function cancelAction() {
    $this->formProcessorId = CRM_Utils_Request::retrieve('form_processor_id', 'Integer', $this, TRUE);
    $redirectUrl = CRM_Utils_System::url('civicrm/admin/automation/formprocessor/validatorconfiguration', array('reset' => 1, 'action' => 'update', 'id' => $this->formProcessorId));
    CRM_Utils_System::redirect($redirectUrl);
  }

  public function postProcess() {
    $redirectUrl = CRM_Utils_System::url('civicrm/admin/automation/formprocessor/validatorconfiguration', array('reset' => 1, 'action' => 'update', 'id' => $this->formProcessorId));
    if ($this->_action == CRM_Core_Action::DELETE) {
      $session = CRM_Core_Session::singleton();
      civicrm_api3('FormProcessorValidateValidator', 'delete', array('id' => $this->validatorId));
      $session->setStatus(E::ts('Validator removed'), E::ts('Removed'), 'success');
      CRM_Utils_System::redirect($redirectUrl);
    }

    $values = $this->exportValues();
    $params['type'] = $values['type'];
    $params['title'] = $values['title'];
    $params['name'] = $values['name'];
    $params['inputs'] = $values['inputs'] ?? 'null';
    $params['form_processor_id'] = $this->formProcessorId;
    if ($this->validatorId) {
      $params['id'] = $this->validatorId;
    }
    if ($this->validatorClass) {
      $configuration = AddValidatorConfigToQuickForm::getSubmittedConfiguration($this, $this->validatorClass, $this->validatorType);
      $params['configuration'] = $configuration;
      $params['mapping'] = AddMappingToQuickForm::processMapping($values,'parameter_', $this->validatorClass->getParameterSpecification());
    }
    $result = civicrm_api3('FormProcessorValidateValidator', 'create', $params);

    CRM_Utils_System::redirect($redirectUrl);
  }

}
