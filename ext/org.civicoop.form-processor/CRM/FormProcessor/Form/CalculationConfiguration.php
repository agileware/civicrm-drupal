<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Civi\ActionProvider\Provider;
use CRM_FormProcessor_ExtensionUtil as E;

class CRM_FormProcessor_Form_CalculationConfiguration extends CRM_FormProcessor_Form_AbstractConfiguration {

  private $availableFields = [];

  public function preProcess() {
    parent::preProcess();
    if (empty($this->formProcessor['calculation_output_configuration']) || !is_array($this->formProcessor['calculation_output_configuration'])) {
      $this->formProcessor['calculation_output_configuration'] = [];
    }
    $this->availableFields = CRM_FormProcessor_BAO_FormProcessorCalculateAction::getFieldsForMapping($this->getActionProvider(), $this->formProcessorId);
  }

  public function buildQuickForm() {
    parent::buildQuickForm();

    $inputs = CRM_FormProcessor_BAO_FormProcessorInput::getValues(['form_processor_id' => $this->formProcessorId]);
    $calculation_output_configuration = [];
    foreach ($inputs as $input) {
      $this->add('select','input_'.$input['name'], E::ts('Input :: %1', [1 => $input['title']]), $this->availableFields, FALSE, [
        'style' => 'min-width:250px',
        'class' => 'crm-select2 huge',
        'placeholder' => E::ts('- select -'),
        'multiple' => false,
      ]);
      $calculation_output_configuration[] = 'input_'.$input['name'];
    }
    $this->assign('calculation_output_configuration', $calculation_output_configuration);

    $this->addButtons(array(
      array('type' => 'next', 'name' => E::ts('Save'), 'isDefault' => TRUE,),
      array('type' => 'cancel', 'name' => E::ts('Cancel'))));
    parent::buildQuickForm();
  }

  /**
   * Function to set default values (overrides parent function)
   *
   * @return array $defaults
   * @access public
   */
  function setDefaultValues(): array {
    $defaults = parent::setDefaultValues();
    $calculation_output_configuration = [];
    foreach ($this->formProcessor['calculation_output_configuration'] as $field => $mapping) {
      $defaults['input_'.$field] =$mapping;
    }
    return $defaults;
  }


  public function postProcess() {
    $submittedValues = $this->getSubmittedValues();
    $inputs = CRM_FormProcessor_BAO_FormProcessorInput::getValues(['form_processor_id' => $this->formProcessorId]);
    $calculation_output_configuration = [];
    foreach ($inputs as $input) {
      if (!empty($submittedValues['input_'.$input['name']])) {
        $calculation_output_configuration[$input['name']] = $submittedValues['input_'.$input['name']];
      }
    }
    $params['id'] = $this->formProcessorId;
    $params['calculation_output_configuration'] = $calculation_output_configuration;
    civicrm_api3('FormProcessorInstance', 'create', $params);
    parent::postProcess();
  }

  /**
   * Returns the action provider
   *
   * @return \Civi\ActionProvider\Provider
   */
  protected function getActionProvider(): Provider {
    return form_processor_get_action_provider_for_calculation();
  }

  /**
   * Returns the current url
   *
   * @return string
   */
  protected function getCurrentCurl(): string {
    return CRM_Utils_System::url('civicrm/admin/automation/formprocessor/calculationconfiguration', array('reset' => 1, 'action' => 'update', 'id' => $this->formProcessorId));
  }

  /**
   * Returns the url for editing a condition.
   *
   * @return string
   */
  protected function getEditConditionUrl(): string {
    return 'civicrm/admin/automation/formprocessor/calculationconfiguration/condition';
  }

  /**
   * Returns the url for editing an action.
   *
   * @return string
   */
  protected function getEditActionUrl(): string {
    return 'civicrm/admin/automation/formprocessor/calculationconfiguration/action';
  }

  /**
   * Returns the url for editing a condition.
   *
   * @return string
   */
  protected function getEditDelayUrl(): string {
    return 'civicrm/admin/automation/formprocessor/calculationconfiguration/actiondelay';
  }

  /**
   * Returns the name of the Dao Class of the action.
   *
   * @return string
   */
  protected function getActionDaoClass(): string {
    return 'CRM_FormProcessor_DAO_FormProcessorCalculateAction';
  }

  /**
   * Returns the action of this configuration
   *
   * @param int $formProcessorId
   *
   * @return array
   */
  protected function getActions(int $formProcessorId): array {
    return CRM_FormProcessor_BAO_FormProcessorCalculateAction::getValues(array('form_processor_id' => $formProcessorId));
  }

}
