<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

use CRM_FormProcessor_ExtensionUtil as E;

class CRM_FormProcessor_Form_RunFormProcessor extends CRM_Core_Form {

  protected $defaultFields = [];

  protected $fields = [];

  protected $formProcessorName;

  /**
   * @var array
   */
  protected $formProcessor = [];

  protected static $results = [];

  protected $snippet;

  /**
   * Preprocess form.
   *
   * This is called before buildForm. Any pre-processing that
   * needs to be done for buildForm should be done here.
   *
   * This is a virtual function and should be redefined if needed.
   */
  public function preProcess() {
    parent::preProcess();
    $this->snippet = CRM_Utils_Request::retrieve('snippet', 'String');
    $this->formProcessorName = CRM_Utils_Request::retrieve('_qf_formProcessorName', 'String', $this, TRUE);
    $this->formProcessor = civicrm_api3('FormProcessorInstance', 'getsingle', ['name' => $this->formProcessorName]);
    CRM_FormProcessor_Form_TabHeader::build($this);
  }

  public function getFormProcessor(): array {
    return $this->formProcessor;
  }

  /**
   * This virtual function is used to build the form.
   *
   * It replaces the buildForm associated with QuickForm_Page. This allows us
   * to put preProcess in front of the actual form building routine
   */
  public function buildQuickForm() {
    $this->add('hidden', '_qf_formProcessorName');
    $this->setDefaults(['_qf_formProcessorName' => $this->formProcessorName]);
    $refreshButton = $this->isRefresh();
    $submittedDefaultData = $this->getSubmittedDefaultData($this->formProcessorName);
    $defaultDataGetFieldParams = $submittedDefaultData;
    $defaultDataGetFieldParams['api_action'] = $this->formProcessorName;
    $formProcessor = $this->formProcessor;
    $defaultFields = civicrm_api3('FormProcessorDefaults', 'getfields', $defaultDataGetFieldParams);
    $this->defaultFields = $defaultFields['values'];
    $defaultFieldNames = [];
    foreach($this->defaultFields as $field) {
      $attributes = [
        'style' => 'min-width:250px',
        'class' => 'huge'
      ];
      if (isset($field['options']) && is_array($field['options'])) {
        $attributes['class'] .= ' crm-select2';
        $attributes['placeholder'] = E::ts('- select -');
        if ($field['formprocessor.is_multiple']) {
          $attributes['multiple'] = 'multiple';
        }
        $this->add('select', $this->formProcessorName . '_default_' . $field['name'], $field['title'], $field['options'], $field['api.required'], $attributes);
      } else {
        $this->add('text', $this->formProcessorName . '_default_' . $field['name'], $field['title'], $attributes, $field['api.required']);
      }
      $defaultFieldNames[] = $this->formProcessorName.'_default_'.$field['name'];
    }
    $this->assign('enableDefault', $formProcessor['enable_default_data']);
    $this->assign('defaultFieldNames', $defaultFieldNames);

    $getFieldParams = $submittedDefaultData;
    $getFieldParams['api_action'] = $this->formProcessorName;
    $fields = civicrm_api3('FormProcessor', 'getfields',$getFieldParams);
    $this->fields = $fields['values'];
    $fieldNames = [];
    foreach($this->fields as $field) {
      $attributes = [
        'style' => 'min-width:250px',
        'class' => 'huge'
      ];
      if (isset($field['options']) && is_array($field['options'])) {
        $attributes['class'] .= ' crm-select2';
        $attributes['placeholder'] = E::ts('- select -');
        if ($field['formprocessor.is_multiple']) {
          $attributes['multiple'] = 'multiple';
        }
        $this->add('select', $field['name'], $field['title'], $field['options'], $field['api.required'], $attributes);
      } else {
        $this->add('text', $field['name'], $field['title'], $attributes, $field['api.required']);
      }
      $fieldNames[] = $field['name'];
    }
    $this->assign('fieldNames', $fieldNames);

    if ($refreshButton) {
      $this->loadDefaultData($this->formProcessorName, $submittedDefaultData);
    }

    $this->addButtons([
      [
        'type' => 'refresh',
        'name' => E::ts('Load'),
        'isDefault' => FALSE,
      ],
      [
        'type' => 'submit',
        'name' => E::ts('Submit'),
        'isDefault' => FALSE,
      ],
    ]);
  }

  /**
   * Get submitted default data.
   *
   * @param string $formProcessorName
   *
   * @return array
   */
  protected function getSubmittedDefaultData(string $formProcessorName): array {
    $data = [];
    foreach ($this->_submitValues as $key => $value) {
      if (strpos($key, $formProcessorName . '_default_') === 0) {
        $data[substr($key, strlen($formProcessorName . '_default_'))] = $value;
      }
    }
    return $data;
  }

  protected function loadDefaultData($formProcessorName, $defaultParams) {
    try {
      $default = civicrm_api3('FormProcessorDefaults', $formProcessorName, $defaultParams);
      if (!isset($default['is_error']) || !$default['is_error']) {
        $this->setDefaults($default);
        foreach($default as $k=>$v) {
          $idx = $this->_elementIndex[$k];
          $this->_elements[$idx]->setValue($v);
        }
        $this->_errors = [];
      } else {
        CRM_Core_Session::setStatus($default['error_message']);
        $this->clearForm();
      }
    }
    catch (CRM_Core_Exception $e) {
      CRM_Core_Session::setStatus($e->getMessage());
      $this->clearForm();
    }
  }

  protected function clearForm() {
    foreach ($this->fields as $field) {
      $idx = $this->_elementIndex[$field['name']];
      $this->_elements[$idx]->setValue('');
    }
  }

  public function isRefresh() {
    $refreshButton = false;
    if (isset($this->_submitValues['_qf_RunFormProcessor_refresh']) && $this->_submitValues['_qf_RunFormProcessor_refresh'] == '1') {
      // CiviCRM version 5.31 onwards set a '1' value on the submit button.
      $refreshButton = TRUE;
    }
    return $refreshButton;
  }

  /**
   * If your form requires special validation, add one or more callbacks here
   */
  public function addRules() {
    $this->addFormRule(array('CRM_FormProcessor_Form_RunFormProcessor', 'runFormProcessorValidation'));
  }

  /**
   * Here's our custom validation callback
   */
  public static function runFormProcessorValidation($values) {
    $formProcessorName = $values['_qf_formProcessorName'];
    $errors = array();
    if (isset($values['_qf_RunFormProcessor_submit']) && $values['_qf_RunFormProcessor_submit'] == '1') {
      try {
        $validationErrors = civicrm_api3('FormProcessorValidation', $formProcessorName, $values);
      }
      catch (CRM_Core_Exception $e) {
        // Do nothing.
      }
      foreach ($validationErrors as $input => $error) {
        $errors[$input] = $input.': '.$error;
      }
    }

    return empty($errors) ? TRUE : $errors;
  }

  /**
   * Performs the server side validation.
   *
   * @return bool
   *   true if no error found
   * @throws    HTML_QuickForm_Error
   * @since     1.0
   */
  public function validate() {
    parent::validate();
    if ($this->isRefresh()) {
      // The load default button is pressed.
      // Do not show the validation errors about the filled in fields as those
      // are not relevant anymore.
      foreach($this->fields as $field) {
        unset($this->_errors[$field['name']]);
      }
    }
    return (0 == count($this->_errors));
  }

  /**
   * Called after the form is validated.
   *
   * Any processing of form state etc should be done in this function.
   * Typically all processing associated with a form should be done
   * here and relevant state should be stored in the session
   *
   * This is a virtual function and should be redefined if needed
   */
  public function postProcess() {
    $submitted = false;
    if (isset($this->_submitValues['_qf_RunFormProcessor_submit']) && $this->_submitValues['_qf_RunFormProcessor_submit'] == '1') {
      // CiviCRM version 5.31 onwards set a '1' value on the submit button.
      $submitted = true;
    }
    if ($submitted && !count($this->_errors)) {
      $params = [];
      foreach($this->fields as $field) {
        $submitFieldName = $field['name'];
        if (isset($this->_submitValues[$submitFieldName])) {
          $params[$field['name']] = $this->_submitValues[$submitFieldName];
        }
      }

      try {
        $result = civicrm_api3('FormProcessor', $this->formProcessorName, $params);
      }
      catch (CRM_Core_Exception $e) {
        $result['is_error'] = '1';
        $result['error_message'] = $e->getMessage();
      }
      $this->assign('result_json', json_encode($result, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
      if (!isset($result['is_error']) || !$result['is_error']) {
        $this->clearForm();
      }
    }
  }

}
