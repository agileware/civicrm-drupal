<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Civi\ActionProvider\Provider;

class CRM_FormProcessor_Form_DefaultDataConfigurationAction extends CRM_FormProcessor_Form_AbstractConfigurationAction {

  /**
   * Returns the action provider
   *
   * @return \Civi\ActionProvider\Provider
   */
  public function getActionProvider(): Provider {
    return form_processor_get_action_provider_for_default_data();
  }

  /**
   * Returns the API 3 entity name.
   * E.g. FormProcessorValidateAction or FormProcessorDefaultDataAction
   *
   * @return string
   */
  protected function getApi3Entity(): string {
    return 'FormProcessorDefaultDataAction';
  }

  /**
   * Returns an array of available fields for the mapping
   *
   * @return array
   */
  protected function getAvailableFieldsForMapping(Provider $provider, int $formProcessorId, int $actionObjectId = NULL): array {
    return CRM_FormProcessor_BAO_FormProcessorDefaultDataAction::getFieldsForMapping($provider, $formProcessorId, $actionObjectId);
  }

  /**
   * Returns the return url
   *
   * @return string
   */
  protected function getReturnUrl(): string {
    return CRM_Utils_System::url('civicrm/admin/automation/formprocessor/defaultdataconfiguration', array('reset' => 1, 'action' => 'update', 'id' => $this->formProcessorId));
  }

  /**
   * @return string
   */
  protected function getBaseUrl(): string {
    return 'civicrm/admin/automation/formprocessor/defaultdataconfiguration/action';
  }

}
