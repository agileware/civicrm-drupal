<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

class CRM_FormProcessor_Form_ConfigurationInput extends CRM_FormProcessor_Form_AbstractConfigurationInput {

  /**
   * Returns the API 3 entity name.
   * E.g. FormProcessorValidateAction or FormProcessorDefaultDataAction
   *
   * @return string
   */
  protected function getApi3Entity(): string {
    return 'FormProcessorInput';
  }

  protected function showDefaultDataMapping(): bool {
    return true;
  }

  protected function showInputMapping(): bool {
    return true;
  }

  /**
   * Returns the return url
   *
   * @return string
   */
  protected function getReturnUrl(): string {
    return CRM_Utils_System::url('civicrm/admin/automation/formprocessor/configuration', array('reset' => 1, 'action' => 'update', 'id' => $this->formProcessorId));
  }

  /**
   * @return string
   */
  protected function getBaseUrl(): string {
    return 'civicrm/admin/automation/formprocessor/configuration/input';
  }



}
