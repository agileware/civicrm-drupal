<?php

use CRM_Mandatoryextensions_ExtensionUtil as E;

/**
 * Collection of upgrade steps.
 */
class CRM_Mandatoryextensions_Upgrader extends CRM_Mandatoryextensions_Upgrader_Base {

  /**
   *
   * Post installation, for disable specific extensions
   * Developer Note: Ensure all extensions to force disable are added to this
   * function as well as included in an upgrade_ function
   *
   * @return TRUE on success
   * @throws Exception
   */
  public function postInstall() {

    // Force disable the Recalculate Recipients extension, if it is enabled
    CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=0 WHERE full_name = "civicrm-recalculate-recipients"');

    // Force disable the Single-Click-Opt-Out for CiviMail extension, if it is enabled
    CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=0 WHERE full_name = "de.ergomation.civimail-tools"');

    // Force disable the Transactional Mail extension, if it is enabled
    CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=0 WHERE full_name = "nz.co.fuzion.transactional"');

    // Force disable the CiviCRM Core extension, Contribution Cancel Actions if it is enabled
    CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=0 WHERE full_name = "contributioncancelactions"');

    // Force disable the CiviCRM Core extension, Financial ACLs if it is enabled
    CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=0 WHERE full_name = "financialacls"');

    // Force disable the CiviCRM Core extension, Sequential credit notes if it is enabled
    CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=0 WHERE full_name = "sequentialcreditnotes"');

    // Force enable the CiviCRM Core extension, Event Cart due to existing CiviCRM core dependencies. See CIVIMEX-22 and CIVICRM-1880
    CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=1 WHERE full_name = "eventcart"');

    $this->_disable_sample_mailing_components();

    return TRUE;
  }

  /**
   * Update 100 function
   *
   * @return TRUE on success
   * @throws Exception
   */
  public function upgrade_100() {
    $this->ctx->log->info('Applying update 100');
    // Force disable the Recalculate Recipients extension, if it is enabled
    CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=0 WHERE full_name = "civicrm-recalculate-recipients"');

    // Force disable the Single-Click-Opt-Out for CiviMail extension, if it is enabled
    CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=0 WHERE full_name = "de.ergomation.civimail-tools"');

    // Force disable the Transactional Mail extension, if it is enabled
    CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=0 WHERE full_name = "nz.co.fuzion.transactional"');

    // Force disable the CiviCRM Core extension, Contribution Cancel Actions if it is enabled
    CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=0 WHERE full_name = "contributioncancelactions"');

    // Force disable the CiviCRM Core extension, Financial ACLs if it is enabled
    CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=0 WHERE full_name = "financialacls"');

    // Force disable the CiviCRM Core extension, Sequential credit notes if it is enabled
    CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=0 WHERE full_name = "sequentialcreditnotes"');

    return TRUE;
  }

  /**
   * Update 105 function
   *
   * @return TRUE on success
   * @throws Exception
   */
  public function upgrade_105() {
    $this->ctx->log->info('Applying update 105');

    // Force enable the CiviCRM Core extension, Event Cart due to existing CiviCRM core dependencies. See CIVIMEX-22 and CIVICRM-1880
    CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=1 WHERE full_name = "eventcart"');

    return TRUE;
  }

  /**
   * Update 110 function
   *
   * @return TRUE on success
   * @throws Exception
   */
  public function upgrade_110() {
    $this->ctx->log->info('Applying update 110');

    // Force disable the Agileware custom reports which are no longer bundled
    CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=0 WHERE full_name = "au.com.agileware.activitiesbyemployerreport"');
    CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=0 WHERE full_name = "au.com.agileware.activitycustomfieldsreport"');
    CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=0 WHERE full_name = "au.com.agileware.contactsummarymembershipreport"');

    return TRUE;
  }

  /**
   * Update 125 function
   *
   * @return TRUE on success
   * @throws Exception
   */
  public function upgrade_125() {
    $this->ctx->log->info('Applying update 125');

    /**
     * Enable required extensions if possible.
     */
    try {
      // Get the saved list of required extensions
      $requires = civicrm_api3('Extension', 'getvalue', [
        'key'    => E::LONG_NAME,
        'return' => 'requires',
      ]);

      // Filter out extensions that are already installed
      $requires = array_filter($requires, function ($ext) {
        return 'installed' != civicrm_api3('Extension', 'getvalue', [
            'key'    => $ext,
            'return' => 'status',
          ]);
      });

      // Enable any required extensions that are not already installed
      if (!empty($requires)) {
        civicrm_api3('Extension', 'enable', ['keys' => $requires]);
      }
    }
    catch (CiviCRM_API3_Exception $e) {
      // We'll end up here if any extensions are not found or any other API error
      // happens.  Log it and move on.
      Civi::log()->error('Enabling mandatory extensions:' . $e->getMessage());
    }

    return TRUE;
  }

  /**
   * CIVICRM-1966 disable undesirable sample mailing components that are
   * automatically added to Mosaico mailings.
   *
   * @return TRUE on success
   * @throws Exception
   */
  private function _disable_sample_mailing_components() {
    $this->ctx->log->info('Disabling "Sample" Mailing Header and Footer ("Components")');

    $sql = <<<EOS
      UPDATE civicrm_mailing_component
        SET is_active = 0, is_default = 0
        WHERE body_html LIKE '%sample%' OR body_text LIKE '%sample%'
      EOS;

    CRM_Core_DAO::executeQuery($sql);

    return TRUE;
  }

  /**
   * Update 130 function.
   *
   * @return TRUE on success
   * @throws Exception
   */
  public function upgrade_130() {
    return $this->_disable_sample_mailing_components();
  }

  /**
   * Update 140 function
   *
   * @return TRUE on success
   * @throws Exception
   */
  public function upgrade_140() {
    // This upgrade function has been replaced by the jobcontrol scheduled job
    return TRUE;
  }

  /**
   * Update 150 - Delete group information where ActionSchedule has limit set
   * to "Also Include," as this is normally going to be incorrect.
   *
   * @return TRUE on success
   * @throws API_Exception
   */
  public function upgrade_150() {
    $this->ctx->log->info('Applying update 150 - Delete group information where ActionSchedule has limit set to "Also Include."');

    \Civi\Api4\ActionSchedule::update(FALSE)
                             ->addWhere('limit_to', '=', 0)
                             ->addValue('group_id', NULL)
                             ->execute();

    return TRUE;
  }

  /**
   * Update 152 function
   *
   * @return TRUE on success
   * @throws Exception
   */
  public function upgrade_152() {
    // Create the Job Control scheduled job now, because CiviCRM managed entity is created AFTER the upgrade

    $this->ctx->log->info('Create Job Control, Scheduled Job');

    $domain_id       = CRM_Core_Config::domainID();
    $jobcontrol_name = 'Job Control';

    $sql     = 'SELECT * from `civicrm_job` WHERE LCASE(`name`)=%1 AND domain_id=%2 LIMIT 1';
    $results = CRM_Core_DAO::executeQuery($sql, [
      1 => [strtolower($jobcontrol_name), 'String'],
      2 => [$domain_id, 'Integer'],
    ]);
    $data    = $results->fetchAll();

    // If the job does not exist then create it now
    if (empty($data)) {
      $sql = 'INSERT INTO `civicrm_job` (`name`,`description`,`api_entity`,`api_action`,`is_active`,`run_frequency`,`domain_id`) VALUES (%1,%2,%3,%4,%5,%6,%7)';

      CRM_Core_DAO::executeQuery($sql, [
        1 => [$jobcontrol_name, 'String'],
        2 => ['Maintains standard scheduling of Scheduled Jobs', 'String'],
        3 => ['Job', 'String'],
        4 => ['Jobcontrol', 'String'],
        5 => [1, 'Integer'],
        6 => ['Daily', 'String'],
        7 => [$domain_id, 'Integer'],
      ]);

      // Insert daily schedule, 0 0 * * *
      $sql = 'INSERT INTO `civicrm_job_scheduled` (`job_id`,`cron`) SELECT `id`,"0 0 * * *" FROM `civicrm_job` WHERE LCASE(name)=%1 AND domain_id=%2 ORDER BY id ASC ON DUPLICATE KEY UPDATE `civicrm_job_scheduled`.`job_id`=`civicrm_job`.`id`';

      CRM_Core_DAO::executeQuery($sql, [
        1 => [strtolower($jobcontrol_name), 'String'],
        2 => [$domain_id, 'Integer'],
      ]);
    }

    return TRUE;
  }

	/**
	 * CIVIMEX-38 Uninstall joineryhq campaigntools extension
	 *
	 * @return TRUE on success
	 * @throws Exception
	 */
	public function upgrade_159() {
		$this->ctx->log->info('Uninstalling com.joineryhq.campaigntools extension');

		CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=0 WHERE full_name = "com.joineryhq.campaigntools"');

		return TRUE;
	}

	public function upgrade_160() {
		$this->ctx->log->info('Uninstalling org.civicoop.recurringcontributionsearch extension');

		CRM_Core_DAO::executeQuery('UPDATE civicrm_extension SET is_active=0 WHERE full_name = "org.civicoop.recurringcontributionsearch"');

		return TRUE;
	}

	/**
	 * CIVICRM-2223 ensure selfcancelrxfer_time has a value for all events
	 */
	 public function upgrade_161() {
		 $this->ctx->log->info('Ensuring Self-Cancel / Transfer time is set for all existing Events');

		 CRM_Core_DAO::executeQuery('UPDATE civicrm_event SET selfcancelxfer_time = 0 WHERE selfcancelxfer_time IS NULL');

		 return TRUE;
	 }

	 /*
	  * CIVILOG-7 - Rebuild triggers for au.com.agileware.loggingwhitelist 1.5
	  */
	public function upgrade_162() {
		$this->ctx->log->info('Rebuilding logging whitelist triggers');

		// Copied from au.com.agileware.loggingwhitelist/CRM/LoggingWhitelist.php

		// Listen for alter log tables event.  Because the module is not "enabled"
		// yet, we have to register the listener ourselves.
		Civi::dispatcher()->addListener('hook_civicrm_alterLogTables', ['CRM_LoggingWhitelist', 'logTablesListener']);

		// Rebuild the triggers.  This will allow us to alter the table list, so
		// that the triggers we don't want are removed.
		CRM_Core_DAO::triggerRebuild();

		// Remove the listener to avoid calling it extra times after the module has
		// finished being enabled.
		Civi::dispatcher()->removeListener('hook_civicrm_alterLogTables', ['CRM_LoggingWhitelist', 'logTablesistener']);

		return TRUE;
	}
}
