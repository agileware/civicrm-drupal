<?php

use CRM_Mandatoryextensions_ExtensionUtil as E;

/**
 * Job.Jobcontrol API specification (optional)
 * This is used for documentation and validation.
 *
 * @param   array  $spec  description of fields supported by this API call
 *
 * @see https://docs.civicrm.org/dev/en/latest/framework/api-architecture/
 */
// TODO
// Why did a scheduled job execution call flushcache?
/*
public static function flushCache() {
  // flush out all cache entries so we can reload new data
  // a bit aggressive, but livable for now
  CRM_Utils_Cache::singleton()->flush();
*/

/*
* JSON string exported and converted from a known good CiviCRM site
* Using SQL:
 * SELECT civicrm_job.name, civicrm_job.api_entity,civicrm_job.api_action,civicrm_job.parameters,civicrm_job.is_active,civicrm_job.run_frequency,civicrm_job_scheduled.cron FROM `civicrm_job_scheduled` LEFT JOIN `civicrm_job` ON `civicrm_job_scheduled`.`job_id`=`civicrm_job`.`id` WHERE `civicrm_job`.`api_action`!='mail_report';
 *
 * Excludes: Mail Reports which may have multiple instances
 *
*/

function _civicrm_api3_job_Jobcontrol_spec(&$spec) {
  $spec['setup'] = [
    'title'        => 'Set up Scheduled Jobs',
    'description'  => 'Use to set up Scheduled Jobs on a new CiviCRM site',
    'type'         => CRM_Utils_Type::T_INT,
    'api.required' => 0,
  ];
}

/**
 * Job.Jobcontrol API
 *
 * @param   array  $params
 *
 * @param   array  $spec  description of fields supported by this API call
 *
 * @return array
 *   API result descriptor
 *
 * @return civicrm_api3_create_success|civicrm_api3_create_error
 *
 * @throws API_Exception
 */
function civicrm_api3_job_Jobcontrol($params) {
  try {

    // Fetch the json data file
    $jobcontrol_plan = json_decode(
      file_get_contents(E::path('data/jobcontrol_plan.json')),
      TRUE
    );

    // Lower case these values which is used in the SQL for comparisons
    foreach ($jobcontrol_plan as $index => $job) {
      $jobcontrol_plan[$index]['api_entity'] = strtolower($job['api_entity']);
      $jobcontrol_plan[$index]['api_action'] = strtolower($job['api_action']);
    }

    // Only update scheduled jobs for this CiviCRM site, in the case of CiviCRM multi-site
    $domain_id = CRM_Core_Config::domainID();

    // Perform set up and enable / disable schedule jobs; create any missing scheduled jobs
    if (array_key_exists('setup', $params) && $params['setup'] == '1') {
      foreach ($jobcontrol_plan as $job) {
        // Apply Scheduled Job plan, enable/disable and define parameters for each Scheduled Job

        $sql     = 'SELECT * from `civicrm_job` WHERE LCASE(`api_entity`)=%1 AND LCASE(`api_action`)=%2 AND domain_id=%3 LIMIT 1';
        $results = CRM_Core_DAO::executeQuery($sql, [
          1 => [$job['api_entity'], 'String'],
          2 => [$job['api_action'], 'String'],
          3 => [$domain_id, 'Integer'],
        ]);
        $data    = $results->fetchAll();

        // If the job does not exist then create it now
        if (empty($data)) {
          $sql = 'INSERT INTO `civicrm_job` (`name`,`api_entity`,`api_action`,`parameters`,`is_active`,`run_frequency`,`domain_id`) VALUES (%1,%2,%3,%4,%5,%6,%7)';

          CRM_Core_DAO::executeQuery($sql, [
            1 => [$job['name'], 'String'],
            2 => [$job['api_entity'], 'String'],
            3 => [$job['api_action'], 'String'],
            4 => [$job['parameters'], 'String'],
            5 => [$job['is_active'], 'Integer'],
            6 => [$job['run_frequency'], 'String'],
            7 => [$domain_id, 'Integer'],
          ]);
        }

        // Apply Scheduled Job plan, enable/disable and define parameters for each Scheduled Job
        $sql = 'UPDATE `civicrm_job` SET `is_active`=%1, `parameters`=%2 WHERE LCASE(`api_entity`)=%3 AND LCASE(`api_action`)=%4 AND `domain_id`=%5';

        CRM_Core_DAO::executeQuery($sql, [
          1 => [$job['is_active'], 'Integer'],
          2 => [$job['parameters'], 'String'],
          3 => [$job['api_entity'], 'String'],
          4 => [$job['api_action'], 'String'],
          5 => [$domain_id, 'Integer'],
        ]);
      }
    }

    // Insert default, Always job schedule for cronplus
    // This is often missing when an Extension is enabled and new Scheduled Job created
    $sql = 'INSERT IGNORE INTO `civicrm_job_scheduled` (`job_id`,`cron`) SELECT `id`,"* * * * *" FROM `civicrm_job` WHERE `domain_id`=%1 ORDER BY `id` ASC';

    CRM_Core_DAO::executeQuery($sql, [
      1 => [$domain_id, 'Integer'],
    ]);

    // Now update each individual job scheduling
    foreach ($jobcontrol_plan as $job) {
      // Update job scheduling for this job
      $sql = 'UPDATE `civicrm_job` SET `run_frequency`=%1 WHERE LCASE(`api_entity`)=%2 AND LCASE(`api_action`)=%3 AND `domain_id`=%4';

      CRM_Core_DAO::executeQuery($sql, [
        1 => [$job['run_frequency'], 'String'],
        2 => [$job['api_entity'], 'String'],
        3 => [$job['api_action'], 'String'],
        4 => [$domain_id, 'Integer'],
      ]);

      // Update job scheduling for cronplus for this job
      $sql = 'REPLACE INTO `civicrm_job_scheduled` (`job_id`,`cron`) SELECT `id`,%1 FROM `civicrm_job` WHERE LCASE(`api_entity`)=%2 AND LCASE(`api_action`)=%3 AND `domain_id`=%4 ORDER BY `id` ASC';

      CRM_Core_DAO::executeQuery($sql, [
        1 => [$job['cron'], 'String'],
        2 => [$job['api_entity'], 'String'],
        3 => [$job['api_action'], 'String'],
        4 => [$domain_id, 'Integer'],
      ]);
    }

    return civicrm_api3_create_success(TRUE, $params, 'mandatoryextensions', 'jobcontrol');
  }
  catch (API_Exception $e) {
    $errorMessage = $e->getMessage();
    CRM_Core_Error::debug_var('mandatoryextensions::jobcontrol', $errorMessage);
    CRM_Core_Error::debug_var('mandatoryextensions::jobcontrol', $params);
    return civicrm_api3_create_error($errorMessage);
  }
}