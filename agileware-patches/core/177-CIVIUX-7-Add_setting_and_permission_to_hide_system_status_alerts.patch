From ff6dcdd29bed481a183341f63662221038eb9277 Mon Sep 17 00:00:00 2001
From: Iris Abarquez <iris@agileware.com.au>
Date: Mon, 26 Aug 2024 11:26:50 +1000
Subject: [PATCH] CIVIUX-7 Add permission and setting to hide system status
 alerts

---
 CRM/Admin/Form/Setting/Miscellaneous.php |  2 ++
 CRM/Core/Permission.php                  |  6 ++++++
 CRM/Utils/Check.php                      |  7 ++++++-
 settings/Core.setting.php                | 15 +++++++++++++++
 4 files changed, 29 insertions(+), 1 deletion(-)

diff --git a/CRM/Admin/Form/Setting/Miscellaneous.php b/CRM/Admin/Form/Setting/Miscellaneous.php
index 12aa23d2a1..da9fe1314b 100644
--- a/CRM/Admin/Form/Setting/Miscellaneous.php
+++ b/CRM/Admin/Form/Setting/Miscellaneous.php
@@ -55,6 +55,7 @@ class CRM_Admin_Form_Setting_Miscellaneous extends CRM_Admin_Form_Setting {
     'import_batch_size' => CRM_Core_BAO_Setting::SEARCH_PREFERENCES_NAME,
     'disable_sql_memory_engine' => CRM_Core_BAO_Setting::SYSTEM_PREFERENCES_NAME,
     'view_delete_in_ui' => CRM_Core_BAO_Setting::SYSTEM_PREFERENCES_NAME,
+    'view_status_alerts_in_ui' => CRM_Core_BAO_Setting::SYSTEM_PREFERENCES_NAME,
   ];
 
   /**
@@ -82,6 +83,7 @@ class CRM_Admin_Form_Setting_Miscellaneous extends CRM_Admin_Form_Setting {
       'import_batch_size',
       'disable_sql_memory_engine',
       'view_delete_in_ui',
+      'view_status_alerts_in_ui',
     ]);
   }
 
diff --git a/CRM/Core/Permission.php b/CRM/Core/Permission.php
index b2cf1a356c..ff1f5322af 100644
--- a/CRM/Core/Permission.php
+++ b/CRM/Core/Permission.php
@@ -964,10 +964,16 @@ class CRM_Core_Permission {
         'label' => $prefix . ts('send SMS'),
         'description' => ts('Send an SMS'),
       ],
+      // CIVIBLD-347
       'view delete action on entities' => [
         'label' => $prefix . ts('view delete action on entities'),
         'description' => ts('View the Delete action link in the UI for key entities. This permission should be given out with care, as deletion of these entities may cause many issues.'),
       ],
+      // CIVIUX-7, CIVIFET-88
+      'view system status alerts' => [
+        'label' => $prefix . ts('view system status alerts'),
+        'description' => ts('View system status alert messages as dialogue boxes in the backend UI.'),
+      ],
       'administer CiviCRM system' => [
         'label' => $prefix . ts('administer CiviCRM System'),
         'description' => ts('Perform all system administration tasks in CiviCRM'),
diff --git a/CRM/Utils/Check.php b/CRM/Utils/Check.php
index 256fc7e3e7..b62a25af83 100644
--- a/CRM/Utils/Check.php
+++ b/CRM/Utils/Check.php
@@ -80,7 +80,12 @@ class CRM_Utils_Check {
    * Display daily system status alerts (admin only).
    */
   public function showPeriodicAlerts() {
-    if (CRM_Core_Permission::check('administer CiviCRM system')) {
+    // CIVIUX-7, CIVIFET-88
+    // Check for the custom setting to view system status alerts.
+    // Do not display the status message dialogue box unless the user has the 'view system status alerts' AND the setting is on.
+    $view_alerts = Civi::settings()->get('view_status_alerts_in_ui');
+
+    if (CRM_Core_Permission::check('administer CiviCRM system') && CRM_Core_Permission::check('view system status alerts') && $view_alerts) {
       $session = CRM_Core_Session::singleton();
       if ($session->timer('check_' . __CLASS__, self::CHECK_TIMER)) {
 
diff --git a/settings/Core.setting.php b/settings/Core.setting.php
index 9b25c9f9c0..f26a9cddd8 100644
--- a/settings/Core.setting.php
+++ b/settings/Core.setting.php
@@ -508,6 +508,21 @@ return [
     'help_text' => NULL,
     'settings_pages' => 'misc',
   ],
+  // CIVIUX-7, CIVIFET-88 Add setting to hide/display system status alert dialogue box (e.g. daily system status message)
+  'view_status_alerts_in_ui' => [
+    'name' => 'view_status_alerts_in_ui',
+    'type' => 'Boolean',
+    'quick_form_type' => 'YesNo',
+    'html_type' => '',
+    'default' => FALSE,
+    'add' => '5.72.3',
+    'title' => ts('View System Status Alerts'),
+    'is_domain' => 1,
+    'is_contact' => 0,
+    'description' => ts('When disabled, the system status alert dialogue box will be hidden. You can still view status messages in the System Status page.'),
+    'help_text' => NULL,
+    'settings_pages' => 'misc',
+  ],
   'securityAlert' => [
     'group_name' => 'CiviCRM Preferences',
     'group' => 'core',
-- 
2.48.1

